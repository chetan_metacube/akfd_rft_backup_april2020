/**
 * Class contains logics related to payment terms 
 * @author : Chetan Sharma
 * Last Edited By : Chetan Sharma
*/
public class PaymentTermsController {
    /**
    * Method to getPayementTerms
    * @param : opportunityId
    * @return : payment terms data
    */
    @AuraEnabled
    public static String getPaymentTerms(Id opportunityId) {
        if( opportunityId == null || opportunityId.getSobjectType() != Schema.Opportunity.getSObjectType() ) { 
            throw getAuraExceptionInstance('Invalid Opportunity Id entered.');
        } else {
            // Validate against user access.
            UserRecordAccess userAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :opportunityId];
            if(userAccess != null && !userAccess.HasEditAccess) {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Insufficient Privileges.'));
                throw getAuraExceptionInstance('Insufficient Privileges.');
            }
            // Get existing payment terms. 
            List<Payment_Term__c> existingPaymentTerms = PaymentTermsSelector.getInstance().selectInstallmentPaymentTermsByOpportunityIds(new Set<Id>{opportunityId});
            String currencySymbol = '';
            //gets the currency symbol used in the PDF
            if(existingPaymentTerms[0].CurrencyIsoCode == 'INR'){
                currencySymbol = 'INR ';
            } else {
                currencySymbol = CurrencyUtils.getSymbol(existingPaymentTerms[0].CurrencyIsoCode);
            }
            // serialize wrapper data and return paymentTerms data
            return JSON.serialize(new PaymentTermsWrapper(existingPaymentTerms, currencySymbol));
        }
    }
    
    /**
    * Method to update payment terms.
    * @params : opportunityId, paymentTermWrapperString
    */
    @AuraEnabled
    public static Boolean updatePaymentTerm(Id opportunityId, String paymentTermWrapperString) {
        if(opportunityId != null && opportunityId.getSobjectType() == Schema.Opportunity.getSObjectType() && paymentTermWrapperString != null && paymentTermWrapperString.length() > 0) {
            PaymentTermsWrapper paymentTermWrapper = (PaymentTermsWrapper)JSON.deserializeStrict(paymentTermWrapperString, PaymentTermsWrapper.class);
            Map<Id, Payment_Term__c> currentPaymentTermsData = new Map<Id, Payment_Term__c>((List<Payment_Term__c>)PaymentTermsSelector.getInstance().selectSObjectsById((new Map<Id, Payment_Term__c>(paymentTermWrapper.paymentTerms)).keySet()));
            List<Payment_Term__c> paymentTermsToBeUpdated = new List<Payment_Term__c>();
            Double totalPercentage = 0.0;
            // Validate payment terms
            for(Payment_Term__c paymentTerm : paymentTermWrapper.paymentTerms) {
                if(paymentTerm.Payment_Percentage__c <= 0) {
                    throw getAuraExceptionInstance('Payment term percentage should be greater than zero.');
                } else if(paymentTerm.Payment_Status_New__c != 'Due' && currentPaymentTermsData.get(paymentTerm.Id).Payment_Percentage__c != paymentTerm.Payment_Percentage__c) {
                    throw getAuraExceptionInstance('Payment Percentage can be edited for Due Payment Terms Only.');
                } else if(paymentTerm.Payment_Percentage__c != currentPaymentTermsData.get(paymentTerm.Id).Payment_Percentage__c) {
                    paymentTermsToBeUpdated.add(paymentTerm);
                }
                totalPercentage += paymentTerm.Payment_Percentage__c;
            }
            
            if(paymentTermsToBeUpdated.size() == 0) {
                throw getAuraExceptionInstance('No Payment Term Value Changed.' );
            }
            //Validate for total percentage.
            if(totalPercentage > 100 ) {
                throw getAuraExceptionInstance('Total Payment Percentage should be less than or equal to 100%.' );
            } else {
                update paymentTermsToBeUpdated;
            }
        } else if(opportunityId == null || opportunityId.getSobjectType() != Schema.Opportunity.getSObjectType()) {
            throw getAuraExceptionInstance('Invalid opportunityId passed.');
        } else {
            throw getAuraExceptionInstance('Invalid paymentTermWrapperString passed.');
        }
        return true;
    }
    
    
     /**
    * Method to getAuraExceptionClassInstance
    * @param : error message
    * @return AuraHandledException
    */
    private static AuraHandledException getAuraExceptionInstance(String message) {
        AuraHandledException exceptionInstance = new AuraHandledException('');
        exceptionInstance.setMessage(message);
        return exceptionInstance;
    }
    
    public class PaymentTermsWrapper {
        public List<Payment_Term__c> paymentTerms {get;set;}
        public String currencySymbol {get;set;}
        public PaymentTermsWrapper(List<Payment_Term__c> paymentTerms, String currencySymbol) {
            this.paymentTerms = paymentTerms;
            this.currencySymbol = currencySymbol;
        }
    }
    
}