@isTest
public class ProjectedStocksTest {
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(customerAccounts, products, 1);
        List<PriceBook_Product_Entry__c> pricebookEntries = MockData.createPriceBookEntries(customerAccounts, products, priceBooks, 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(customerAccounts, opportunities, products, 1);
        
        MockData.createProformaInvoices(opportunities, products, 1);
    }
    
    @isTest static void positiveTestCaseFor_BeforeAndAfterInsertProjectedStock() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = proformaInvoiceLineItem.Product__c, ProformaInvoice__c = proformaInvoice.Id, Required_Quantity__c = 5, Status__c = 'New');
        insert projectedStock;
        EmailMessage emailMessage = [SELECT Id, ParentId FROM EmailMessage];
        System.assert(emailMessage != null);
    }
    
    @isTest static void negativeTestCaseFor_BeforeAndAfterInsertProjectedStock_WhenInvalidDataPassed() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = null, ProformaInvoice__c = proformaInvoice.Id, Required_Quantity__c = null, Status__c = 'New');
        try {
            ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
            insert projectedStock;
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
    }
    
    @isTest static void negativeTestCaseFor_BeforeAndAfterInsertProjectedStock_WhenNotExecutingFromCodeIsFalse() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = proformaInvoiceLineItem.Product__c, ProformaInvoice__c = proformaInvoice.Id, Required_Quantity__c = 5, Status__c = 'New');
        try {
            insert projectedStock;
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
    }
    
    @isTest static void negativeTestCaseFor_BeforeAndAfterUpdateProjectedStock_WhenInvlaidDataPassed() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = proformaInvoiceLineItem.Product__c, ProformaInvoice__c = proformaInvoice.Id, Required_Quantity__c = 5, Status__c = 'New');
        insert projectedStock;
        EmailMessage emailMessage = [SELECT Id, ParentId FROM EmailMessage];
        System.assert(emailMessage != null);
        projectedStock.Product__c = null;
        projectedStock.Required_Quantity__c = null;
        projectedStock.ProformaInvoice__c = null;
        try {
            update projectedStock;
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
    }
    
    
    @isTest static void negativeTestCaseFor_BeforeAndAfterUpdateProjectedStock_WhenNotExecutingFromCodeIsFalse() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = proformaInvoiceLineItem.Product__c, ProformaInvoice__c = proformaInvoice.Id, Required_Quantity__c = 5, Status__c = 'New');
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert projectedStock;
        try {
            ApexTriggerCheck.getInstance().isExecutinngFromApexCode = null;
            projectedStock.ProformaInvoice__c = null;
            update projectedStock;
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
    }
    
    @isTest static void negativeTestCaseFor_BeforeAndAfterUpdateProjectedStock_WhenEditingInProductionProjectedStock() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = proformaInvoiceLineItem.Product__c, Required_Quantity__c = 5, Status__c = 'New');
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert projectedStock;
        try {
            ApexTriggerCheck.getInstance().isExecutinngFromApexCode = null;
            projectedStock.ProformaInvoice__c = null;
            projectedStock.Status__c = 'In Production';
            projectedStock.Required_Quantity__c = projectedStock.Required_Quantity__c + 3;
            update projectedStock;
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
    }
    

}