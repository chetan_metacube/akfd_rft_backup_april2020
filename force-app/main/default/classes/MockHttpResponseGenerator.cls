@isTest
public class MockHttpResponseGenerator implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://122.180.240.44:8069/xmlrpc/2/object', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setBody('<?xml version=\'1.0\'?>'
                    + '<methodResponse>'
                    + '<params>'
                    + '<param>'
                    + '<value><string>1</string></value>'
                    + '</param>'
                    + '</params>'
                    + '</methodResponse>'
                   );
        res.setStatusCode(200);
        return res;
    }
}