@isTest
public class AllotedStocksTest {
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(customerAccounts, products, 1);
        List<PriceBook_Product_Entry__c> pricebookEntries = MockData.createPriceBookEntries(customerAccounts, products, priceBooks, 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(customerAccounts, opportunities, products, 1);
        
        List<Warehouse__c> warehouses = MockData.createWarehouses(1);
        MockData.createWarehouseLineItems(warehouses, products, 1);
        MockData.createProformaInvoices(opportunities, products, 1);
    }
    
    @isTest static void positiveTestCase_For_allotStocksWhenAllotedStockCreatedAndUpdated() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Product2 product = [SELECT Id, Name FROM Product2 LIMIT 1];
        Warehouse_Line_Item__c productWarehouse = [SELECT Id, Name, Current_Stock__c, Product__c, Warehouse__c FROM Warehouse_Line_Item__c WHERE Product__c =:  product.Id LIMIT 1];
        // Create Alloted Stock
        Alloted_Stock__c bookedStock = new Alloted_Stock__c(Stock__c = 10, Product__c = productWarehouse.Product__c,
                                                            ProformaInvoice__c = proformaInvoice.Id, Warehouse__c = productWarehouse.Warehouse__c, Warehouse_Product__c = productWarehouse.Id);
		ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert bookedStock;
        System.assert(bookedStock.Id !=  null);
        bookedStock.Stock__c = 12;
        update bookedStock;
        delete bookedStock;
        try {
            bookedStock = [SELECT Id, Name FROM Alloted_Stock__c WHERE Id =: bookedStock.Id];
        } catch(Exception exc) {
            System.assert(exc.getMessage().contains('List has no rows for assignment to SObject'));
        }
        
    }
    
    @isTest static void negativeTestCase_For_allotStocksWhenCodeExecutionPermissionNotGivenForInsertion() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Product2 product = [SELECT Id, Name FROM Product2 LIMIT 1];
        Warehouse_Line_Item__c productWarehouse = [SELECT Id, Name, Current_Stock__c, Product__c, Warehouse__c FROM Warehouse_Line_Item__c WHERE Product__c =:  product.Id LIMIT 1];
        // Create Alloted Stock
        Alloted_Stock__c bookedStock = new Alloted_Stock__c(Stock__c = 10, Product__c = productWarehouse.Product__c,
                                                            ProformaInvoice__c = proformaInvoice.Id, Warehouse__c = productWarehouse.Warehouse__c, Warehouse_Product__c = productWarehouse.Id);
        try {
            insert bookedStock;
        } catch(Exception exc) {
            System.debug('Exception ' + exc);
            System.assert(exc.getMessage().contains('Booked Stock Cannot Be Created.'));
        }
    }
    
    @isTest static void negativeTestCase_For_allotStocksWhenCodeExecutionPermissionNotGivenForUpdation() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Product2 product = [SELECT Id, Name FROM Product2 LIMIT 1];
        Warehouse_Line_Item__c productWarehouse = [SELECT Id, Name, Current_Stock__c, Product__c, Warehouse__c FROM Warehouse_Line_Item__c WHERE Product__c =:  product.Id LIMIT 1];
        // Create Alloted Stock
        Alloted_Stock__c bookedStock = new Alloted_Stock__c(Stock__c = 10, Product__c = productWarehouse.Product__c,
                                                            ProformaInvoice__c = proformaInvoice.Id, Warehouse__c = productWarehouse.Warehouse__c, Warehouse_Product__c = productWarehouse.Id);
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert bookedStock;
        System.assert(bookedStock.Id !=  null);
        bookedStock.Stock__c = 12;
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = null;
        try {
            update bookedStock;
        } catch(Exception exc) {
            System.assert(exc.getMessage().contains('Booked Stock Cannot Be Changed.'));
        }
    }
    
    

}