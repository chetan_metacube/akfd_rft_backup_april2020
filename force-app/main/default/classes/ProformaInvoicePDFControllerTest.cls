@isTest
public class ProformaInvoicePDFControllerTest {
    @testSetup static void setup() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        //MockData.createQuotation(opportunities, 1, desiredProducts, priceBooks);
        List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        
    }
    
    @isTest
    public static void testProformaInvoicePDFControllerMethodWithoutBankInfo() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        ProformaInvoice__c proformaInvoice = [SELECT Id FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proformaInvoice);
        ProformaInvoicePDFController proformaInvoicePdfObj = new ProformaInvoicePDFController(stdCtrl);
        System.assert(ApexPages.getMessages() != null);
    }
    @isTest
    public static void testProformaInvoicePDFControllerMethodWithBankInfo() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        ProformaInvoice__c proformaInvoice = [SELECT Id, Opportunity__r.Merchant__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        Bank_Info__c bank = new Bank_Info__c(Account__c = proformaInvoice.Opportunity__r.Merchant__c, CurrencyIsoCode='INR',Bank_Details__c='<p><span style="font-size: 14px; color: rgb(43, 40, 38);">This is Indian, Currency</span></p>');
        insert bank;
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proformaInvoice);
        ProformaInvoicePDFController proformaInvoicePdfObj = new ProformaInvoicePDFController(stdCtrl);
        System.assert(ApexPages.getMessages() != null);
    }
    @isTest
    public static void testRedirectMethod() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        ProformaInvoice__c proformaInvoice = [SELECT Id, Opportunity__r.Merchant__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        Bank_Info__c bank = new Bank_Info__c(Account__c = proformaInvoice.Opportunity__r.Merchant__c, CurrencyIsoCode='INR',Bank_Details__c='<p><span style="font-size: 14px; color: rgb(43, 40, 38);">This is Indian, Currency</span></p>');
        insert bank;
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proformaInvoice);
        ProformaInvoicePDFController proformaInvoicePdfObj = new ProformaInvoicePDFController(stdCtrl);
        System.assert(proformaInvoicePdfObj.redirect() != null);
    }
    
    @isTest
    public static void testgoBackMethod() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        ProformaInvoice__c proformaInvoice = [SELECT Id, Opportunity__r.Merchant__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proformaInvoice);
        ProformaInvoicePDFController proformaInvoicePdfObj = new ProformaInvoicePDFController(stdCtrl);
        System.assert(proformaInvoicePdfObj.goBack() != null);
    }
    
}