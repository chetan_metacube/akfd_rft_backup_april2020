public class ExportAccountController {
    /**
    * Properties and variables
    */
    public List<String> headings { get; set; }    
    public List<Object> recordList { get; set; }   
    public String newLine { get; set; }
    public String currentDateTime { get; set; }
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    /**
    * Constructor
    */
    public ExportAccountController() {   
        newLine = '\n';
        headings = new List<String>();    
        List<String> apiNames = new List<String>();       
        currentDateTime = '(' + System.Now().format('dd-MM-YYYY HH-mm a','IST') + ')';
        String query = 'SELECT ';
        Map<Id,String> accountsContact=new Map<Id,String>();
        List<Account> fieldDataMapping=[ SELECT Id, Name, Phone,Mobile__c, Fax,Email__c, 
                                        PAN_Number__c, Registration_Type__c, CreatedDate, Group__c,
                                        (SELECT Name FROM Contacts),
                                        (SELECT Name, Address_Type__c, Street__c, City__c, State_Province__c, 
                                         Country__c, Zip_Postal_Code__c, GST_Id__c 
                                         FROM Addresses__r 
                                         WHERE Address_Type__c = 'Billing' LIMIT 1)
                                        FROM Account WHERE RecordType.Name = 'Customer'];
        
        headings.add('Ledger Name');
        headings.add('Alias');
        headings.add('Group');
        headings.add('Mailing Name');
        headings.add('Address 1');
        headings.add('Address 2');
        headings.add('Address 3');
        headings.add('Address 4');
        headings.add('Address 5');
        headings.add('Country');
        headings.add('State Mandatory');
        headings.add('Pin Code');
        headings.add('Contact Person');
        headings.add('Phone');
        headings.add('Mobile No.');
        headings.add('FAX');
        headings.add('Email id');
        headings.add('Maintain Bill by Bill');
        headings.add('Credit Period');
        headings.add('Is Costcentre');
        headings.add('PAN/ IT No.');
        headings.add('REGISTRATION TYPE');
        headings.add('GSTIN/UIN No');
        headings.add('Amount');
        headings.add('Type (Dr/Cr)');
        headings.add('Created Date');
        recordList = new List<List<String>>();
        List<String> records;
        String formattedDate = '';
        String contactPersons;
        for(Account account : fieldDataMapping) {
            records=new List<String>();
            contactPersons = '';
            records.add(account.Name == null ? '' : account.Name);
            records.add(account.Id);
            records.add(account.Group__c);
            records.add(account.Name == null ? '' : account.Name);
            records.add(account.Addresses__r.size() == 0 || account.Addresses__r[0].Street__c == null ? '' : account.Addresses__r[0].Street__c);
            records.add(account.Addresses__r.size() == 0 || account.Addresses__r[0].City__c == null ? '' : account.Addresses__r[0].City__c);
            records.add('');
            records.add('');
            records.add('');
            records.add(account.Addresses__r.size()==0 || account.Addresses__r[0].Country__c == null ? '' : account.Addresses__r[0].Country__c);
            records.add(account.Addresses__r.size()==0 || account.Addresses__r[0].State_Province__c == null ? '' : account.Addresses__r[0].State_Province__c);
            records.add(account.Addresses__r.size()==0 || account.Addresses__r[0].Zip_Postal_Code__c == null ? '' : account.Addresses__r[0].Zip_Postal_Code__c);
            
            for(Contact contact : account.Contacts) {
            	contactPersons = contactPersons + contact.Name + ',';
            }
            
        	records.add(contactPersons);
            records.add(account.Phone == null ? '' : account.Phone);
            records.add(account.Mobile__c == null ? '' : account.Mobile__c);
            records.add(account.Fax == null ? '' : account.Fax);
            records.add(account.Email__c == null ? '' : account.Email__c);
            records.add('Yes');
            records.add('');
            records.add('');
            records.add(account.PAN_Number__c == null ? '' : account.PAN_Number__c);
            switch on account.Registration_Type__c{
                when 'Consumer (End User with no GST)'{
                    records.add('Consumer');
                }
                when 'Regular (having GST)' {
                    records.add('Regular');
                }
                when 'Unregistered (No GST company)' {
                    records.add('Unregistered');
                }
                when 'Composition' {
                    records.add('Composition');
                }
                when else {
                    records.add('Regular');
                }
            }
            records.add(account.Addresses__r.size() == 0 || account.Addresses__r[0].GST_Id__c == null ? '' : account.Addresses__r[0].GST_Id__c);
            records.add('');
            records.add('');
            formattedDate = DateTime.newInstance(account.CreatedDate.year(), account.CreatedDate.month(), account.CreatedDate.day()).format('d/MMM/YYYY');
            records.add(formattedDate);
            recordList.add(records);
        }
    }              
}