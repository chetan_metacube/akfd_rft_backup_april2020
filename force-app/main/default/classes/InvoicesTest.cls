@isTest 
public class InvoicesTest {
    @testSetup static void setup() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        //MockData.createQuotation(opportunities, 1, desiredProducts, priceBooks);
        List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        proformaInvoices[0].Status__c = 'Accepted';
        update proformaInvoices;
        List<Warehouse__c> warehouses = MockData.createWarehouses(1);
        
        List<Warehouse_Line_Item__c> productWarehouses = MockData.createWarehouseLineItems(warehouses, products, 1);
        
        Alloted_Stock__c allotedStock = new Alloted_Stock__c(Product__c = products[0].Id, Stock__c = 10, Warehouse__c = warehouses[0].Id, Warehouse_Product__c = productWarehouses[0].Id, ProformaInvoice__c = proformaInvoices[0].Id);
        
        
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert allotedStock;
        
        Opportunity oppObj = [Select Id, (Select Id, Name, Opportunity__c From Payment_Terms__r) From Opportunity Where Id =: opportunities[0].Id LIMIT 1];
        List<Payment_Term__c> existingPaymentTerms = new List<Payment_Term__c>();
        for(Payment_Term__c term : oppObj.Payment_Terms__r) {
            existingPaymentTerms.add(term);
        }
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);        
        List<Proforma_Invoice_Line_Item__c> piLineItems = [SELECT Id, Required_Quantity__c, Invoiced_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoices[0].Id LIMIT 1]; 
        System.debug('Proforma_Invoice_Line_Item__c'+piLineItems);
        List<Invoice__c> invoices = MockData.createInvoices(opportunities, 1, products, piLineItems);
       
    }
    
    @isTest static void testBeforeInsert() {
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        List<Invoice__c> invoices = [SELECT Id, Name, Status__c, Conversion_Rate_To_INR__c FROM Invoice__c WHERE Opportunity__c =: opportunity.Id];
        List<Invoiced_Items__c> invoicedItems = [SELECT Id, Name, Invoice__c FROM Invoiced_Items__c WHERE Invoice__c IN : invoices];
        System.debug('Invoiced Items ' + invoicedItems);
        List<Invoiced_Product__c> invoicedProducts = [SELECT Id, Invoiced_Item__c FROM Invoiced_Product__c WHERE Invoiced_Item__c IN : invoicedItems];
        System.debug('Invoiced Products ' + invoicedProducts);

        invoices[0].Status__c = 'Reviewed'; 
        invoices[0].Conversion_Rate_To_INR__c = 1.5;
        update invoices;
    }
    
}