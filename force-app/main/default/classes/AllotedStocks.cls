public class AllotedStocks extends fflib_SObjectDomain {
    public AllotedStocks(List<Alloted_Stock__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new AllotedStocks(records);
        }
    }
    
    public override void onValidate() {
        // Validate
        if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
            for(Alloted_Stock__c allotedStock : (List<Alloted_Stock__c>) Records) {
                allotedStock.addError('Booked Stock Cannot Be Created.');
            }
        }
        
    }
    
    public override void onValidate(Map<Id,SObject> existingRecords) {
        // Validate 
        if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
            for(Alloted_Stock__c allotedStock : (List<Alloted_Stock__c>) Records) {
                allotedStock.addError('Booked Stock Cannot Be Changed.');
            }
        }
        
    }
    
    public override void onAfterInsert() {
        Map<Id, Decimal> newAllotedStocks = new Map<Id, Decimal>();
        for(Alloted_Stock__c allotedStock : (List<Alloted_Stock__c>) Records) {
            if(newAllotedStocks.containsKey(allotedStock.Product__c)) {
                newAllotedStocks.put(allotedStock.Product__c, newAllotedStocks.get(allotedStock.Product__c) + allotedStock.Stock__c);
            } else {
                newAllotedStocks.put(allotedStock.Product__c, allotedStock.Stock__c);
            }
        }
        
        fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
        
        List<Product2> products = (List<Product2>)ProductsSelector.getInstance().selectSObjectsById(newAllotedStocks.keySet());
        for(Product2 product : products) {
            if(product.Alloted_Stock__c == null) { 
                product.Alloted_Stock__c = 0; 
            }
            product.Alloted_Stock__c += newAllotedStocks.get(product.Id);
        }
        uow.registerDirty(products);
        uow.commitWork();
    }
    
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        Map<Id, Alloted_Stock__c> existingAllotedStocks = (Map<Id, Alloted_Stock__c>) existingRecords;
        Map<Id, Decimal> newAllotedStocks = new Map<Id, Decimal>();
        for(Alloted_Stock__c allotedStock : (List<Alloted_Stock__c>) Records) {
            if(newAllotedStocks.containsKey(allotedStock.Product__c)) {
                newAllotedStocks.put(allotedStock.Product__c, newAllotedStocks.get(allotedStock.Product__c) + allotedStock.Stock__c - existingAllotedStocks.get(allotedStock.Id).Stock__c);
            } else {
                newAllotedStocks.put(allotedStock.Product__c, allotedStock.Stock__c - existingAllotedStocks.get(allotedStock.Id).Stock__c);
            }
        }
        
        fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
        
        List<Product2> products = (List<Product2>)ProductsSelector.getInstance().selectSObjectsById(newAllotedStocks.keySet());
        for(Product2 product : products) {
            if(product.Alloted_Stock__c == null) { 
                product.Alloted_Stock__c = 0; 
            }
            product.Alloted_Stock__c += newAllotedStocks.get(product.Id);
        }
        
        uow.registerDirty(products);
        uow.commitWork();
    }
    
    public override void onBeforeDelete() {
        if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
            for(Alloted_Stock__c allotedStock : (List<Alloted_Stock__c>) Records) {
                allotedStock.addError('Booked Stock Cannot Be Deleted.');
            }
            return;
        }
        Map<Id, Decimal> existingAllotedStocks = new Map<Id, Decimal>();
        for(Alloted_Stock__c allotedStock : (List<Alloted_Stock__c>) Records) {
            if(existingAllotedStocks.containsKey(allotedStock.Product__c)) {
                existingAllotedStocks.put(allotedStock.Product__c, existingAllotedStocks.get(allotedStock.Product__c) + allotedStock.Stock__c);
            } else {
                existingAllotedStocks.put(allotedStock.Product__c, allotedStock.Stock__c);
            }
        }
        
        fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
        
        List<Product2> products = (List<Product2>)ProductsSelector.getInstance().selectSObjectsById(existingAllotedStocks.keySet());
        for(Product2 product : products) {
            if(product.Alloted_Stock__c == null) { 
                product.Alloted_Stock__c = 0; 
            }
            product.Alloted_Stock__c -= existingAllotedStocks.get(product.Id);
        }
        
        uow.registerDirty(products);
        uow.commitWork();
    }
    
}