@isTest
public class CaseControllerTest {
    @testSetup static void setup() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
 		Account accountObj = new Account(Name = 'Metacube Software PVT Ltd');
        insert accountObj;
        
        Contact contactObj = new Contact(AccountId = accountObj.Id, LastName = 'Test Account', Email = 'test@test.com');
        insert contactObj;
        
        List<Case> cases = new List<Case>();
        cases.add(new Case(AccountId = accountObj.Id, ContactId = contactObj.Id, Status = 'New', SuppliedEmail = 'test@test.com', Sales_Process__c = 'Export'));
        cases.add(new Case(AccountId = accountObj.Id, ContactId = contactObj.Id, Status = 'New', SuppliedEmail = 'test1@test.com', Sales_Process__c = 'Support'));
        cases.add(new Case(AccountId = accountObj.Id, ContactId = contactObj.Id, Status = 'Converted', SuppliedEmail = 'test2@test.com', Sales_Process__c = 'Retail'));
        cases.add(new Case(AccountId = accountObj.Id, ContactId = contactObj.Id, Status = 'New', SuppliedEmail = 'test3@test.com', Sales_Process__c = 'Project'));
        insert cases;
        //create user and run in user context
     }
      
    @isTest static void testInitMethod(){
        Case caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case Limit 1];
        System.assert(CaseController.getCase(caseObj.Id) !=null);
    }
    
     @isTest static void negativeTestfor_caseConvertMethod_WhenUserDonotHaveEditingRights() {
         Case caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case WHERE Sales_Process__c = 'Export' Limit 1];
         System.debug('Case Obj ' + caseObj);
         Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
         insert leadSourceObj;
         caseObj = CaseController.getCase(caseObj.Id);
         Map<String, String> leadFields = new Map<String, String>{'Title' => 'Test Lead', 'LeadSource' => leadSourceObj.Id };
             User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' AND Alias LIKE 'user%' LIMIT 1];
         try {
             System.runAs(chatterFreeUser) {
                 System.assert(CaseController.convert(caseObj, leadFields).title == 'Test Lead');
             }
         } catch(Exception error) {
             System.debug(error.getMessage());
             System.assert(error.getMessage().contains('Something went wrong!!!'));
         }
    }
    
  
    
    @isTest static void testInitMethodWhenUserDonotHaveAccess(){
        Case caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case WHERE Sales_Process__c = 'Export' Limit 1];
        System.assert(CaseController.getCase(caseObj.Id) !=null);
    }
    
    @isTest static void testConvertMethod() {
        Case caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case WHERE Sales_Process__c = 'Export' Limit 1];
        System.debug('Case Obj ' + caseObj);
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        caseObj = CaseController.getCase(caseObj.Id);
        Map<String, String> leadFields = new Map<String, String>{'Title' => 'Test Lead', 'LeadSource' => leadSourceObj.Id };
        System.assert(CaseController.convert(caseObj, leadFields).title == 'Test Lead');
    }
   
    @isTest static void negativeTestConvertMethod(){
        Case caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case WHERE Sales_Process__c = 'Export' Limit 1];
        caseObj.AccountId = null;
        caseObj.ContactId = null;
        caseObj.Sales_Process__c = null;
        update caseObj;
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        caseObj = CaseController.getCase(caseObj.Id);
        Map<String, String> leadFields = new Map<String, String>{'Title' => null , 'LeadSource' => null };
            //Test case 1
            try {
                CaseController.convert(caseObj, leadFields);
            } catch(AuraHandledException exc){
                System.assert(exc.getMessage().contains('Script-thrown exception') == true);
            }
        //Test case 2 
        try {
            Case nullCaseObj;
            CaseController.convert(nullCaseObj, leadFields);
        } catch(AuraHandledException exc){
            System.assert(exc.getMessage().contains('Script-thrown exception') == true);
        }
        
        caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case WHERE Sales_Process__c = 'Retail' Limit 1];
        try {
            CaseController.convert(caseObj, leadFields);
        } catch(AuraHandledException exc){
            System.assert(exc.getMessage().contains('Script-thrown exception') == true);
        }
        
        caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case WHERE Sales_Process__c = 'Support' Limit 1];
        try {
            CaseController.convert(caseObj, leadFields);
        } catch(AuraHandledException exc){
            System.assert(exc.getMessage().contains('Script-thrown exception') == true);
        }
    }
    
    @isTest static void negativeTestConvertMethodWhenCaseIsAlreadyClosed(){
        Case caseObj = [SELECT Id, AccountId, ContactId, Status, SuppliedEmail, Sales_Process__c FROM Case Limit 1];
        caseObj.Status = 'Closed';
        update caseObj;
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        caseObj = CaseController.getCase(caseObj.Id);
        Map<String, String> leadFields = new Map<String, String>{'Title' => 'TestLeadTitle' , 'LeadSource' => leadSourceObj.Id };
            try {
                CaseController.convert(caseObj, leadFields);
            } catch(AuraHandledException exc){
                exc.getMessage().contains('Lead Title: You must enter a value.');
            }
    }


}