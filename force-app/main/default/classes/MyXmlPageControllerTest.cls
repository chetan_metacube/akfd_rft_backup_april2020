@isTest
public class MyXmlPageControllerTest {
    @testSetup
    static void testSetup() {
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id agentRecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent Contact').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        List<Account> acc=new List<Account>();
        Account accountCustomerObj=new Account(Name='AccountTest',RecordTypeId=customerRecordTypeIdAccount,CurrencyIsoCode='INR');
        Account accountMerchantObj=new Account(Name='AccountTest',RecordTypeId=merchantcustomerRecordTypeIdAccount);
        acc.add(accountCustomerObj);
        acc.add(accountMerchantObj);
        insert acc;
        
        List<Address__c> addresses = new List<Address__c>();
        Address__c billingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Billing', Countries__c = 'India', State__c = 'Rajasthan');
        Address__c shippingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Delivery', Countries__c = 'India', State__c = 'Rajasthan');
        addresses.add(billingAddress);
        addresses.add(shippingAddress);
        insert addresses;
        
        Contact contactObj=new Contact(FirstName='Contact',LastName='Test',RecordTypeId=agentRecordTypeIdContact,AccountId=accountCustomerObj.Id,Email='test@test.com');
        insert contactObj;
        Id customerRecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Id retailRecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        
        List<Opportunity> opportunities = new List<Opportunity>();
        Opportunity oppObj = new Opportunity(Name='Test Opportunity',RecordTypeId=customerRecordTypeIdOpportunity,StageName='Client Setup',Merchant__c=accountMerchantObj.Id,AccountId=accountCustomerObj.Id,Contact_Person__c=contactObj.Id,CloseDate=Date.today(),CurrencyIsoCode='USD');
        opportunities.add(oppObj);
        
        Opportunity oppObj2 = new Opportunity(Name='Test Opportunity2',RecordTypeId=retailRecordTypeIdOpportunity,StageName='Client Setup',Merchant__c=accountMerchantObj.Id,AccountId=accountCustomerObj.Id,Contact_Person__c=contactObj.Id,CloseDate=Date.today(),CurrencyIsoCode='USD');
        opportunities.add(oppObj2);
        
        insert opportunities;
        
        HSN_Tax__c hsnTax = new HSN_Tax__c(Name = '93014556', Tax__c = 18.00);
        insert hsnTax;
        
        List<Product2> products = new List<Product2>();
        Product2 product=new Product2(Name='Test Product',Family='Furniture', Brand__c = 'AKFD', Retail_Price__c = 0.00, HSN_Tax__c = hsnTax.Id);
        products.add(product);
        
        Product2 product1=new Product2(Name='Test Product1',Family='Furniture', Brand__c = 'AKFD', Retail_Price__c = 10.00, HSN_Tax__c = hsnTax.Id);
        products.add(product1);
        
        insert products;
        
        List<PriceBook_Product__c> pricesBooks = new List<PriceBook_Product__c>();
        PriceBook_Product__c priceBookObj=new PriceBook_Product__c(Customer__c=accountCustomerObj.Id,Product__c=product.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        pricesBooks.add(priceBookObj);
        
        PriceBook_Product__c priceBookObj1=new PriceBook_Product__c(Customer__c=accountCustomerObj.Id,Product__c=product1.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        pricesBooks.add(priceBookObj1);
        
        insert pricesBooks;
        
        List<PriceBook_Product_Entry__c> pricesBookEntries = new List<PriceBook_Product_Entry__c>();
        PriceBook_Product_Entry__c priceBookEntryObj=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        pricesBookEntries.add(priceBookEntryObj);
        
        PriceBook_Product_Entry__c priceBookEntryObj1=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj1.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        pricesBookEntries.add(priceBookEntryObj1);
        
        insert pricesBookEntries;
        
        List<PO_Product__c> desiredProducts = new List<PO_Product__c>();
        PO_Product__c desiredProductObj1=new PO_Product__c(Product__c=product.Id,Quantity__c=5,Opportunity__c=oppObj.Id, Discount_Percentage__c = 0.00);
        desiredProducts.add(desiredProductObj1);
        
        PO_Product__c desiredProductObj2 = new PO_Product__c(Product__c=product1.Id,Quantity__c=5,Opportunity__c=oppObj.Id, Discount_Percentage__c = 0.00);
        desiredProducts.add(desiredProductObj2);
        
        PO_Product__c desiredProductObj3 = new PO_Product__c(Product__c=product.Id,Quantity__c=5,Opportunity__c=oppObj2.Id, Discount_Percentage__c = 0.00);
        desiredProducts.add(desiredProductObj3);
        
        PO_Product__c desiredProductObj4 = new PO_Product__c(Product__c=product1.Id,Quantity__c=5,Opportunity__c=oppObj2.Id, Discount_Percentage__c = 0.00);
        desiredProducts.add(desiredProductObj4);
        
        insert desiredProducts;
        
        List<ProformaInvoice__c> proformaInvoices = new List<ProformaInvoice__c>();
        ProformaInvoice__c proformaInvoice1 = new ProformaInvoice__c(Opportunity__c = oppObj.Id, Status__c = 'New', Conversion_Rate__c = 1.00);
        proformaInvoices.add(proformaInvoice1);
        ProformaInvoice__c proformaInvoice2 = new ProformaInvoice__c(Opportunity__c = oppObj2.Id, Status__c = 'New', Conversion_Rate__c = 1.00);
        proformaInvoices.add(proformaInvoice2);
        insert proformaInvoices;
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem1 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj1.Product__c, Quantity__c = 10, Required_Quantity__c = 15, Price__c = 10.00, PO_Product__c = desiredProductObj1.Id);
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem2 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj2.Product__c, Quantity__c = 12, Required_Quantity__c = 20, Price__c = 20.00, PO_Product__c = desiredProductObj2.Id);
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem3 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice2.Id, Product__c = desiredProductObj3.Product__c, Quantity__c = 10, Required_Quantity__c = 15, Price__c = 10.00, PO_Product__c = desiredProductObj3.Id);
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem4 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice2.Id, Product__c = desiredProductObj4.Product__c, Quantity__c = 12, Required_Quantity__c = 20, Price__c = 20.00, PO_Product__c = desiredProductObj4.Id);
        
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = new List<Proforma_Invoice_Line_Item__c>();
        proformaInvoiceLineItems.add(proformaInvoiceLineItem1);
        proformaInvoiceLineItems.add(proformaInvoiceLineItem2);
        proformaInvoiceLineItems.add(proformaInvoiceLineItem3);
        proformaInvoiceLineItems.add(proformaInvoiceLineItem4);
        insert proformaInvoiceLineItems;
        
        proformaInvoice1.Status__c = 'Accepted';        
        proformaInvoice2.Status__c = 'Accepted';
        update proformaInvoices;
        
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        Alloted_Stock__c allotedStock1 = new Alloted_Stock__c(Product__c = product.Id, Stock__c = 30, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse.Id, ProformaInvoice__c = proformaInvoice1.Id);
        Alloted_Stock__c allotedStock2 = new Alloted_Stock__c(Product__c = product.Id, Stock__c = 20, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse.Id, ProformaInvoice__c = proformaInvoice2.Id);
        
        List<Alloted_Stock__c> allotedStocks = new List<Alloted_Stock__c>();
        allotedStocks.add(allotedStock1);
        allotedStocks.add(allotedStock2);
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert allotedStocks;
        
        List<Payment_Term__c> existingPaymentTerms = [Select Id, Name, Opportunity__c From Payment_Term__c Where Opportunity__c IN :opportunities];
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Payment_Term__c term3 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj2.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term4 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj2.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2, term3, term4}, false);
        
        
        List<Invoice__c> invoices = new List<Invoice__c>();
        Invoice__c invoice = new Invoice__c(Docket_Number__c='TestDocket', Account__c = accountCustomerObj.Id, Billing_Address__c = billingAddress.Id, Delivery_Address__c = shippingAddress.Id, Date__c=Date.today(), Opportunity__c=oppObj.Id, Product_Liability_Insurance_Percentage__c = 1.00, Packing_Charges__c = 1.00, Shipping_Charges__c = 1.00, Trade_Discount__c = 0.00, Conversion_Rate_To_INR__c =45.00);
        invoices.add(invoice);
        
        Id projectRecordTypeIdInvoice = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        
        Invoice__c invoice2 = new Invoice__c(RecordTypeId = projectRecordTypeIdInvoice, Docket_Number__c='TestDocket', Account__c = accountCustomerObj.Id, Billing_Address__c = billingAddress.Id, Delivery_Address__c = shippingAddress.Id, Date__c=Date.today(), Opportunity__c=oppObj.Id, Product_Liability_Insurance_Percentage__c = 1.00, Packing_Charges__c = 1.00, Shipping_Charges__c = 1.00, Trade_Discount__c = 0.00, Conversion_Rate_To_INR__c =45.00);
        invoices.add(invoice2);
        insert invoices;
        
        List<Invoiced_Items__c> invoiceItems = new List<Invoiced_Items__c>();
        Invoiced_Items__c invoiceItem = new Invoiced_Items__c(Invoice__c=invoice.Id, Opportunity__c = oppObj.Id);
        Invoiced_Items__c invoiceItem2 = new Invoiced_Items__c(Invoice__c=invoice2.Id, Opportunity__c = oppObj2.Id);
        invoiceItems.add(invoiceItem);
        invoiceItems.add(invoiceItem2);
        insert invoiceItems;
        
        Invoiced_Product__c invoicedProduct1 = new Invoiced_Product__c(Invoiced_Item__c=invoiceItem.Id, PO_Product__c = desiredProductObj1.Id, Product__c=proformaInvoiceLineItem1.Product__c, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItem1.Id, Quantity__c=10, Unit_Price__c=250.00);
        Invoiced_Product__c invoicedProduct2 = new Invoiced_Product__c(Invoiced_Item__c=invoiceItem2.Id, PO_Product__c = desiredProductObj3.Id, Product__c=proformaInvoiceLineItem3.Product__c, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItem3.Id, Quantity__c=10, Unit_Price__c=250.00);
        List<Invoiced_Product__c> invoiceProducts=new List<Invoiced_Product__c>();
        invoiceProducts.add(invoicedProduct1);
        invoiceProducts.add(invoicedProduct2);
        insert invoiceProducts;
        
    }

    static testMethod void testGetInvoiceXml() {
        Test.startTest();
        Id projectRecordTypeIdInvoice = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        Invoice__c invoice = [Select Id, Status__c From Invoice__c Where RecordTypeId = :projectRecordTypeIdInvoice];
        invoice.status__c = 'Reviewed';
        update invoice;
        Test.stopTest();
    }

    static testMethod void testGetExportXml() {
        Test.startTest();
        Id exportRecordTypeIdInvoice = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Invoice__c invoice = [Select Id, Status__c From Invoice__c Where RecordTypeId = :exportRecordTypeIdInvoice];
        invoice.status__c = 'Reviewed';
        update invoice;
        Test.stopTest();
    }
    
}