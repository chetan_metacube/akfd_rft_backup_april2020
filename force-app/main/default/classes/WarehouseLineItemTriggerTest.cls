@isTest
public class WarehouseLineItemTriggerTest {
    /*
     * Description : Test method to test Warehouse Line Item Trigger
     * Last Modified: Yash Surana  
	*/
	public static testMethod void testTrigger() {
        // Create Product
        Product2 product=new Product2(Name='Test Product',Family='Furniture',Brand__c = 'AKFD', Hidden_Total_Stock_Field__c = 100);
        insert product;
        
        // Create Warehouse
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        
        // Create Warehouse product
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        // Update Warehouse product
        productWarehouse.Current_Stock__c = 60;
        update productWarehouse;
        
        delete productWarehouse;
    }
}