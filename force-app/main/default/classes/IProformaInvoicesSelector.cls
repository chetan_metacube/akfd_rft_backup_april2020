public interface IProformaInvoicesSelector extends fflib_ISObjectSelector {
	List<ProformaInvoice__c> selectProformaInvoicesWithLineItemsByOpportunityIds(Set<Id> opportunityIds);
	List<ProformaInvoice__c> selectAcceptedProformaInvoicesByOpportunityIds(Set<Id> opportunityIds);
}