/**
* Class contains the logics related to document
* @author : Chetan Sharma 
* Last edited by : Chetan Sharma
*/
public class DocumentController {
    /**
    * Method to get possible receipents
    * @param : opportunityId, documentId
    * @return : Recepients JSON string
    */
    @AuraEnabled 
    public static String getPossibleRecipients(Id opportunityId, Id documentId) {
        // Validate arguments
        if(String.isNotBlank(opportunityId) && 'Opportunity' == opportunityId.getSObjectType().getDescribe().getName()) {
            // Query related data
            Opportunity opportunity =  ((List<Opportunity>)OpportunitiesSelector.getInstance().selectSObjectsById(new Set<Id>{opportunityId}))[0];
            List<AccountContactRelation> accountRelatedContacts = (List<AccountContactRelation>)AccountContactRelationshipsSelector.getInstance().selectByAccountId(new Set<Id>{opportunity.AccountId});
            List<User> allUsers = [SELECT Name, Email, UserRoleId, UserRole.Name FROM User WHERE Profile.UserLicense.Name = 'Salesforce'];
          	List<RelatedContact> relatedContacts = new List<RelatedContact>();
            for(AccountContactRelation contact : accountRelatedContacts) {
                relatedContacts.add(new RelatedContact(contact));
            }
            PossibleRecipients possibleRecepients = new PossibleRecipients(relatedContacts, allUsers, isDocumentAlreadyShared(documentId));
            return JSON.serialize(possibleRecepients);
        } else {
            throw getAuraExceptionInstance('Invalid Opportunity Id passed.');
        }
    } 
    
    /**
    * Method to identify whether document is already shared or not
    * @param : documentId
    * @return : Boolean result
    */
    private static Boolean isDocumentAlreadyShared(Id documentId) {
        if(String.isNotBlank(documentId)) {
            try {
                Boolean result = false;
                String query = 'SELECT Id, Name, Document_Shared__c FROM '+ documentId.getSObjectType() + ' WHERE Id=: documentId';
                List<SObject> documentsToBeShared = Database.query(query);
                if(documentsToBeShared != null && documentsToBeShared.size() > 0 ) {
                    result = (Boolean)documentsToBeShared[0].get('Document_Shared__c'); 
                } 
                return result;
            } catch (Exception excpn) {
                 throw getAuraExceptionInstance('Wrong Document Id Passed.');
            }
        } else {
            throw getAuraExceptionInstance('Empty Document Id Passed.');
        }
    }
    
    /**
    * Method to share document
    * @param : all receipent data
    * @Return : String Success Message
    */
    @AuraEnabled 
    public static String share(String data) {
        String result =  '';
        // Apply validations on data string
        Data recipientData = new Data((Object)JSON.deserializeUntyped(data));
        if(recipientData.isDocketNumberSharePage == true && !'Export'.equals(recipientData.salesProcess)  && 'Invoice__c'.equals(recipientData.documentId.getSObjectType().getDescribe().getName())) {
            if(String.isNotBlank(recipientData.docketNumber))  {
                // Send DocketNo Email
                if(recipientData.isDocketNumberUpdated) {
                    updateDocketNumber(recipientData.documentId, recipientData.docketNumber); 
                }
                EmailManager sendEmail = new EmailManager();
                sendEmail.sendDocketNumberEmail('docketNumberTemplate', recipientData.documentId, recipientData.recipientEmails, recipientData.targetObjectId);
                result = 'Docket Number has been shared.';
            } else {
                throw getAuraExceptionInstance('Please Add the Docket number.');
            }
        } else {
            try {
                // Set the document shared variable after sharing document.
                if(recipientData.isDocumentAlreadyShared != null && !recipientData.isDocumentAlreadyShared) {
                    String query = 'SELECT Id, Name, Document_Shared__c FROM '+ recipientData.documentId.getSObjectType().getDescribe().getName() + ' WHERE Id = \''+  recipientData.documentId + '\'';
                    List<SObject> documentsToBeShared = Database.query(query);
                    documentsToBeShared[0].put('Document_Shared__c', true);
                    update documentsToBeShared;
                }
                EmailManager sendEmail = new EmailManager();
                sendEmail.sendEmailUsingEmailTemplate(recipientData.documentId, recipientData.attachmentId, recipientData.templateName, recipientData.recipientEmails, recipientData.targetObjectId);
                result = 'Document has been shared.';
            } catch (Exception e) {
                throw getAuraExceptionInstance(e.getMessage());                
            }
       }
        return result;
    }
	    
    private static void updateDocketNumber(Id documentId, String docketNumber){
        String query = 'SELECT Name, Docket_Number__c FROM ' + documentId.getSObjectType() + ' WHERE Id= :documentId LIMIT 1';
        SObject record = Database.query(query);
        record.put('Docket_Number__c',  docketNumber);
        update record;
    }
    
    public class Data {
        public List<String> recipientEmails {get;set;}
        public String salesProcess {get;set;}
        public Id attachmentId {get;set;}
        public String templateName {get;set;}
        public String docketNumber {get;set;}
        public Id documentId {get;set;}
        public Id targetObjectId {get;set;}
        public Boolean isDocketNumberUpdated {get;set;}
        public Boolean isDocumentAlreadyShared {get;set;}
        public Boolean isDocketNumberSharePage {get;set;}
        
        public Data() {
            
        }
        
        public Data(Object data) {
            Map<String, Object> dataFields = (Map<String, Object>)data;
            this.recipientEmails = new List<String> ();
            for(Object recipientEmail : (List<Object>)dataFields.get('recipientEmails')) {
                this.recipientEmails.add(String.valueOf(recipientEmail));
            }
            this.attachmentId = (Id)dataFields.get('attachmentId');
            this.templateName = (String)dataFields.get('templateName');
            this.docketNumber = (String)dataFields.get('docketNumber');
            this.salesProcess = (String)dataFields.get('salesProcess');
            this.documentId = (Id)dataFields.get('documentId');
            this.targetObjectId = (Id)dataFields.get('targetObjectId');
            this.isDocumentAlreadyShared = (Boolean)dataFields.get('isDocumentAlreadyShared');
            this.docketNumber = (String)dataFields.get('docketNumber');
            this.isDocketNumberSharePage = Boolean.valueOf(dataFields.get('isDocketNumberSharePage'));
            this.isDocketNumberUpdated = Boolean.valueOf(dataFields.get('isDocketNumberUpdated'));
        } 
    }
    
    public class PossibleRecipients {
        public List<RelatedContact> relatedContacts {get;set;}
        public List<User> allUsers {get;set;}
        public Set<String> otherRecipientsEmails {get;set;}
        public Boolean isDocumentAlreadyShared {get;set;}
        
        public PossibleRecipients(List<RelatedContact> relatedContacts, List<User> allUsers, Boolean isDocumentAlreadyShared) {
            this.relatedContacts = relatedContacts;
            this.allUsers = allUsers;
            otherRecipientsEmails = new Set<String>();
            this.isDocumentAlreadyShared = isDocumentAlreadyShared;
        }
    }
    
    public class RelatedContact {
        public Id id {get;set;}
        public String recordTypeName {get;set;}
        public String name {get;set;}
        public String email {get;set;}
        public String department {get;set;}
        
        public RelatedContact(AccountContactRelation relationContact) {
            id = relationContact.contactId;
            name = relationContact.Contact.Name;
            email = relationContact.Contact.Email;
            department = relationContact.Contact.Department__c;
            recordTypeName = Schema.SObjectType.Contact.getRecordTypeInfosById().get(relationContact.Contact.RecordTypeId).getname();
        }
    }
    
    
     /**
    * Method to getAuraExceptionClassInstance
    * @param : error message
    * @return AuraHandledException
    */
    private static AuraHandledException getAuraExceptionInstance(String message) {
        AuraHandledException exceptionInstance = new AuraHandledException('');
        exceptionInstance.setMessage(message);
        return exceptionInstance;
    }
    
    

}