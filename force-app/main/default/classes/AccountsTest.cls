@isTest
public class AccountsTest {

    @isTest static void testAccountUpdateCaseMethod() {
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        accounts = [SELECT Id, Name, CurrencyIsoCode FROM Account WHERE Id IN : accounts];
        // Default is INR
        accounts[0].CurrencyIsoCode = 'USD';
        try {
            update accounts;
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Account Currency cannot be changed as Pricebook already exists.'));
        }
    } 
}