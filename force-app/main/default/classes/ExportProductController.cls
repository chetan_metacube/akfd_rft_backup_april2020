public with sharing class ExportProductController {
    /**
    * Properties and variables
    */
    public List<String> headings { get; set; }    
    public List<Object> recordList { get; set; }   
    public Map<String, Object> recordsMap { get; set; }   
    public String newLine { get; set; }
    public String currentDateTime { get; set; }
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    /**
    * Constructor
    */
    public ExportProductController() {   
        newLine = '\n';
        headings = new List<String>();
        currentDateTime = '(' + System.Now().format('dd-MM-YYYY HH-mm a','IST') + ')';
        List<Product2> fieldDataMapping = [Select Id, Name, Brand__c, Product_SKU__c,
                                                    Description__c, Stock_Group__c, Unit__c, Root_Price__c, CreatedDate,
                                                    Root_Price_Updated_Date__c, Retail_Price__c, Retail_Price_Updated_Date__c,
                                                    HSN_Tax__r.Name, HSN_Tax__r.GST_Applicable_Date__c, HSN_Tax__r.HSN_Description__c, 
                                                    HSN_Tax__r.Tax__c, Cess__c
                                                    From Product2 LIMIT 100];
        headings.add('Item Name');
        headings.add('Alias');
        headings.add('Part No');
        headings.add('Description');
        headings.add('Stock Group');
        headings.add('UOM');
        headings.add('Category');
        headings.add('Standard Cost- Rate');        
        headings.add('Standard Cost- Date');
        headings.add('Selling Price- Rate');
        headings.add('Selling Price- Date');
        headings.add('Maintain Batches');
        headings.add('GST Applicable');
        headings.add('GST Applicable Date');
        headings.add('HSN Description');
        headings.add('HSN/SAC Code');
        headings.add('Taxability');
        headings.add('Integrated Tax %');
        headings.add('Cess %');
        headings.add('Type of Supply');
        headings.add('Quantity');
        headings.add('Rate');
        headings.add('Amount');
        headings.add('Created Date');
        recordList = new List<List<String>>();
        List<String> records;
        String formattedDate = '';
        for(Product2 product : fieldDataMapping) {
            records = new List<String>();
            records.add(product.Name);
            records.add(product.Product_SKU__c == null ? '' : product.Product_SKU__c);
            records.add('');
            records.add(product.Description__c == null ? '' : product.Description__c);
            records.add(product.Stock_Group__c == null ? '' : product.Stock_Group__c.equals('Mfg') ? 'Manufacturing' : product.Stock_Group__c.equals('Trg') ? 'Trading' : '');
            records.add(product.Unit__c == null ? '' : product.Unit__c);
            records.add(product.Brand__c == null ? '' : product.Brand__c);
            records.add(product.Root_Price__c == null ? '' : String.valueOf(product.Root_Price__c));
            records.add(product.Root_Price_Updated_Date__c == null ? '' : String.valueOf(product.Root_Price_Updated_Date__c));
            records.add(product.Retail_Price__c == null ? '' : String.valueOf(product.Retail_Price__c));
            records.add(product.Retail_Price_Updated_Date__c == null ? '' : String.valueOf(product.Retail_Price_Updated_Date__c));
            records.add('');
            records.add(product.HSN_Tax__r.Tax__c == null || product.HSN_Tax__r.Tax__c == 0 ? 'No' : 'Yes');
            records.add(product.HSN_Tax__r.GST_Applicable_Date__c == null ? '' : String.valueOf(product.HSN_Tax__r.GST_Applicable_Date__c));
            records.add(product.HSN_Tax__r.HSN_Description__c == null ? '' : String.valueOf(product.HSN_Tax__r.HSN_Description__c));
            records.add(product.HSN_Tax__r.Name == null ? '' : String.valueOf(product.HSN_Tax__r.Name));
            records.add(product.HSN_Tax__r.Tax__c == null || product.HSN_Tax__r.Tax__c == 0 ? 'Exempt' : 'Taxable');
            records.add(product.HSN_Tax__r.Tax__c == null ? '' : String.valueOf(product.HSN_Tax__r.Tax__c));
            records.add(product.Cess__c == null || product.Cess__c == 0.00? '' : String.valueOf(product.Cess__c));
            records.add('');
            records.add('');
            records.add('');
            records.add('');
            formattedDate = DateTime.newInstance(product.CreatedDate.year(), product.CreatedDate.month(), product.CreatedDate.day()).format('d/MMM/YYYY');
            records.add(formattedDate);
            recordList.add(records);
        }
    }       
}