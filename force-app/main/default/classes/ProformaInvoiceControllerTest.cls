@isTest
public class ProformaInvoiceControllerTest {
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(customerAccounts, products, 1);
        List<PriceBook_Product_Entry__c> pricebookEntries = MockData.createPriceBookEntries(customerAccounts, products, priceBooks, 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(customerAccounts, opportunities, products, 1);
       
        
    }
    
    @isTest static void positiveTestCase_For_getOpportunityRecordByIdMethod() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity Limit 1];
        System.assert(ProformaInvoiceController.getOpportunityRecordById(opportunity.Id) != null);
    }
   
    @isTest static void negativeTestCase_For_getOpportunityRecordByIdMethod_WhenPIAlreadyAccepted() { 
        Opportunity opportunity = [SELECT Id, Name, AccountId, RecordTypeId, RecordType.Name, Account.CurrencyIsoCode, Owner.Name, Account.Name, Owner.Email FROM Opportunity Limit 1];
        List<PO_Product__c> desiredProducts = [SELECT Id, Name, Quantity__c FROM PO_Product__c];
        Id proformaInvoiceId = ProformaInvoiceController.generateProformaInvoice(opportunity, desiredProducts, 1, 'INR');
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Id =: proformaInvoiceId];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        try {
            ProformaInvoiceController.getOpportunityRecordById(opportunity.Id);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Accepted/Revised PI already exists.'));
        }
        
    }
    
    @isTest static void negativeTestCase_For_getOpportunityRecordByIdMethod_WhenOpportunityIdIsNull() { 
        try {
            ProformaInvoiceController.getOpportunityRecordById(null);
        } catch (Exception error) {
            System.assert(error.getMessage().contains('Invalid Opportunity Id.'));
        }
    }
    
    @isTest static void positiveTestCase_For_getCurrencyIsoCodesMethod() { 
        System.assert(ProformaInvoiceController.getCurrencyIsoCodes() != null);
    }
    
    @isTest static void positiveTestCase_For_generateProformaInvoiceMethod() { 
        Opportunity opportunity = [SELECT Id, Name, AccountId, RecordTypeId, RecordType.Name, Account.CurrencyIsoCode, Owner.Name, Account.Name, Owner.Email FROM Opportunity Limit 1];
        List<PO_Product__c> desiredProducts = [SELECT Id, Name, Quantity__c FROM PO_Product__c];
        Id proformaInvoiceId = ProformaInvoiceController.generateProformaInvoice(opportunity, desiredProducts, 1, 'INR');
        System.assert(proformaInvoiceId != null);
    }
    
    @isTest static void negativeTestCase_For_generateProformaInvoiceMethod_WhenUserDonotHaveAccess() { 
        User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' LIMIT 1];
        try {
            System.runAs(chatterFreeUser) {
                Opportunity opportunity = [SELECT Id, Name, AccountId, RecordTypeId, RecordType.Name, Account.CurrencyIsoCode, Owner.Name, Account.Name, Owner.Email FROM Opportunity Limit 1];
                List<PO_Product__c> desiredProducts = [SELECT Id, Name, Quantity__c FROM PO_Product__c];
                Id proformaInvoiceId = ProformaInvoiceController.generateProformaInvoice(opportunity, desiredProducts, 1, 'INR');
            }
        } catch(Exception exc) {
            System.assert(exc.getMessage().contains('Insufficient Privileges.'));
        }
    }
    
	@isTest static void negativeTestCase_For_generateProformaInvoiceMethod_WhenOpportunityIsNull() { 
        List<PO_Product__c> desiredProducts = [SELECT Id, Name, Quantity__c FROM PO_Product__c];
        try {
            Id proformaInvoiceId = ProformaInvoiceController.generateProformaInvoice(null, desiredProducts, 1, 'INR');
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Something Went Wrong!!!'));
        }
     }
    
    @isTest static void negativeTestCase_For_generateProformaInvoiceMethod_WhenDesiredProductQuasntityIsInvlid() { 
        Opportunity opportunity = [SELECT Id, Name, AccountId, RecordTypeId, RecordType.Name, Account.CurrencyIsoCode, Owner.Name, Account.Name, Owner.Email FROM Opportunity Limit 1];
        List<PO_Product__c> desiredProducts = [SELECT Id, Name, Quantity__c FROM PO_Product__c];
        desiredProducts[0].Quantity__c = -2;
        //update desiredProducts[0];
        try {
            Id proformaInvoiceId = ProformaInvoiceController.generateProformaInvoice(opportunity, desiredProducts, 1, 'INR');
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Invalid quantity entered. Quantity should be greater than zero.'));
        }
    }
    
    
    
    
}