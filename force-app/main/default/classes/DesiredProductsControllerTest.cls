@isTest
public class DesiredProductsControllerTest {
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        
        
    }
    
    @isTest static void positiveTestCase_For_insertDesiredProducts() { 
         Product2 selectedProduct = [SELECT Id, Name, StockKeepingUnit, Status__c, Family, Unit__c FROM Product2 LIMIT 1];
            Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];        
            DesiredProductsController.SelectedProductsWrapper selectedProductWrapper = new DesiredProductsController.SelectedProductsWrapper();
            selectedProductWrapper.id = selectedProduct.Id;
            selectedProductWrapper.name = selectedProduct.Name;
            selectedProductWrapper.quantity = 10;
            selectedProductWrapper.status = selectedProduct.Status__c;
            selectedProductWrapper.family = selectedProduct.Family;
            selectedProductWrapper.stockKeepingUnit = selectedProduct.StockKeepingUnit;
            selectedProductWrapper.unit = selectedProduct.Unit__c;
            String selectedProductString = '[' + JSON.serialize(selectedProductWrapper) + ']';
            String resultMessage = DesiredProductsController.insertDesiredProducts(selectedProductString, opportunity.Id);
            System.assert(resultMessage.contains('Success'));
     }
    
    @isTest static void negativeTestCase_For_insertDesiredProductsWhenProductIdIsPassedNull() { 
        Product2 selectedProduct = [SELECT Id, Name, StockKeepingUnit, Status__c, Family, Unit__c FROM Product2 LIMIT 1];
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];        
        DesiredProductsController.SelectedProductsWrapper selectedProductWrapper = new DesiredProductsController.SelectedProductsWrapper();
        selectedProductWrapper.id = null;
        selectedProductWrapper.name = selectedProduct.Name;
        selectedProductWrapper.quantity = 10.89;
        selectedProductWrapper.status = selectedProduct.Status__c;
        selectedProductWrapper.family = selectedProduct.Family;
        selectedProductWrapper.stockKeepingUnit = selectedProduct.StockKeepingUnit;
        selectedProductWrapper.unit = selectedProduct.Unit__c;
        String selectedProductString = '[' + JSON.serialize(selectedProductWrapper) + ']';
        try  {
            String resultMessage = DesiredProductsController.insertDesiredProducts(selectedProductString, opportunity.Id);
            System.assert(resultMessage.contains('Success'));
        } catch(Exception exc ) {
            System.assert(exc.getMessage().contains('Script-thrown exception'));
        }
        
    }
    
    @isTest static void negativeTestCase_For_insertDesiredProducts_WhenDesiredProductIsAlreadyAdded() { 
        Product2 selectedProduct = [SELECT Id, Name, StockKeepingUnit, Status__c, Family, Unit__c FROM Product2 LIMIT 1];
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];        
        DesiredProductsController.SelectedProductsWrapper selectedProductWrapper = new DesiredProductsController.SelectedProductsWrapper();
        selectedProductWrapper.id = selectedProduct.Id;
        selectedProductWrapper.name = selectedProduct.Name;
        selectedProductWrapper.quantity = 10;
        selectedProductWrapper.status = selectedProduct.Status__c;
        selectedProductWrapper.family = selectedProduct.Family;
        selectedProductWrapper.stockKeepingUnit = selectedProduct.StockKeepingUnit;
        selectedProductWrapper.unit = selectedProduct.Unit__c;
        String selectedProductString = '[' + JSON.serialize(selectedProductWrapper) + ']';
        String resultMessage = DesiredProductsController.insertDesiredProducts(selectedProductString, opportunity.Id);
        System.assert(resultMessage.contains('Success'));
        
        try {
            resultMessage = DesiredProductsController.insertDesiredProducts(selectedProductString, opportunity.Id);
        } catch (Exception error ) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    
    @isTest static void negativeTestCase_For_insertDesiredProducts_WhenSelectedProductsStringIsNull() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];        
        try {
            String resultMessage = DesiredProductsController.insertDesiredProducts(null, opportunity.Id);
        } catch (Exception error ) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    
    @isTest static void negativeTestCase_For_insertDesiredProducts_WhenUserDoesnotHaveOpportunityEditAcceess() { 
        User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' LIMIT 1];
        try {
            System.runAs(chatterFreeUser) {
                Product2 selectedProduct = [SELECT Id, Name, StockKeepingUnit, Status__c, Family, Unit__c FROM Product2 LIMIT 1];
                Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];        
                DesiredProductsController.SelectedProductsWrapper selectedProductWrapper = new DesiredProductsController.SelectedProductsWrapper();
                selectedProductWrapper.id = selectedProduct.Id;
                selectedProductWrapper.name = selectedProduct.Name;
                selectedProductWrapper.quantity = 10;
                selectedProductWrapper.status = selectedProduct.Status__c;
                selectedProductWrapper.family = selectedProduct.Family;
                selectedProductWrapper.stockKeepingUnit = selectedProduct.StockKeepingUnit;
                selectedProductWrapper.unit = selectedProduct.Unit__c;
                String selectedProductString = '[' + JSON.serialize(selectedProductWrapper) + ']';
                String resultMessage = DesiredProductsController.insertDesiredProducts(selectedProductString, opportunity.Id);
                System.assert(resultMessage.contains('Success'));
            }
        } catch (Exception error ) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    
    @isTest static void negativeTestCase_For_insertDesiredProducts_WhenSelectedProductsStringIsInvalid() { 
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];        
        try {
            String resultMessage = DesiredProductsController.insertDesiredProducts('[]', opportunity.Id);
        } catch (Exception error ) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
   
    @isTest static void negativeTestCase_For_insertDesiredProducts_WhenOpportunityIdIsNull() { 
        Product2 selectedProduct = [SELECT Id, Name, StockKeepingUnit, Status__c, Family, Unit__c FROM Product2 LIMIT 1];
        DesiredProductsController.SelectedProductsWrapper selectedProductWrapper = new DesiredProductsController.SelectedProductsWrapper();
        selectedProductWrapper.id = selectedProduct.Id;
        selectedProductWrapper.name = selectedProduct.Name;
        selectedProductWrapper.quantity = 10;
        selectedProductWrapper.status = selectedProduct.Status__c;
        selectedProductWrapper.family = selectedProduct.Family;
        selectedProductWrapper.stockKeepingUnit = selectedProduct.StockKeepingUnit;
        selectedProductWrapper.unit = selectedProduct.Unit__c;
        String selectedProductString = '[' + JSON.serialize(selectedProductWrapper) + ']';       
        try {
            String resultMessage = DesiredProductsController.insertDesiredProducts(selectedProductString, null);
        } catch (Exception error ) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
   }

}