public interface IInvoicedItemsSelector extends fflib_ISObjectSelector {
	List<Invoiced_Items__c> selectInvoicedItemsWithInvoicedProductsByInvoiceIds(Set<Id> invoiceIds);
}