public class DeleteScript {
    public DeleteScript() {
        try {
            //Delete all cases
            List<Case> cases=[SELECT Id From Case];
            Delete cases;
            System.debug('SUCCESS!! Cases Deleted Successfully');
            
            //Delete all leads
            List<Lead> leads=[SELECT Id From Lead];
            Delete leads;
            System.debug('SUCCESS!! Leads Deleted Successfully');
            
            //Delete all LeadSource
            List<Lead_Source__c> leadSources=[SELECT Id From Lead_Source__c];
            Delete leadSources;
            System.debug('SUCCESS!! LeadSources Deleted Successfully');
            
            
            //Delete all opportunities and childs(DesiredProducts,PaymentTerm and Payment Transaction,Quotation and QuotationLineItems,ProformaInvoice and PILineItems,Invoice and InvoicedItems and InvoicedProducts)
            List<Opportunity> opportunities=[SELECT Id From Opportunity];
            Delete opportunities;
            System.debug('SUCCESS!! Opportunities and its childs Deleted Successfully');
            
            //Delete all contacts 
            List<Contact> contacts=[SELECT Id From Contact];
            Delete contacts;
            System.debug('SUCCESS!! Contacts Deleted Successfully');
            
            List<AccountContactRelation> accountRelatedContacts=[SELECT Id From AccountContactRelation];
            Delete accountRelatedContacts;
            
            
            
            //Delete all accounts,childs(priceBook and priceBookEntries) 
            List<Account> accounts=[SELECT Id From Account Where id!='0010k00000eSgEXAA0'];
            Delete accounts;
            System.debug('SUCCESS!! Account and its childs Deleted Successfully');
            
            
            
            
            //Delete all products
            List<Product2> products=[Select Id From Product2];
            Delete products;
            System.debug('SUCCESS!! Products Deleted Successfully');
            
            //Delete warehouses and childs(product warehouse)
            List<Warehouse__c> warehouses=[Select Id From Warehouse__c];
            Delete warehouses;
            System.debug('SUCCESS!! Warehouses and its childs Deleted Successfully');
            
            
            
            //Delete all projected Stock
            List<Projected_Stock__c> projectedStocks=[Select Id From Projected_Stock__c];
            Delete projectedStocks;
            System.debug('SUCCESS!! Projected Stocks Deleted Successfully');
            
            
            //Delete all Bank Info
            List<Bank_Info__c> banks=[Select Id From Bank_Info__c WHERE id not in ('a020k000004al1vAAA','a020k000004al20AAA')];
            Delete banks;
            System.debug('SUCCESS!! Bank Details Deleted Successfully');
            
            List<HSN_Tax__c> hsnTaxes = [SELECT id FROM HSN_Tax__c];
            delete hsnTaxes;
            System.debug('SUCCESS!! HSN_Tax Deleted Successfully');
            
            System.debug('SUCCESS!! Deleted Successfully');
        } catch(Exception e) {
            System.Debug('Exception'+e.getStackTraceString());
        }
        
        
        
    }
}