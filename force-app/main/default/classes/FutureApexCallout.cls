public class FutureApexCallout {
    @future(Callout=true)
    public static void productCalloutApex(String apiType, String entityName, String xmlBody) {
        system.debug('FutureApexCallout ApexTriggerCheck.getInstance() ' + ApexTriggerCheck.getInstance());
        if(!Test.isRunningTest() || (ApexTriggerCheck.getInstance() != null && ApexTriggerCheck.getInstance().isFutureCalling != null)) {
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint('http://122.180.240.44:8069/xmlrpc/2/object');
            request.setMethod('POST');
            
            //String targetUrl = 'application/xml';
            //request.setHeader(<span class="string">'Content-Type'</span>, <span class="string">'application/xml'</span>);
            // Set the body as a JSON object
            // xmlBody.replace('[Alt + 255]', '[Space]');
            
            xmlBody = xmlBody.replace(' ', ' ');
            xmlBody = xmlBody.replace('&', '&amp;');
            system.debug('xmlBody ' + xmlBody);
            request.setBody(xmlBody);
            system.debug('request ' + request);
            
            Dom.Document doc;
            String message;
            HttpResponse response;
            try {
                response = http.send(request);
                doc = response.getBodyDocument();
                message = parseXML(doc.getRootElement());
                System.debug('Response ' + response.getBody());
            } catch(Exception e) {
                message = e.getMessage();
            }
            // Parse the JSON response
            fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
                new Schema.SObjectType[] {
                    API_Log__c.SObjectType,
                        Attachment.SObjectType
                        }
            );
            API_Log__c apiLog = new API_Log__c();
            apiLog.Name = entityName;
            apiLog.Status__c = !message.equals('1') ? 'Failed' : 'Success';
            apiLog.Message__c = message;
            system.debug('apiType ' + apiType);
            apiLog.Type__c = apiType;
            uow.registerNew(apiLog);
            
            Attachment requestBodyAttachment = new Attachment();
            requestBodyAttachment.Body = Blob.valueOf(xmlBody);
            requestBodyAttachment.Name = 'Request Body.txt';
            requestBodyAttachment.IsPrivate = false;
            
            uow.registerNew(requestBodyAttachment, Attachment.ParentId, apiLog);
            
            Attachment responseBodyAttachment = new Attachment();
            responseBodyAttachment.Body = response == null ? Blob.valueOf('NIL') : Blob.valueOf(response.getBody());
            responseBodyAttachment.Name = 'Response Body.txt';
            responseBodyAttachment.IsPrivate = false;
            uow.registerNew(responseBodyAttachment, Attachment.ParentId, apiLog);
            uow.commitWork();
            /*
                // Creating the CSV file
                String attName = 'XMLBody.xml';
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
                
                // Create the email attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(attName);
                efa.setBody(Blob.valueOf(xmlBody));
                
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                email.setSubject( 'UAT ' + entityName );
                email.setHtmlBody( 'Response: ' + JSON.serialize(response.getBody()) + '<br/><br/>User: ' + UserInfo.getUserName());
                email.setToAddresses( new String[] {'deepak.sharma@metacube.com', 'akshit.bhandari@metacube.com'});
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
			*/
        }
    }
    
    private static String parseXML(DOM.XMLNode methodResponse) {
        DOM.XMLNode params = methodResponse.getChildElement('params', null);
        DOM.XMLNode param = params.getChildElement('param', null);
        DOM.XMLNode value = param.getChildElement('value', null);
        String resultString = value.getChildElement('string', null).getText();
        return resultString;
    }
    /*
    private static String parseErrorXML(DOM.XMLNode methodResponse) {
        DOM.XMLNode fault = methodResponse.getChildElement('fault', null);
        DOM.XMLNode value = fault.getChildElement('value', null);
        DOM.XMLNode struct = value.getChildElement('struct', null);
        DOM.XMLNode member = struct.getChildElement('member', null);
        DOM.XMLNode name = struct.getChildElement('name', null);
        DOM.XMLNode member = struct.getChildElement('member', null);
        DOM.XMLNode member = struct.getChildElement('member', null);
        return resultString;
    };*/
}