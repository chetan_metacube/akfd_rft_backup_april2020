public class ProformaInvoicesServiceImpl implements IProformaInvoicesService {

    public List<ProformaInvoice__c> getProformaInvoicesWithLineItemsByOpportunityIds(Set<Id> opportunityIds) {
        
        return ProformaInvoicesSelector.getInstance().selectProformaInvoicesWithLineItemsByOpportunityIds(opportunityIds);
        
    }
       
    public List<Id> generate(List<ProformaInvoicesService.ProformaInvoicesWrapper> proformaInvoiceWrappers) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        
        // Invoicing Factory helps domain classes produce invoices
        ProformaInvoicesService.ProformaInvoiceFactory proformaInvoiceFactory = new ProformaInvoicesService.ProformaInvoiceFactory(uow);
        List<Id> accountIds = new List<Id>();
        List<Id> productIds = new List<Id>();
        List<PO_Product__c> desiredProducts = new List<PO_Product__c>();
        Map<Id, PriceBook_Product__c> priceBooks = new Map<Id, PriceBook_Product__c>();
        Map<Id, List<PriceBook_Product_Entry__c>> priceBookEntries = new Map<Id, List<PriceBook_Product_Entry__c>>();
        Map<Id, Map<Id, Id>> customersProductSpecificPriceBooks = new Map<Id, Map<Id, Id>>();
        Map<Id, PriceBook_Product_Entry__c> quantityRelevantPricebooksEntry = new Map<Id, PriceBook_Product_Entry__c>();
        Map<Id, List<Id>> opportunityDesiredProductIds = new Map<Id, List<Id>>();
        List<ProformaInvoicesService.DesiredProductsIdsWrapper> opportunitySpecificRecords = new List<ProformaInvoicesService.DesiredProductsIdsWrapper>();
        Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>();
        for(ProformaInvoicesService.ProformaInvoicesWrapper proformaInvoiceWrapper : proformaInvoiceWrappers) {
            accountIds.add(proformaInvoiceWrapper.opportunity.AccountId);
            desiredProducts.addAll(proformaInvoiceWrapper.desiredProducts);
            opportunities.put(proformaInvoiceWrapper.opportunity.Id, proformaInvoiceWrapper.opportunity);
            opportunitySpecificRecords.add(new ProformaInvoicesService.DesiredProductsIdsWrapper(proformaInvoiceWrapper.opportunity, new List<Id>((new Map<Id, PO_Product__c>(proformaInvoiceWrapper.desiredProducts)).keySet()), proformaInvoiceWrapper.ConversionRate, proformaInvoiceWrapper.CurrencyIsoCode));
            for(PO_Product__c  desiredProduct : proformaInvoiceWrapper.desiredProducts) {
                productIds.add(desiredProduct.Product__c);
            }
        }
       	System.debug('productIds@@@' + productIds);
        System.debug('accountIds@@@' + accountIds);
        priceBooks = new Map<Id, PriceBook_Product__c>([SELECT Id, Name, CurrencyIsoCode, Customer__c, Client_SKU__c, Effective_End_Date__c, Effective_Start_Date__c, Product__c, Unit__c, (SELECT Id, Name, CurrencyIsoCode, PriceBook_Product__c, Lead_Time_for_MOQ__c, Max_Quantity__c, Min_Quantity__c, Price__c, Unit_of_Price__c FROM PriceBook_Product_Entries__r) FROM PriceBook_Product__c WHERE Customer__c IN :accountIds AND Product__c IN : productIds ]);
        
        System.debug('priceBooks@@@' + priceBooks);
        for(PriceBook_Product__c priceBook : priceBooks.values()) {
            Map<Id, Id> productsPriceBooks;
            if(customersProductSpecificPriceBooks.containsKey(priceBook.Customer__c) && !(customersProductSpecificPriceBooks.get(priceBook.Customer__c)).containsKey(priceBook.Product__c)) {
                // Check Product entry 
                // If exists then add leave 
                // Else Do New Entry
                productsPriceBooks = customersProductSpecificPriceBooks.get(priceBook.Customer__c);
                productsPriceBooks.put(priceBook.Product__c, priceBook.Id);
                customersProductSpecificPriceBooks.put(priceBook.Customer__c, productsPriceBooks);
            } else {
                // Do Fresh Product entry 
                productsPriceBooks = new Map<Id, Id>(); 
                productsPriceBooks.put(priceBook.Product__c, priceBook.Id);
                customersProductSpecificPriceBooks.put(priceBook.Customer__c, productsPriceBooks);
            }
            priceBookEntries.put(priceBook.Id, priceBook.PriceBook_Product_Entries__r);
        }
        
        //############################## 1. Validations Starts ###########################################
        // Opportunity Specific validations 
        Map<Id, String> opportunitySpecificErrors =  new Map<Id, String>();
        Map<Id, String> opportunitySpecificProductsWithoutImages =  new Map<Id, String>();
        
        for(ProformaInvoicesService.ProformaInvoicesWrapper proformaInvoiceWrapper : proformaInvoiceWrappers) {
            String errorMessage = '';
            List<String> productsWithoutPriceBooks = new List<String>();
            List<String> productsWithoutPriceBookEntries = new List<String>();
            List<String> productsWithInvalidPriceBooks = new List<String>();
            //List<String> productsWithInvalidQuantities = new List<String>();
            List<String> productsWithInvalidRetailPrice = new List<String>();
            List<String> productsWithoutImages = new List<String>();
            
            
            //Pricebook validation : customerSpecific and product specific pricebook should exist 
            for(PO_Product__c  desiredProduct : proformaInvoiceWrapper.desiredProducts) {
                //Pricebook check (Available or not available)
                if(desiredProduct.Quantity__c <= 0) {
                    opportunitySpecificErrors.put(proformaInvoiceWrapper.opportunity.Id , desiredProduct + ' Invalid Quantity Entered');
                    continue;
                }
                
                if( proformaInvoiceWrapper.opportunity.RecordType.Name != 'Retail') {
                    if(customersProductSpecificPriceBooks.size() == 0 || !customersProductSpecificPriceBooks.get(proformaInvoiceWrapper.Opportunity.AccountId).containsKey(desiredProduct.Product__c)) {
                        // Add Error When PriceBook Doesn't Exist
                        productsWithoutPriceBooks.add(desiredProduct.Product__r.Name);
                    } else if(productsWithoutPriceBooks.size() == 0) {
                        Id priceBookId = customersProductSpecificPriceBooks.get(proformaInvoiceWrapper.Opportunity.AccountId).get(desiredProduct.Product__c);
                        PriceBook_Product__c priceBook = priceBooks.get(priceBookId);    
                        if(priceBook.Effective_Start_Date__c != null && priceBook.Effective_End_Date__c != null && Date.today() >= priceBook.Effective_Start_Date__c && Date.today() <= priceBook.Effective_End_Date__c) {
                            
                            // Validate pricebook entries
                            if(productsWithInvalidPriceBooks.size() == 0 && priceBookEntries.size() > 0 && priceBookEntries.containsKey(priceBook.Id)) {
                                //Check if quantity exists in or not
                                Boolean isPriceBookEntryFound = false;
                                for(PriceBook_Product_Entry__c priceBookEntry : priceBookEntries.get(priceBook.Id)) {
                                    if((priceBookEntry.Min_Quantity__c != null && priceBookEntry.Max_Quantity__c != null && desiredProduct.Quantity__c >= priceBookEntry.Min_Quantity__c && desiredProduct.Quantity__c <= priceBookEntry.Max_Quantity__c)) {
                                        isPriceBookEntryFound = true;
                                        quantityRelevantPricebooksEntry.put(priceBook.Id, priceBookEntry);
                                        break;
                                    }
                                } 
                                if(!isPriceBookEntryFound) {
                                    productsWithoutPriceBookEntries.add(desiredProduct.Name);
                                    // Add Error That pricebook not found
                                }
                            } else if(productsWithInvalidPriceBooks.size() == 0) {
                                productsWithoutPriceBookEntries.add(desiredProduct.Name);
                                //Add Error Pricebook doesnot exists.
                            }
                        } else {
                            // Add Start Date and End date Error
                            productsWithInvalidPriceBooks.add(desiredProduct.Name );
                        }
                    }
                } else if(desiredProduct.Retail_Price__c <= 0.0) {
                    productsWithInvalidRetailPrice.add(desiredProduct.name);
                }
                
                if( String.isBlank(desiredProduct.Product__r.Image__c) ) {
                    productsWithoutImages.add(desiredProduct.Name);
                }
                
                //PriceBook Entries 
            }
            
            if(productsWithInvalidRetailPrice.size() > 0) {
                errorMessage += 'Retail Price should be greater than zero for following Desired Products : ' + String.join( productsWithInvalidRetailPrice, ', ');
            } else if(productsWithoutPriceBooks.size() > 0) {
                errorMessage += 'PriceBook not found for following Products : ' + String.join( productsWithoutPriceBooks, ', ');
            } else if(productsWithInvalidPriceBooks.size() > 0) {
                errorMessage += 'PriceBook not Valid for following Desired Products : ' + String.join( productsWithInvalidPriceBooks, ', ');
            } else if(productsWithoutPriceBookEntries.size() > 0) {
                errorMessage += 'No PriceBook Entry Found for Following Desired Products : ' + String.join( productsWithoutPriceBookEntries, ', ');
            }  
            
            if(errorMessage.length() > 0) {
                opportunitySpecificErrors.put(proformaInvoiceWrapper.opportunity.Id, errorMessage);
            }
            if(productsWithoutImages.size() > 0) {
                opportunitySpecificProductsWithoutImages.put(proformaInvoiceWrapper.opportunity.Id, String.join( productsWithoutImages, ', '));
            }
        }
        //############################## Validations Ends ################################################
        
        
        //############################## 2. Add PI Fields Starts ##########################
        // Call DPDomain
        
        
        if(opportunitySpecificErrors.size() == 0 ) {
            ProformaInvoicesService.DesiredProductsWithPriceBooksWrapper desiredProductsWithPriceBooksWrapper = new ProformaInvoicesService.DesiredProductsWithPriceBooksWrapper(opportunitySpecificRecords,  priceBooks, customersProductSpecificPriceBooks, quantityRelevantPricebooksEntry);
            fflib_ISObjectDomain desiredProductDomain = Application.Domain.newInstance(desiredProducts);
            if(desiredProductDomain instanceof ProformaInvoicesService.ISupportProformaInvoicing) { 
                // Ask the domain object to generate its invoices
                ProformaInvoicesService.ISupportProformaInvoicing proformaInvoicing = (ProformaInvoicesService.ISupportProformaInvoicing) desiredProductDomain;
                
                proformaInvoicing.generateProformaInvoices(proformaInvoiceFactory, desiredProductsWithPriceBooksWrapper);	
                // Commit generated invoices to the database	
                try{	
                    uow.commitWork();
                    
                } catch(Exception e) {
                    throw new AKFDException(e);
                }
                //Send Emails
                //1. Send Proforma Invoice creation email
                //Need to send List of emails to be sent
                EmailManager sendEmail = new EmailManager();
                List<ProformaInvoice__c> newlyInsertedProformaInvoices = [SELECT Id, Name, Opportunity__c FROM ProformaInvoice__c WHERE Id IN :proformaInvoiceFactory.ProformaInvoices];
                for(ProformaInvoice__c proformaInvoice : newlyInsertedProformaInvoices) {
                    sendEmail.sendNotification((SObject)proformaInvoice, opportunities.get(proformaInvoice.Opportunity__c));
                }
                //2. Send Image not found email
                for(Id opportunityId : opportunitySpecificProductsWithoutImages.keySet()) {
                    sendEmail.sendNoImageNotification(opportunities.get(opportunityId), opportunitySpecificProductsWithoutImages.get(opportunityId));
                }
                
                // List of proformaInvoices generated
                List<Id> proformaInvoiceIds = new List<Id>();
                for(ProformaInvoice__c proformaInvoice : proformaInvoiceFactory.ProformaInvoices) {
                    proformaInvoiceIds.add(proformaInvoice.Id);
                }
                return proformaInvoiceIds;
            }
        } else {
            throw new AKFDException(JSON.serialize(opportunitySpecificErrors));
        }
        
        //############################## Add PI Fields End ################################
        
        
        //############################## 3. Add Pricebook Related Fields Starts ##########################
        // Call PriceBookDomain
        //############################## Add Pricebook Related Fields End ################################
        
        // Get wrapper <Opportunity, <DPs>, conversionRate>
        
        // Generate followings
        // 1. Query Pricebooks and Line Items
        // 2. Generate Map<Pricebook, <Entries>> [For Pricebook Domain records]
        // 			   List<DPs>				 [For Dps Domain records]
        // 			   List<AccountIds>
        // 			   
        // 3. Generate Wrapper : papWR == <<ProductId, PriceBookId> , <AccountId, ProductId> [For Validations and assignment and has searching method] , 
        // 								  <OpportunityId, <DPIds>, conversionRate> 		 	 [For DPDomain],
        // 								  < papWR , <AccountId, <PIs>> 					 [For PI Domain]
        // 
        // 
        
        // Steps
        //1.Validate( PriceBooks , Pricebook Entries, Quantity Validations, Start And End Date Validations )
        //2.Add PI and lineitems Fields
        //3.Add PriceBook related fields
        //4.Commit and return Ids of PI's
        return null;
    }
    
}