public interface IWarehouseLineItemsSelector extends fflib_ISObjectSelector {
    
    List<Warehouse_Line_Item__c> selectByProductIdsWhichHasCurrentStock(Set<Id> productIds);
    List<Warehouse_Line_Item__c> selectByProductAndWarehouse(Set<Id> productIds, Set<Id> warehouseIds);
}