@isTest
public class PaymentTransactionControllerTest {
    @testSetup
    static void testSetup() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
    	List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        proformaInvoices[0].Status__c = 'Accepted';
        update proformaInvoices;
        List<Warehouse__c> warehouses = MockData.createWarehouses(1);
        
        List<Warehouse_Line_Item__c> productWarehouses = MockData.createWarehouseLineItems(warehouses, products, 1);
        
        Alloted_Stock__c allotedStock = new Alloted_Stock__c(Product__c = products[0].Id, Stock__c = 10, Warehouse__c = warehouses[0].Id, Warehouse_Product__c = productWarehouses[0].Id, ProformaInvoice__c = proformaInvoices[0].Id);
        
       
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert allotedStock;
        
		Opportunity oppObj = [Select Id, (Select Id, Name, Opportunity__c From Payment_Terms__r) From Opportunity Where Id =: opportunities[0].Id LIMIT 1];
        List<Payment_Term__c> existingPaymentTerms = new List<Payment_Term__c>();
        for(Payment_Term__c term : oppObj.Payment_Terms__r) {
            existingPaymentTerms.add(term);
        }
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);        
       	List<Proforma_Invoice_Line_Item__c> piLineItems = [SELECT Id FROM Proforma_Invoice_Line_Item__c WHERE Id = :proformaInvoices[0].Id]; 
        List<Invoice__c> invoices = MockData.createInvoices(opportunities, 1, products, piLineItems);
        /*invoices[0].Conversion_Rate_To_INR__c = 5.00;
        invoices[0].Status__c = 'Reviewed';
        update invoices;
        /*oppObj = [Select Id, (Select Id, Name, Opportunity__c From Payment_Terms__r) From Opportunity Where Id =: oppObj.Id];
        List<Payment_Term__c> existingPaymentTerms = new List<Payment_Term__c>();
        for(Payment_Term__c term : oppObj.Payment_Terms__r) {
            existingPaymentTerms.add(term);
        }
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);
        
        Invoice__c invoice = new Invoice__c(Docket_Number__c='TestDocket', Account__c = accountCustomerObj.Id, Billing_Address__c = billingAddress.Id, Delivery_Address__c = shippingAddress.Id, Date__c=Date.today(), Opportunity__c=oppObj.Id, Product_Liability_Insurance_Percentage__c = 1.00, Packing_Charges__c = 1.00, Shipping_Charges__c = 1.00, Trade_Discount__c = 0.00, Conversion_Rate_To_INR__c =45.00);
        insert invoice;
        
        Invoiced_Items__c invoiceItem = new Invoiced_Items__c(Invoice__c=invoice.Id, Opportunity__c=oppObj.Id);
        insert invoiceItem;
        
        Invoiced_Product__c invoicedProduct1 = new Invoiced_Product__c(Invoiced_Item__c=invoiceItem.Id, Product__c=proformaInvoiceLineItem1.Product__c, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItem1.Id, Quantity__c=10, Unit_Price__c=250.00);
        List<Invoiced_Product__c> invoiceProducts=new List<Invoiced_Product__c>();
        invoiceProducts.add(invoicedProduct1);
        insert invoiceProducts;
        
        invoice.Status__c = 'Reviewed';
        update invoice;*/
        
    }
    
    static testMethod void testGetPaymentTerm() {
        Test.startTest();
        List<Payment_Term__c> terms = [Select Id From Payment_Term__c];
        Id termId = terms[0].Id;
        List<Payment_Term__c> terms1 = PaymentTransactionController.getPaymentTerm(termId);
        System.assertEquals(terms[0].Id, terms1[0].Id);
        Test.stopTest();
    }
    
    static testMethod void testGetInvoicedItems() {
        Test.startTest();
        Opportunity oppObj = [Select Id From Opportunity];
        Invoice__c invoice = [Select Id From Invoice__c];
        Invoiced_Items__c item = [Select Id From Invoiced_Items__c LIMIT 1];
        system.debug('invoice ' + invoice);
        system.debug('item ' + item);
        List<Invoiced_Items__c> items = PaymentTransactionController.getInvoicedItems(oppObj.Id);
        Test.stopTest();
        //System.assertEquals(oppObj.Id, items[0].Opportunity__c);
    }
   
    static testMethod void testCreateTransactionFailure() {
        List<Payment_Term__c> terms = [Select Id From Payment_Term__c];
        Payment_Transaction__c paymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[0].Id, Amount__c = 10000000000.00);
        String data = JSON.serialize(paymentTransaction);
        try {
            Test.startTest();
            paymentTransaction = PaymentTransactionController.createTransaction(data);
            Test.stopTest();
        } catch(AuraHandledException e) {
            paymentTransaction = null;
        }
        System.assertEquals(null, paymentTransaction);
        
    }
    static testMethod void testCreateTransactionSuccess() {
        List<Payment_Term__c> terms = [Select Id From Payment_Term__c];
        Payment_Transaction__c paymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[0].Id, Amount__c = 1.00);
        String data = JSON.serialize(paymentTransaction);
        Test.startTest();
        paymentTransaction = PaymentTransactionController.createTransaction(data);
        Test.stopTest();
        System.assert(paymentTransaction != null );
        
    }
    
}