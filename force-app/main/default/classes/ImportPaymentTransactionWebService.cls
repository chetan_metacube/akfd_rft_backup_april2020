@RestResource(urlMapping='/api/payment/')
global class ImportPaymentTransactionWebService {
    @HttpPost
    global static String createPaymentTransaction() {
        String response;
        List<String> paymentMethods = new List<String>();
        List<String> currencies = new List<String>();
        List<String> refTypes = new List<String>{'Agst', 'Advance'};
            Schema.DescribeFieldResult paymentMethodFieldResult = Payment_Transaction__c.Payment_Method__c.getDescribe();
        List<Schema.PicklistEntry> paymentMethodPickListEntries = paymentMethodFieldResult.getPicklistValues();
        for(Schema.PicklistEntry entry : paymentMethodPickListEntries) {
            if(entry.isActive() && !'None'.equalsIgnoreCase(entry.getLabel())) {
                paymentMethods.add(entry.getLabel());
            }
        }
        
        Schema.DescribeFieldResult currencyFieldResult = Payment_Transaction__c.CurrencyIsoCode.getDescribe();
        List<Schema.PicklistEntry> currencyPickListEntries = currencyFieldResult.getPicklistValues();
        for(Schema.PicklistEntry entry : currencyPickListEntries) {
            if(entry.isActive()) {
                currencies.add(entry.getValue());
            }
        }
        
        
        RestRequest request = RestContext.request;
        Decimal conversionRateToINR;
        String invoicedCurrency;
        // Deserialize the JSON string into name-value pairs
        PaymentTransactionWrapper wrapper; 
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Payment_Transaction__c.SObjectType,
                    API_Log__c.SObjectType,
                    Attachment.SObjectType
                    }
        );
        try {
            wrapper = (PaymentTransactionWrapper)JSON.deserialize(request.requestBody.toString(), PaymentTransactionWrapper.class);
            if(String.isBlank(wrapper.RefType)){
                throw new StringException('RefType should not be empty!');
            } else if(refTypes.contains(wrapper.RefType)) {
                if(String.isBlank(wrapper.RefNumber)) {
                    throw new StringException('RefNumber is mandatory for transaction!');
                }
            } else {
                throw new StringException('Incorrect RefType! Available RefType: ' + refTypes);
            }
            
            if(String.isBlank(wrapper.PaymentMethod)) {
                throw new StringException('Payment Method should not be empty!');
            } else if(!paymentMethods.contains(wrapper.PaymentMethod)) {
                throw new StringException('Incorrect Payment Method! Available Payment Methods: ' + paymentMethods);
            }
            
            if(wrapper.Amount <= 0.00) {
                throw new StringException('Payment amount should be more than zero!');
            }
            
            invoicedCurrency = wrapper.Narration.substringBefore(' @');
            conversionRateToINR = Decimal.valueOf(wrapper.Narration.substringAfter(' @').substringBefore('##'));
            
            if(conversionRateToINR <= 0.00) {
                throw new StringException('Payment Conversion Rate To INR should be more than zero!');
            }
            
            if(String.isBlank(wrapper.TransactionDate)) {
                throw new StringException('Transaction Date should not be empty!');
            }
            
            Date transactionDate = Date.valueOf(wrapper.TransactionDate);
            
            if(String.isBlank(invoicedCurrency)) {
                throw new StringException('Invoiced Currency should not be empty!');
            } else if(!currencies.contains(invoicedCurrency)) {
                throw new StringException('Incorrect Invoiced Currency \'' + invoicedCurrency + '\'! Available Invoiced Currency: ' + currencies);
            }
            
            ProformaInvoice__c proformaInvoice;
            Invoiced_Items__c invoiceItem;
            Id opportunityId;
            
            Invoice__c invoice;
            if('Advance'.equalsIgnoreCase(wrapper.RefType)) {
                proformaInvoice = [Select Id, Name, Opportunity__c, CurrencyIsoCode From ProformaInvoice__c Where Name = :wrapper.RefNumber];
                
                if(!invoicedCurrency.equalsIgnoreCase(proformaInvoice.CurrencyIsoCode)) {
                    throw new StringException('Transaction Currency is not same as PO currency!');
                }
                
                opportunityId = proformaInvoice.Opportunity__c;
            } else if('Agst'.equalsIgnoreCase(wrapper.RefType)) {
                invoice = [Select Id, Name, Opportunity__c, CurrencyIsoCode, Net_Invoice_Amount__c, Net_Settled_Amount__c, (Select Id, Name, Opportunity__c From Invoiced_Items__r) From Invoice__c Where Name = :wrapper.RefNumber];                
                
                if(!invoicedCurrency.equalsIgnoreCase(invoice.CurrencyIsoCode)) {
                    throw new StringException('Invoiced Currency is not same as PO currency!');
                }
                
                if(wrapper.Amount > invoice.Net_Invoice_Amount__c - invoice.Net_Settled_Amount__c) {
                    throw new StringException('Transaction amount should not be greater than Invoiced balance amount!');
                }
                
                opportunityId = invoice.Opportunity__c;
                
                for(Invoiced_Items__c item : invoice.Invoiced_Items__r) {
                    if(item.Opportunity__c == opportunityId) {
                        invoiceItem = item;
                        break;
                    }
                }
                
            }
            
            List<Payment_Term__c> paymentTerms = [Select Id, Name, Payment_Percentage__c, Payment_Amount__c, Balance_Amount__c From Payment_Term__c Where Opportunity__c = :opportunityId];
            List<Payment_Transaction__c> newTransactions = new List<Payment_Transaction__c>();
            Payment_Transaction__c newPaymentTransaction;
            Decimal balance = wrapper.Amount;
            for(Payment_Term__c paymentTerm : paymentTerms) {
                if(paymentTerm.Balance_Amount__c > 0){
                    if(paymentTerm.Balance_Amount__c >= balance) {
                        newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = paymentTerm.Id, Amount__c = balance);
                        
                    } else {
                        newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = paymentTerm.Id, Amount__c = paymentTerm.Balance_Amount__c);
                    }
                    
                    balance -= paymentTerm.Balance_Amount__c;
                    newPaymentTransaction.Transaction_Date__c = transactionDate;
                    newPaymentTransaction.Invoiced_Item__c = invoiceItem != null ? invoiceItem.Id : null;
                    newPaymentTransaction.Conversion_Rate_To_INR__c = conversionRateToINR;
                    newPaymentTransaction.Payment_Method__c = wrapper.PaymentMethod;
                    newPaymentTransaction.Unique_Transaction_Number__c = wrapper.TransactionNumber;
                    newPaymentTransaction.CurrencyIsoCode = invoicedCurrency;
                    newTransactions.add(newPaymentTransaction);
                    if(balance <= 0) break;
                }
            }
            
            if(balance > 0) {
                throw new StringException('Excess amount is not allowed for the PO.');
            }
            
            if(!newTransactions.isEmpty()) uow.registerNew(newTransactions);
            response = 'Success! ';
            
            EmailManager.sendEmail('UAT ImportPaymentTransactionWebService Success!', response + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
        } catch(TypeException typeException) {
            if(typeException.getMessage().contains('Invalid decimal')) {
                response = 'Convesrion rate to INR is invalid or not in correct format.';
            } else {
                response = 'Invalid date format! (Format: yyyy-MM-dd)';
            }
            EmailManager.sendEmail('UAT ImportPaymentTransactionWebService Error!', 'Error Message: \'' + response + '\'' + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
        } catch(QueryException queryException) {
            if('Advance'.equalsIgnoreCase(wrapper.RefType)) {
                response = 'No Proforma Invoice is associated with specified PI Number.';
            } else {
                response = 'No Invoice is associated with specified Invoice Number.';
            }
            EmailManager.sendEmail('UAT ImportPaymentTransactionWebService Error!', response + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
        } catch(StringException stringException) {
            
            response = stringException.getMessage();
            EmailManager.sendEmail('UAT ImportPaymentTransactionWebService Error!', response + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});            
        } catch (Exception e) {
            
            response = 'JSON Malformed! ' + e.getMessage();
            EmailManager.sendEmail('UAT ImportPaymentTransactionWebService Error!', response + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
        }
        // Iterate through each parameter field and value
        System.debug('wrapper ' + wrapper);
        

        
        API_Log__c apiLog = new API_Log__c();
        apiLog.Name = 'Import Payment Transaction';
        apiLog.Status__c = response.equals('Success!') ? 'Success' : 'Failed';
        apiLog.Type__c = 'PAYMENT';
        apiLog.Message__c = response;
        uow.registerNew(apiLog);
        
        Attachment requestBodyAttachment = new Attachment();
        requestBodyAttachment.Body = Blob.valueOf(JSON.serialize(wrapper));
        requestBodyAttachment.Name = 'Request Body.JSON';
        requestBodyAttachment.IsPrivate = false;
        
        uow.registerNew(requestBodyAttachment, Attachment.ParentId, apiLog);
        
        Attachment responseBodyAttachment = new Attachment();
        responseBodyAttachment.Body = Blob.valueOf(JSON.serialize(response));
        responseBodyAttachment.Name = 'Response Body.JSON';
        responseBodyAttachment.IsPrivate = false;
        uow.registerNew(responseBodyAttachment, Attachment.ParentId, apiLog);
        uow.commitWork();
        
        return response;
    }
    
    private class PaymentTransactionWrapper {
        public String RefType;
        public String RefNumber;
        public Decimal Amount;
        public String PaymentMethod;
        public String TransactionNumber;
        public String TransactionDate;
        public String Narration;
    }
}