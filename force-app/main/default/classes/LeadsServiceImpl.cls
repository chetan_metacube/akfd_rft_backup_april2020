public class LeadsServiceImpl implements ILeadsService {
	private final String INVALID_LEAD_ID_DML_MESSAGE = 'Valid Lead Id is required';
    private final String LEAD_NOT_EXIST_MESSAGE = 'Lead doesn\'t exist.';
    private final String LEAD_CONVERTED_STATUS = 'Qualified';
    
    // Method to get lead records by ids
    public List<Lead> getLeadsById(Set<Id> ids) {
        return LeadsSelector.getInstance().selectLeadsById(ids);
    }
    
    // Method to upsert lead records
    public List<Lead> upsertLeads(List<Lead> leads) {
        Set<Id> contactIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        for(Lead leadRecord : leads) {
            contactIds.add(leadRecord.Contact__c);
            accountIds.add(leadRecord.Account__c);
        }
        
        Map<Id, Contact> leadContacts = new Map<Id, Contact>((List<Contact>)ContactsSelector.getInstance().selectSObjectsById(contactIds));
        Map<Id, Account> leadAccounts = new Map<Id, Account>((List<Account>)AccountsSelector.getInstance().selectSObjectsById(accountIds));
        //Setting the lead record fields
        for(Lead leadRecord : leads) {
            leadRecord.Salutation = leadContacts.get(leadRecord.Contact__c).Salutation;
            leadRecord.FirstName = leadContacts.get(leadRecord.Contact__c).FirstName;
            leadRecord.MiddleName = leadContacts.get(leadRecord.Contact__c).MiddleName;
            leadRecord.LastName = leadContacts.get(leadRecord.Contact__c).LastName;
            leadRecord.Suffix = leadContacts.get(leadRecord.Contact__c).Suffix;
            //contact email set to email of lead
            leadRecord.Email = leadContacts.get(leadRecord.Contact__c).Email;
            //setting company field which is required
            leadRecord.Company = leadAccounts.get(leadRecord.Account__c).Name;
            if(leadRecord.Sales_Process__c == 'Project' && leadAccounts.get(leadRecord.Account__c).CurrencyISOCode != 'INR') {
               throw new AuraHandledException('Please select an Account with currency INR for Project Sales Process.'); 
            }
        }
        
        fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
        UpsertUnitOfWorkLeadsHelper upsertLeadsWork = new UpsertUnitOfWorkLeadsHelper();
        uow.registerWork(upsertLeadsWork);
        upsertLeadsWork.registerLeadUpsert(leads);
        try {
            uow.commitWork();
        } catch(DmlException e) {
            throw New AKFDException(e.getDmlMessage(0));
        }
        return leads;
    }
    
    public List<Opportunity> convertLeadToOpportunity(List<LeadsService.LeadConvertWrapper> leads) {
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        Map<Id, Opportunity> convertedOpportunities = new Map<Id, Opportunity>();
        Map<Id, Lead> leadsMap = new Map<Id, Lead>();
        List<Database.LeadConvert> leadsToBeConverted = new List<Database.LeadConvert>();
        Database.LeadConvert leadConvert;
        
        for(LeadsService.LeadConvertWrapper leadConvertWrapper : leads) {
            leadConvert = new Database.LeadConvert();
            leadConvert.setLeadId(leadConvertWrapper.leadRecord.Id);
            leadConvert.setAccountId(leadConvertWrapper.leadRecord.Account__c);
            leadConvert.setContactId(leadConvertWrapper.leadRecord.Contact__c);
            leadConvert.setOpportunityName(leadConvertWrapper.opportunityName);
            leadConvert.setConvertedStatus(LEAD_CONVERTED_STATUS);
            leadConvert.setOwnerId(leadConvertWrapper.leadRecord.OwnerId);
            leadsToBeConverted.add(leadConvert);
            leadsMap.put(leadConvertWrapper.leadRecord.Id, leadConvertWrapper.leadRecord);
        }
        
        Set<Id> convertedOpportunitiesIds = new Set<Id>();
        List<Database.LeadConvertResult> leadsConvertResult;
        try {
            leadsConvertResult = Database.convertLead(leadsToBeConverted);
            System.debug('leadsConvertResult ' + leadsConvertResult);
        } catch(DmlException e) { 
            System.debug('DML Exception ' + e);
            if(e.getDmlMessage(0).contains(INVALID_LEAD_ID_DML_MESSAGE)) {
                throw New AKFDException(LEAD_NOT_EXIST_MESSAGE);
            } else {
                throw New AKFDException(e.getDmlMessage(0));
            }
        }
        
        for(Database.LeadConvertResult leadConvertResult : leadsConvertResult) {
            convertedOpportunitiesIds.add(leadConvertResult.getOpportunityId());
        }
        System.debug('convertedOpportunitiesIds ' + convertedOpportunitiesIds);
        //Update fields of opportunity which are on Lead Already
        convertedOpportunities = new Map<Id, Opportunity>((List<Opportunity>)OpportunitiesSelector.getInstance().selectSObjectsById(new Set<Id>(convertedOpportunitiesIds)));
        List<Id> accountIds = new List<Id>();
        for(Opportunity opp : convertedOpportunities.values()) {
            accountIds.add(opp.AccountId);
        }
        List<Address__c> accountAddresses = [SELECT Id,Address_Type__c,Account__c FROM Address__c WHERE Account__c IN :accountIds AND Address_Type__c !='Other'];
        Map<ID, List<Address__c>> accountAddressesMap= new Map<ID, List<Address__c>>();
        for(Id accountId : accountIds) {
            List<Address__c> tempAddresses=new List<Address__c>();
            for(Address__c address : accountAddresses)
            {
                if(address.Account__c == accountId){
                    tempAddresses.add(address);
                }
            }
            accountAddressesMap.put(accountId,tempAddresses);
        }
        for(Database.LeadConvertResult leadConvertResult : leadsConvertResult) {
            Opportunity currentOpportunity = convertedOpportunities.get(leadConvertResult.getOpportunityId());
            currentOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(leadsMap.get(leadConvertResult.getLeadId()).Sales_Process__c).getRecordTypeId();
            if( 'Retail'.equals(leadsMap.get(leadConvertResult.getLeadId()).Sales_Process__c) ) {
                currentOpportunity.CurrencyIsoCode = 'INR' ;
            }
            
            
            currentOpportunity.Lead_Source__c = leadsMap.get(leadConvertResult.getLeadId()).Lead_Source__c;
            currentOpportunity.Contact_Person__c = leadsMap.get(leadConvertResult.getLeadId()).Contact__c;
            currentOpportunity.Merchant__c = leadsMap.get(leadConvertResult.getLeadId()).Merchant__c;
            currentOpportunity.CloseDate = Date.today();
            for(Address__c accountAddress : accountAddressesMap.get(currentOpportunity.AccountId)) {
                if(accountAddress.Address_Type__c == 'Billing') {
                    currentOpportunity.Billing_Address__c =accountAddress.Id;
                } else if(accountAddress.Address_Type__c == 'Shipping') {
                    currentOpportunity.Delivery_Address__c = accountAddress.Id;
                }
            }
        }
        
        List<Opportunity> opportunitiesToBeUpdated = new List<Opportunity>(convertedOpportunities.values());
        fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
        uow.registerDirty(opportunitiesToBeUpdated);
        uow.commitWork();
        return opportunitiesToBeUpdated;
    }
    
    /*
     * Unit of work upsert helper class - UpsertUnitOfWorkProjectedStockHelper
	*/
    private class UpsertUnitOfWorkLeadsHelper implements fflib_SObjectUnitOfWork.IDoWork { 
        
        public Database.UpsertResult[] Results {get; private set;}
        
        private List<Lead> m_records;
        
        public UpsertUnitOfWorkLeadsHelper() {  
            m_records = new List<Lead>();
        }
        
        public void registerLeadUpsert(List<Lead> records) {
            m_records.addAll(records);
        }
        
        public void doWork() {
            Results = Database.upsert(m_records, false);                
        }
        
    }
}