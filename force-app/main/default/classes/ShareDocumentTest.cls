@isTest
public class ShareDocumentTest {
    
    @isTest static void testShareDocument() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contacts = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contacts, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        List<Quotation__c> quotations = MockData.createQuotation(opportunities, 1, desiredProducts, priceBooks);
        
        Id attachmentId = MockData.createAttachmentForDocument(opportunities[0], quotations[0].Id);
        
        PageReference pageRef = Page.QuotationPDFExport;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getParameters().put('opportunityId', opportunities[0].Id);
        ApexPages.currentPage().getParameters().put('attachmentId', attachmentId);
        ApexPages.currentPage().getParameters().put('templateName', 'TemplateQuotation');
        ApexPages.currentPage().getParameters().put('documentId', quotations[0].Id);
        ApexPages.currentPage().getParameters().put('invalidQuotationProducts', '');
        ApexPages.currentPage().getParameters().put('salesProcess', 'Export');
        ApexPages.currentPage().getParameters().put('targetObjectId', contacts[0].Id);
        ApexPages.currentPage().getParameters().put('docketNumber', null);
        ShareDocument shareDocument = new ShareDocument();
        System.assert(shareDocument.attachmentId == attachmentId);
        System.assert(shareDocument.documentId == quotations[0].Id);
        System.assert(shareDocument.opportunityId ==  opportunities[0].Id);
        System.assert(shareDocument.invalidQuotationProducts == '');
        System.assert(shareDocument.templateName == 'TemplateQuotation');
        System.assert(shareDocument.salesProcess == 'Export');
        System.assert(shareDocument.targetObjectId == contacts[0].Id);
        System.assert(shareDocument.docketNumber == null);
        System.assert(shareDocument.isDocketNumberSharePage == false);
        
    }

}