@isTest
public class TallyAPICalloutsTest {
	@isTest
    static void upsertProductTestMethod() {
        Product2 product = new Product2(Name='New Chair1',Family='Accessory', Brand__c = 'AKFD',Retail_Price__c=150.00,Material__c='Iron;Felt');
        insert product;
        HSN_Tax__c hsnTax = new HSN_Tax__c(Name = '101', Tax__c =15.00);
        insert hsnTax;
        TallyAPICallouts.ProductInput productInputInstance = new TallyAPICallouts.ProductInput();
        productInputInstance.hsnTax = hsnTax;
        productInputInstance.product = product;
        TallyAPICallouts.upsertProduct(new List<TallyAPICallouts.ProductInput>{productInputInstance});
    }
}