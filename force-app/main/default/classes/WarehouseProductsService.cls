public class WarehouseProductsService {
    
    private static IWarehouseProductsService instance;
    
    private static IWarehouseProductsService service() {
        if(instance == null) {
            instance = (IWarehouseProductsService) Application.service().newInstance(IWarehouseProductsService.class);
        }
        
        return instance;
    }
    
    public static void allotStock(Id proformaInvoiceId, List<ProductData> productsData) {
        
        service().allotStock(proformaInvoiceId, productsData);
        
    }
    
    public static  List<WarehouseProductsService.ProductData> getProductWarehousesByProformaInvoiceId(Id proformaInvoiceId) {
        return service().getProductWarehousesByProformaInvoiceId(proformaInvoiceId);
    }
    
   /*
    * Wrapper Class - ProductWarehouseWrapper
    */
    public class ProductWarehouseWrapper {
        public ProductWrapper productWrapper{get; set;}
        public List<WarehouseWrapper> productWarehouses{get; set;}
        public ProductWarehouseWrapper(ProductWrapper productWrapper, List<WarehouseWrapper> productWarehouses) {
            this.productWrapper = productWrapper;
            this.productWarehouses = productWarehouses;
        }
    }
    
    
     /**
    * Wrapper Class - ProductData
    */
    public class ProductData {
        public Id id{set; get;}
        public String name{get; set;}
        public Decimal requiredQuantity{get; set;}
        public Decimal bookedQuantity{get; set;}
        public Decimal projectedStock{get; set;}
        public boolean isWarehouseAlloted{get; set;}
        public List<WarehouseWrapper> productWarehouses{get; set;}
        
        public ProductData(ProductWrapper productWrapper, List<WarehouseWrapper> productWarehouses) {
            this.name = productWrapper.name;
            this.id = productWrapper.id;
            this.requiredQuantity = productWrapper.requiredQuantity;
            this.bookedQuantity = productWrapper.bookedQuantity;
            this.projectedStock = productWrapper.projectedStock;
            this.isWarehouseAlloted = productWrapper.isWarehouseAlloted;
            this.productWarehouses = productWarehouses;
        }
        
    }
    
    /**
    * Wrapper Class - WarehouseWrapper
    */
    public class WarehouseWrapper {
        public String productName {get;set;}
        public Warehouse_Line_Item__c warehouseLineItem {get; set;}
        public Boolean isSelected {get; set;}
        public Decimal quantity{get; set;}
        public Decimal allotments{get; set;}
        public Boolean isZeroBooked{get;set;}
        public Id allotStockId{get;set;}
        public Decimal currentAllotment {get; set;}
        
        //public Id productWarehouseId {get; set;}
        public String warehouseLocation {get;set;}
        public Decimal currentStock {get;set;}
        public String warehouseName {get;set;}
        public String warehouseId {get;set;}
        public List<BookedStockWrapper> allotedStocks {get;set;}
        
        //public String compoundLocation {get; set;}
        public WarehouseWrapper(Warehouse_Line_Item__c warehouseLineItem) {
            this.productName = warehouseLineItem.Product__r.Name;
            this.warehouseLineItem = warehouseLineItem;
            this.isSelected = false;
            this.quantity = 0.00;
            this.warehouseLocation = warehouseLineItem.Warehouse__r.Compound_Location__c;
            this.currentStock = warehouseLineItem.Current_Stock__c;
            this.warehouseName = warehouseLineItem.Warehouse__r.Name;
            this.allotedStocks = new List<BookedStockWrapper>();
            for(Alloted_Stock__c allotedStock : warehouseLineItem.Alloted_Stocks__r) {
               
                    allotedStocks.add(new BookedStockWrapper(allotedStock));
               
            }
          //  this.compoundLocation = warehouseLineItem.Warehouse__r.compound_Location__c;
        }
    }
    
    /*
    * Wrapper Class - ProductWrapper
    */
    public class ProductWrapper {
        public Id id{set; get;}
        public String name{get; set;}
        public Decimal requiredQuantity{get; set;}
        public Decimal bookedQuantity{get; set;}
        public Decimal projectedStock{get; set;}
        public boolean isWarehouseAlloted{get; set;}
        
        public ProductWrapper(Id id, String name, Decimal requiredQuantity, Decimal projectedStock, Decimal bookedQuantity, Boolean isWarehouseAlloted) {
            this.id = id; 
            this.name = name;
            this.requiredQuantity = requiredQuantity;
            this.projectedStock = projectedStock;
            this.isWarehouseAlloted = false;
            this.bookedQuantity = bookedQuantity;
            
        }
    }
    
    public class BookedStockWrapper {
        public Id id{set; get;}
        public String name{get; set;}
        public Id productId{set; get;}
        public String productName{get; set;}
        public String productUrl{get; set;}
        public Id warehouseId{set; get;}
        public String warehouseName{get; set;}
        public String warehouseUrl{get; set;}
        public Id proformaInvoiceId{set; get;}
        public String proformaInvoiceName{get; set;}
        public String proformaInvoiceUrl{get; set;}
        public Decimal stock{get; set;}
        public Alloted_Stock__c allotedStock{get; set;}
        
        public BookedStockWrapper(Alloted_Stock__c allotedStock) {
            this.name = allotedStock.name;
            this.id = allotedStock.id;
            this.productId = allotedStock.Product__c;
            this.productName = allotedStock.Product__r.Name;
            this.productUrl = '/' + allotedStock.Product__c;
            this.warehouseId = allotedStock.Warehouse__c;
            this.warehouseName = allotedStock.Warehouse__r.Name;
            this.warehouseUrl = '/' + allotedStock.Warehouse__c;
            this.proformaInvoiceId = allotedStock.ProformaInvoice__c;
            this.proformaInvoiceName = allotedStock.ProformaInvoice__r.Name;
            this.proformaInvoiceUrl = '/' + allotedStock.ProformaInvoice__c;
            this.stock = allotedStock.Stock__c;
            this.allotedStock = allotedStock;
        }
        
    }
   
    
}