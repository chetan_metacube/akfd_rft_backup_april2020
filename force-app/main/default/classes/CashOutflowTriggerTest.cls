@isTest
public class CashOutflowTriggerTest {
    static testMethod void testBeforeInsert() {
        Cash_Outflow__c outflowData = new Cash_Outflow__c(Name = 'Test Expense', Amount__c = 10.00);
        insert outflowData;
        outflowData = [Select Id, CurrencyIsoCode From Cash_Outflow__c];
        System.assertEquals('INR', outflowData.CurrencyIsoCode);
    }
}