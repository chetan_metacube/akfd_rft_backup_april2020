public class AccountFormControllerExtension {
    public Id accountId {get; set;}
    public Account accountRecord {get; set;}
    public boolean errorExists {get; set;}
    private List<Address__c> accountAddresses {get;set;}
    public List<Contact> accountContacts {get;set;}
    public Address__c accountBillingAddress{get;set;}
    public Address__c accountDeliveryAddress{get;set;}
    public Address__c accountOtherAddress{get;set;}
    public List<Bank_Info__c> accountBankInfo {get;set;}
    
    public AccountFormControllerExtension(ApexPages.StandardController controller) {
        accountId = ApexPages.currentPage().getParameters().get('id');
        
        if(String.isNotBlank(accountId) && 'Account'.equals(accountId.getSObjectType().getDescribe().getName())) {
            accountRecord = ((List<Account>)AccountsSelector.getInstance().selectSObjectsById(new Set<Id>{accountId}))[0];
            accountAddresses = [SELECT Id, Name, Street__c, City__c, State_Province__c, Countries__c, Zip_Postal_Code__c, Address_Type__c,
                                GST_Id__c FROM Address__c WHERE Account__c = :accountId];
            accountBankInfo = [SELECT Id, Name, Bank_Details__c FROM Bank_Info__c WHERE Account__c = :accountId];
            accountContacts = [SELECT Id, Name, MobilePhone, Email, Role__c FROM Contact WHERE AccountId = :accountId];
            System.debug('++++++++++++++');
            if(accountContacts.size() < 1) {
                accountContacts = new List<Contact>();
                Contact c = new Contact();
                accountContacts.add(c);
            }
            System.debug('++++++++++++++'+accountContacts[0].Name);
            for(Address__c accountAddress : accountAddresses) {
                if(accountAddress.Address_Type__c.equals('Billing') ) {
                    accountBillingAddress = accountAddress;
                } else if(accountAddress.Address_Type__c.equals('Delivery')){
                    accountDeliveryAddress = accountAddress;
                } else if(accountOtherAddress != null) {
                    accountOtherAddress = accountAddress;
                }
            }
            System.debug('Hi I am clicked constructor' + accountId);
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid access.'));
            errorExists = true;
        }
    }
    
    public void save() {
        try {
            update accountRecord;
            List<Address__c> updatedAddresses = new List<Address__c>{accountBillingAddress, accountDeliveryAddress, accountOtherAddress};
            update updatedAddresses;
            update accountBankInfo;
            if(accountContacts[0].Id != null) {
            	update accountContacts;
            } else {
                insert accountContacts;
            }
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            errorExists = true;
        }
        
        System.debug('Hi I am clicked');
    }
}