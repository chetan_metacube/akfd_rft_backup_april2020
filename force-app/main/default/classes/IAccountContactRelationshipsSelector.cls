public interface IAccountContactRelationshipsSelector extends fflib_ISObjectSelector {
	List<AccountContactRelation> selectByAccountId(Set<Id> accountIds);
}