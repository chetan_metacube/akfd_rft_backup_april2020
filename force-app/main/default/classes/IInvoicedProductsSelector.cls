public interface IInvoicedProductsSelector extends fflib_ISObjectSelector {
    List<Invoiced_Product__c> selectInvoiceProductById();
}