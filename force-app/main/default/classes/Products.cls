public class Products extends fflib_SObjectDomain {
	public Products(List<Bank_Info__c> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Products(sObjectList);
        }
    }
    public override void onApplyDefaults() {}
    
    public override void onValidate() {}
    
    public override void onValidate(Map<Id,SObject> existingRecords) {}
    
    public override void onBeforeInsert() {
    	Set<String> productNameSet = new Set<String>(); 
        List<Product2> lstProducts = getExistingProducts((List<Product2>) Records); 
        
        for (Product2 reftype : lstProducts) { 
            productNameSet.add(reftype.Name);
        } 
        
        for (Product2 product : (List<Product2>) Records) {
            
            if (productNameSet.contains(product.Name)) {
                product.addError('Product Name should be unique.');
            }
        }
    }
    
    public override void onAfterInsert() {
        String subject = ' New Product created in Salesforce';
        String message = 'Dear Amplified Powerhouses,<br /><br/>Congratulations,<br /><br />Here are the New Products created in Salesforce :<br />';
        List<String> productNames = new List<String>();
        for (Product2 ref : (List<Product2>) Records) { 
            productNames.add(ref.Name);
        }
        //Integer counter = 0;
        message += '<ol>';
        for(String productName : productNames) {
            //counter += 1;
            message += '<li> '+ productName + '</li>';
        }
        message += '</ol>Pitch them and Stock them via all relevant Sales Channels: Retailers, Online Shops, Shop In Shop counters,<br />' + 
					'Export Clients, Architects, Hospitality clients,'+'Gifting clients, HNI clients, Others. <br /><br />' + 
            		'Have orders coming in within the month.<br/>AWESOMENESS deserves to reach the GLOBE.' + 
            		'<br />Go For It!';
        List<User> users = [SELECT Id, Email FROM User WHERE CompanyName = 'AKFD'];
        List<String> toAddresses = new List<String>();
        for(User currentUser : users) {
            toAddresses.add(currentUser.Email);
        	//toAddresses.add('yash.surana@metacube.com');
        }
        //calling emailManager funtion to send email to all on product creation,
        emailManager.sendEmail(subject, message, toAddresses);
    }
    
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        Set<String> productNameSet = new Set<String>(); 
        List<Product2> lstProducts = getExistingProducts((List<Product2>) Records); 
        Map<Id,Product2> newProductMap = new Map<Id,Product2>();
        for(Product2 prod : (List<Product2>) Records) {
            newProductMap.put(prod.Id, prod);
        }
        for (Product2 product : lstProducts) { 
            productNameSet.add(product.Name);
        } 
        
        for (Product2 pro : newProductMap.values()) {
            if(((Product2)(existingRecords.get(pro.Id))).HSN_Tax__c != newProductMap.get(pro.Id).HSN_Tax__c) {
                newProductMap.get(pro.Id).HSN_Applicable_Date__c = System.today();
            } else if(newProductMap.get(pro.Id).HSN_Tax__c != null && newProductMap.get(pro.Id).HSN_Applicable_Date__c == null) {
                newProductMap.get(pro.Id).HSN_Applicable_Date__c = System.today();
            }
            if(((Product2)existingRecords.get(pro.Id)).Root_Price__c != newProductMap.get(pro.Id).Root_Price__c) {
                newProductMap.get(pro.Id).Root_Price_Updated_Date__c = System.today();
            }
            
            if(((Product2)existingRecords.get(pro.Id)).Retail_Price__c != newProductMap.get(pro.Id).Retail_Price__c) {
                newProductMap.get(pro.Id).Retail_Price_Updated_Date__c = System.today();
            }
            
            if( productNameSet.contains(pro.Name) && ((Product2)existingRecords.get(pro.Id)).Name != newProductMap.get(pro.Id).Name ) {
                    pro.addError('Product Name should be Unique along with AKFD SKU.');
            }
        }
    }
    
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {
        Set<String> productMOQDeails = new Set<String>();
        boolean isTotalStockUpdated;
        boolean isTotalStockIsLessThanMinimumStockQuantity;
        String products = '';
        Map<Id,Product2> newProductsMap = new Map<Id,Product2>();
        for(Product2 prod : (List<Product2>) Records) {
            newProductsMap.put(prod.Id, prod);
        }
        for (Product2 pro : newProductsMap.values()) {
            isTotalStockUpdated = true;
            isTotalStockUpdated = ((Product2)existingRecords.get(pro.Id)).Total_Stock__c != newProductsMap.get(pro.Id).Total_Stock__c;
            isTotalStockIsLessThanMinimumStockQuantity = newProductsMap.get(pro.Id).Total_Stock__c <= newProductsMap.get(pro.Id).Minimum_Stock_Quantity__c;
            if (isTotalStockIsLessThanMinimumStockQuantity && isTotalStockUpdated) {
                productMOQDeails.add(newProductsMap.get(pro.Id).Name);
                products+=newProductsMap.get(pro.Id).Name+', the stock of which should be ' + newProductsMap.get(pro.Id).Minimum_Stock_Quantity__c + ', is currently '+ newProductsMap.get(pro.Id).Total_Stock__c+'.<br />';
            }
        }
        if (productMOQDeails.size() > 0) {
            
            String subject = 'MSQ exceeds for Master Product';
            String message = 'Dear User<br/><br/>Minimum Stock Quantity values for below Product has been exceeded. Please take the appropriate action. <br/>';
            message += '<br/>' + products;
            AKFD_Configuration__c appConfig = AKFD_Configuration__c.getOrgDefaults(); 
            List<String> toAddresses = new List<String>();
            
            //getting admin email id for sending email
            if ((appConfig.AKF_Process_Admin_Email__c) !=null) {
                toAddresses.add(appConfig.AKF_Process_Admin_Email__c);
            }
            //getting co admin email id for sending email
            if ((appConfig.AKFD_Co_Admin_Email__c) !=null) {
                toAddresses.add(appConfig.AKFD_Co_Admin_Email__c);
            }
            //getting inventory manager email id for sending email
            if ((appConfig.AKFD_Inventory_Manager_Email__c) !=null) {
                toAddresses.add(appConfig.AKFD_Inventory_Manager_Email__c);
            }
            //getting procurement manager email id for sending email
            if ((appConfig.AKFD_Procurement_Manager_Email__c) !=null) {
                toAddresses.add(appConfig.AKFD_Procurement_Manager_Email__c);
            }
            System.debug('UserInfo.getUserEmail()'+UserInfo.getUserEmail());
            toAddresses.add(UserInfo.getUserEmail()); 
            //calling emailManager funtion to send email to admin for MSQ,
            //emailManager.sendEmail(subject,message,toAddresses);
        }
    }
	
    /*
     * Method - getExistingProduct
     * Params - List of product
     * Return - List of existing products
     * Description - Get List of Product if the same name as the new product name.
	*/
    private static List<Product2> getExistingProducts(List<Product2> newProducts) {
        Set<String> productNameSet = new Set<String>();
        
        for (Product2 ref : newProducts) { 
            productNameSet.add(ref.Name);
        } 
        List<Product2> productList= ProductsSelector.getInstance().selectProductsByName(productNameSet);
        return productList;
    }
}