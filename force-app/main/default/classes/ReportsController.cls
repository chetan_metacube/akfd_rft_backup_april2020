public class ReportsController {
	@AuraEnabled
      public static List <Report> getReports() {
        return [SELECT Id, FolderName, Name, Description,developerName FROM Report where FolderName='AKFD Reports'];
      }
}