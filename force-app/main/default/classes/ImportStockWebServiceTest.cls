@isTest
public class ImportStockWebServiceTest {
    @isTest static void updateProductWarehouseTestMethodWithoutWarehouse() {
        Product2 product = new Product2(Name='Test Product',Family='Furniture',Brand__c = 'AKFD',Image__c='<img src="" />', Retail_Price__c = 0.00);
        insert product;
        product = [SELECT Id, Name, SKU_Number__c FROM Product2 LIMIT 1];
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        ApexTriggerCheck.getInstance().isExecutingFromAPIRequest = true;
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        ImportStockWebService.WarehouseProductWrapper warehouseProductWrapperInstance = new ImportStockWebService.WarehouseProductWrapper();
        warehouseProductWrapperInstance.ProductName = product.Name;
        warehouseProductWrapperInstance.ProductSKU = product.SKU_Number__c;
        warehouseProductWrapperInstance.Stock = 30;
        warehouseProductWrapperInstance.Warehouse = '';
        String myJSON = JSON.serialize(warehouseProductWrapperInstance);
        RestRequest request = new RestRequest();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/api/productStock/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		String response = ImportStockWebService.updateProductWarehouse();
    }
    @isTest static void updateProductWarehouseTestMethodWithNegativeStock() {
        Product2 product = new Product2(Name='Test Product',Family='Furniture',Brand__c = 'AKFD',Image__c='<img src="" />', Retail_Price__c = 0.00);
        insert product;
        product = [SELECT Id, Name, SKU_Number__c FROM Product2 LIMIT 1];
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        ApexTriggerCheck.getInstance().isExecutingFromAPIRequest = true;
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        ImportStockWebService.WarehouseProductWrapper warehouseProductWrapperInstance = new ImportStockWebService.WarehouseProductWrapper();
        warehouseProductWrapperInstance.ProductName = product.Name;
        warehouseProductWrapperInstance.ProductSKU = product.SKU_Number__c;
        warehouseProductWrapperInstance.Stock = -7;
        warehouseProductWrapperInstance.Warehouse = warehouse.Name;
        String myJSON = JSON.serialize(warehouseProductWrapperInstance);
        RestRequest request = new RestRequest();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/api/productStock/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		String response = ImportStockWebService.updateProductWarehouse();
    }
    @isTest static void updateProductWarehouseTestMethodWithoutProductSKU() {
        Product2 product = new Product2(Name='Test Product',Family='Furniture',Brand__c = 'AKFD',Image__c='<img src="" />', Retail_Price__c = 0.00);
        insert product;
        product = [SELECT Id, Name, SKU_Number__c FROM Product2 LIMIT 1];
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        ApexTriggerCheck.getInstance().isExecutingFromAPIRequest = true;
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        ImportStockWebService.WarehouseProductWrapper warehouseProductWrapperInstance = new ImportStockWebService.WarehouseProductWrapper();
        warehouseProductWrapperInstance.ProductName = product.Name;
        warehouseProductWrapperInstance.ProductSKU = '';
        warehouseProductWrapperInstance.Stock = 30;
        warehouseProductWrapperInstance.Warehouse = warehouse.Name;
        String myJSON = JSON.serialize(warehouseProductWrapperInstance);
        RestRequest request = new RestRequest();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/api/productStock/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		String response = ImportStockWebService.updateProductWarehouse();
    }
    
    @isTest static void updateProductWarehouseTestMethodWithoutProductName() {
        Product2 product = new Product2(Name='Test Product',Family='Furniture',Brand__c = 'AKFD',Image__c='<img src="" />', Retail_Price__c = 0.00);
        insert product;
        product = [SELECT Id, Name, SKU_Number__c FROM Product2 LIMIT 1];
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        ApexTriggerCheck.getInstance().isExecutingFromAPIRequest = true;
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        ImportStockWebService.WarehouseProductWrapper warehouseProductWrapperInstance = new ImportStockWebService.WarehouseProductWrapper();
        warehouseProductWrapperInstance.ProductName = '';
        warehouseProductWrapperInstance.ProductSKU = product.SKU_Number__c;
        warehouseProductWrapperInstance.Stock = 30;
        warehouseProductWrapperInstance.Warehouse = warehouse.Name;
        String myJSON = JSON.serialize(warehouseProductWrapperInstance);
        RestRequest request = new RestRequest();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/api/productStock/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		String response = ImportStockWebService.updateProductWarehouse();
    }
    
    @isTest static void updateProductWarehouseTestMethod() {
        Product2 product = new Product2(Name='Test Product',Family='Furniture',Brand__c = 'AKFD',Image__c='<img src="" />', Retail_Price__c = 0.00);
        insert product;
        product = [SELECT Id, Name, SKU_Number__c FROM Product2 LIMIT 1];
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        ApexTriggerCheck.getInstance().isExecutingFromAPIRequest = true;
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        ImportStockWebService.WarehouseProductWrapper warehouseProductWrapperInstance = new ImportStockWebService.WarehouseProductWrapper();
        warehouseProductWrapperInstance.ProductName = product.Name;
        warehouseProductWrapperInstance.ProductSKU = product.SKU_Number__c;
        warehouseProductWrapperInstance.Stock = 30;
        warehouseProductWrapperInstance.Warehouse = warehouse.Name;
        String myJSON = JSON.serialize(warehouseProductWrapperInstance);
        RestRequest request = new RestRequest();
        request.requestUri = System.URL.getSalesforceBaseURL().toExternalForm() + '/services/apexrest/api/productStock/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);
		
        RestContext.request = request;
		String response = ImportStockWebService.updateProductWarehouse();
    }
}