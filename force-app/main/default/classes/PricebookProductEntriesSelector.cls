public class PricebookProductEntriesSelector extends fflib_SObjectSelector implements IPricebookProductEntriesSelector {
	private static IPricebookProductEntriesSelector instance = null;
    
    public static IPricebookProductEntriesSelector getInstance() {
        
        if(instance == null) {
            instance = (IPricebookProductEntriesSelector)Application.selector().newInstance(Pricebook_Product_Entry__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            PriceBook_Product_Entry__c.Id,
            PriceBook_Product_Entry__c.Name,
            PriceBook_Product_Entry__c.PriceBook_Product__c,
            PriceBook_Product_Entry__c.Min_Quantity__c,
            PriceBook_Product_Entry__c.Max_Quantity__c,
            PriceBook_Product_Entry__c.Unit_of_Price__c,
            PriceBook_Product_Entry__c.Price__c
            
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return PriceBook_Product_Entry__c.sObjectType;
    }
    
    public List<PriceBook_Product_Entry__c> selectPricebookEntries() {
        fflib_QueryFactory query = newQueryFactory();
        //query.selectField('Proforma_Invoice_Line_Item__r.ProformaInvoice__c');
        query.setLimit( Limits.getLimitQueryRows() );
        
        return (List<PriceBook_Product_Entry__c>) Database.query(
            query.toSOQL()
        );
    }
}