public class ExportToTallyUtils {
    public static List<String> invoiceColumnsKey = new List<String>{
        	'Invoice__r.RecordType.Name', 'Invoice__r.Name', 'Invoice__r.Date__c', 'Invoice__r.CurrencyIsoCode',
            'Invoice__r.Account__r.Name', 'Product__r.Name', 'Quantity__c', 'Unit_Price__c', 'Discount Percent', 'Discount Amount', 
            'Total Discount', 'Sales Amount', 'Total Invoice Amount', 'Conversion Rate', 'Invoice_Amount_In_INR__c', 'Invoice__c', 'ShipmentGodown', 'SalesLedger', 'IGST 12.00',
            'IGST 18.00', 'IGST 3.00', 'IGST 5.00', 'LOCAL Packing_Percentage__c', 'Export Packing_Charges__c',
            'Intrastate Packing_Percentage__c', 'Export Shipping_Charges__c', 'LOCAL Freight_Amount__c',
            'Intrastate Freight_Amount__c', 'CGST 1.50', 'SGST 1.50', 'CGST 2.50', 'SGST 2.50',
            'CGST 6.00', 'SGST 6.00', 'CGST 9.00', 'SGST 9.00', 'CGST 14.00',
            'SGST 14.00', 'Invoice__r.Billing_Address__r.GST_Id__c', 'Billing_Address__r', 'Delivery_Address__r'    
            };
                
    public static Map<String, String> invoiceColumnsMapping = new Map<String, String>{
        	'Invoice__r.RecordType.Name' => 'Voucher Type',
            'Invoice__r.Name' => 'Invoice No',
            'Invoice__r.Date__c' => 'Invoice Date',
            'Invoice__r.Account__r.Name' => 'Party Name',
            'Product__r.Name' => 'Name of Item',
            'Quantity__c' => 'Qty',
            'Invoice__r.CurrencyIsoCode' => 'Invoice Currency',
            'Unit_Price__c' => 'Rate',
            'Discount Percent' => 'Discount % Per Quantity', 
            'Discount Amount' => 'Discount Amount/Product', 
            'Total Discount' => 'Total Invoice Discount (Export)',
            'Sales Amount' => 'Sales Amount',
            'Total Invoice Amount' => 'Total Invoice Amount',
            'Conversion Rate' => 'Conversion Rate',
            'Invoice_Amount_In_INR__c' => 'Invoice Amount In INR',
            'Invoice__c' => 'Invoice Salesforce Id',
            'ShipmentGodown' => 'Shipment Godown',
            'SalesLedger' => 'Sales Ledger',
            'IGST 12.00' => 'Output Inter State Gst@12%',
            'IGST 18.00' => 'Output Inter State Gst@18%',
            'IGST 3.00' => 'Output Inter State Gst@3%',
            'IGST 5.00' => 'Output Inter State Gst@5%',
            'LOCAL Packing_Percentage__c' => 'PACKING CHARGE (GST 18%) REC',
            'Export Packing_Charges__c' => 'PACKING CHARGE RECD. EXPORT',
            'Intrastate Packing_Percentage__c' => 'Packing Charges (IGST 18%) REC',
            'Export Shipping_Charges__c' => 'SHIPMENT & FORWARDING REC EXPORT',
            'LOCAL Freight_Amount__c' => 'Freight Charges (GST @ 18%) REC',
            'Intrastate Freight_Amount__c' => 'Freight Charges (IGST @ 18%) Rec',
            'CGST 1.50' => 'OUTPUT CGST @ 1.5%',
            'SGST 1.50' => 'OUTPUT SGST @ 1.5%',
            'CGST 2.50' => 'OUTPUT CGST @ 2.5%',
            'SGST 2.50' => 'OUTPUT SGST @ 2.5%',
            'CGST 6.00' => 'OUTPUT CGST @ 6%',
            'SGST 6.00' => 'OUTPUT SGST @ 6%',
            'CGST 9.00' => 'OUTPUT CGST @ 9%',
            'SGST 9.00' => 'OUTPUT SGST @ 9%',
            'CGST 14.00' => 'OUTPUT CGST @ 14%',
            'SGST 14.00' => 'OUTPUT SGST @ 14%',
            'Invoice__r.Billing_Address__r.GST_Id__c' => 'GSTIN',
            'Billing_Address__r' => 'Billing Address',
            'Delivery_Address__r' => 'Delivery Address'
            };      
}