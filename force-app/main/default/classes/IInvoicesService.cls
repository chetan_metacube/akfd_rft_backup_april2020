public interface IInvoicesService {
    List<InvoicesService.ProformaInvoiceWrapper> getProformaInvoices(List<Id> opportunityIds);
    List<Id> generate(List<InvoicesService.ProformaInvoicesIdsWrapper> mergeInvoicesProformaInvoices, List<ProformaInvoice__c> allProformaInvoices);
}