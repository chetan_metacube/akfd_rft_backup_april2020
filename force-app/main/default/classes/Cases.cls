public class Cases extends fflib_SObjectDomain {
    
    public Cases(List<Case> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Cases(sObjectList);
        }
    }
    
    public override void onApplyDefaults() {}
    
    public override void onBeforeInsert() {}
   
    
}