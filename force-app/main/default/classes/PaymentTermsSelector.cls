public class PaymentTermsSelector extends fflib_SObjectSelector implements IPaymentTermsSelector {
    private static IPaymentTermsSelector instance = null;
    
    public static IPaymentTermsSelector getInstance() {
        if(instance == null) {
            instance = (IPaymentTermsSelector)Application.selector().newInstance(Payment_Term__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            	Payment_Term__c.Id,
                Payment_Term__c.Name,
                Payment_Term__c.Payment_Percentage__c, 
                Payment_Term__c.Opportunity__c, 
                Payment_Term__c.Payment_Amount__c,
                Payment_Term__c.CurrencyIsoCode,
                Payment_Term__c.Payment_Status_New__c,
                Payment_Term__c.Total_Transaction_Amount__c,
                Payment_Term__c.Opportunity__c
                };
    }
    
    public Schema.SObjectType getSObjectType() {
        return Payment_Term__c.sObjectType;
    }
    
    public List<Payment_Term__c> selectInstallmentPaymentTermsByOpportunityIds(Set<Id> opportunityIds) {
        Id installmentRecordTypeId = Schema.SObjectType.Payment_Term__c.RecordTypeInfosByName.get('Installment Payment Terms').RecordTypeId;
        fflib_QueryFactory query = newQueryFactory();
        query.setCondition('Opportunity__c IN :opportunityIds AND RecordTypeId = :installmentRecordTypeId');
        query.addOrdering('Name', fflib_QueryFactory.SortOrder.ASCENDING);
        return (List<Payment_Term__c>) Database.query( query.toSOQL() );
    }
 
}