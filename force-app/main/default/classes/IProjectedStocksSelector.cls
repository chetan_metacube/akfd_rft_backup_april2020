public interface IProjectedStocksSelector extends fflib_ISObjectSelector {
    
    List<Projected_Stock__c> selectByProformaInvoiceId(Id proformaInvoiceId);
    
}