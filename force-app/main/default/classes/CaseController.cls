/**
 * Used in CaseConvertlightningComponent
 * Class responsible for case related functionality
 * author: Chetan Sharma (1 Feb 2019)
 * Last Modified By: Chetan Sharma (15 Feb 2019)
 */

public class CaseController { 
    /** 
    * Method return caseRecord 
    * @param caseId. 
    * @return Case Record, for valid case id. 
    */
    @AuraEnabled
    public static Case getCase(Id caseId) {
        // Validating caseId
        if( caseId == null || caseId.getSobjectType() != Schema.Case.getSObjectType() ) { 
            throw new AuraHandledException('Invalid Case Id entered.');
        } else {
            // Validating Record Level Access of User
            System.debug('before record access');
            List<UserRecordAccess> userRecordsAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :caseId];
            System.debug(userRecordsAccess[0]);
            if(!userRecordsAccess[0].HasEditAccess) { 
                throw new AuraHandledException('Insufficient Privileges.');
            } else {
                // Queried case fields with related details using controller caseObject id
                List<Case> cases = CasesService.getCasesById(new Set<Id>{caseId});
                return cases.get(0);
            }
        }
    }
    
    /** 
    * Method responsible for converting case to lead 
    * @param caseObj and leadFields Unused. 
    * @return Lead Object. 
    */
    @AuraEnabled
    public static Lead convert(Case caseObj, Map<String, String> leadFields) {
        if(caseObj != null && caseObj.Id.getSobjectType() == Schema.Case.getSObjectType()) {
            List<Lead> convertedLeads = new List<Lead>();
            // Validating case fields and if error exists, thrown to lightning component   
            String conversionValidationString = validateCaseFields(caseObj);
            if(String.isNotEmpty(conversionValidationString)){
                throw new AuraHandledException(conversionValidationString);
            }
            // Validating lead fields and if error exists, thrown to lightning component  
            String fieldValidationString = validateLeadFields(caseObj, leadFields);
            if(String.isNotEmpty(fieldValidationString)){
                throw new AuraHandledException(fieldValidationString);
            }
            
            try{ 
                // Set CaseConvertWrapper fields and convert leads
                List<CasesService.CaseConvertWrapper> cases = new List<CasesService.CaseConvertWrapper>();
                cases.add(new CasesService.CaseConvertWrapper(caseObj, leadFields.get('Title'), leadFields.get('LeadSource')));
                convertedLeads = CasesService.convertToLead(cases);
                return convertedLeads.get(0);
            } catch(DmlException dml){
                // Catch DML Exception and throw AuraHandledException to lightning component with same message
                throw new AuraHandledException(dml.getDMLMessage(0));
            }
        } else {
            throw new AuraHandledException('Invalid case entry.');
        }
    }
    
    /** 
    * Private method responsible for validating case Fields 
    * @param caseObj Unused. 
    * @return ErrorString. 
    */
    private static String validateCaseFields(Case caseObj) {
        String errorMessage = '';
        // Add Error when fields are null
        if( caseObj.Status == 'Converted'){
            errorMessage = 'Already converted case cannot be converted to Lead.\n';
        } else if(caseObj.Status == 'Closed'){
            errorMessage = 'Closed case cannot be converted to Lead.\n'
                + 'You can reopen the case if you want to convert the case.';
        } else if(caseObj.Sales_Process__c == 'Support' || caseObj.Sales_Process__c == 'Press' || caseObj.Sales_Process__c == 'Design'){
            errorMessage = caseObj.Sales_Process__c + ' Sales Process case cannot be converted to Lead.';
        } else if('Project'.equals(caseObj.Sales_Process__c) && caseObj.Account.CurrencyIsoCode != 'INR') {
            errorMessage = 'Please select an Account with currency INR for Project Sales.';
        }
        return errorMessage;
    } 
    
    /** 
    * Private method responsible for validating Lead related Fields 
    * @param caseObj and related lead fields map Unused. 
    * @return ErrorString. 
    */
    private static String validateLeadFields(Case caseObj, Map<String, String> leadFields) {
        String title = leadFields.get('Title');
        String leadSource = leadFields.get('LeadSource');
        String status = leadFields.get('Status');
        String errorMessage = '';
        // Add Error when fields are null
        if(String.isBlank(title)) { 
            errorMessage += 'Lead Title: You must enter a value.\n';
        }
        
        if(String.isBlank(caseObj.Sales_Process__c)) {
            errorMessage += 'Case Sales process is not selected.\n';
        }
        
        if(String.isBlank(leadSource)) {
            errorMessage += 'Lead Source: You must enter a value.\n';
        }
        
        if(String.isBlank(caseObj.AccountId) || caseObj.AccountId.getSobjectType() != Schema.Account.getSObjectType()) {
            errorMessage += 'Account: Please select value on Case.\n';
            errorMessage += 'Contact: Please select value on Case.\n';
        } else if(String.isBlank(caseObj.ContactId) || caseObj.ContactId.getSobjectType() != Schema.Contact.getSObjectType()) { 
            errorMessage += 'Contact: Please select value on Case.\n';
        }
        
        return errorMessage;
    }

}