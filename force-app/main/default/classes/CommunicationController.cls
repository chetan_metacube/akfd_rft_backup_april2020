public class CommunicationController {
    //Method to insert communication record
	@AuraEnabled
    public static Id saveCommunication(String communication) {
        if(!String.isBlank(communication)) {
            Communication__c currentCommunication = (Communication__c)JSON.deserialize(communication, Communication__c.class);
            //Check if communication name is blank or contact is blank
            if(String.isBlank(currentCommunication.Name)) {
                throw new AuraHandledException('Enter Communication Name.');
            } else if(String.isBlank(currentCommunication.Contact__c)) {
                throw new AuraHandledException('Select a Contact.');
            } else {
                try {
                    insert currentCommunication;
                } catch(Exception e) {
                    throw new AuraHandledException(e.getMessage());
                }
            }
            return currentCommunication.Id;
        } else {
            throw new AuraHandledException('Empty communication string passed.');
        }
    }
    //Method to fetch the communication object picklist values
    @AuraEnabled
    public static String getCommunicationFieldValues(Id opportunityId){
        if(opportunityId != null) {
            List<String> typeValuesList = new List<String>();
            List<String> stageValuesList = new List<String>();
            Schema.DescribeFieldResult fieldTypeResult = Communication__c.Type__c.getDescribe();
            List<Schema.PicklistEntry> typePickList = fieldTypeResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : typePickList){
                typeValuesList.add(pickListVal.getLabel());
            }
            Schema.DescribeFieldResult fieldStageResult = Communication__c.Opportunity_Stages__c.getDescribe();
            List<Schema.PicklistEntry> stagePickList = fieldStageResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : stagePickList){
                stageValuesList.add(pickListVal.getLabel());
            }
            Opportunity opportunity = [SELECT Name, AccountId, Account.Name FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
            communicationPicklistValuesWrapper communicationPicklistvaluesWrapper= new communicationPicklistValuesWrapper(typeValuesList, stageValuesList, opportunity);
            return JSON.serialize(communicationPicklistvaluesWrapper);
        } else {
            throw new AuraHandledException('Opportunity Id is null.');
        }
        
    }
    //Wrapper for communication picklist values
    private class communicationPicklistValuesWrapper {
        List<String> communicationType;
        List<String> opportunityStage;
        Opportunity currentOpportunity;
        private communicationPicklistValuesWrapper(List<String> communicationType, List<String> opportunityStage, Opportunity opportunity) {
            this.communicationType = communicationType;
            this.opportunityStage = opportunityStage;
            this.currentOpportunity = opportunity;
        }
    }
}