public interface IDesiredProductsSelector extends fflib_ISObjectSelector {
    
    List<PO_Product__c> getRelatedDesiredProductsByOpportunityIds(Set<Id> oppportunityIds); 
    
    List<PO_Product__c> selectDesiredProductsByIds(Set<Id> desiredProducts);
    
}