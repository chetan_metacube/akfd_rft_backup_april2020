public interface IQuotationsService {
	List<Quotation__c> generateQuotation(List<QuotationsService.SelectedOpportunityProductsWrapper> opportunityDesiredProductMap);
    /*List<Opportunity> convertLeadToOpportunity(List<LeadsService.LeadConvertWrapper> leadData);
    List<Lead> upsertLeads(List<Lead> leads);*/
}