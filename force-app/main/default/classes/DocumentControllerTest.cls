@isTest
public class DocumentControllerTest {
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(customerAccounts, products, 1);
        List<PriceBook_Product_Entry__c> pricebookEntries = MockData.createPriceBookEntries(customerAccounts, products, priceBooks, 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(customerAccounts, opportunities, products, 1);
        
        List<Warehouse__c> warehouses = MockData.createWarehouses(1);
        MockData.createWarehouseLineItems(warehouses, products, 1);
        List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = [SELECT Id, Name, ProformaInvoice__c, Quantity__c, Product__c, GST__c, Invoiced_Quantity__c, Balanced_Quantity__c, Price__c 
                                                                        FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c IN : proformaInvoices];
        List<Invoice__c> invoices = MockData.createInvoices(opportunities, 1, products, proformaInvoiceLineItems);
        Id attachmentId = MockData.createAttachmentForDocument(opportunities[0], proformaInvoices[0].Id);
    }
    
    @isTest static void positiveTestCaseFor_getPossibleRecipientsMethod() {
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id];
        String result = DocumentController.getPossibleRecipients(opportunity.Id, proformaInvoice.Id);
        System.assert(result != null);
        DocumentController.PossibleRecipients possibleReceipentsData = (DocumentController.PossibleRecipients)JSON.deserialize(result, DocumentController.PossibleRecipients.class);
        AggregateResult  userCountResult = [Select Count(Id) NoOfUsers FROM User WHERE Profile.UserLicense.Name = 'Salesforce'];
        System.assert(possibleReceipentsData.allUsers.size() == userCountResult.get('NoOfUsers'));
        System.assert(possibleReceipentsData.isDocumentAlreadyShared == false);
        System.assert(possibleReceipentsData.otherRecipientsEmails.size() == 0);
        System.assert(possibleReceipentsData.relatedContacts[0].name == 'Test Contact1' 
                      && possibleReceipentsData.relatedContacts[0].email == 'test2@test.com' 
                      && possibleReceipentsData.relatedContacts[0].recordTypeName == 'Agent Contact');
        System.assert(possibleReceipentsData.relatedContacts.size() == 1);
    }
    
    @isTest static void negativeTestCaseFor_getPossibleRecipientsMethod_WhenOpportunityIdIsNull() {
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id];
        try {
            String result = DocumentController.getPossibleRecipients(null, proformaInvoice.Id);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Invalid Opportunity Id passed.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_getPossibleRecipientsMethod_WhenDocumentIdIsNull() {
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        try {
            String result = DocumentController.getPossibleRecipients(opportunity.Id, null);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Empty Document Id Passed.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_getPossibleRecipientsMethod_WhenWrongDocumentIdIsPassed() {
        Opportunity opportunity = [SELECT Id, Name FROM Opportunity LIMIT 1];
        try {
            String result = DocumentController.getPossibleRecipients(opportunity.Id, opportunity.Id);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Wrong Document Id Passed.'));
        }
    }
    
    @isTest static void positiveTestCaseFor_shareMethod() {
        Opportunity opportunity = [SELECT Id, Name, AccountId FROM Opportunity LIMIT 1];
        Contact contact = [SELECT Id, Name FROM Contact WHERE AccountId =: opportunity.AccountId LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id];
        Attachment attachment = [SELECT Id, Name FROM Attachment WHERE ParentId =: opportunity.Id LIMIT 1];
        
        DocumentController.Data  recepientData = new DocumentController.Data();
        recepientData.salesProcess = 'Export';
        recepientData.documentId = proformaInvoice.Id;
        recepientData.targetObjectId = contact.Id;
        recepientData.isDocketNumberUpdated = false;
        recepientData.isDocketNumberSharePage = false;
        recepientData.isDocumentAlreadyShared = false;
        recepientData.templateName = 'TemplateProformaInvoice';
        recepientData.attachmentId = attachment.Id;
        recepientData.recipientEmails  = new List<String> {'test1@gmail.com'};
        System.assert(DocumentController.share(JSON.serialize(recepientData)).contains('Document has been shared.'));
            
    }
    
    @isTest static void positiveTestCaseFor_shareMethod_WhenInvalidDataPassed() {
        Opportunity opportunity = [SELECT Id, Name, AccountId FROM Opportunity LIMIT 1];
        Contact contact = [SELECT Id, Name FROM Contact WHERE AccountId =: opportunity.AccountId LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c WHERE Opportunity__c =: opportunity.Id];
        Attachment attachment = [SELECT Id, Name FROM Attachment WHERE ParentId =: opportunity.Id LIMIT 1];
        
        DocumentController.Data  recepientData = new DocumentController.Data();
        recepientData.salesProcess = 'Export';
        recepientData.documentId = proformaInvoice.Id;
        recepientData.isDocketNumberUpdated = false;
        recepientData.isDocketNumberSharePage = false;
        recepientData.isDocumentAlreadyShared = false;
        recepientData.templateName = 'TemplateProformaInvoice';
        recepientData.attachmentId = attachment.Id;
        recepientData.recipientEmails  = new List<String> {'test1@gmail.com'};
        try {
            DocumentController.share(JSON.serialize(recepientData));
        } catch(Exception e) {
            System.assert(e.getMessage().contains('REQUIRED_FIELD_MISSING'));
        }
    }
    
    @isTest static void positiveTestCaseFor_shareMethod_WithDocketNumber() {
        Opportunity opportunity = [SELECT Id, Name, AccountId FROM Opportunity LIMIT 1];
        Contact contact = [SELECT Id, Name FROM Contact WHERE AccountId =: opportunity.AccountId LIMIT 1];
        Invoice__c invoice = [SELECT Id, Name, Docket_Number__c FROM Invoice__c WHERE Opportunity__c =: opportunity.Id];
        Attachment attachment = [SELECT Id, Name FROM Attachment WHERE ParentId =: opportunity.Id LIMIT 1];
        
        DocumentController.Data  recepientData = new DocumentController.Data();
        recepientData.salesProcess = 'Project';
        recepientData.docketNumber = 'HKKS';
        recepientData.documentId = invoice.Id;
        recepientData.targetObjectId = contact.Id;
        recepientData.isDocketNumberUpdated = true;
        recepientData.isDocketNumberSharePage = true;
        recepientData.isDocumentAlreadyShared = false;
        recepientData.templateName = 'TemplateInvoice';
        recepientData.attachmentId = attachment.Id;
        recepientData.recipientEmails  = new List<String> {'test1@gmail.com'};
        System.assert(DocumentController.share(JSON.serialize(recepientData)).contains('Docket Number has been shared.'));
            
    }
    
    @isTest static void negativeTestCaseFor_shareMethod_WithDocketNumber_WhenDocketNumberIsNull() {
        Opportunity opportunity = [SELECT Id, Name, AccountId FROM Opportunity LIMIT 1];
        Contact contact = [SELECT Id, Name FROM Contact WHERE AccountId =: opportunity.AccountId LIMIT 1];
        Invoice__c invoice = [SELECT Id, Name, Docket_Number__c FROM Invoice__c WHERE Opportunity__c =: opportunity.Id];
        Attachment attachment = [SELECT Id, Name FROM Attachment WHERE ParentId =: opportunity.Id LIMIT 1];
        
        DocumentController.Data  recepientData = new DocumentController.Data();
        recepientData.salesProcess = 'Project';
        recepientData.docketNumber = null;
        recepientData.documentId = invoice.Id;
        recepientData.targetObjectId = contact.Id;
        recepientData.isDocketNumberUpdated = true;
        recepientData.isDocketNumberSharePage = true;
        recepientData.isDocumentAlreadyShared = false;
        recepientData.templateName = 'TemplateInvoice';
        recepientData.attachmentId = attachment.Id;
        recepientData.recipientEmails  = new List<String> {'test1@gmail.com'};
         try {
            DocumentController.share(JSON.serialize(recepientData));
        } catch(Exception e) {
            System.assert(e.getMessage() != null);
        }
    }
    

}