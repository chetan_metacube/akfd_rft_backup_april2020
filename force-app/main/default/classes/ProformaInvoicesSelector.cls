public class ProformaInvoicesSelector extends fflib_SObjectSelector implements IProformaInvoicesSelector {
    private static IProformaInvoicesSelector instance = null;
    
    public static IProformaInvoicesSelector getInstance() {
        
        if(instance == null) { 
            instance = (IProformaInvoicesSelector)Application.selector().newInstance(ProformaInvoice__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            ProformaInvoice__c.Id,
                ProformaInvoice__c.Name,
                ProformaInvoice__c.Status__c,
                ProformaInvoice__c.Opportunity__c,
                ProformaInvoice__c.Country_Of_Final_Origin__c,
                ProformaInvoice__c.Final_Destination__c,
                ProformaInvoice__c.Mode_of_Payment__c,
                ProformaInvoice__c.Freight__c,
                ProformaInvoice__c.Advance_Payment__c,
                ProformaInvoice__c.Packing_Charges__c,
                ProformaInvoice__c.Port_Of_Discharge__c,
                ProformaInvoice__c.Place_Of_Receipt__c,
                ProformaInvoice__c.Port_of_Loading__c,
                ProformaInvoice__c.Pre_Carriage_By__c,
                ProformaInvoice__c.Product_Liability_Insurance_Percentage__c,
                ProformaInvoice__c.Shipping_Charges__c,
                ProformaInvoice__c.Trade_Discount__c,
                ProformaInvoice__c.Is_Revised__c,
                ProformaInvoice__c.Product_Alloted__c,
                ProformaInvoice__c.CurrencyIsoCode,
                ProformaInvoice__c.All_Items_Total_Price__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return ProformaInvoice__c.sObjectType;
    }
    
    public List<ProformaInvoice__c> selectById(Set<ID> idSet) {
        return (List<ProformaInvoice__c>) selectSObjectsById(idSet);
    }
    
    public List<ProformaInvoice__c> selectProformaInvoicesWithLineItemsByOpportunityIds(Set<Id> opportunityIds) {
        System.debug('selectProformaInvoicesWithLineItemsByOpportunityIds');
        System.debug('selector+opportunityIds'+opportunityIds);
        fflib_QueryFactory query = newQueryFactory();
        
        fflib_QueryFactory proformaInvoiceLineItemsQueryFactory = new ProformaInvoiceLineItemsSelector().addQueryFactorySubselect(query);
        
        proformaInvoiceLineItemsQueryFactory.selectField('Product__r.Name');
        proformaInvoiceLineItemsQueryFactory.selectField('Product__r.GST__c');
        proformaInvoiceLineItemsQueryFactory.selectField('Product__r.Image__c');
        proformaInvoiceLineItemsQueryFactory.selectField('Product__r.Total_Stock__c');
        proformaInvoiceLineItemsQueryFactory.selectField('Product__r.Alloted_Stock__c');
        proformaInvoiceLineItemsQueryFactory.selectField('ProformaInvoice__r.Opportunity__c');
        proformaInvoiceLineItemsQueryFactory.selectField('ProformaInvoice__c.Name');
        proformaInvoiceLineItemsQueryFactory.selectField('PO_Product__r.Id');
        proformaInvoiceLineItemsQueryFactory.selectField('PO_Product__r.Discount_Percentage__c');
        proformaInvoiceLineItemsQueryFactory.setCondition('Balanced_Quantity__c > 0');
        
        query.selectField('Opportunity__r.Name');
        query.selectField('Opportunity__r.Freight_Amount__c');        
        query.selectField('Opportunity__r.Packing_Percentage__c');        
        query.selectField('Opportunity__r.Transportation_Mode__c');       
        query.selectField('Opportunity__r.AccountId');        
        query.selectField('Opportunity__r.Billing_Address__c');        
        query.selectField('Opportunity__r.Notify_Address__c');        
        query.selectField('Opportunity__r.Delivery_Address__c');        
        query.selectField('Opportunity__r.Contact_Person__c');        
        query.selectField('Opportunity__r.Notify_Contact__c');
        query.selectField('Opportunity__r.Contact__c');
        query.selectField('RecordType.Name');
        
        query.setCondition('Opportunity__c IN :opportunityIds AND All_Items_Total_Records__c > 0 AND Status__c = \'Accepted\'');
        
        query.setLimit( Limits.getLimitQueryRows() );
        System.debug('query.toSOQL()'+query.toSOQL());
        return Database.query(
            query.toSOQL()
        );
    }
    
    public List<ProformaInvoice__c> selectAcceptedProformaInvoicesByOpportunityIds(Set<Id> opportunityIds) {
        fflib_QueryFactory query = newQueryFactory(false);
        query.selectField('Id');
        query.selectField('Opportunity__c');
        query.setCondition('Opportunity__c IN :opportunityIds AND Status__c = \'Accepted\'');
        return (List<ProformaInvoice__c>) Database.query( query.toSOQL() );
    }
    
    
}