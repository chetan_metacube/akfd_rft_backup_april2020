@isTest
public class AddressTriggerTest {
	@testSetup static void setup() {
        List<Account> accountObj = MockData.createAccounts('Customer', 1);
        Contact contactObj = MockData.createContacts(accountObj, 1)[0];
        List<Address__c> address = MockData.createAddressess(new List<String>{'Billing','Other'}, accountObj);   
    	System.debug('address' + [SELECT Id FROM Address__c]);
    }
    
     @isTest static void testOnBeforeDeleteForOtherAddressMethod() {
      	 Address__c address = [SELECT Id, Address_Type__c FROM Address__c WHERE Address_Type__c = 'Other' LIMIT 1];
         delete address;
         //System.assert(address == null);
     }
     @isTest static void testOnBeforeDeleteForBillingAddressMethod() {
      	 Address__c address = [SELECT Id, Address_Type__c FROM Address__c WHERE Address_Type__c = 'Billing' LIMIT 1];
         delete address;
         //System.assert(address == null);
     }
     @isTest static void testOnBeforeUpdateMethod() {
      	 Address__c address = [SELECT Id, Address_Type__c, State__c, State_Other_than_India__c FROM Address__c WHERE Address_Type__c = 'Billing' LIMIT 1];
         address.State__c = 'Rajasthan';
         //address.State_Other_than_India__c = 'Dallas';
         update address;
         System.assert(address != null);
     }
}