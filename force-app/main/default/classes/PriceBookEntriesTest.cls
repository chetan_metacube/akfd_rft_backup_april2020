@isTest
public class PriceBookEntriesTest {
    
    @isTest static void testFor_beforeInsertPricebookEntries() {
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        accounts = [SELECT Id, Name, CurrencyIsoCode FROM Account WHERE Id IN : accounts];
        // Default is INR
        accounts[0].CurrencyIsoCode = 'USD';
        update accounts;
       	List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        priceBooks = [SELECT Id, Name, CurrencyIsoCode FROM PriceBook_Product__c WHERE Customer__c =: accounts[0].Id];
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        priceBookEntries = [SELECT Id, Name, CurrencyIsoCode, PriceBook_Product__c FROM PriceBook_Product_Entry__c WHERE PriceBook_Product__c =: priceBooks[0].Id];
        System.assert(priceBookEntries[0].CurrencyIsoCode == priceBooks[0].CurrencyIsoCode);
    }

}