public class QuotationsServiceImpl implements IQuotationsService {
    private List<PO_Product__c> selectedDesiredProducts;
    
	public List<Quotation__c> generateQuotation(List<QuotationsService.SelectedOpportunityProductsWrapper> opportunityDesiredProductWrapper){
        Set<Id> allOpportunitIds = new Set<Id>();
        List<PO_Product__c> desiredProducts = new List<PO_Product__c>();
        List<QuotationsService.DesiredProduct> allDesiredProducts = new List<QuotationsService.DesiredProduct>();
        List<QuotationsService.DesiredProduct> allProjectExportDesiredProducts = new List<QuotationsService.DesiredProduct>();
        Set<Id> allProjectExportProductIds = new Set<Id>();
        Set<Id> allRetailProductIds = new Set<Id>();
        for(QuotationsService.SelectedOpportunityProductsWrapper opportunityWrapper : opportunityDesiredProductWrapper) {
            allOpportunitIds.add(opportunityWrapper.OpportunityId);
            for(QuotationsService.DesiredProduct desiredProduct : opportunityWrapper.SelectedProducts) {
                if(desiredProduct.RecordTypeName != 'Retail') {
                    //All Products that belong to Export and Project
                	allProjectExportProductIds.add(desiredProduct.ProductId);
                    allProjectExportDesiredProducts.add(desiredProduct);
                }
                allDesiredProducts.add(desiredProduct);
                
            }
        }
        Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>(OpportunitiesSelector.getInstance().selectOpportunitiesByIds(allOpportunitIds));
        String error = '';
        Set<Id> accountIds = new Set<Id>();
        for(Opportunity opportunityObj : opportunityMap.values()) {
            accountIds.add(opportunityObj.AccountId);
        }
        Map<Id,Pricebook_Product__c> productPriceBookIdMap = new Map<Id,Pricebook_Product__c>();
        Map<Id, PriceBook_Product_Entry__c> poWithPriceBookEntry = new Map<Id, PriceBook_Product_Entry__c>();
            
        if(allProjectExportProductIds.size() > 0) {
            List<PriceBook_Product__c> priceBookProducts = PricebookProductsSelector.getInstance().selectPricebooksWithPricebookEntriesByAccountAndProductIds(accountIds, allProjectExportProductIds);
            Map<Id, Map<Id, PriceBook_Product__c>> accountPricebookMap = new Map<Id, Map<Id, PriceBook_Product__c>>();
            for(PriceBook_Product__c priceBook : priceBookProducts) {
                if(accountPricebookMap.containsKey(priceBook.Customer__c)) {
                    Map<Id, PriceBook_Product__c> pricebookProductMap = accountPricebookMap.get(priceBook.Customer__c);
                    if(!pricebookProductMap.containsKey(priceBook.Product__c)) {
                        pricebookProductMap.put(priceBook.Product__c, priceBook);
                    }
                } else {
                    Map<Id, PriceBook_Product__c> pricebookProductMap = new Map<Id, PriceBook_Product__c>();
                    pricebookProductMap.put(priceBook.Product__c, priceBook);
                    accountPricebookMap.put(priceBook.Customer__c, pricebookProductMap);
                } 
            }
            Boolean isPriceBookWithOppCurrencyExists = false;
            List<String> misMatchCurrencyProducts = new List<String>();
            //PriceBookProduct map with product Id as key and Pricebook Object as Value
            for(PriceBook_Product__c priceBook : priceBookProducts){
                productPriceBookIdMap.put(priceBook.Product__c, priceBook);
            }
            Map<ID, List<PriceBook_Product_Entry__c>> pricebookEntryMap = new Map<ID, List<PriceBook_Product_Entry__c>>();
            for(PriceBook_Product__c priceBook : priceBookProducts) {
                pricebookEntryMap.put(priceBook.ID, priceBook.PriceBook_Product_Entries__r);
            }
            // Validate if pricebook exists
            if(allProjectExportDesiredProducts.size() >0) {
                for(QuotationsService.DesiredProduct selectedPO : allProjectExportDesiredProducts) {
                    if(!productPriceBookIdMap.containsKey(selectedPO.ProductId)) {
                        if(String.isEmpty(error)) { 
                            error += selectedPO.ProductName;
                        } else {
                            error += ', ' + selectedPO.ProductName;
                        }
                    }
                }
                if(!String.isEmpty(error)) {
                    throw new AuraHandledException('Please define the Pricebook for \n' + error + '.');
                }
                
                String productsDoesnotLieInDefinedRange = '';
                String productsPriceBookExpired = '';
                // Validate if pricebook is not expired and pricebook entry for that value exists
                for(QuotationsService.DesiredProduct selectedPO : allProjectExportDesiredProducts) {
                    Opportunity opportuntiyObj = opportunityMap.get(selectedPO.opportunityId);
                    if(accountPricebookMap.containsKey(opportuntiyObj.AccountId)) {
                        Map<Id, PriceBook_Product__c> priceBookProductMap = accountPricebookMap.get(opportuntiyObj.AccountId);
                        if(priceBookProductMap.containsKey(selectedPO.productId)) {
                            PriceBook_Product__c priceBook = productPriceBookIdMap.get(selectedPO.productId);
                            if(priceBook != null) {
                                // Check if priceBook Effective Start Date and Effective End Date applicable at the time of PI creation
                                if((priceBook.Effective_Start_Date__c != null && Date.today() >= priceBook.Effective_Start_Date__c) && (priceBook.Effective_End_Date__c != null && Date.today() <= priceBook.Effective_End_Date__c)) {
                                    List<PriceBook_Product_Entry__c> priceBookEntries = pricebookEntryMap.get(priceBook.Id);
                                    Boolean found = false;
                                    for(PriceBook_Product_Entry__c priceBookEntry : priceBookEntries) {
                                        // Check if entered quantity for that desired product lies with in the price book entry range.
                                        if((priceBookEntry.Min_Quantity__c != null && selectedPO.quantity >= priceBookEntry.Min_Quantity__c) && (priceBookEntry.Max_Quantity__c != null && selectedPO.quantity <= priceBookEntry.Max_Quantity__c)) {
                                            poWithPriceBookEntry.put(selectedPO.productId, priceBookEntry);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if(!found){
                                        if(String.isEmpty(productsDoesnotLieInDefinedRange)) {
                                            productsDoesnotLieInDefinedRange += selectedPO.productName;
                                        } else {
                                            productsDoesnotLieInDefinedRange += ', ' + selectedPO.productName;
                                        }
                                    }
                                } else {
                                    if(String.isEmpty(productsPriceBookExpired)) {
                                        productsPriceBookExpired += selectedPO.productName;
                                    } else {
                                        productsPriceBookExpired += ', ' + selectedPO.productName;
                                    }
                                }
                            }
                        }
                    }
                    
                }
                if(productsDoesnotLieInDefinedRange.length() > 0) {
                    throw new AuraHandledException('It seems Pricebook Entry is not created for this Required Quantity for ' + productsDoesnotLieInDefinedRange);
                }
                if(productsPriceBookExpired.length() > 0) {
                    throw new AuraHandledException('It seems Pricebook is expired for ' + productsPriceBookExpired);
                }
            }
        }
            
        // Create unit of work to capture work and commit it under one transaction
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        List<QuotationsService.Quotation> quotationWrapperList = new List<QuotationsService.Quotation>();
        // Quotation Factory helps domain classes produce Quotations
        QuotationsService.QuotationFactory quotationFactory = new QuotationsService.QuotationFactory(uow);
        Map<Id, String> opportunityProductsWithoutImage = new Map<Id, String>();
        //Integer i =0;
        for(QuotationsService.SelectedOpportunityProductsWrapper opportunityWrapper : opportunityDesiredProductWrapper) {
            //i = i+1;
            String productsWithNoImage = '';
            opportunityWrapper.OpportunityRecord = opportunityMap.get(opportunityWrapper.OpportunityId);
            Opportunity opp = opportunityMap.get(opportunityWrapper.OpportunityId);
            List<QuotationsService.QuotationLineItem> quotationLineItemWrapperList = new List<QuotationsService.QuotationLineItem>();
            for(QuotationsService.DesiredProduct selectedProduct : opportunityWrapper.SelectedProducts) {
                
                if(selectedProduct.ProductImage == null) {
                    if(String.isEmpty(productsWithNoImage)) { 
                        productsWithNoImage += selectedProduct.ProductName;
                    } else {
                        productsWithNoImage += ', ' + selectedProduct.ProductName;
                    }
                } 
                
            }
            opportunityProductsWithoutImage.put(opportunityWrapper.OpportunityId, productsWithNoImage);
        }
        QuotationsService.DesiredProductsWithPriceBooksWrapper productsWithPriceBooksWrapper = new QuotationsService.DesiredProductsWithPriceBooksWrapper(opportunityDesiredProductWrapper, productPriceBookIdMap, poWithPriceBookEntry);
        fflib_ISObjectDomain desiredProductDomain = Application.Domain.newInstance(desiredProducts);
            if(desiredProductDomain instanceof QuotationsService.ISupportQuotation) { 
                // Ask the domain object to generate its quotation
                QuotationsService.ISupportQuotation quotationGeneration = (QuotationsService.ISupportQuotation) desiredProductDomain;
                quotationGeneration.generateQuotations(quotationFactory, productsWithPriceBooksWrapper);
                // Commit generated quotations to the database		
                try{
                    uow.commitWork();
                } catch(Exception e) {
                    throw new AuraHandledException('ERROR:'+e.getMessage() );
                }
            }
        
        List<Quotation__c> quotations = [SELECT Id, Name, Opportunity__c FROM Quotation__c WHERE Id IN :quotationFactory.Quotations];
        System.debug('quotations' + quotations);
        EmailManager sendEmailObj = new EmailManager();
        for(Quotation__c quotation : quotations) {
        	sendEmailObj.sendNotification((SObject)quotation, opportunityMap.get(quotation.Opportunity__c));
        }
        for(Id opportunityId : opportunityProductsWithoutImage.keySet()) {
            sendEmailObj.sendNoImageNotification(opportunityMap.get(opportunityId), opportunityProductsWithoutImage.get(opportunityId));
        }            
        
        return quotationFactory.Quotations;
        
    }
}