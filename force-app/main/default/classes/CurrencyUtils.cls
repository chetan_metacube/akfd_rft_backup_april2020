/** 
 * Class for Currency Symbol
 * Author : Chetan Sharma
*/
public class CurrencyUtils {
    private static final String AUD = 'A$';
    private static final String USD = '$';
    private static final String GBP = '£';
    private static final String INR = '₹';
    private static final String EURO = '€';
    private static Map<String,String> currencyCodeWithSymbols = new Map<String,String>();
    
    static {        
        currencyCodeWithSymbols.put('AUD', 'A$');
        currencyCodeWithSymbols.put('USD', '$');
        currencyCodeWithSymbols.put('GBP', '£');
        currencyCodeWithSymbols.put('INR', '₹');
        currencyCodeWithSymbols.put('EUR', '€');
    }

    //get currency symbol from CurrencyIsoCode
    public static String getSymbol(String code) {
        return currencyCodeWithSymbols.get(code);
    }
}