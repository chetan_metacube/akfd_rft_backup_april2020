@isTest
public class PaymentTransactionTriggerTest {
    @testSetup static void setup() {
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
    	List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        proformaInvoices[0].Status__c = 'Accepted';
        update proformaInvoices;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = opportunities[0].Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = opportunities[0].Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);        
       	Payment_Transaction__c newTransaction = new Payment_Transaction__c(Payment_Term__c = term1.Id, Amount__c = 5.00);
        insert newTransaction;
        List<Payment_Term__c> pt = [SELECT Id FROM Payment_Term__c WHERE Opportunity__c = :opportunities[0].Id];
        System.debug('pt'+pt);
    }
    @isTest static void testDeleteTransactionTriggerMethod() {
        Payment_Transaction__c paymentTransaction = [SELECT Id FROM Payment_Transaction__c LIMIT 1];
        Delete paymentTransaction;
        //System.assert(paymentTransaction == null);
    }
    @isTest static void testUpdateTransactionTriggerMethod() {
        Payment_Transaction__c paymentTransaction = [SELECT Id, Amount__c FROM Payment_Transaction__c LIMIT 1];
        paymentTransaction.Amount__c = 10.00;
        update paymentTransaction;
        System.assert(paymentTransaction.Amount__c == 10.00);
    }
}