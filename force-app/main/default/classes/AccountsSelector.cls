public class AccountsSelector extends fflib_SObjectSelector implements IAccountsSelector {    
    
    private static IAccountsSelector instance = null;
    
    public static IAccountsSelector getInstance() {
        
        if(instance == null) {
            instance = (IAccountsSelector)Application.selector().newInstance(Account.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Account.Id,
            Account.Name,
            Account.Phone,
            Account.Mobile__c,
            Account.Email__c,
            Account.Website,
            Account.CurrencyISOCode,
            Account.Mode_of_Payment__c,
            Account.PAN_Number__c,
            Account.Alternative_Email__c,
            Account.Constitution_of_Entity__c	
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return Account.sObjectType;
    }
    
    public List<Account> selectById(Set<ID> idSet) {
        return (List<Account>) selectSObjectsById(idSet);
    }
}