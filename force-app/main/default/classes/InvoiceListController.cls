public with sharing class InvoiceListController {
    @AuraEnabled
    public static List<Payment_Term__c> getPaymentTerm(Id termId) {
        return [Select Id, Name, Opportunity__c, Balance_Amount__c, RecordType.Name, Invoice__c, Conversion_Rate_To_INR__c From Payment_Term__c Where Id =:termId];
    }
    
    @AuraEnabled
    public static List<Invoiced_Items__c> getInvoicedItems(Id opportunityId) {
        system.debug('opportunityId ' + opportunityId);
        List<Invoice__c> invoices = [Select Id, Name, Status__c, 
                                     (Select Id, Name, 
                                      Settled_Amount__c, Balance_Amount__c, 
                                      Total_Price__c 
                                      From Invoiced_Items__r) 
                                     From Invoice__c Where Status__c = 'Reviewed' And Opportunity__c =:opportunityId];
        List<Invoiced_Items__c> result = new List<Invoiced_Items__c>();
        for(Invoice__c invoice : invoices) {
            for(Invoiced_Items__c item : invoice.Invoiced_Items__r) {
                result.add(item);
            }
        }
        return result;
    }
    
    @AuraEnabled
    public static Payment_Transaction__c createTransaction(String data) {
        String error;
        Payment_Transaction__c paymentTransaction = null;
        try {
            paymentTransaction = (Payment_Transaction__c)JSON.deserialize(data, Payment_Transaction__c.class);
            insert paymentTransaction;
        } catch(DMLException e) {
            error = e.getdmlMessage(0);
            paymentTransaction = null;
            system.debug('Exception e ' + e);
            throw new AuraHandledException(error); 
        }
        return paymentTransaction;
    }
}