@isTest
public class LeadControllerTest {
    @testSetup static void setup(){
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        Account accountMerchantObj = MockData.createAccounts('Company/Consignor', 1)[0];
        Account accountObj = MockData.createAccounts('Customer', 1)[0];
        Contact contactObj = MockData.createContacts(new List<Account>{accountObj}, 1)[0];
        MockData.createAddressess(new List<String>{'Billing'}, new List<Account>{accountObj});
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        Lead leadObj = new Lead(Title = 'Test Title', Lead_Source__c = leadSourceObj.Id, Lead_Receipt_Date__c = System.today(), Sales_Process__c = 'Project',
                                Account__c = accountObj.Id, Contact__c = contactObj.Id, Merchant__c = accountMerchantObj.Id, Salutation = contactObj.Salutation,
                                FirstName = contactObj.FirstName, MiddleName = contactObj.MiddleName, LastName = contactObj.LastName,
                                Suffix = contactObj.Suffix, Email = contactObj.Email, Company = accountObj.Name );
        insert leadObj;
    }
    
    @isTest static void testGetLeadMethodWithoutRecordAccess(){
       User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' AND Alias LIKE 'user%' LIMIT 1];
       Lead leadObj = [SELECT Id FROM Lead LIMIT 1];
       try {
            System.runAs(chatterFreeUser) {
                String leadId = LeadController.getData(leadObj.Id);
                System.assert(leadId != null);
            }
        } catch(Exception error) {
            System.debug(error.getMessage());
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    @isTest static void testGetLeadMethodWithRecordAccess(){
       User adminUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'System Administrator' AND Alias LIKE 'user%' LIMIT 1];
       Lead leadObj = [SELECT Id FROM Lead LIMIT 1];
       try {
            System.runAs(adminUser){
                String leadId = LeadController.getData(leadObj.Id);
                System.assert(leadId != null);
            }
        } catch(Exception error) {
            System.debug(error.getMessage());
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    @isTest static void testGetLeadMethodWithRecordAccessWithoutLeadId(){
       User adminUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'System Administrator' AND Alias LIKE 'user%' LIMIT 1];
       //Lead leadObj = [SELECT Id FROM Lead LIMIT 1];
       try {
            System.runAs(adminUser){
                String leadId = LeadController.getData(null);
                System.assert(leadId != null);
            }
        } catch(Exception error) {
            System.debug(error.getMessage());
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    
    @isTest static void testSaveLeadMethodSuccess(){
        Account accountObj = [SELECT Id, Name FROM Account LIMIT 1];
        Contact contactObj = [SELECT Id, AccountId, Salutation, FirstName, MiddleName, Suffix, LastName, Email FROM Contact LIMIT 1];
        Lead_Source__c leadSourceObjNew = new Lead_Source__c(Name = 'Test exhibition New');
        insert leadSourceObjNew;
        
        Lead leadObjNew = new Lead(Title = 'Test Title1', Lead_Source__c = leadSourceObjNew.Id, Lead_Receipt_Date__c = System.today(), 
                                Account__c = accountObj.Id, Merchant__c=accountObj.Id , Contact__c = contactObj.Id, Salutation = contactObj.Salutation,
                                FirstName = contactObj.FirstName, MiddleName = contactObj.MiddleName, LastName = contactObj.LastName,
                                Suffix = contactObj.Suffix, Email = contactObj.Email, Company = accountObj.Name );
        Lead insertedLead = LeadController.saveLead(JSON.serialize(leadObjNew));
        System.assert(insertedLead != null);
    }
    
    @isTest static void testSaveLeadMethodError(){
        Account accountObj = [SELECT Id, Name FROM Account LIMIT 1];
        Contact contactObj = [SELECT Id, AccountId, Salutation, FirstName, MiddleName, Suffix, LastName, Email FROM Contact LIMIT 1];
        Lead_Source__c leadSourceObjNew = new Lead_Source__c(Name = 'Test exhibition New');
        insert leadSourceObjNew;
        
        Lead leadObjNew = new Lead();
        //insert leadObj;
        try {
            //Lead leadObj = [SELECT Id, Title, Lead_Source__c, Merchant__c, Lead_Receipt_Date__c, Account__c, Contact__c FROM Lead LIMIT 1];
            Lead insertedLead = LeadController.saveLead(JSON.serialize(leadObjNew));
        System.assert(insertedLead != null);
        } catch(Exception error) {
            System.debug(error.getMessage());
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }

    @isTest static void testcreateOpportunityMethodSuccess(){
        Lead leadObj = [SELECT Id, Title, Lead_Source__c, Merchant__c, Lead_Receipt_Date__c, Account__c, Contact__c, OwnerId, Sales_Process__c FROM Lead LIMIT 1];
        System.debug('leadObj'+leadObj);    
        Opportunity convertedOpportunity = LeadController.createOpportunity(JSON.serialize(leadObj), 'Test opp'); 
        System.assert(convertedOpportunity != null);
        
    }

    @isTest static void testcreateOpportunityMethodWithoutOpportunityName(){
        Lead leadObj = [SELECT Id, Title, Lead_Source__c, Merchant__c, Lead_Receipt_Date__c, Account__c, Contact__c, OwnerId, Sales_Process__c FROM Lead LIMIT 1];
        System.debug('leadObj'+leadObj);    
        try{
            Opportunity convertedOpportunity = LeadController.createOpportunity(JSON.serialize(leadObj), '');
        } catch(Exception error)
        {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    
    @isTest static void testcreateOpportunityMethodWithoutMerchant(){
        Account accountObj = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Customer' LIMIT 1];
        Contact contactObj = [SELECT Id, AccountId, Salutation, FirstName, MiddleName, Suffix, LastName, Email FROM Contact LIMIT 1];
        Lead_Source__c leadSourceObjNew = new Lead_Source__c(Name = 'Test exhibition New');
        insert leadSourceObjNew;
        
        Lead leadObjNew = new Lead(Title = 'Test Title1', Lead_Source__c = leadSourceObjNew.Id, Sales_Process__c = 'Project', Lead_Receipt_Date__c = System.today(), 
                                Account__c = accountObj.Id,Contact__c = contactObj.Id, Salutation = contactObj.Salutation,
                                FirstName = contactObj.FirstName, MiddleName = contactObj.MiddleName, LastName = contactObj.LastName,
                                Suffix = contactObj.Suffix, Email = contactObj.Email, Company = accountObj.Name );
        insert leadObjNew;
        System.debug('leadObj'+leadObjNew);    
        try{
            Opportunity convertedOpportunity = LeadController.createOpportunity(JSON.serialize(leadObjNew), 'Test Opp');
        } catch(Exception error)
        {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    @isTest static void testcreateOpportunityMethodWithoutLead(){
        
        try{
            Opportunity convertedOpportunity = LeadController.createOpportunity(null, 'Test Opp');
        } catch(Exception error)
        {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
}