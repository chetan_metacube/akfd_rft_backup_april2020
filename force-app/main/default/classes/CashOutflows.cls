public class CashOutflows extends fflib_SObjectDomain {
    public CashOutflows(List<Projected_Stock__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new CashOutflows(records);
        }
    }
    public override void onBeforeInsert() {
        for(Cash_Outflow__c outflow : (List<Cash_Outflow__c>) records) {
            outflow.CurrencyIsoCode = 'INR';
        }
    }
}