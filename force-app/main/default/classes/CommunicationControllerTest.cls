@isTest
public class CommunicationControllerTest {
    @testSetup static void setup() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> currentOpportunity = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, currentOpportunity, products, 1);
        
    }
    
    @isTest
    static void testGetCommunicationFieldValuesMethodSuccess() {
        Id OpportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        String communication = CommunicationController.getCommunicationFieldValues(OpportunityId);
        System.assert(communication != null);
    }
    @isTest
    static void testGetCommunicationFieldValuesMethodWhenOpportunityIdNull() {
        try{
            String communication = CommunicationController.getCommunicationFieldValues(null);
        } catch(Exception error) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
        
    }
    
    @isTest
    static void testSaveCommunicationMethodSuccess() {
        Id contactId = [SELECT Id From Contact LIMIT 1].id;
        Id accountId = [SELECT Id From Account WHERE RecordType.Name = 'Customer' LIMIT 1].id;
        Id opportunityId = [SELECT Id From Opportunity LIMIT 1].id;
        Communication__c currentCommunication = new Communication__c(Name = 'Test Communication', Contact__c = contactId,
                                                                    Account__c = accountId, Opportunity__c = opportunityId);
        Id insertedCommunnicationId = CommunicationController.saveCommunication(JSON.serialize(currentCommunication));
        System.assert(insertedCommunnicationId != null);
    }
    @isTest
    static void testSaveCommunicationMethodWhenCommunicationNameIsBlank() {
        Id contactId = [SELECT Id From Contact LIMIT 1].id;
        Id accountId = [SELECT Id From Account WHERE RecordType.Name = 'Customer' LIMIT 1].id;
        Id opportunityId = [SELECT Id From Opportunity LIMIT 1].id;
        Communication__c currentCommunication = new Communication__c(Name = '', Account__c = accountId, Opportunity__c = opportunityId);
        try {
            Id insertedCommunnicationId = CommunicationController.saveCommunication(JSON.serialize(currentCommunication));
        } catch(Exception error) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
    }
    
    @isTest
    static void testSaveCommunicationMethodWhenContactIsNull() {
        Id contactId = [SELECT Id From Contact LIMIT 1].id;
        Id accountId = [SELECT Id From Account WHERE RecordType.Name = 'Customer' LIMIT 1].id;
        Id opportunityId = [SELECT Id From Opportunity LIMIT 1].id;
        Communication__c currentCommunication = new Communication__c(Name = 'Test Communication', Account__c = accountId, Opportunity__c = opportunityId);
        try {
            Id insertedCommunnicationId = CommunicationController.saveCommunication(JSON.serialize(currentCommunication));
        } catch(Exception error) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
    }
    @isTest
    static void testSaveCommunicationMethodWhenCommuncationStringIsBlank() {
        try {
            Id insertedCommunnicationId = CommunicationController.saveCommunication('');
        } catch(Exception error) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
    }
}