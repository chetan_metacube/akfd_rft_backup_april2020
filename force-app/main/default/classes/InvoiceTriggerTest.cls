@isTest
public class InvoiceTriggerTest {
    @testSetup static void setup() {
    	Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        List<Account> acc=new List<Account>();
        Account accountCustomerObj=new Account(Name='AccountTest',RecordTypeId=customerRecordTypeIdAccount,CurrencyIsoCode='USD');
        Account accountMerchantObj=new Account(Name='AccountTest',RecordTypeId=merchantcustomerRecordTypeIdAccount);
        acc.add(accountCustomerObj);
        acc.add(accountMerchantObj);
        insert acc;
        
        Contact contactObj=new Contact(FirstName='Contact',LastName='Test',AccountId=accountCustomerObj.Id,Email='test@test.com');
        insert contactObj;
        Id customerRecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Opportunity oppObj=new Opportunity(Name='Test Opportunity',RecordTypeId=customerRecordTypeIdOpportunity,StageName='Client Setup',Merchant__c=accountMerchantObj.Id,AccountId=accountCustomerObj.Id,Contact_Person__c=contactObj.Id,CloseDate=Date.today(),CurrencyIsoCode='USD');
        insert oppObj;
        
        Product2 product=new Product2(Name='Test Product', Brand__c = 'AKFD',Family='Furniture', Retail_Price__c = 0.00);
        insert product;
        
        Product2 product1=new Product2(Name='Test Product1', Brand__c = 'AKFD',Family='Furniture', Retail_Price__c = 10.00);
        insert product1;
        
        PriceBook_Product__c priceBookObj=new PriceBook_Product__c(Customer__c=accountCustomerObj.Id,Product__c=product.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        insert priceBookObj;
        
        PriceBook_Product__c priceBookObj1=new PriceBook_Product__c(Customer__c=accountCustomerObj.Id,Product__c=product1.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        insert priceBookObj1;
        
        PriceBook_Product_Entry__c priceBookEntryObj=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        insert  priceBookEntryObj;
        
        PriceBook_Product_Entry__c priceBookEntryObj1=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj1.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        insert  priceBookEntryObj1;
        
        PO_Product__c desiredProductObj1=new PO_Product__c(Product__c=product.Id,Quantity__c=5,Opportunity__c=oppObj.Id, Discount_Percentage__c = 0.00);
        insert desiredProductObj1;
        
        PO_Product__c desiredProductObj2=new PO_Product__c(Product__c=product1.Id,Quantity__c=5,Opportunity__c=oppObj.Id, Discount_Percentage__c = 0.00);
        insert desiredProductObj2;
        
        ProformaInvoice__c proformaInvoice1 = new ProformaInvoice__c(Opportunity__c = oppObj.Id, Status__c = 'New', Conversion_Rate__c = 1.00);
        List<ProformaInvoice__c> proformaInvoices = new List<ProformaInvoice__c>();
        proformaInvoices.add(proformaInvoice1);
        insert proformaInvoices;
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem1 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj1.Product__c, Quantity__c = 5, Required_Quantity__c = 5, Price__c = 10.00, PO_Product__c = desiredProductObj1.Id);
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem2 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj2.Product__c, Quantity__c = 5, Required_Quantity__c = 5, Price__c = 20.00, PO_Product__c = desiredProductObj2.Id);
        
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = new List<Proforma_Invoice_Line_Item__c>();
        proformaInvoiceLineItems.add(proformaInvoiceLineItem1);
        proformaInvoiceLineItems.add(proformaInvoiceLineItem2);
        insert proformaInvoiceLineItems;
        
        proformaInvoice1.Status__c = 'Accepted';
        update proformaInvoice1;
        
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 150, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        Warehouse_Line_Item__c productWarehouse1 = new Warehouse_Line_Item__c(Product__c = product1.Id, Current_Stock__c = 150, Warehouse__c = warehouse.Id);
        insert productWarehouse1;
        
        Alloted_Stock__c allotedStock1 = new Alloted_Stock__c(Product__c = product.Id, Stock__c = 5, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse.Id, ProformaInvoice__c = proformaInvoice1.Id);
        Alloted_Stock__c allotedStock2 = new Alloted_Stock__c(Product__c = product1.Id, Stock__c = 5, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse1.Id, ProformaInvoice__c = proformaInvoice1.Id);
        
        List<Alloted_Stock__c> allotedStocks = new List<Alloted_Stock__c>();
        allotedStocks.add(allotedStock1);
        allotedStocks.add(allotedStock2);
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert allotedStocks;
        System.debug('alloted stocl'+[SELECT Id,Name,Product__r.Name FROM Alloted_Stock__c]);
        oppObj = [Select Id, (Select Id, Name, Opportunity__c From Payment_Terms__r) From Opportunity Where Id =: oppObj.Id];
        List<Payment_Term__c> existingPaymentTerms = new List<Payment_Term__c>();
        for(Payment_Term__c term : oppObj.Payment_Terms__r) {
            existingPaymentTerms.add(term);
        }
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);
        
        Invoice__c invoice = new Invoice__c(Docket_Number__c='TestDocket',Date__c=Date.today(), Opportunity__c=oppObj.Id);
        insert invoice;
        
        Invoiced_Items__c invoiceItem = new Invoiced_Items__c(Invoice__c=invoice.Id, Opportunity__c=oppObj.Id);
        insert invoiceItem;
        
        Invoiced_Product__c invoicedProduct1 = new Invoiced_Product__c(Invoiced_Item__c=invoiceItem.Id, Product__c=proformaInvoiceLineItem1.Product__c, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItem1.Id, Quantity__c=5, Unit_Price__c=250.00);
        Invoiced_Product__c invoicedProduct2 = new Invoiced_Product__c(Invoiced_Item__c=invoiceItem.Id, Product__c=proformaInvoiceLineItem2.Product__c, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItem2.Id, Quantity__c=5, Unit_Price__c=250.00);
        List<Invoiced_Product__c> invoiceProducts=new List<Invoiced_Product__c>();
        invoiceProducts.add(invoicedProduct1);
        invoiceProducts.add(invoicedProduct2);
        insert invoiceProducts;
    }
	
    @isTest static void reviewInvoice() {
        Invoice__c invoice = [SELECT Id, Conversion_Rate_To_INR__c,Status__c  FROM Invoice__c LIMIT 1];
        invoice.Conversion_Rate_To_INR__c = 35.00;
        invoice.Status__c = 'Reviewed';
        update invoice;
    }
}