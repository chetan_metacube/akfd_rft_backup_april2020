public interface IDesiredProductsService {
    
    List<PO_Product__c> addDesiredProducts(Map<Id, List<PO_Product__c>> desiredProducts);
    
}