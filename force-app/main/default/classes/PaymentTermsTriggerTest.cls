@isTest
public class PaymentTermsTriggerTest {
    @testSetup static void setup() {
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        List<Product2> products = MockData.createProducts(1);
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Payment_Term__c> existingPaymentTerms = new List<Payment_Term__c>();
        for(Payment_Term__c term : opportunities[0].Payment_Terms__r) {
            existingPaymentTerms.add(term);
        }
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = opportunities[0].Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = opportunities[0].Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);
       	
    }
    
     @isTest()
    static void TestOnBeforeInsert() {
        Opportunity currentOpportunity = [SELECT Id FROM Opportunity];
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = currentOpportunity.Id, Payment_Percentage__c = 50.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = currentOpportunity.Id, Payment_Percentage__c = 50.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);
    }
    
    @isTest()
    static void TestOnBeforeUpdate() {
        List<Payment_Term__c> terms = [SELECT Id, Name, Opportunity__c, Payment_Percentage__c FROM Payment_Term__c];
        system.debug('terms ' + terms);
        for(Payment_Term__c term : terms) {
            term.Payment_Percentage__c = 30.00;
        }
        Database.saveResult[] result = Database.update(terms, false);
    }
    
    @isTest()
    static void TestOnAfterUpdate() {
        List<Payment_Term__c> terms = [SELECT Id, Name, Opportunity__c, Payment_Percentage__c, Balance_Amount__c FROM Payment_Term__c];
        system.debug('terms ' + terms);
        
        List<Payment_Transaction__c> paymentTransactions = new List<Payment_Transaction__c>();
        Payment_Transaction__c newPaymentTransaction;
        newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[0].Id, Amount__c = terms[0].Balance_Amount__c);
        paymentTransactions.add(newPaymentTransaction);
        newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[1].Id, Amount__c = terms[1].Balance_Amount__c / 3);
        paymentTransactions.add(newPaymentTransaction);
        newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[1].Id, Amount__c = terms[1].Balance_Amount__c / 3);
        paymentTransactions.add(newPaymentTransaction);
        newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[1].Id, Amount__c = terms[1].Balance_Amount__c / 3);
        paymentTransactions.add(newPaymentTransaction);
        Database.saveResult[] resultTransactions = Database.insert(paymentTransactions, false);
        
        terms = [SELECT Id, Name, Opportunity__c, Payment_Percentage__c, Dummy_Payment_Amount__c, Balance_Amount__c FROM Payment_Term__c];
        system.debug('terms ' + terms);
        for(Payment_Term__c term : terms) {
            term.Dummy_Payment_Amount__c = 100;
        }
        Database.saveResult[] result = Database.update(terms, false);
    }
    
    @isTest()
    static void TestOnBeforeDelete() {
        List<Payment_Term__c> terms = [SELECT Id, Name, Opportunity__c, Balance_Amount__c, Payment_Percentage__c FROM Payment_Term__c];
        List<Payment_Transaction__c> paymentTransactions = new List<Payment_Transaction__c>();
        Payment_Transaction__c newPaymentTransaction;
        for(Payment_Term__c term : terms) {
            newPaymentTransaction = new Payment_Transaction__c(Payment_Term__c = term.Id, Amount__c = term.Balance_Amount__c);
            paymentTransactions.add(newPaymentTransaction);
        }
        Database.saveResult[] resultTransactions = Database.insert(paymentTransactions, false);
        terms = [SELECT Id, Name, Opportunity__c, Payment_Percentage__c, Dummy_Payment_Amount__c, Balance_Amount__c FROM Payment_Term__c];
        Database.DeleteResult[] result = Database.delete(terms, false);
    }
}