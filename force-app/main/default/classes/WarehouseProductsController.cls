/**
 * Used in WarehouseProductsController
 * Class responsible for case related functionality
 * Author: Chetan Sharma (1 March 2019)
 * Last Modified By: Chetan Sharma (24 June 2019)
 */
public class WarehouseProductsController {
    
    private static Id currentProformaInvoiceId {get;set;}
    
    /**
    * Method used in WarehouseAllocationComponent (Lightning Component)
    * @param proformaInvoiceId. 
    * @return productWarehousesData.  
	*/
    @AuraEnabled
    public static String getProductWarehouses(Id proformaInvoiceId) {
        if(String.isNotBlank(proformaInvoiceId) && proformaInvoiceId.getSobjectType() == Schema.ProformaInvoice__c.getSObjectType()) {
            currentProformaInvoiceId = proformaInvoiceId;
            // Checking For User Access 
            UserRecordAccess userAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :proformaInvoiceId];
            if(userAccess != null && !userAccess.HasEditAccess) {
                throw getAuraExceptionInstance('Insufficient Privileges.');
            }
            
            List<WarehouseProductsService.ProductData> productWarehouseData = new List<WarehouseProductsService.ProductData>();
            productWarehouseData = WarehouseProductsService.getProductWarehousesByProformaInvoiceId(proformaInvoiceId);
            System.debug('productWarehouseData@@' + productWarehouseData);
            String resultString =  JSON.serialize(productWarehouseData);
            return JSON.serialize(productWarehouseData);
        } else {
            throw getAuraExceptionInstance('Invalid proforma invoice id passed.');
        }
    }
    
    /**
    * Method used in WarehouseAllocationComponent (Lightning Component) to allocate stock
    * @param proformaInvoiceId and productWarehousesWrapperString  
	*/
    @AuraEnabled
    public static void allotStock(Id proformaInvoiceId, String productWarehousesWrapperString) {
        // Validate if booking quantity is greater than current stock of warehouse for that product.
        if(String.isNotBlank(proformaInvoiceId) && proformaInvoiceId.getSobjectType() == Schema.ProformaInvoice__c.getSObjectType() && String.isNotBlank(productWarehousesWrapperString)) {
            List<WarehouseProductsService.ProductData> productsData = (List<WarehouseProductsService.ProductData>)JSON.deserialize(productWarehousesWrapperString, List<WarehouseProductsService.ProductData>.Class);
           	//ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
            WarehouseProductsService.allotStock(proformaInvoiceId, productsData);
        } else if(String.isNotBlank(productWarehousesWrapperString)){
            throw getAuraExceptionInstance('Invalid proforma invoice id passed.');
        } else {
            throw getAuraExceptionInstance('Invalid Product Wrapper passed.');
        }
       
    }
    
    
     /**
    * Method to getAuraExceptionClassInstance
    * @param : error message
    * @return AuraHandledException
    */
    private static AuraHandledException getAuraExceptionInstance(String message) {
        AuraHandledException exceptionInstance = new AuraHandledException(message);
        exceptionInstance.setMessage(message);
        return exceptionInstance;
    }
    
}