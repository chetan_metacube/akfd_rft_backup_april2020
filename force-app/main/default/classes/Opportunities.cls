public class Opportunities extends fflib_SObjectDomain {
    private static Map<Id,List<PO_Product__c>> opportunityDesiredProductMap {get; set;}
    public Opportunities(List<Opportunity> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Opportunities(sObjectList);
        }
    }
    
    //called before insert
    public override void onApplyDefaults() {
        List<Id> accountIds = new List<Id>();
        Id opportunityRetailRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        for(Opportunity opp : (List<Opportunity>) Records) {
            accountIds.add(opp.AccountId);
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, CurrencyISOCode FROM Account where Id IN :accountIds]);
        for(Opportunity opp : (List<Opportunity>) Records) {
            //default currency to INR for Retail
            if(opp.RecordTypeId == opportunityRetailRecordTypeId){
                opp.CurrencyISOCode = 'INR';
            } else {
                opp.CurrencyISOCode = accountMap.get(opp.AccountId).CurrencyISOCode;
            }
        }
    }
    
    public override void onValidate() {
        
    }
    
    public override void onValidate(Map<Id,SObject> existingRecords) {
        
    }
    
    public override void onBeforeInsert() {
         // Add logic for checking contact field
       List<Id> accountIds = new List<Id>();
        List<AccountContactRelation> accountcontacts;
        Map<Id, Set<Id>> accountsContacts = new Map<Id, Set<Id>>();
        for(Opportunity currentOpportunity : (List<Opportunity>) Records) {
            if(currentOpportunity.AccountId != null) {
                accountIds.add(currentOpportunity.AccountId);
            }
        }
        if(accountIds.size() > 0) {
            accountcontacts = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE AccountId IN: accountIds];
        }
        if(accountcontacts != null && accountcontacts.size() > 0) {
            for(AccountContactRelation accountContact : accountcontacts) {
                if(!accountsContacts.containsKey(accountContact.AccountId)) {
                    accountsContacts.put(accountContact.AccountId, new Set<Id>{accountContact.ContactId});
                } else {
                    Set<Id> existingContacts = accountsContacts.get(accountContact.AccountId);
                    existingContacts.add(accountContact.ContactId);
                    accountsContacts.put(accountContact.AccountId, existingContacts);
                }
            }
        }
        
        
        Set<String> uniqueOpportunityNames = new Set<String>();
        uniqueOpportunityNames = getAllOpportunityNames();
        Id opportunityProjectRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        for(Opportunity currentOpportunity : (List<Opportunity>) Records) {
            if(uniqueOpportunityNames.contains(currentOpportunity.Name)) {
                currentOpportunity.Name.addError('Opportunity with the same name already exists.');
                //opp.Name.addError('Already Exists, please enter another name.');
            }
            if(currentOpportunity.AccountId != null) {
                Set<Id> existingContacts = accountsContacts.get(currentOpportunity.AccountId);
                if(existingContacts != null && existingContacts.size() > 0) {
                    if(currentOpportunity.Contact_Person__c != null && !existingContacts.contains(currentOpportunity.Contact_Person__c)) {
                        currentOpportunity.Contact_Person__c.addError('This contact is not related to this account');
                    } else if(currentOpportunity.Contact__c != null && !existingContacts.contains(currentOpportunity.Contact__c)) {
                        currentOpportunity.Contact__c.addError('This contact is not related to this account');
                    } else if(currentOpportunity.Notify_Contact__c != null && !existingContacts.contains(currentOpportunity.Notify_Contact__c)) {
                        currentOpportunity.Notify_Contact__c.addError('This contact is not related to this account');
                    } else if(currentOpportunity.Buyer_Contact__c != null && !existingContacts.contains(currentOpportunity.Buyer_Contact__c)) {
                        currentOpportunity.Buyer_Contact__c.addError('This contact is not related to this account');
                    }
                } else {
                    //un-comment if deepak sir identify this issue
                    currentOpportunity.Buyer_Contact__c.addError('This contact is not related to this account');
                }
            }
			/*
            if(opportunityProjectRecordTypeId == opp.RecordTypeId && !'INR'.equals(opp.Account.CurrencyISOCode)) {
                opp.AccountId.addError('Please select an Account with currency INR for Project Sales.');
            }
			*/
        }
        
       
    }
    
    public override void onAfterInsert() {
       
        
        List<Default_Payment_Term__c> defaultPaymentTerms = [Select Id, Name, Payment_Percentage__c From Default_Payment_Term__c];
        if(defaultPaymentTerms.size() > 0) {
            List<Payment_Term__c> paymentTerms = new List<Payment_Term__c>();
            Payment_Term__c paymentTerm;
            for(Default_Payment_Term__c defaultPaymentTerm : defaultPaymentTerms) {
                paymentTerm = new Payment_Term__c();
                paymentTerm.Name = defaultPaymentTerm.Name;
                paymentTerm.Dummy_Payment_Status__c = 'Due';
                paymentTerm.Payment_Percentage__c = defaultPaymentTerm.Payment_Percentage__c;
                paymentTerms.add(paymentTerm);
            }
            for(Opportunity opp : (List<Opportunity>) Records) {
                for(Payment_Term__c defaultPaymentTerm : paymentTerms) {
                    defaultPaymentTerm.Opportunity__c = opp.Id;
                    defaultPaymentTerm.CurrencyIsoCode = opp.CurrencyIsoCode;
                }
            }
            insert paymentTerms;
        }
    }
    
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        Set<String> uniqueOpportunityNames = new Set<String>();
        Id opportunityProjectRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        uniqueOpportunityNames = getAllOpportunityNames();
        for(Opportunity opp : (List<Opportunity>) Records) {
            //Unique Name for an opportunity.
            if(uniqueOpportunityNames.contains(opp.Name) && ((Opportunity)existingRecords.get(opp.Id)).Name != opp.Name) {
                opp.Name.addError('Already Exists, please enter another name.');
            }
            if(opp.AccountId != ((Opportunity)existingRecords.get(opp.Id)).AccountId) {
                opp.AccountId.adderror('Cannot change account after processing an opportunity.');
            }
            if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
                if(opp.RecordTypeId != ((Opportunity)existingRecords.get(opp.Id)).RecordTypeId) {
                    opp.RecordTypeId.adderror('Cannot change record type after processing an opportunity.');
                }
            }
            
        }
        List<Id> accountIds = new List<Id>();
        List<AccountContactRelation> accountcontacts;
        Map<Id, Set<Id>> accountsContacts = new Map<Id, Set<Id>>();
        for(Opportunity currentOpportunity : (List<Opportunity>) Records) {
            if(currentOpportunity.AccountId != null) {
                accountIds.add(currentOpportunity.AccountId);
            }
        }
        if(accountIds.size() > 0) {
            accountcontacts = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE AccountId IN: accountIds];
        }
        if(accountcontacts != null && accountcontacts.size() > 0) {
            for(AccountContactRelation accountContact : accountcontacts) {
                if(!accountsContacts.containsKey(accountContact.AccountId)) {
                    accountsContacts.put(accountContact.AccountId, new Set<Id>{accountContact.ContactId});
                } else {
                    Set<Id> existingContacts = accountsContacts.get(accountContact.AccountId);
                    existingContacts.add(accountContact.ContactId);
                    accountsContacts.put(accountContact.AccountId, existingContacts);
                }
            }
        }
        for(Opportunity currentOpportunity : (List<Opportunity>) Records) {
            if(currentOpportunity.AccountId != null) {
                Set<Id> existingContacts = accountsContacts.get(currentOpportunity.AccountId);
                if(existingContacts != null && existingContacts.size() > 0) {
                    if(currentOpportunity.Contact_Person__c != null && !existingContacts.contains(currentOpportunity.Contact_Person__c)) {
                        currentOpportunity.Contact_Person__c.addError('This contact is not related to this account');
                    } else if(currentOpportunity.Contact__c != null && !existingContacts.contains(currentOpportunity.Contact__c)) {
                        currentOpportunity.Contact__c.addError('This contact is not related to this account');
                    } else if(currentOpportunity.Notify_Contact__c != null && !existingContacts.contains(currentOpportunity.Notify_Contact__c)) {
                        currentOpportunity.Notify_Contact__c.addError('This contact is not related to this account');
                    } else if(currentOpportunity.Buyer_Contact__c != null && !existingContacts.contains(currentOpportunity.Buyer_Contact__c)) {
                        currentOpportunity.Buyer_Contact__c.addError('This contact is not related to this account');
                    }
                } else {
                    //un-comment if deepak sir identify this issue
                    currentOpportunity.Buyer_Contact__c.addError('This contact is not related to this account');
                }
            }
        }
        
    }
    
    public override void onAfterUpdate(Map<Id,SObject> existingRecords) {}
    
    private Set<String> getAllOpportunityNames(){
        List<Opportunity> opportunities = [SELECT Id, Name, AccountId, (SELECT Id FROM PO_Products__r) FROM Opportunity];
        opportunityDesiredProductMap = new Map<Id,List<PO_Product__c>>();
        Set<String> uniqueOpportunityName = new Set<String>();
        for(Opportunity oppo : opportunities) {
            uniqueOpportunityName.add(oppo.Name);
            if(oppo.PO_Products__r.size() > 0) {
                opportunityDesiredProductMap.put(oppo.Id, oppo.PO_Products__r);
            }
        }
        return uniqueOpportunityName;
    }
}