@isTest
public class LeadsServiceTest {
    @testSetup static void setup(){
        Account accountObj = new Account(Name = 'Metacube Software PVT Ltd');
        insert accountObj;
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        Account accountMerchantObj = new Account(Name= 'MerchantAccountTest', RecordTypeId = merchantcustomerRecordTypeIdAccount);
        insert accountMerchantObj;
        Contact contactObj = new Contact(AccountId = accountObj.Id, LastName = 'Test Account', Email = 'test@test.com');
        insert contactObj;
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        Lead leadObj = new Lead(Title = 'Test Title', Lead_Source__c = leadSourceObj.Id, Lead_Receipt_Date__c = System.today(), 
                                Account__c = accountObj.Id, Contact__c = contactObj.Id, Merchant__c = accountMerchantObj.Id, Salutation = contactObj.Salutation,
                                FirstName = contactObj.FirstName, MiddleName = contactObj.MiddleName, LastName = contactObj.LastName,
                                Suffix = contactObj.Suffix, Email = contactObj.Email, Company = accountObj.Name );
        insert leadObj;
    }
    
    @isTest static void testGetLeadMethod(){
        Lead leadObj = [SELECT Id FROM Lead LIMIT 1];
        Set<Id> ids = new Set<Id>{leadObj.Id};
            System.assert(LeadsService.getLeadsById(ids) != null);
    }
    
    @isTest static void testUpsertLeadMethodSuccess(){
        Account accountObj = [SELECT Id, Name FROM Account WHERE Name = 'Metacube Software PVT Ltd'];
        Contact contactObj = [SELECT Id, AccountId, Salutation, FirstName, MiddleName, Suffix, LastName, Email FROM Contact WHERE Email = 'test@test.com'];
        Lead_Source__c leadSourceObjNew = new Lead_Source__c(Name = 'Test exhibition New');
        insert leadSourceObjNew;
        
        Lead leadObjNew = new Lead(Title = 'Test Title1', Lead_Source__c = leadSourceObjNew.Id, Lead_Receipt_Date__c = System.today(), 
                                   Account__c = accountObj.Id, Merchant__c = accountObj.Id , Contact__c = contactObj.Id, Salutation = contactObj.Salutation,
                                   FirstName = contactObj.FirstName, MiddleName = contactObj.MiddleName, LastName = contactObj.LastName,
                                   Suffix = contactObj.Suffix, Email = contactObj.Email, Company = accountObj.Name );
        List<Lead> leadObjList=new List<Lead>{leadObjNew};
        System.assert(LeadsService.upsertLeads(leadObjList) != null);

        
    }
    
    /*
    @IsTest
    private static void callingServiceShouldCallSelectorApplyDiscountInDomainAndCommit() {
        // Create mocks
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_ISObjectUnitOfWork uowMock = new fflib_SObjectMocks.SObjectUnitOfWork(mocks);
        //IOpportunities domainMock = new Mocks.Opportunities(mocks);
        ILeadsSelector selectorMock = new Mocks.LeadsSelector(mocks);
        
        // Given
        mocks.startStubbing();
        List<Lead> testOppsList = new List<Lead> {
            new Lead(
                Id = fflib_IDGenerator.generate(Lead.SObjectType),
                Account__c = fflib_IDGenerator.generate(Account.SObjectType),
                Contact__c = fflib_IDGenerator.generate(Contact.SObjectType),
                Name = 'Test Opportunity',
                StageName = 'Open',
                Amount = 1000,
                CloseDate = System.today()) };
                    Set<Id> testOppsSet = new Map<Id, Opportunity>(testOppsList).keySet();
        mocks.when(domainMock.sObjectType()).thenReturn(Opportunity.SObjectType);
        mocks.when(selectorMock.sObjectType()).thenReturn(Opportunity.SObjectType);
        mocks.when(selectorMock.selectByIdWithProducts(testOppsSet)).thenReturn(testOppsList);
        mocks.stopStubbing();
        Decimal discountPercent = 10;
        Application.UnitOfWork.setMock(uowMock);
        Application.Domain.setMock(domainMock);
        Application.Selector.setMock(selectorMock);
        
        // When
        OpportunitiesService.applyDiscounts(testOppsSet, discountPercent);
        
        // Then
        ((IOpportunitiesSelector)
         mocks.verify(selectorMock)).selectByIdWithProducts(testOppsSet);
        ((IOpportunities)
         mocks.verify(domainMock)).applyDiscount(discountPercent, uowMock);
        ((fflib_ISObjectUnitOfWork)
         mocks.verify(uowMock, 1)).commitWork();
    }*/
}