public interface IWarehouseProductsService {
     
    List<WarehouseProductsService.ProductData> getProductWarehousesByProformaInvoiceId(Id proformaInvoiceId);
    
    void allotStock(Id proformaInvoiceId, List<WarehouseProductsService.ProductData> productsData);
    
}