public class PaymentTerms extends fflib_SObjectDomain {
	public PaymentTerms(List<Payment_Term__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new PaymentTerms(records);
        }
    }
    /* 
     * Method - onBeforeInsert
     * Description - Method to execute when trigger is of type Before and Insert
     * To update the payment term automatically using the payment received and balanced amount.
     * To validate if new payment term percentage is crossing the 100% limit.
	*/
    public override void onBeforeInsert() {
        Id installmentRecordTypeId = Schema.SObjectType.Payment_Term__c.RecordTypeInfosByName.get('Installment Payment Terms').RecordTypeId;
        Set<Id> opportunityIds = new Set<Id>();
        
        for(Payment_Term__c paymentTerm : (List<Payment_Term__c>) records) {
            opportunityIds.add(paymentTerm.Opportunity__c);
        }
        
        Map<Id, Opportunity> opportunitiesMap = new Map<Id, Opportunity>([Select Id, Name, CurrencyIsoCode From Opportunity Where Id IN :opportunityIds]);
        List<Payment_Term__c> paymentTerms = [Select Id, Payment_Percentage__c, Payment_Amount__c, Opportunity__c, Opportunity__r.CurrencyIsoCode From Payment_Term__c Where Opportunity__c =:opportunityIds AND RecordTypeId =:installmentRecordTypeId];
        Map<Id, List<Payment_Term__c>> paymentTermsMap = new Map<Id, List<Payment_Term__c>>();
        List<Payment_Term__c> storedTerms;
        
        //Creating payment terms map with opportunity, key -> opportunity id, value -> list of payment terms.
        for(Payment_Term__c paymentTerm : paymentTerms) {
            if(paymentTermsMap.containsKey(paymentTerm.Opportunity__c)) {
                storedTerms = paymentTermsMap.get(paymentTerm.Opportunity__c);
            } else {
                storedTerms = new List<Payment_Term__c>();
            }
            storedTerms.add(paymentTerm);
            paymentTermsMap.put(paymentTerm.Opportunity__c, storedTerms);
        }
        Decimal percentage;
        Decimal paymentAmount;
        Decimal totalAmount;
        String opportunityCurrency;
        
        //Iteration to update the payment terms status and validate if new payment term percentage exceeding the 100%.
        for(Payment_Term__c newPaymentTerm : (List<Payment_Term__c>) records) {
            if(newPaymentTerm.RecordTypeId == installmentRecordTypeId) {
                percentage = 0;
                totalAmount = 0;
                if(paymentTermsMap.get(newPaymentTerm.Opportunity__c) != null) {
                    for(Payment_Term__c paymentTerm : paymentTermsMap.get(newPaymentTerm.Opportunity__c)) {
                        percentage += paymentTerm.Payment_Percentage__c;
                        if(totalAmount == 0 && paymentTerm.Payment_Amount__c > 0) {
                            totalAmount = paymentTerm.Payment_Amount__c * 100 / paymentTerm.Payment_Percentage__c;
                        }
                        opportunityCurrency = paymentTerm.Opportunity__r.CurrencyIsoCode;
                    }
                }
                percentage += newPaymentTerm.Payment_Percentage__c;
                if(percentage > 100.00) {
                    newPaymentTerm.addError('Total Payment Percentage should be less than or equal to 100%.');
                } else if(totalAmount > 0) {
                    paymentAmount = totalAmount * newPaymentTerm.Payment_Percentage__c / 100;
                    newPaymentTerm.Dummy_Payment_Amount__c = paymentAmount;
                }
            }
            
            newPaymentTerm.CurrencyIsoCode = opportunitiesMap.get(newPaymentTerm.Opportunity__c).CurrencyIsoCode;
        }
    }
    
     /* 
     * Method - onBeforeUpdate
     * Param - Map of Payment terms and Ids - equivalent to Trigger.oldMap
     * Description - Method to execute when trigger is of type Before and Update
	*/
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        Id installmentRecordTypeId = Schema.SObjectType.Payment_Term__c.getRecordTypeInfosByName().get('Installment Payment Terms').getRecordTypeId();
        Map<Id, Payment_Term__c> existingRecordsMap = (Map<Id, Payment_Term__c>)existingRecords;
        Set<Id> opportunityIds = new Set<Id>();
        Map<Id, Payment_Term__c> paymentTermsWithUpdatedPercentage = new Map<Id, Payment_Term__c>();
        Map<Id, Payment_Term__c> currentPaymentTerms = new Map<Id, Payment_Term__c>();
		        
        //Iteration to update the payment terms status.
        for(Payment_Term__c paymentTerm : (List<Payment_Term__c>) records) {
            if(existingRecordsMap.get(paymentTerm.Id).Payment_Percentage__c != paymentTerm.Payment_Percentage__c) {
                paymentTermsWithUpdatedPercentage.put(paymentTerm.Id, paymentTerm);
                opportunityIds.add(paymentTerm.Opportunity__c);
            }
            currentPaymentTerms.put(paymentTerm.Id, paymentTerm);
        }
        
        List<Payment_Term__c> paymentTerms = [Select Id, Payment_Percentage__c, Payment_Amount__c, Opportunity__c From Payment_Term__c Where Opportunity__c =:opportunityIds AND RecordTypeId =:installmentRecordTypeId];
        Map<Id, List<Payment_Term__c>> paymentTermsMap = new Map<Id, List<Payment_Term__c>>();
        List<Payment_Term__c> storedTerms;
        
        //Creating payment terms with opportunity map, key -> opportunity id, value -> list of payment terms.
        for(Payment_Term__c paymentTerm : paymentTerms) {
            if(paymentTermsMap.containsKey(paymentTerm.Opportunity__c)) {
                storedTerms = paymentTermsMap.get(paymentTerm.Opportunity__c);
            } else {
                storedTerms = new List<Payment_Term__c>();
            }
            storedTerms.add(paymentTerm);
            paymentTermsMap.put(paymentTerm.Opportunity__c, storedTerms);
        }
        Decimal percentage;
        Decimal paymentAmount;
        Decimal totalAmount;
        
        //Iteration to validate if new payment term percentage exceeding the 100%.
        for(Payment_Term__c newPaymentTerm : paymentTermsWithUpdatedPercentage.values()) {
            system.debug('newPaymentTermRecord ' + newPaymentTerm);
            if(newPaymentTerm.RecordTypeId == installmentRecordTypeId) {
                system.debug('newPaymentTerm ' + newPaymentTerm);
                system.debug('existingRecords ' + existingRecordsMap.get(newPaymentTerm.Id).Total_Transaction_Amount__c);
                if(newPaymentTerm.Payment_Percentage__c != existingRecordsMap.get(newPaymentTerm.Id).Payment_Percentage__c && newPaymentTerm.Payment_Status_New__c != 'Due') {
                    newPaymentTerm.addError('Payment Transactions exists for this Payment Term.');
                } else {
                    percentage = 0;
                    totalAmount = 0;
                    system.debug('paymentTermsMap.get(newPaymentTerm.Opportunity__c) ' + paymentTermsMap.get(newPaymentTerm.Opportunity__c));
                    if(paymentTermsMap.get(newPaymentTerm.Opportunity__c) != null) {
                        for(Payment_Term__c paymentTerm : paymentTermsMap.get(newPaymentTerm.Opportunity__c)) {
                            percentage += paymentTerm.Payment_Percentage__c;
                            if(totalAmount == 0 && paymentTerm.Payment_Amount__c > 0) {
                                totalAmount = paymentTerm.Payment_Amount__c * 100 / paymentTerm.Payment_Percentage__c;
                            }
                        }
                    }
                    percentage += newPaymentTerm.Payment_Percentage__c;
                    percentage -= existingRecordsMap.get(newPaymentTerm.Id).Payment_Percentage__c;
                    system.debug('percentage ' + percentage);
                    if(percentage > 100.00) {
                        newPaymentTerm.addError('Total Payment Percentage should be less than 100%.');
                    } else if(totalAmount > 0) {
                        paymentAmount = totalAmount * newPaymentTerm.Payment_Percentage__c / 100;
                        newPaymentTerm.Dummy_Payment_Amount__c = paymentAmount;
                    }
                }
            }
            system.debug('before update');
            system.debug('newPaymentTerm.Balance_Amount__c ' + newPaymentTerm.Balance_Amount__c);
            system.debug('newPaymentTerm.Total_Transaction_Amount__c ' + newPaymentTerm.Total_Transaction_Amount__c);
        }
    }
    
	/* 
     * Method - onAfterUpdate
     * Param - Map of Payment terms and Ids - equivalent to Trigger.oldMap
     * Description - Method to execute when trigger is of type After and Update
	*/
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        Id installmentRecordTypeId = Schema.SObjectType.Payment_Term__c.getRecordTypeInfosByName().get('Installment Payment Terms').getRecordTypeId();
        Id overpaidRecordTypeId = Schema.SObjectType.Payment_Term__c.getRecordTypeInfosByName().get('Over Paid Payment Terms').getRecordTypeId();
        Map<Id, Payment_Term__c> existingRecordsMap = (Map<Id, Payment_Term__c>)existingRecords;
        Map<Id, List<Payment_Term__c>> opportunitiesPaymentTerms = new Map<Id, List<Payment_Term__c>>();
        Map<Id, Payment_Term__c> paymentTermsWithOverPaidAmount= new Map<Id, Payment_Term__c>();
        Map<Id, Payment_Term__c> paymentTermsWithBandwidth = new Map<Id, Payment_Term__c>();
        List<Payment_Term__c> paymentTerms;
        
        // Filter and validate payment terms. Payment terms with overpaid amount are stored separately.
        for(Payment_Term__c paymentTerm : (List<Payment_Term__c>) records) {
            if(paymentTerm.RecordTypeId == installmentRecordTypeId) {
                if(existingRecordsMap.get(paymentTerm.Id).Payment_Amount__c != paymentTerm.Payment_Amount__c && paymentTerm.Balance_Amount__c < 0) {
                    paymentTermsWithOverPaidAmount.put(paymentTerm.Id, paymentTerm);
                }
                if(opportunitiesPaymentTerms.containsKey(paymentTerm.Opportunity__c)) {
                    paymentTerms = opportunitiesPaymentTerms.get(paymentTerm.Opportunity__c);
                } else {
                    paymentTerms = new List<Payment_Term__c>();
                }
                paymentTerms.add(paymentTerm);
                opportunitiesPaymentTerms.put(paymentTerm.Opportunity__c, paymentTerms);
            }
        }
        List<Payment_Transaction__c> paymentTransactions = [SELECT Id, Transaction_Date__c, Amount__c, Invoiced_Item__c, Comments__c, Conversion_Rate_To_INR__c, CurrencyIsoCode, Payment_Term__c, Payment_Method__c, Unique_Transaction_Number__c FROM Payment_Transaction__c Where Payment_Term__c = :paymentTermsWithOverPaidAmount.keySet()];
    	List<Payment_Term__c> overPaidPaymentTerms = [Select Id, Opportunity__c, RecordTypeId, Dummy_Payment_Amount__c, Dummy_Balance_Amount_v2__c, CurrencyIsoCode From Payment_Term__c Where Opportunity__c =: opportunitiesPaymentTerms.keySet() AND RecordTypeId = :overpaidRecordTypeId];
        Map<Id, Payment_Term__c> opportunityOverPaidPaymentTerm = new Map<Id, Payment_Term__c>();
        
        for(Payment_Term__c term : overPaidPaymentTerms) {
            opportunityOverPaidPaymentTerm.put(term.Opportunity__c, term);
        }
        
        for(Payment_Term__c paymentTerm : (List<Payment_Term__c>) records) {
            if(existingRecordsMap.get(paymentTerm.Id).Payment_Amount__c != paymentTerm.Payment_Amount__c && paymentTerm.Balance_Amount__c > 0 && opportunityOverPaidPaymentTerm.containsKey(paymentTerm.Opportunity__c)) {
                paymentTermsWithBandwidth.put(paymentTerm.Id, paymentTerm);
            }
        }
        
        Map<Id, List<Payment_Transaction__c>> paymentTransactionsOfPaymentTerms = new Map<Id, List<Payment_Transaction__c>>();
        List<Payment_Transaction__c> existingPaymentTransactions;
        
        //Creating payment transactions map with payment term as key, key -> payment term id, value -> list of payment transactions.
        for(Payment_Transaction__c paymentTransaction : paymentTransactions) {
            if(paymentTransactionsOfPaymentTerms.containsKey(paymentTransaction.Payment_Term__c)) {
                existingPaymentTransactions = paymentTransactionsOfPaymentTerms.get(paymentTransaction.Payment_Term__c);
            } else {
                existingPaymentTransactions = new List<Payment_Transaction__c>();
            }
            existingPaymentTransactions.add(paymentTransaction);
            paymentTransactionsOfPaymentTerms.put(paymentTransaction.Payment_Term__c, existingPaymentTransactions);
        }
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Payment_Term__c.SObjectType,
                Payment_Transaction__c.SObjectType
            }
        );
        Id opportunityId;
        Decimal overPaidAmount;
        Decimal balancePaidAmount;
        Payment_Transaction__c newPaymentTransaction;
        List<Payment_Transaction__c> newTransactions;
        List<Payment_Transaction__c> transactionsToBeInserted = new List<Payment_Transaction__c>();
        Map<Id, List<Payment_Transaction__c>> overPaidTransactionsOfOpportunitiesToBeInserted = new Map<Id, List<Payment_Transaction__c>>();
        Set<Payment_Transaction__c> transactionsToBeUpdated = new Set<Payment_Transaction__c>();
        Set<Payment_Transaction__c> transactionsToBeDeleted = new Set<Payment_Transaction__c>();
        Map<Id, Decimal> opportunitWiseOverPaidAmount = new Map<Id, Decimal>();
        
        //Iterate over every payment term with overpaid amount to adjust the existing payment transactions and create/delete transactions with required.
        for(Id paymentTermId : paymentTermsWithOverPaidAmount.keySet()) {
            newTransactions = new List<Payment_Transaction__c>();
            newPaymentTransaction = null;
            balancePaidAmount = 0.00;
            overPaidAmount = -paymentTermsWithOverPaidAmount.get(paymentTermId).Balance_Amount__c;
            opportunityId = paymentTermsWithOverPaidAmount.get(paymentTermId).Opportunity__c;
            
            //Iteration to check if any transaction exists with enough bandwidth to create a new overpaid transaction. New transaction will be the clone of the selected transaction.
            for(Payment_Transaction__c paymentTransaction : paymentTransactionsOfPaymentTerms.get(paymentTermId)) {
                if(paymentTransaction.Amount__c - overPaidAmount > 0) {
                    newPaymentTransaction = new Payment_Transaction__c(Amount__c = overPaidAmount);
                    newPaymentTransaction.Transaction_Date__c = paymentTransaction.Transaction_Date__c;
                    newPaymentTransaction.Invoiced_Item__c = paymentTransaction.Invoiced_Item__c;
                    newPaymentTransaction.Comments__c = paymentTransaction.Invoiced_Item__c;
                    newPaymentTransaction.Conversion_Rate_To_INR__c = paymentTransaction.Conversion_Rate_To_INR__c;
                    newPaymentTransaction.Payment_Method__c = paymentTransaction.Payment_Method__c;
                    newPaymentTransaction.Unique_Transaction_Number__c = paymentTransaction.Unique_Transaction_Number__c;
                    newPaymentTransaction.CurrencyIsoCode = paymentTransaction.CurrencyIsoCode;
                    newTransactions.add(newPaymentTransaction);
                    paymentTransaction.Amount__c -= overPaidAmount;
                    transactionsToBeUpdated.add(paymentTransaction);
                    break;
                }
            }
            
            //If no transaction with enough bandwidth exists, delete an existing transaction or delete chunks of existing transaction to create new overpaid transaction.
            if(newPaymentTransaction == null) {
                balancePaidAmount = overPaidAmount;
                for(Payment_Transaction__c paymentTransaction : paymentTransactionsOfPaymentTerms.get(paymentTermId)) {
                    if(paymentTransaction.Amount__c - balancePaidAmount <= 0) {
                        newPaymentTransaction = new Payment_Transaction__c(Amount__c = paymentTransaction.Amount__c);
                        balancePaidAmount -= paymentTransaction.Amount__c;
                        transactionsToBeDeleted.add(paymentTransaction);
                    } else {
                        newPaymentTransaction = new Payment_Transaction__c(Amount__c = balancePaidAmount);
                        paymentTransaction.Amount__c -= balancePaidAmount;
                        balancePaidAmount = 0;
                        transactionsToBeUpdated.add(paymentTransaction);
                    }
                    newPaymentTransaction.Transaction_Date__c = paymentTransaction.Transaction_Date__c;
                    newPaymentTransaction.Invoiced_Item__c = paymentTransaction.Invoiced_Item__c;
                    newPaymentTransaction.Comments__c = paymentTransaction.Invoiced_Item__c;
                    newPaymentTransaction.Conversion_Rate_To_INR__c = paymentTransaction.Conversion_Rate_To_INR__c;
                    newPaymentTransaction.Payment_Method__c = paymentTransaction.Payment_Method__c;
                    newPaymentTransaction.Unique_Transaction_Number__c = paymentTransaction.Unique_Transaction_Number__c;
                    newPaymentTransaction.CurrencyIsoCode = paymentTransaction.CurrencyIsoCode;
                    newTransactions.add(newPaymentTransaction);
                }
            }
            
            Boolean isOverPaidPaymentTermNeeded = true;
            //Iteration to check if any payment term exists which has sufficient bandwidth to store the new overpaid transaction.
            for(Payment_Term__c paymentTerm : opportunitiesPaymentTerms.get(opportunityId)) {
                if(paymentTerm.Id != paymentTermId) {
                    if(paymentTerm.Balance_Amount__c >= overPaidAmount) {
                        for(Payment_Transaction__c newTransaction : newTransactions) {
                            newTransaction.Payment_Term__c = paymentTerm.Id;
                            newTransaction.CurrencyIsoCode = paymentTerm.CurrencyIsoCode;
                        }
                        isOverPaidPaymentTermNeeded = false;
                    }
                }
            }
            Decimal overPaidPaymentAmount = 0.00;
            
            //If no payment term with sufficient space is found, new overpaid payment term will be created. Calculate the total overpaid amount for the new overpaid payment term.
            if(isOverPaidPaymentTermNeeded) {
                for(Payment_Transaction__c newTransaction : newTransactions) {
                    overPaidPaymentAmount += newTransaction.Amount__c;
                }
                if(opportunitWiseOverPaidAmount.containsKey(opportunityId)) {
                    overPaidPaymentAmount += opportunitWiseOverPaidAmount.get(opportunityId);
                }
                opportunitWiseOverPaidAmount.put(opportunityId, overPaidPaymentAmount);
                if(overPaidTransactionsOfOpportunitiesToBeInserted.containsKey(opportunityId)) {
                    newTransactions.addAll(overPaidTransactionsOfOpportunitiesToBeInserted.get(opportunityId));
                }
                overPaidTransactionsOfOpportunitiesToBeInserted.put(opportunityId, newTransactions);
            } else {
                transactionsToBeInserted.addAll(newTransactions);
            }
        }
        for(Id paymentTermId : paymentTermsWithBandwidth.keySet()) {
            // code to be written
        }
        
        Map<Id, Payment_Term__c> opportunityOverPaidPaymentTermsToBeUpserted = new Map<Id, Payment_Term__c>();
        
        Payment_Term__c overPaidPaymentTerm;
        
        //Create overpaid payment term if not exists else update the overpaid payment term.
        if(opportunitWiseOverPaidAmount.size() > 0) {
            for(Id opportunityWiseId : opportunitWiseOverPaidAmount.keySet()) {
                if(opportunityOverPaidPaymentTerm.containsKey(opportunityWiseId)) {
                    overPaidPaymentTerm = opportunityOverPaidPaymentTerm.get(opportunityWiseId);
                } else {
                    overPaidPaymentTerm = new Payment_Term__c(Name = 'Over Paid Payment Term', Opportunity__c = opportunityWiseId, RecordTypeId = overpaidRecordTypeId, Dummy_Payment_Amount__c = 0.00);
                }
                
                overPaidPaymentTerm.Dummy_Payment_Amount__c += opportunitWiseOverPaidAmount.get(opportunityWiseId);
                opportunityOverPaidPaymentTerm.put(opportunityWiseId, overPaidPaymentTerm);
                opportunityOverPaidPaymentTermsToBeUpserted.put(opportunityWiseId, overPaidPaymentTerm);
            }
        }
        
        //Insert new overpaid payment transaction.
        for(Id opportunityWiseId : overPaidTransactionsOfOpportunitiesToBeInserted.keySet()) {
            for(Payment_Transaction__c newTransaction : overPaidTransactionsOfOpportunitiesToBeInserted.get(opportunityWiseId)) {
                uow.registerRelationship(newTransaction, Payment_Transaction__c.Payment_Term__c, opportunityOverPaidPaymentTermsToBeUpserted.get(opportunityWiseId)); 
                newTransaction.Payment_Term__c = opportunityOverPaidPaymentTermsToBeUpserted.get(opportunityWiseId).Id;
                opportunityOverPaidPaymentTermsToBeUpserted.get(opportunityWiseId).CurrencyIsoCode = newTransaction.CurrencyIsoCode;
                transactionsToBeInserted.add(newTransaction);
            }
        }
        
        for(Payment_Term__c overPaidTerm : opportunityOverPaidPaymentTermsToBeUpserted.values()) {
            if(overPaidTerm.Id == null) {
                uow.registerNew(overPaidTerm);
            } else {
                uow.registerDirty(overPaidTerm);
            }
        }
        
        uow.registerNew(new List<Payment_Transaction__c>(transactionsToBeInserted));
        uow.registerDirty(new List<Payment_Transaction__c>(transactionsToBeUpdated));
        uow.registerDeleted(new List<Payment_Transaction__c>(transactionsToBeDeleted));
        uow.commitWork();
    }    
    /* 
     * Method - onBeforeDelete
     * Description - Method to execute when trigger is of type Before and delete
	*/
    public override void onBeforeDelete() {
        
        //Restriction to delete any payment term with existing payment transaction.
        for(Payment_Term__c newPaymentTerm : (List<Payment_Term__c>) records) {
            if(newPaymentTerm.Payment_Status_New__c != 'Due') {
                newPaymentTerm.addError('Payment Transactions exists for this Payment Term.');
            }
        }
    }
    
}