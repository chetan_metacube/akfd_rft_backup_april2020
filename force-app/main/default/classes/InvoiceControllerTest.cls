@isTest
public class InvoiceControllerTest {
	@testSetup static void setup(){
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
    	List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = products[0].Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        Alloted_Stock__c allotedStock1 = new Alloted_Stock__c(Product__c = products[0].Id, Stock__c = 10, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse.Id, ProformaInvoice__c = proformaInvoices[0].Id);
        //Alloted_Stock__c allotedStock2 = new Alloted_Stock__c(Product__c = product[0].Id, Stock__c = 12, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse.Id, ProformaInvoice__c = proformaInvoice2.Id);
        
        List<Alloted_Stock__c> allotedStocks = new List<Alloted_Stock__c>();
        allotedStocks.add(allotedStock1);
        //allotedStocks.add(allotedStock2);
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert allotedStocks;
    }
    @isTest
    public static void testGetProformaInvoicesMethodWithoutOpportunityId() {
        try {
            String proformaInvoiceListString = InvoiceController.getProformaInvoices('');
            System.assert(proformaInvoiceListString != null);
        } catch(Exception error) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
    }
    @isTest
    public static void testGetProformaInvoicesMethodWithoutAcceptedPI() {
        try {
            Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
            ProformaInvoice__c proformaInvoice = [SELECT Id, Status__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
            String proformaInvoiceListString = InvoiceController.getProformaInvoices(opportunityId);
            System.assert(proformaInvoiceListString != null);
        } catch(Exception error) {
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
    }
    @isTest
    public static void testGetProformaInvoicesMethodWithoutAccess() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' AND Alias LIKE 'user%' LIMIT 1];
        ProformaInvoice__c proformaInvoice = [SELECT Id, Status__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        try {
            System.runAs(chatterFreeUser) {
                String proformaInvoiceListString = InvoiceController.getProformaInvoices(opportunityId);
            }
        } catch(Exception error) {
            System.debug(error.getMessage());
            System.assert(error.getMessage().contains('Script-thrown exception'));
        }
        
    }
    @isTest
    public static void testGetProformaInvoicesMethodSuccess() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        ProformaInvoice__c proformaInvoice = [SELECT Id, Status__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        String proformaInvoiceListString = InvoiceController.getProformaInvoices(opportunityId);
        System.assert(proformaInvoiceListString != null);
    }
    
    @isTest
    public static void testGenerateInvoiceSuccess() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Price__c, Product__r.Name FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c = :proformaInvoice.Id LIMIT 1];
        String proformaInvoicesWrapper ='[{"Quantity":"2","proformaInvoiceName":"'+ proformaInvoice.Name +
            '","proformaInvoiceLineItemId":"' + proformaInvoiceLineItem.Id + 
            '","proformaInvoiceId":"' + proformaInvoice.Id + 
            '","productPrice":' + proformaInvoiceLineItem.Price__c + 
            ',"productName":"' + proformaInvoiceLineItem.Product__r.Name + 
            '","currentStock":59,"bookedStock":2,"bookedQuantity":2,"balancedQuantity":2}]';
        System.assert(InvoiceController.generateInvoice(proformaInvoicesWrapper) !=null);
    }
}