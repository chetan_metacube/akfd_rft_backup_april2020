public interface IQuotationsSelector extends fflib_ISObjectSelector {
	List<Quotation__c> selectQuotationsByIds(Set<Id> quotationIds);
	//List<ProformaInvoice__c> selectAcceptedProformaInvoicesByOpportunityIds(Set<Id> opportunityIds);
}