public class ProjectedStocksSelector extends fflib_SObjectSelector implements IProjectedStocksSelector {

    private static IProjectedStocksSelector instance = null;
    
    public static IProjectedStocksSelector getInstance() {
        
        if(instance == null) {
            instance = (IProjectedStocksSelector)Application.selector().newInstance(Projected_Stock__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            Projected_Stock__c.Id, 
                Projected_Stock__c.Product__c, 
                Projected_Stock__c.Required_Quantity__c, 
                Projected_Stock__c.ProformaInvoice__c, 
                Projected_Stock__c.Status__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return Projected_Stock__c.sObjectType;
    }
    
    public List<Projected_Stock__c> selectByProformaInvoiceId(Id proformaInvoiceId) {
        fflib_QueryFactory query = newQueryFactory();
        query.setCondition('ProformaInvoice__c =: proformaInvoiceId');
        return (List<Projected_Stock__c>) Database.query( query.toSOQL() );
    }
    
}