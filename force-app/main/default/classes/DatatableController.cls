/**
* Class to handle generic datatable logic
* @Author : Chetan Sharma
* Last edited by : Chetan Sharma
*/
public class DatatableController {
    /**
     * Method to get records
     * @param datatableDTOString
     * @return result records
     */
    @AuraEnabled
    public static String getRecords(String datatableDTOString) {
        if(String.isNotBlank(datatableDTOString)) {
            DatatableDTO datatableDTO = (DatatableDTO)JSON.deserializeStrict(datatableDTOString, DatatableDTO.class);
            ResultWrapper searchResult;
            Integer searchResultRecordCount = 0;
            List<Map<String, Object>> records = new List<Map<String, Object>>();
            String whereClause = getWhereClause(datatableDTO);
            String queryString = 'Select count() FROM ' + datatableDTO.objectApiName + ' WHERE ' + whereClause ;
            try{
                searchResultRecordCount = Database.countQuery(queryString);
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
            if(searchResultRecordCount != 0) {
                records = queryRecords(datatableDTO);
            }
            searchResult = new ResultWrapper(records, searchResultRecordCount);
            return JSON.serialize(searchResult);
        } else {
            throw new AuraHandledException('Invalid Datatable DTO passed.');
        }
    }
   /**
    * private method to query data
    * @param datatableDTO
    * @return Records data
    */
    private static List<Map<String, Object>> queryRecords(DatatableDTO datatableDTO){
        // Apply validations
        List<SObject> resultRecords = new List<SObject>();
        List<Map<String, Object>> records = new List<Map<String, Object>>();
        if(datatableDTO.recordsPerPage > 0) {
            String whereClause = getWhereClause(datatableDTO);
            String queryString = 'SELECT ' +  String.join(datatableDTO.objectFields, ',') + ' FROM ' + datatableDTO.objectApiName + ' WHERE ' + whereClause + ' ORDER BY Name ASC LIMIT ' + datatableDTO.recordsPerPage + ' OFFSET ' + datatableDTO.currentOffset;
            try { 
                resultRecords = Database.query(queryString);
            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
            records = getFiledMapList(resultRecords, datatableDTO.objectFields);
        } 
        
        return records;
    }
    
    /**
    * private method to get field map
    * @param result records and objectFields
    * @return Records data
    */
    private static List<Map<String, Object>> getFiledMapList(List<SObject> resultRecords, List<String> objectFields){
        List<Map<String, Object>> records = new List<Map<String, Object>>();
        Map<String, Object> recordfields;
        for(SObject result : resultRecords) {
            recordfields = new Map<String, Object>();
            for(String objectField : objectFields) { 
               	Object value;
                if(objectField.contains('.')) {
                    value = result.getSObject(objectField.substringBefore('.')).get(objectField.substringAfter('.'));
                } else {
                    value = result.get(objectField);
                }            	
               recordfields.put(objectField, value);
            }
            records.add(recordfields);
        }
        return records;
    }
    
    /**
    * Method generateWhere clause
    * @param datatableDTO
    * @return where clause
    */
    private static String getWhereClause(DatatableDTO datatableDTO) {
        Schema.SObjectType leadSchema = Schema.getGlobalDescribe().get(datatableDTO.objectApiName);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        String whereClause = '';
        String searchCondition = '';
        if(String.isBlank(datatableDTO.searchTerm)) {
            datatableDTO.searchTerm = '';
        }
        datatableDTO.searchTerm = datatableDTO.searchTerm.trim();
        String searchPattern = '\'%' + datatableDTO.searchTerm + '%\'';
        //Add filter in where clause
        if(datatableDTO.filter != null && datatableDTO.filter.length() > 0){
            datatableDTO.filter = datatableDTO.filter.trim();
            whereClause += datatableDTO.filter + ' AND ';
        }
        //Generate SearchConditionString for where clause
        for(String objectField : datatableDTO.objectFields) {
            if(!objectField.contains('.')){
                try {
                    Schema.DisplayType fieldDataType = fieldMap.get(objectField).getDescribe().getType();
                    System.debug('objectField data type ' + fieldDataType);
                    if(fieldDataType == Schema.DisplayType.STRING || fieldDataType == Schema.DisplayType.PICKLIST) {
                        if(searchCondition.length() > 0) {
                            searchCondition += ' OR ';
                        } 
                        searchCondition += objectField + ' LIKE ' + searchPattern;
                    }
                } catch (Exception e) {
                    System.debug('Exception ' + e);
                }
            }
        }
        if(searchCondition.length() > 0) {
            whereClause += ' ( ' + searchCondition + ' ) ';
        }
        return whereClause;
    }
    
    public class ResultWrapper {
        public Boolean isResultFound = false;
        public Integer totalRecordCount = 0;
        public List<Map<String, Object>> records;
        public String responseMessage {get;set;}
        public ResultWrapper(List<Map<String, Object>> records, Integer totalRecordCount) {
            if(records != null &&  records.size() > 0 ) {
                isResultFound = true;
                this.records = records;
                this.totalRecordCount = totalRecordCount;
                this.responseMessage = 'Result Found';
            } else {
                this.responseMessage = 'No Result Found';
            }
        }
    }
    
    public class DatatableDTO {
        public Integer recordsPerPage {get;set;}
        public String objectApiName {get;set;}
        public List<String> objectFields {get;set;}
        public String filter {get;set;}
        public String searchTerm {get;set;}
        public Integer currentOffset {get;set;}
    }
    
}