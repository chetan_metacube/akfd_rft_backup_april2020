//@isTest
public with sharing class MockData {
    public static List<Account> createAccounts(String recordTypName,Integer noOfAccounts){
        List<Account> accounts=new List<Account>();
        try{
            for(Integer i=0;i<noOfAccounts;i++){
                Id recordTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypName).getRecordTypeId();
                accounts.add(new Account(Name=recordTypName+'Test Account'+i+1,RecordTypeId=recordTypeId));
            } 
            insert accounts;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return accounts;
    }
    
    public static List<Contact> createContacts(List<Account> accounts,Integer noOfContactsPerAccount){
        List<Contact> contacts=new List<Contact>();
        Id agentRecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent Contact').getRecordTypeId();
        
        try{
            Integer count=1;
            for(Account accountObj:accounts){
                for(Integer i=0;i<noOfContactsPerAccount;i++){
                    
                    contacts.add(new Contact(FirstName='Test',LastName='Contact'+count++,AccountId=accountObj.Id,Email='chetan.sharma'+count+'@metacube.com',RecordTypeId=agentRecordTypeIdContact));
                }
            } 
            insert contacts;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return contacts;
    }
    
    public static List<Case> createCases(Integer noOfCases){
        List<Case> cases=new List<Case>();
        try{
            for(Integer i=0;i<noOfCases;i++){
                cases.add(new Case(Origin='Manual',Sales_Process__c='Export',Status='new',Priority='Medium',SuppliedEmail='test'+i+'@test.com'));
            }
            insert cases;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return cases;
    }
    
    public static List<Lead_Source__c> createLeadSources(Integer noOfLeadSources){
        List<Lead_Source__c> leadSources=new List<Lead_Source__c>();
        try{
            for(Integer i=0;i<noOfLeadSources;i++){
                leadSources.add(new Lead_Source__c(Name='Test Lead Source'+i+1));
            }
            insert leadSources;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return leadSources;
    }
    
    public static List<Lead> createLeads(List<Account> accounts,List<Contact> contacts,List<Lead_Source__c> leadSources,Integer noOfLeads){
        List<Lead> leads=new List<Lead>();
        try{
            for(Integer i=0;i<noOfLeads;i++){
                leads.add(new Lead(Title='Test Lead Record'+i+1,Status='New',Lead_Source__c=leadSources[i].Id,Lead_Receipt_Date__c=Date.today(),Account__c=accounts[i].Id,Contact__c=contacts[i].Id,FirstName=contacts[i].FirstName,LastName=contacts[i].LastName,Company=accounts[i].Name));
            }
            insert leads;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return leads;
    }
    
    public static List<Opportunity> createOpportunities(List<Account> customerAccounts,List<Account> companyConsignorAccounts,List<Contact> contacts,List<Lead_Source__c> leadSources,Integer noOfOpportunities){
        List<Opportunity> opportunities=new List<Opportunity>();
        try{
            for(Integer i=0;i<noOfOpportunities;i++){
                opportunities.add(new Opportunity(Name='Test Opportunity Record'+i+1,AccountId=customerAccounts[i].Id,Contact_Person__c=contacts[i].Id,Merchant__c=companyConsignorAccounts[i].Id,StageName='Client Setup',CloseDate=Date.today(),Close_Date__c=Date.today().addDays(15)));
            }
            insert opportunities;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return opportunities;
    }
    
    public static List<Product2> createProducts(Integer noOfProducts){
        List<Product2> products=new List<Product2>();
        try{
            for(Integer i=0;i<noOfProducts;i++){
                products.add(new Product2(Name='Test Product'+i+1,Family='Furniture'));
            }
            insert products;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return products;
    }
    
    public static List<PO_Product__c> createDesiredProducts(List<Account> accounts,List<Opportunity> opportunities,List<Product2> products,Integer noOfDesiredProducts){
        List<PO_Product__c> desiredProducts=new List<PO_Product__c>();
        try{
            for(Integer i=0;i<noOfDesiredProducts;i++){
                desiredProducts.add(new PO_Product__c(Account__c=accounts[i].Id, Opportunity__c=opportunities[i].Id, Product__c=products[i].Id, Quantity__c=15 ));
            }
            insert desiredProducts;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return desiredProducts;
    }
    
    public static List<PriceBook_Product__c> createPriceBooks(List<Account> accounts,List<Product2> products,Integer noOfPriceBooks){
        List<PriceBook_Product__c> priceBooks=new List<PriceBook_Product__c>();
        try{
            for(Integer i=0;i<noOfPriceBooks;i++){
                priceBooks.add(new PriceBook_Product__c(Customer__c=accounts[i].Id, Effective_Start_Date__c=Date.today(), Effective_End_Date__c=Date.today().addDays(365), Product__c=products[i].Id, Sampling_Charges__c=200.00+i));
            }
            insert priceBooks;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return priceBooks;
    }
    
    public static List<PriceBook_Product_Entry__c> createPriceBookEntries(List<Account> accounts,List<Product2> products,List<PriceBook_Product__c> priceBooks,Integer noOfPriceBookEntriesPerPriceBook){
        List<PriceBook_Product_Entry__c> priceBookEntries=new List<PriceBook_Product_Entry__c>();
        try{
            for(PriceBook_Product__c priceBook:priceBooks){
                for(Integer i=0,counter=0;i<noOfPriceBookEntriesPerPriceBook;i++){
                    priceBookEntries.add(new PriceBook_Product_Entry__c(Lead_Time_for_MOQ__c=15+i, Min_Quantity__c=1+counter, Max_Quantity__c=counter+10, PriceBook_Product__c=priceBook.Id, Unit_of_Price__c='Each', Price__c=100.00+i));
                    counter+=10;
                }
            }
            insert priceBookEntries;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return priceBookEntries;
    }
    
    public static List<Quotation__c> createQuotation(List<Opportunity> opportunities,Integer noOfQuotations){
        List<Quotation__c> quotations=new List<Quotation__c>();
        try{
            for(Integer i=0;i<noOfQuotations;i++){
                quotations.add(new Quotation__c(Effective_Start_Date__c=Date.today(), End_Date__c=Date.today().addDays(365), Opportunity__c=opportunities[i].Id));
            }
            insert quotations;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return quotations;
    }
    
    public static List<Quotation_Line_Item__c> createQuotation(List<Product2> products,List<PO_Product__c> desiredProducts,List<Quotation__c> quotations,List<PriceBook_Product__c> priceBooks,Integer noOfQuotationLineItemsPerQuotation){
        List<Quotation_Line_Item__c> quotationLineItems=new List<Quotation_Line_Item__c>();
        try{
            for(Quotation__c quotation:quotations){
                for(Integer i=0;i<noOfQuotationLineItemsPerQuotation;i++){
                    quotationLineItems.add(new Quotation_Line_Item__c(PO_Product__c=desiredProducts[i].Id, PriceBook_Product__c=priceBooks[i].Id, Product__c=products[i].Id, Quantity__c=35, Quotation__c=quotation.Id));
                }
            }
            insert quotationLineItems;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return quotationLineItems;
    }
    
    public static List<ProformaInvoice__c> createProformaInvoices(List<Opportunity> opportunities,Integer noOfProformaInvoices){
        List<ProformaInvoice__c> proformaInvoices=new List<ProformaInvoice__c>();
        try{
            for(Integer i=0;i<noOfProformaInvoices;i++){
                proformaInvoices.add(new ProformaInvoice__c(Opportunity__c=opportunities[i].Id));
            }
            insert proformaInvoices;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return proformaInvoices;
    }
    
    public static List<Proforma_Invoice_Line_Item__c> createProformaInvoiceLineItems(List<Product2> products,List<ProformaInvoice__c> proformaInvoices,Integer noOfProformaInvoices){
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems=new List<Proforma_Invoice_Line_Item__c>();
        try{
            for(ProformaInvoice__c proformaInvoice:proformaInvoices){
                for(Integer i=0;i<noOfProformaInvoices;i++){
                    proformaInvoiceLineItems.add(new Proforma_Invoice_Line_Item__c(Price__c=125.0, Product__c=products[i].Id, ProformaInvoice__c=proformaInvoice.Id, Quantity__c=25, Required_Quantity__c=12));
                }
            }
            insert proformaInvoiceLineItems;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return proformaInvoiceLineItems;
    }
    
    public static List<Invoice__c> createInvoices(List<Opportunity> opportunities,Integer noOfInvoices){
        List<Invoice__c> invoices=new List<Invoice__c>();
        try{
            for(Integer i=0;i<noOfInvoices;i++){
                invoices.add(new Invoice__c(Docket_Number__c='TestDocket'+i+1,Date__c=Date.today(), Opportunity__c=opportunities[i].Id));
            }
            insert invoices;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return invoices;
    }
    
    public static List<Invoiced_Items__c> createInvoiceLineItems(List<Opportunity> opportunities,List<Invoice__c> invoices,Integer noOfInvoiceLineItemsPerInvoice){
        List<Invoiced_Items__c> invoiceLineItems=new List<Invoiced_Items__c>();
        try{
            for(Invoice__c invoice:invoices){
                for(Integer i=0;i<noOfInvoiceLineItemsPerInvoice;i++){
                    invoiceLineItems.add(new Invoiced_Items__c(Invoice__c=invoice.Id, Opportunity__c=opportunities[i].Id));
                }
            }
            insert invoiceLineItems;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return invoiceLineItems;
    }
    
    public static List<Invoiced_Product__c> createInvoiceProducts(List<Invoiced_Items__c> invoicedItems,List<Product2> products,List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems, Integer noOfInvoiceProducts){
        List<Invoiced_Product__c> invoiceProducts=new List<Invoiced_Product__c>();
        try{
            for(Invoiced_Items__c invoicedItem:invoicedItems){
                for(Integer i=0;i<noOfInvoiceProducts;i++){
                    invoiceProducts.add(new Invoiced_Product__c(Invoiced_Item__c=invoicedItem.Id, Product__c=products[i].Id, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItems[i].Id, Quantity__c=20, Unit_Price__c=250.00));
                }
            }
            insert invoiceProducts;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return invoiceProducts;
    }
    
    public static List<Projected_Stock__c> createProjectedStock(List<Product2> products, List<ProformaInvoice__c> proformaInvoices, Integer noOfProjectedStockObjs){
        List<Projected_Stock__c> projectedStockObjs=new List<Projected_Stock__c>();
        try{
            for(Integer i=0;i<noOfProjectedStockObjs;i++){
                projectedStockObjs.add(new Projected_Stock__c(Product__c = products[i].Id, ProformaInvoice__c=proformaInvoices[i].Id,Required_Quantity__c=50));
            } 
            insert projectedStockObjs;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return projectedStockObjs;
    } 
    
    public static List<Warehouse__c> createWarehouses(Integer noOfWarehouses){
        List<Warehouse__c> warehouses=new List<Warehouse__c>();
        try{
            for(Integer i=0;i<noOfWarehouses;i++){
                warehouses.add(new Warehouse__c(Name='Test Warehouse'+i+1,Warehouse_Street__c='test'));
            } 
            insert warehouses;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return warehouses;
    }
    
    public static List<Warehouse_Line_Item__c> createWarehouseLineItems(List<Warehouse__c> warehouses,List<Product2> products, Integer noOfWarehouseLineItemsPerWarehouse){
        List<Warehouse_Line_Item__c> warehouseLineItems=new List<Warehouse_Line_Item__c>();
        try{
            for(Warehouse__c warehouse:warehouses){
                for(Integer i=0;i<noOfWarehouseLineItemsPerWarehouse;i++){
                    warehouseLineItems.add(new Warehouse_Line_Item__c(Product__c=products[i].Id, Current_Stock__c=100, Warehouse__c=warehouse.Id));
                }
            }
            insert warehouseLineItems;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return warehouseLineItems;
    }
    
    public static List<Alloted_Stock__c> createBookedStocks(List<Warehouse__c> warehouses,List<Warehouse_Line_Item__c> warehouseProducts,List<ProformaInvoice__c> proformaInvoices,List<Product2> products, Integer noOfBookedStockObjs){
         List<Alloted_Stock__c> bookedStockObjs=new List<Alloted_Stock__c>();
        try{
            for(Integer i=0;i<noOfBookedStockObjs;i++){
                bookedStockObjs.add(new Alloted_Stock__c(Warehouse_Product__c=warehouseProducts[i].Id,ProformaInvoice__c=proformaInvoices[i].Id,Warehouse__c=warehouses[i].Id,Product__c=products[i].Id));
            } 
            insert bookedStockObjs;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return bookedStockObjs;
    }
    
    public static List<Bank_Info__c> createBankInfoEntries(Integer noOfBankObjs){
         List<Bank_Info__c> bankInfoObjs=new List<Bank_Info__c>();
        try{
            for(Integer i=0;i<noOfBankObjs;i++){
                bankInfoObjs.add(new Bank_Info__c(Bank_Details__c='Test Bank Detail'+i+1,CurrencyISOCode='USD'));
            } 
            insert bankInfoObjs;
        }catch(DmlException e){
            System.debug('DMlException'+e);
        }catch(Exception e){
            System.debug('Exception'+e);
        }
        return bankInfoObjs;
    }
    
}