public class DesiredProductsSelector extends fflib_SObjectSelector implements IDesiredProductsSelector {
    
    private static IDesiredProductsSelector instance = null;
    
    public static IDesiredProductsSelector getInstance() {
        
        if(instance == null) {
            instance = (IDesiredProductsSelector)Application.selector().newInstance(PO_Product__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            PO_Product__c.Id,
                PO_Product__c.Name,
                PO_Product__c.RecordTypeId,
                PO_Product__c.Product__c, 
                PO_Product__c.Quantity__c,
                PO_Product__c.Opportunity__c,
                PO_Product__c.Retail_Price__c, 
                PO_Product__c.Unit__c, 
                PO_Product__c.Lead_Time_In_Days__c};
                    }
    
    public Schema.SObjectType getSObjectType() {
        return PO_Product__c.sObjectType;
    }
    
    public List<PO_Product__c> selectById(Set<ID> idSet) {
        return (List<PO_Product__c>) selectSObjectsById(idSet);
    }
    
    public List<PO_Product__c> getRelatedDesiredProductsByOpportunityIds(Set<Id> opportunityIds) {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('RecordType.Name');
        query.setCondition('Opportunity__c IN :opportunityIds');
        return (List<PO_Product__c>) Database.query( query.toSOQL() );
    }
    
    public List<PO_Product__c> selectDesiredProductsByIds(Set<Id> desiredProducts) {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('RecordType.Name');
        query.selectField('Product__r.Name');
        query.selectField('Opportunity__r.AccountId');
        query.selectField('Product__r.Image__c');
        query.selectField('Product__r.GST__c');
        query.setCondition('Id IN: desiredProducts');
        return (List<PO_Product__c>) Database.query( query.toSOQL() );
    }

}