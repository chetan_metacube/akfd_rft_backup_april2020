@isTest
public class QuotationPDFControllerTest {

    @testSetup static void setup() {
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        MockData.createQuotation(opportunities, 1, desiredProducts, priceBooks);
    }
    
    @isTest
    public static void testQuotationPDFControllerMethod() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        Quotation__c quotation = [SELECT Id FROM Quotation__c WHERE Opportunity__c = :opportunityId LIMIT 1];
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(quotation);
        QuotationPDFController quotationPdfObj = new QuotationPDFController(stdCtrl);
    }
    
    @isTest
    public static void testQuotationPDFControllerForRetailProcessMethod() {
		Test.startTest();
        List<Account> accountMerchantObj = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Company/Consignor' LIMIT 1];
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'Customer' LIMIT 1];
        List<Contact> contactObj = [SELECT Id, Name FROM Contact WHERE AccountId = :accounts[0].Id  LIMIT 1];
        Lead_Source__c leadSourceObj = [SELECT Id, Name FROM Lead_Source__c LIMIT 1];
        Bank_Info__c bank = new Bank_Info__c(Account__c = accountMerchantObj[0].Id, CurrencyIsoCode='INR',Bank_Details__c='<p><span style="font-size: 14px; color: rgb(43, 40, 38);">This is Indian Currency</span></p>');
        insert bank;
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Retail');
    	System.debug('opportunities'+opportunities[0]);
        System.debug([SELECT RecordType.Name From Opportunity WHERE Id = :opportunities[0].Id]);
        List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        List<Quotation__c> quotations = MockData.createQuotation(opportunities, 1, desiredProducts, priceBooks);
        System.debug('quotations'+quotations);
        //Quotation__c quotation = [SELECT Id FROM Quotation__c WHERE Opportunity__c = :opportunities[0].id LIMIT 1];
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(quotations[0]);
        QuotationPDFController quotationPdfObj = new QuotationPDFController(stdCtrl);
        Test.stopTest();
    }
    
    @isTest
    public static void testRedirectMethod() {
        Opportunity currentOpportunity = [SELECT Id, AccountId, Merchant__c FROM Opportunity LIMIT 1];
        Quotation__c quotation = [SELECT Id, Effective_Start_Date__c FROM Quotation__c WHERE Opportunity__c = :currentOpportunity.Id LIMIT 1];
        Bank_Info__c bank = new Bank_Info__c(Account__c = currentOpportunity.Merchant__c, CurrencyIsoCode='INR',Bank_Details__c='<p><span style="font-size: 14px; color: rgb(43, 40, 38);">This is Indian Currency</span></p>');
        insert bank;
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(quotation);
        QuotationPDFController quotationPdfObj = new QuotationPDFController(stdCtrl);
        //quotationPdfObj.opportunityObj = currentOpportunity;
        System.assert(quotationPdfObj.redirect() != null);
    }
    
    @isTest
    public static void testgoBackMethod() {
        Opportunity currentOpportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Quotation__c quotation = [SELECT Id, Effective_Start_Date__c FROM Quotation__c WHERE Opportunity__c = :currentOpportunity.Id LIMIT 1];
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(quotation);
        QuotationPDFController quotationPdfObj = new QuotationPDFController(stdCtrl);
        //quotationPdfObj.opportunityObj = currentOpportunity;
        System.assert(quotationPdfObj.goBack() != null);
    }
}