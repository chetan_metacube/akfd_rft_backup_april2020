public class Accounts extends fflib_SObjectDomain {
    
    public Accounts(List<Account> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Accounts(sObjectList);
        }
    }
    
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        List<PriceBook_Product__c> priceBooks = [SELECT Id, Customer__c FROM PriceBook_Product__c WHERE Customer__c =: records];
        Map<Id,PriceBook_Product__c> accountSpecificPriceBookMap = new Map<Id, PriceBook_Product__c>();
        for(PriceBook_Product__c pricebook : priceBooks) {
            accountSpecificPriceBookMap.put(pricebook.Customer__c, pricebook);
        }
        
        // If Pricebook already created on account of previous currency then preventing from Updating Account Currency
        for(Account acc : (List<Account>)records) {
            if(!((Account)existingRecords.get(acc.Id)).CurrencyIsoCode.equals(acc.CurrencyIsoCode) && accountSpecificPriceBookMap.containsKey(acc.Id)) {
                acc.addError('Account Currency cannot be changed as Pricebook already exists.');
            }
        }
    }
    
}