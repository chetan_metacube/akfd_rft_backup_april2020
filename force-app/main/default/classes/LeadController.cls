public class LeadController {
    //Method to get existing Lead Data and picklist values
    @AuraEnabled
    public static String getData(Id leadId) {
        String result = '';
        Map<String, PicklistWrapper> leadStatusPicklist = getLeadStatusValues();
        //Check if exisitng lead or new lead
        if(String.isNotBlank(leadId) ) {
            //Check if valid lead id
            if('Lead'.equals(leadId.getSObjectType().getDescribe().getName())) {
                List<UserRecordAccess> userRecordsAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :leadId];
                //Check if user has edit access to the record
                if(userRecordsAccess != null && userRecordsAccess[0].HasEditAccess) {
                    try{
                        List<Lead> leadRecords = LeadsService.getLeadsById(new Set<id> {leadId}); 
                        if(!'Qualified'.equals(leadRecords.get(0).Status)) {
                            leadStatusPicklist.remove('Qualified');
                        }
                        leadStatusPicklist.get(leadRecords.get(0).Status).selected = true;
                        Data leadData = new Data(leadRecords.get(0), JSON.serialize(leadStatusPicklist.values()));
                        result = JSON.serialize(leadData);
                    } catch(Exception e) {
                        throw new AuraHandledException(e.getMessage());
                    }
                } else {
                    throw new AuraHandledException('Insufficient Privileges.');
                } 
            } else {
                throw new AuraHandledException('Invalid Lead Id !!!');
            }
        } else {
            leadStatusPicklist.remove('Qualified');
            leadStatusPicklist.get('New').selected = true;
            Data leadData = new Data(null, JSON.serialize(leadStatusPicklist.values()));
            result = JSON.serialize(leadData);
        }
        return result; 
    }
    
    //Method to upsert the lead record
    @AuraEnabled
    public static Lead saveLead(String leadObject) {
        Lead leadObj = (Lead)System.JSON.deserialize(leadObject, Lead.Class);
        Lead leadRecord;
        String errorMessage='';
        //check if lead record received is not null
        if(leadObj != null) {
            //check if lead title is not blank
            if(String.isEmpty(leadObj.Title)) {
                errorMessage='Please add Lead Title.';
            }
            //check if lead source is not null
            if(leadObj.Lead_Source__c == null) {
                errorMessage+='\n Please add Lead Source.';
            }
            //check if lead receipt date is not null
            if(leadObj.Lead_Receipt_Date__c == null) {
                errorMessage+='\n Please add Lead Receipt Date.';
            }
            //check if lead account is not null
            if(leadObj.Account__c == null) {
                errorMessage+='\n Please add Account.';
            }
            //check if lead contact is not null
            if(leadObj.Contact__c == null) {
                errorMessage+='\n Please add Contact.';
            }
            
            if(!String.isEmpty(errorMessage)) {
                throw new AuraHandledException(errorMessage);
            } else {
                try {
                    leadRecord = LeadsService.upsertLeads(new List<Lead> {leadObj}).get(0);
                } catch(AKFDException e) {
                    throw new AuraHandledException(e.getMessage());
                }
            }
        } else {
            throw new AuraHandledException('Invalid Lead Record!');
        }
        return leadRecord;
    }
    
    //Method to insert opportunity/ converted lead
    @AuraEnabled
    public static Opportunity createOpportunity(String lead, String opportunityName) {
        List<Opportunity> convertedOpportunities;
        Lead leadRecord;
        String errorMessage = '';
        
        if(String.isEmpty(lead)) {
            throw new AuraHandledException('Lead record should not be null.');
        } else {
            leadRecord = (Lead)System.JSON.deserialize(lead, Lead.Class);
        }
        
        if(String.isEmpty(opportunityName)) {
            errorMessage = 'Opportunity name should not be blank.';
        }
        
        if(leadRecord.Merchant__c == null) {
            errorMessage += '\n Company should not be blank.';
        }
        
        if(!String.isEmpty(errorMessage)) {
            throw new AuraHandledException(errorMessage);
        } else {
            try {
                convertedOpportunities = LeadsService.convertLeadToOpportunity(new List<LeadsService.LeadConvertWrapper>{new LeadsService.LeadConvertWrapper(leadRecord, opportunityName)});
                
                if(convertedOpportunities == null || convertedOpportunities.isEmpty()) {
                    throw new AuraHandledException('Invalid Lead Record!');
                } else {
                    return convertedOpportunities.get(0);
                }
                
            } catch(AKFDException ex) {
                String exceptionMessage = ex.getMessage();
                if( exceptionMessage.contains('Opportunity with the same name already exists.')) { 
                    exceptionMessage = 'Opportunity with the same name already exists.';
                } 
                throw new AuraHandledException(exceptionMessage);
            }
        }
    }
    
    //Method to get the picklist values of lead object
    private static Map<String, PicklistWrapper> getLeadStatusValues() {
        //Map<String, String> pickListValues = new Map<String, String>();
        Schema.DescribeFieldResult leadStatusFieldResult = Lead.Status.getDescribe();
        List<Schema.PicklistEntry> statusPicklistEntries = leadStatusFieldResult.getPicklistValues();
        Map<String, PicklistWrapper> statusPicklistWrapper = new Map<String, PicklistWrapper>(); 
        for( Schema.PicklistEntry statusPicklistValue : statusPicklistEntries ) {
            //pickListValues.put(pickListVal.getLabel(), pickListVal.getValue());
            statusPicklistWrapper.put(statusPicklistValue.getLabel(), new PicklistWrapper(statusPicklistValue.getLabel(), statusPicklistValue.getValue()));
        } 
        return statusPicklistWrapper;
        //return JSON.serialize(picklistWrapper);
    }
    
    //Wrapper class to hold picklist values
    public class PicklistWrapper {
        public String label {get;set;}
        public String value {get;set;}
        public Boolean selected {get;set;}
        
        public PicklistWrapper(String label, String value) {
            this.label = label;
            this.value = value;
            this.selected = false;
        }
    }
    //Front end Wrapper to hold lead and status
    public class Data {
        public Lead lead {get;set;}
        public String status {get;set;}
        
        public Data(Lead lead, String status) {
            this.lead = lead;
            this.status = status;
        }
    }
    
}