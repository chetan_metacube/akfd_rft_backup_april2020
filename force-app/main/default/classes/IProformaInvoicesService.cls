public interface IProformaInvoicesService {
	List<ProformaInvoice__c> getProformaInvoicesWithLineItemsByOpportunityIds(Set<Id> opportunityIds);
      
    List<Id> generate(List<ProformaInvoicesService.ProformaInvoicesWrapper> proformaInvoiceWrappers);
}