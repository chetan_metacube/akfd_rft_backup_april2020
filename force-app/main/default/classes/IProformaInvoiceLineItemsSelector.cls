public interface IProformaInvoiceLineItemsSelector extends fflib_ISObjectSelector {
    List<Proforma_Invoice_Line_Item__c> selectByProformaInvoices(Set<Id> proformaInvoiceIds);
    
    List<Proforma_Invoice_Line_Item__c> selectByProformaInvoiceIdWhichHasBalancedQuantity(Set<Id> proformaInvoiceIds);
}