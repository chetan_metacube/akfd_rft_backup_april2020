public class TallyAPICallouts {
	@InvocableMethod
    public static void upsertProduct(List<ProductInput> productInputs) {
        system.debug('++++productInputs ' + productInputs);
        if(productInputs.size() == 1) {
            String productXml = MyXmlPageController.getProductXml(productInputs[0].product, productInputs[0].hsnTax);
            System.debug('getProductXml ' + productXml);
            if(String.isNotBlank(productXml)) {
                Futureapexcallout.productCalloutApex(String.valueOf(RequestBodyXML.APIType.PRODUCT), productInputs[0].product.Name, productXml);
            }
        }
    }
    
    public class ProductInput {
        
        @InvocableVariable
        public Product2 product;
        
        @InvocableVariable
        public HSN_Tax__c hsnTax;
        
    }
    
   
}