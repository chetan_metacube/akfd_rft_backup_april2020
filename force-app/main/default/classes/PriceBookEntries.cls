public class PriceBookEntries extends fflib_SObjectDomain {
    public PriceBookEntries(List<PriceBook_Product_Entry__c> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new PriceBookEntries(sObjectList);
        }
    }
    
    public override void onBeforeInsert() {
        Set<ID> pricebooks = new Set<ID>();
        
        for(PriceBook_Product_Entry__c priceBookEntry : (List<PriceBook_Product_Entry__c>)records){
            pricebooks.add(priceBookEntry.PriceBook_Product__c);
        }
        
        Map<Id, PriceBook_Product__c> pricebookProducts = new Map<Id, PriceBook_Product__c>([Select ID, CurrencyIsoCode, Unit__c from PriceBook_Product__c where ID = :pricebooks]);
        
        for(PriceBook_Product_Entry__c priceBookEntry : (List<PriceBook_Product_Entry__c>)records){
            priceBookEntry.CurrencyIsoCode = pricebookProducts.get(priceBookEntry.PriceBook_Product__c).CurrencyIsoCode;
            priceBookEntry.Unit_of_Price__c = pricebookProducts.get(priceBookEntry.PriceBook_Product__c).Unit__c;
        }
        
    }
  
}