public class ProjectedStocks extends fflib_SObjectDomain {
    public ProjectedStocks(List<Projected_Stock__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new ProjectedStocks(records);
        }
    }
    
    public override void onBeforeInsert() {
        Boolean hasError;
        for(Projected_Stock__c projectedStock : (List<Projected_Stock__c>) records) {
            hasError = false;
            if(projectedStock.Product__c == null) {
                hasError = true;
                projectedStock.Product__c.addError('Product is required.');
            }
            if(projectedStock.Required_Quantity__c == null) {
                hasError = true;
                projectedStock.Required_Quantity__c.addError('Required quantity is required.');
            }
           
            if(!hasError) {
                if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
                    if(projectedStock.ProformaInvoice__c != null) {
                        projectedStock.ProformaInvoice__c.addError('Action not allowed.');
                    }
                }
            }
        }
    }
    
    public override void onAfterInsert() {
        sendEmailNotification((List<Projected_Stock__c>) records);
    }
    
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) { 
        Map<Id, Projected_Stock__c> existingRecordsMap = (Map<Id, Projected_Stock__c>) existingRecords;
        List<Projected_Stock__c> inProductionProjectedStock = new List<Projected_Stock__c>();
        Boolean hasError;
        for(Projected_Stock__c projectedStock : (List<Projected_Stock__c>) records) {
            hasError = false;
            if(projectedStock.Product__c == null) {
                hasError = true;
                projectedStock.Product__c.addError('Product is required.');
            }
            if(projectedStock.Required_Quantity__c == null) {
                hasError = true;
                projectedStock.Required_Quantity__c.addError('Required quantity is required.');
            }
            if(!hasError) {
                if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
                    if(projectedStock.ProformaInvoice__c != null && existingRecordsMap.get(projectedStock.Id).ProformaInvoice__c == null) {
                        projectedStock.ProformaInvoice__c.addError('Action not allowed.');
                    } else if(projectedStock.ProformaInvoice__c == null && existingRecordsMap.get(projectedStock.Id).ProformaInvoice__c != null){
                        projectedStock.ProformaInvoice__c.addError('Action not allowed.');
                    } else if(projectedStock.ProformaInvoice__c != null && existingRecordsMap.get(projectedStock.Id).ProformaInvoice__c != null && projectedStock.ProformaInvoice__c != existingRecordsMap.get(projectedStock.Id).ProformaInvoice__c) {
                        projectedStock.ProformaInvoice__c.addError('Action not allowed.');
                    } else if(projectedStock.ProformaInvoice__c != null && projectedStock.Product__c != existingRecordsMap.get(projectedStock.Id).Product__c) {
                        projectedStock.Product__c.addError('Action not allowed.');
                    } else if(projectedStock.ProformaInvoice__c != null && projectedStock.Required_Quantity__c != existingRecordsMap.get(projectedStock.Id).Required_Quantity__c) {
                        projectedStock.Required_Quantity__c.addError('Action not allowed.');
                    } else {
                        if(projectedStock.status__c == 'In Production') {
                            if(projectedStock.Product__c != existingRecordsMap.get(projectedStock.Id).Product__c) {
                                projectedStock.Product__c.addError('Action not allowed.');
                            } else if(projectedStock.Required_Quantity__c != existingRecordsMap.get(projectedStock.Id).Required_Quantity__c) {
                                projectedStock.Required_Quantity__c.addError('Action not allowed.');
                            }
                        }
                    }
                }
            }
        }
    }
    
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        sendEmailNotification((List<Projected_Stock__c>) records);
    }
    
    public override void onBeforeDelete() {
        for(Projected_Stock__c projectedStock : (List<Projected_Stock__c>) records) {
            system.debug('projectedStock.ProformaInvoice__c ' + projectedStock.ProformaInvoice__c);
            if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
                if(projectedStock.ProformaInvoice__c != null) {
                    projectedStock.addError('Action not allowed.');
                } else if(projectedStock.Status__c == 'In Production') {
                    projectedStock.addError('Action not allowed.');
                }
            }
        }
    }
    
    private void sendEmailNotification(List<Projected_Stock__c> projectedStocks) {
        AKFD_Configuration__c appConfig = AKFD_Configuration__c.getOrgDefaults(); 
        String[] toAddresses = new String[] { appConfig.AKF_Process_Admin_Email__c };
            List<Id> productIds = new List<Id>();
        for(Projected_Stock__c projectedStock : projectedStocks) {
            productIds.add(projectedStock.Product__c);
        }
        
        Map<Id, Product2> products = new Map<Id, Product2>([SELECT Name FROM Product2 WHERE Id =: productIds]);
        Map<Id, Projected_Stock__c> projectedStocksCreatedForPI = new Map<Id, Projected_Stock__c>([SELECT Id, Name, ProformaInvoice__c, ProformaInvoice__r.Account__c, ProformaInvoice__r.Name, ProformaInvoice__r.Opportunity__r.Owner.Email, Product__r.Name,Product__c, Required_Quantity__c FROM Projected_Stock__c WHERE Id IN: projectedStocks]);
        System.debug('Projected Stocks ' + projectedStocksCreatedForPI);
        String proformaInvoiceName = '';
        String subject = 'Projected Stock has been added ';
        String body = 'Dear Manager,<br/>' + '<br/>New Projected quantity record has been added. <br />Here are the details:<br />';
        String opportunityOwnerEmail = '';
        for(Projected_Stock__c projectedStock : projectedStocksCreatedForPI.values()) {
            if(projectedStock.ProformaInvoice__c != null) {
                if(String.isEmpty(proformaInvoiceName)) {
                    proformaInvoiceName = projectedStock.ProformaInvoice__r.Name;
                }
                body += 'Product: ' + products.get(projectedStock.Product__c).Name 
                    + '<br/>Projected Quantity : ' + projectedStock.Required_Quantity__c + '<br/>';
                if(String.isEmpty(opportunityOwnerEmail)){
                    opportunityOwnerEmail = projectedStock.ProformaInvoice__r.Opportunity__r.Owner.Email;
                }
            } else {
                body += 'Product: ' + products.get(projectedStock.Product__c).Name 
                    + '<br/>Projected Quantity : ' + projectedStock.Required_Quantity__c + '<br/>';
            }
            
        }
        if(String.isEmpty(proformaInvoiceName)){
            body += 'This has been added by Inventory Manager';
        } else {
            subject +=' for '+proformaInvoiceName;
            body +='Above products have been added for '+proformaInvoiceName;
            if(!String.isEmpty(opportunityOwnerEmail)){
                toAddresses.add(opportunityOwnerEmail);
            }
        }
        if(!toAddresses.contains(null)) {
            emailManager.sendEmail(subject, body, toAddresses);
        }
    }
}