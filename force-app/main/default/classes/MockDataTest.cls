@isTest
public class MockDataTest {
    public static testMethod void testMockData() {
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        System.assert(true, accounts.size() == 1);
        List<Account> accountsConsignor = MockData.createAccounts('Company/Consignor', 1);
        System.assert(true, accountsConsignor.size() == 1);
        List<Contact> contacts = MockData.createContacts(accounts, 1);
        System.assert(true, contacts.size() == 1);
        List<Case> cases = MockData.createCases(1);
        System.assert(true, cases.size() == 1);
        List<Lead_Source__c> leadSources = MockData.createLeadSources(1);
        System.assert(true, leadSources.size() == 1);
        List<Lead> leads = MockData.createLeads(accounts, contacts, leadSources, 1);
        System.assert(true, leads.size() == 1);
        List<Opportunity> opportunities = MockData.createOpportunities(accounts, accountsConsignor, contacts, leadSources, 1);
        System.assert(true, opportunities.size() == 1);
        List<Product2> products = MockData.createProducts(1);
        System.assert(true, products.size() == 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, opportunities, products, 1);
        System.assert(true, desiredProducts.size() == 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        System.assert(true, priceBooks.size() == 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
        System.assert(true, accounts.size() == 1);
        List<Quotation__c> quotations = MockData.createQuotation(opportunities, 1);
        System.assert(true, quotations.size() == 1);
        List<Quotation_Line_Item__c> quotationLineItems = MockData.createQuotation(products, desiredProducts, quotations, priceBooks, 1);
        System.assert(true, quotationLineItems.size() == 1);
        List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, 1);
        System.assert(true, proformaInvoices.size() == 1);
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = MockData.createProformaInvoiceLineItems(products, proformaInvoices, 1);
        System.assert(true, proformaInvoiceLineItems.size() == 1);
        List<Invoice__c> invoices = MockData.createInvoices(opportunities, 1);
        System.assert(true, invoices.size() == 1);
        List<Invoiced_Items__c> invoiceLineItems = MockData.createInvoiceLineItems(opportunities, invoices, 1);
        System.assert(true, invoiceLineItems.size() == 1);
        List<Invoiced_Product__c> invoiceProducts = MockData.createInvoiceProducts(invoiceLineItems, products, proformaInvoiceLineItems, 1);
        System.assert(true, invoiceProducts.size() == 1);
        List<Projected_Stock__c> projectedStockObjs = MockData.createProjectedStock(products, proformaInvoices, 1);
        System.assert(true, projectedStockObjs.size() == 1);
        List<Warehouse__c> warehouses = MockData.createWarehouses(1);
        System.assert(true, warehouses.size() == 1);
        List<Warehouse_Line_Item__c> warehouseLineItems = MockData.createWarehouseLineItems(warehouses, products, 1);
        System.assert(true, warehouseLineItems.size() == 1);
        List<Alloted_Stock__c> bookedStockObjs = MockData.createBookedStocks(warehouses, warehouseLineItems, proformaInvoices, products, 1);
        System.assert(true, bookedStockObjs.size() == 1);
        List<Bank_Info__c> bankInfoObjs = MockData.createBankInfoEntries(1);
        System.assert(true, bankInfoObjs.size() == 1);
    }
}