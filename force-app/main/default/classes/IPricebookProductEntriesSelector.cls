public interface IPricebookProductEntriesSelector extends fflib_ISObjectSelector {
    
    List<Pricebook_Product_Entry__c> selectPricebookEntries();
}