@isTest
public class InvoiceListControllerTest {
    @testSetup
    static void testSetup() {
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id agentRecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent Contact').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        List<Account> acc=new List<Account>();
        Account accountCustomerObj=new Account(Name='AccountTest',RecordTypeId=customerRecordTypeIdAccount,CurrencyIsoCode='INR');
        Account accountMerchantObj=new Account(Name='AccountTest',RecordTypeId=merchantcustomerRecordTypeIdAccount);
        acc.add(accountCustomerObj);
        acc.add(accountMerchantObj);
        insert acc;
        
        List<Address__c> addresses = new List<Address__c>();
        Address__c billingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Billing', Countries__c = 'India', State__c = 'Rajasthan');
        Address__c shippingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Delivery', Countries__c = 'India', State__c = 'Rajasthan');
        addresses.add(billingAddress);
        addresses.add(shippingAddress);
        insert addresses;
        
        Contact contactObj=new Contact(FirstName='Contact',LastName='Test',RecordTypeId=agentRecordTypeIdContact,AccountId=accountCustomerObj.Id,Email='test@test.com');
        insert contactObj;
        Id customerRecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Opportunity oppObj=new Opportunity(Name='Test Opportunity',RecordTypeId=customerRecordTypeIdOpportunity,StageName='Client Setup',Merchant__c=accountMerchantObj.Id,AccountId=accountCustomerObj.Id,Contact_Person__c=contactObj.Id,CloseDate=Date.today(),CurrencyIsoCode='USD');
        insert oppObj;
        
        Product2 product=new Product2(Name='Test Product',Family='Furniture', Brand__c = 'AKFD', Retail_Price__c = 0.00);
        insert product;
        
        Product2 product1=new Product2(Name='Test Product1',Family='Furniture', Brand__c = 'AKFD', Retail_Price__c = 10.00);
        insert product1;
        
        PriceBook_Product__c priceBookObj=new PriceBook_Product__c(Customer__c=accountCustomerObj.Id,Product__c=product.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        insert priceBookObj;
        
        PriceBook_Product__c priceBookObj1=new PriceBook_Product__c(Customer__c=accountCustomerObj.Id,Product__c=product1.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        insert priceBookObj1;
        
        PriceBook_Product_Entry__c priceBookEntryObj=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        insert  priceBookEntryObj;
        
        PriceBook_Product_Entry__c priceBookEntryObj1=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj1.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        insert  priceBookEntryObj1;
        
        PO_Product__c desiredProductObj1=new PO_Product__c(Product__c=product.Id,Quantity__c=5,Opportunity__c=oppObj.Id, Discount_Percentage__c = 0.00);
        insert desiredProductObj1;
        
        PO_Product__c desiredProductObj2=new PO_Product__c(Product__c=product1.Id,Quantity__c=5,Opportunity__c=oppObj.Id, Discount_Percentage__c = 0.00);
        insert desiredProductObj2;
        
        ProformaInvoice__c proformaInvoice1 = new ProformaInvoice__c(Opportunity__c = oppObj.Id, Status__c = 'New', Conversion_Rate__c = 1.00);
        List<ProformaInvoice__c> proformaInvoices = new List<ProformaInvoice__c>();
        proformaInvoices.add(proformaInvoice1);
        insert proformaInvoices;
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem1 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj1.Product__c, Quantity__c = 10, Required_Quantity__c = 15, Price__c = 10.00, PO_Product__c = desiredProductObj1.Id);
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem2 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj2.Product__c, Quantity__c = 12, Required_Quantity__c = 20, Price__c = 20.00, PO_Product__c = desiredProductObj2.Id);
        
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = new List<Proforma_Invoice_Line_Item__c>();
        proformaInvoiceLineItems.add(proformaInvoiceLineItem1);
        proformaInvoiceLineItems.add(proformaInvoiceLineItem2);
        insert proformaInvoiceLineItems;
        
        proformaInvoice1.Status__c = 'Accepted';
        update proformaInvoice1;
        
        Warehouse__c warehouse = new Warehouse__c(Name = 'Test Warehouse');
        insert warehouse;
        
        Warehouse_Line_Item__c productWarehouse = new Warehouse_Line_Item__c(Product__c = product.Id, Current_Stock__c = 50, Warehouse__c = warehouse.Id);
        insert productWarehouse;
        
        Alloted_Stock__c allotedStock1 = new Alloted_Stock__c(Product__c = product.Id, Stock__c = 30, Warehouse__c = warehouse.Id, Warehouse_Product__c = productWarehouse.Id, ProformaInvoice__c = proformaInvoice1.Id);
        
        List<Alloted_Stock__c> allotedStocks = new List<Alloted_Stock__c>();
        allotedStocks.add(allotedStock1);
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        insert allotedStocks;
        
        oppObj = [Select Id, (Select Id, Name, Opportunity__c From Payment_Terms__r) From Opportunity Where Id =: oppObj.Id];
        List<Payment_Term__c> existingPaymentTerms = new List<Payment_Term__c>();
        for(Payment_Term__c term : oppObj.Payment_Terms__r) {
            existingPaymentTerms.add(term);
        }
        delete existingPaymentTerms;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = oppObj.Id, Payment_Percentage__c = 80.00, Dummy_Payment_Amount__c = 800.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = oppObj.Id, Payment_Percentage__c = 20.00, Dummy_Payment_Amount__c = 200.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);
        
        Invoice__c invoice = new Invoice__c(Docket_Number__c='TestDocket', Account__c = accountCustomerObj.Id, Billing_Address__c = billingAddress.Id, Delivery_Address__c = shippingAddress.Id, Date__c=Date.today(), Opportunity__c=oppObj.Id, Product_Liability_Insurance_Percentage__c = 1.00, Packing_Charges__c = 1.00, Shipping_Charges__c = 1.00, Trade_Discount__c = 0.00, Conversion_Rate_To_INR__c =45.00);
        insert invoice;
        
        Invoiced_Items__c invoiceItem = new Invoiced_Items__c(Invoice__c=invoice.Id, Opportunity__c=oppObj.Id);
        insert invoiceItem;
        
        Invoiced_Product__c invoicedProduct1 = new Invoiced_Product__c(Invoiced_Item__c=invoiceItem.Id, Product__c=proformaInvoiceLineItem1.Product__c, Proforma_Invoice_Line_Item__c=proformaInvoiceLineItem1.Id, Quantity__c=10, Unit_Price__c=250.00);
        List<Invoiced_Product__c> invoiceProducts=new List<Invoiced_Product__c>();
        invoiceProducts.add(invoicedProduct1);
        insert invoiceProducts;
        
        invoice.Status__c = 'Reviewed';
        update invoice;
        
    }
    
    static testMethod void testGetPaymentTerm() {
        Test.startTest();
        List<Payment_Term__c> terms = [Select Id From Payment_Term__c];
        Id termId = terms[0].Id;
        List<Payment_Term__c> terms1 = InvoiceListController.getPaymentTerm(termId);
        System.assertEquals(terms[0].Id, terms1[0].Id);
        Test.stopTest();
    }
    
    static testMethod void testGetInvoicedItems() {
        Test.startTest();
        Opportunity oppObj = [Select Id From Opportunity];
        Invoice__c invoice = [Select Id From Invoice__c];
        Invoiced_Items__c item = [Select Id From Invoiced_Items__c LIMIT 1];
        system.debug('invoice ' + invoice);
        system.debug('item ' + item);
        List<Invoiced_Items__c> items = InvoiceListController.getInvoicedItems(oppObj.Id);
        Test.stopTest();
        //System.assertEquals(oppObj.Id, items[0].Opportunity__c);
    }
    
    static testMethod void testCreateTransaction() {
        Test.startTest();
        List<Payment_Term__c> terms = [Select Id From Payment_Term__c];
        Payment_Transaction__c paymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[0].Id, Amount__c = 10000000000.00);
        String data = JSON.serialize(paymentTransaction);
        try {
            paymentTransaction = InvoiceListController.createTransaction(data);
        } catch(AuraHandledException e) {
            paymentTransaction = null;
        }
        System.assertEquals(null, paymentTransaction);
        paymentTransaction = new Payment_Transaction__c(Payment_Term__c = terms[0].Id, Amount__c = 1.00);
        data = JSON.serialize(paymentTransaction);
        paymentTransaction = InvoiceListController.createTransaction(data);
        System.assertNotEquals(null, paymentTransaction);
        Test.stopTest();
    }
    
}