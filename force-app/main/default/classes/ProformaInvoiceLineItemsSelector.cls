public class ProformaInvoiceLineItemsSelector extends fflib_SObjectSelector implements IProformaInvoiceLineItemsSelector {
    private static IProformaInvoiceLineItemsSelector instance = null;
    
    public static IProformaInvoiceLineItemsSelector getInstance() {
        
        if(instance == null) {
            instance = (IProformaInvoiceLineItemsSelector)Application.selector().newInstance(Proforma_Invoice_Line_Item__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            Proforma_Invoice_Line_Item__c.Id,
                Proforma_Invoice_Line_Item__c.Name,
                Proforma_Invoice_Line_Item__c.ProformaInvoice__c,
                Proforma_Invoice_Line_Item__c.Product__c,
                Proforma_Invoice_Line_Item__c.SKU_Number__c,
                Proforma_Invoice_Line_Item__c.Quantity__c,
                Proforma_Invoice_Line_Item__c.Invoiced_Quantity__c,
                Proforma_Invoice_Line_Item__c.Balanced_Quantity__c,
                Proforma_Invoice_Line_Item__c.ProformaInvoice__c,
                Proforma_Invoice_Line_Item__c.Required_Quantity__c,
                Proforma_Invoice_Line_Item__c.Unit__c,
                Proforma_Invoice_Line_Item__c.Price__c,
                Proforma_Invoice_Line_Item__c.Total_Price__c,
                Proforma_Invoice_Line_Item__c.Last_Updated_Quantity__c,
                Proforma_Invoice_Line_Item__c.Last_Updated_Price__c,
                Proforma_Invoice_Line_Item__c.GST__c 
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return 	Proforma_Invoice_Line_Item__c.sObjectType;
    }
    
    public List<Proforma_Invoice_Line_Item__c> selectById(Set<ID> idSet) {
        return (List<Proforma_Invoice_Line_Item__c>) selectSObjectsById(idSet);
    }
    
    public List<Proforma_Invoice_Line_Item__c> selectByProformaInvoices(Set<Id> proformaInvoiceIds) {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('PO_Product__r.Discount_Percentage__c');
        query.setCondition('ProformaInvoice__c IN :proformaInvoiceIds');
        return (List<Proforma_Invoice_Line_Item__c>) Database.query( query.toSOQL() );
    }
    
    public List<Proforma_Invoice_Line_Item__c> selectByProformaInvoiceIdWhichHasBalancedQuantity(Set<Id> proformaInvoiceIds) {
        fflib_QueryFactory query = newQueryFactory();
        
        query.selectField('Product__r.Name');
       
        query.setCondition('ProformaInvoice__c IN :proformaInvoiceIds AND Balanced_Quantity__c >= 0');
        return (List<Proforma_Invoice_Line_Item__c>) Database.query( query.toSOQL() );
    }
    
    
    
}