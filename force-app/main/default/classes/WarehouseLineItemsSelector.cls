public class WarehouseLineItemsSelector extends fflib_SObjectSelector implements IWarehouseLineItemsSelector {
    private static IWarehouseLineItemsSelector instance = null;
    
    public static IWarehouseLineItemsSelector getInstance() {
        
        if(instance == null) {
            instance = (IWarehouseLineItemsSelector)Application.selector().newInstance(Warehouse_Line_Item__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            Warehouse_Line_Item__c.Id,
                Warehouse_Line_Item__c.Warehouse__c, 
                Warehouse_Line_Item__c.Product__c,
                Warehouse_Line_Item__c.Current_Stock__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return Warehouse_Line_Item__c.sObjectType;
    }
    
    public List<Warehouse_Line_Item__c> selectByProductIdsWhichHasCurrentStock(Set<Id> productIds) {
        fflib_QueryFactory query = newQueryFactory();
        fflib_QueryFactory allotedStocksQueryFactory = new AllotedStocksSelector().addQueryFactorySubselect(query);
        
        allotedStocksQueryFactory.selectField('Product__r.Name');
        allotedStocksQueryFactory.selectField('Warehouse__r.Name');
        allotedStocksQueryFactory.selectField('ProformaInvoice__r.Name');
        
        allotedStocksQueryFactory.setCondition('ProformaInvoice__c != null');
        
        query.selectField('Warehouse__r.Compound_Location__c');
        query.selectField('Product__r.Name');
        query.selectField('Warehouse__r.Name');
        
        query.setCondition('Product__c IN :productIds AND Current_Stock__c > 0');
       
        
        query.setLimit( Limits.getLimitQueryRows() );
        return (List<Warehouse_Line_Item__c>) Database.query( query.toSOQL() );
    }
   
    
    public List<Warehouse_Line_Item__c> selectByProductAndWarehouse(Set<Id> productIds, Set<Id> warehouseIds) {
        fflib_QueryFactory query = newQueryFactory();
        
        query.setCondition('Product__c IN :productIds And Warehouse__c IN :warehouseIds');
        
        return (List<Warehouse_Line_Item__c>) Database.query( query.toSOQL() );
    }
	
    
}