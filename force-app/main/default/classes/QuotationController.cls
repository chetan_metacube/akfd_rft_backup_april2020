public class QuotationController {
    
    /*
* Method used in CreateQuotationLightning component
*/
    @AuraEnabled
    public static String createQuotation(String opportunityId, String desiredProducts) {
        List<QuotationsService.DesiredProduct> selectedProducts = new List<QuotationsService.DesiredProduct>();
        for(Object selectedProduct : (List<Object>)JSON.deserializeUntyped((desiredProducts))) {
            selectedProducts.add(new QuotationsService.DesiredProduct(selectedProduct));
        }
        QuotationsService.SelectedOpportunityProductsWrapper selectedOpportunityProducts = new QuotationsService.SelectedOpportunityProductsWrapper(opportunityId, selectedProducts);
        List<Quotation__c> quotationList = QuotationsService.createQuotation(new List<QuotationsService.SelectedOpportunityProductsWrapper>{selectedOpportunityProducts});
        return quotationList[0].Id;
    }
    
    
    
}