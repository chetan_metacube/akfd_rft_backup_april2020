public class InvoicedItemsSelector extends fflib_SObjectSelector implements IInvoicedItemsSelector {
    private static IInvoicedItemsSelector instance = null;
    
    public static IInvoicedItemsSelector getInstance() {
        
        if(instance == null) {
            instance = (IInvoicedItemsSelector)Application.selector().newInstance(Invoiced_Items__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            Invoiced_Items__c.Id,
                Invoiced_Items__c.Name,
                Invoiced_Items__c.Invoice__c,
                Invoiced_Items__c.Opportunity__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return Invoiced_Items__c.sObjectType;
    }
    
    public List<Invoiced_Items__c> selectInvoicedItemsWithInvoicedProductsByInvoiceIds(Set<Id> invoiceIds) {
        fflib_QueryFactory query = newQueryFactory();
        
        fflib_QueryFactory invoicedProductsQueryFactory = new InvoicedProductsSelector().addQueryFactorySubselect(query);
        
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.Price__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.Product__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.Invoiced_Quantity__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.Required_Quantity__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.ProformaInvoice__r.Opportunity__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.ProformaInvoice__r.All_Items_Total_Price__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.ProformaInvoice__r.Total_Including_Taxes__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.GST__c');
        invoicedProductsQueryFactory.selectField('Proforma_Invoice_Line_Item__r.Balanced_Quantity__c');
        invoicedProductsQueryFactory.selectField('PO_Product__r.Discount_Percentage__c');
        invoicedProductsQueryFactory.selectField('Product__r.GST__c');
        invoicedProductsQueryFactory.selectField('Invoiced_Item__r.Invoice__c');
        invoicedProductsQueryFactory.selectField('Invoiced_Item__r.Invoice__r.Name');
        
        query.selectField('Opportunity__r.Freight_Amount__c');        
        query.selectField('Opportunity__r.Packing_Percentage__c');        
        query.selectField('Invoice__r.Packing_Percentage__c');
        
        query.setCondition('Invoice__c IN :invoiceIds');
        
        query.setLimit( Limits.getLimitQueryRows() );
        
        return (List<Invoiced_Items__c>) Database.query(
            query.toSOQL()
        );
    }
}