public class PricebookProductsSelector extends fflib_SObjectSelector implements IPricebookProductsSelector {
	private static IPricebookProductsSelector instance = null;
    
    public static IPricebookProductsSelector getInstance() {
        
        if(instance == null) {
            instance = (IPricebookProductsSelector)Application.selector().newInstance(PriceBook_Product__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            PriceBook_Product__c.Id,
            PriceBook_Product__c.Name,
            PriceBook_Product__c.Product__c,
            PriceBook_Product__c.Customer__c,
            PriceBook_Product__c.Effective_Start_Date__c,
            PriceBook_Product__c.Effective_End_Date__c,
            PriceBook_Product__c.CurrencyIsoCode
            
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return PriceBook_Product__c.sObjectType;
    }
    
    public List<Pricebook_Product__c> selectPricebooksWithPricebookEntriesByAccountAndProductIds(Set<Id> accountIds, Set<Id> productIds) {
        fflib_QueryFactory query = newQueryFactory();
        fflib_QueryFactory pricebookProductEntriesQueryFactory = new PricebookProductEntriesSelector().addQueryFactorySubselect(query);
        query.selectField('Product__r.Name');
        query.selectField('Product__r.Image__c');
        query.setCondition('customer__c IN :accountIds AND Product__c IN :productIds');
        return Database.query(
            query.toSOQL()
        );
    }
}