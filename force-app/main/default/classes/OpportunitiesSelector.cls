public class OpportunitiesSelector extends fflib_SObjectSelector implements IOpportunitiesSelector {
    
    private static IOpportunitiesSelector instance = null; 
    
    public static IOpportunitiesSelector getInstance() {
        
        if(instance == null) {
            instance = (IOpportunitiesSelector)Application.selector().newInstance(Opportunity.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Opportunity.Id,
            Opportunity.Name,
            Opportunity.Contact__c,
            Opportunity.Lead_Source__c,
            Opportunity.Merchant__c,
            Opportunity.OwnerId,
            Opportunity.AccountId,
            Opportunity.RecordTypeId,
            Opportunity.CurrencyIsoCode,
            Opportunity.Transportation_Mode__c,
            Opportunity.Freight_Amount__c,
            Opportunity.Freight_Amount__c,
            Opportunity.Packing_Percentage__c
            
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return Opportunity.sObjectType;
    }
    
    // This method is not used anywhere and can be removed.
    public List<Opportunity> selectById(Set<ID> idSet) {
        return (List<Opportunity>) selectSObjectsById(idSet);
    }
    
    //Method to get opportunities by opportunity Ids
    public List<Opportunity> selectOpportunitiesByIds(Set<Id> opportunityIds) {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('RecordType.Name');
        query.selectField('Owner.Name');
        query.selectField('Owner.Email');
        query.selectField('Account.Name');
        query.setCondition('Id IN :opportunityIds');
        return Database.query(
            query.toSOQL()
        );
    }
    
     
    // Method to get opportunities by specified fields
    public Opportunity selectOpportunityWithPIsById(Id oppportunityId) {
        fflib_QueryFactory query = newQueryFactory(false);
        
        fflib_QueryFactory proformaInvoicesQueryFactory = new ProformaInvoicesSelector().addQueryFactorySubselect(query);
        proformaInvoicesQueryFactory.setCondition('Status__c In (\'Accepted\', \'Revised\')');
        
        query.selectField('Account.CurrencyIsoCode');
        query.selectField('Owner.Name');
        query.selectField('Account.Name');
        query.selectField('RecordType.Name');
        query.selectField('Owner.Email');
        
        query.setCondition('Id = :oppportunityId');
        query.setLimit( Limits.getLimitQueryRows() );
        return (Opportunity) Database.query( query.toSOQL() );
    }
    
    // Method to get opportunities by specified fields
    public Opportunity getOpportunityWithByIdFieldSet(Id oppportunityId, Set<String> fieldSet) {
        fflib_QueryFactory query = newQueryFactory(false);
        query.selectFields( fieldSet );
        query.setCondition('Id = :oppportunityId');
        return (Opportunity) Database.query( query.toSOQL() );
    }

     
}