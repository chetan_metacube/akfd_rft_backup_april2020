public class QuotationPDFController {
//All Common Fields
    public Opportunity opportunityObj {get; private set;}
    public Quotation__c quotation {get; private set;}
    public List<Quotation_Line_Item__c> quotationLineItems {get; private set;}
    public Account customerAccount {get; private set;}
    public String currencySymbol {get; private set;}
    private String accessType = 'Preview';
    public List<Payment_Term__c> paymentTerms{get;set;}
    public String renderAs {get;set;}
    public Bank_Info__c bankingDetails {get; private set;}
    public String invalidQuotationProducts {get; private set;}
    
    
    //Project Process Related Fields
    //public Account consignorAccount {get; private set;}
    
    // Retail + Project Process Related Fields
    public List<String> bankingDetail {get; private set;}
    public Boolean showDiscountColumn{get;set;}
    public Decimal extraChargesTaxPercentage{get;set;}
    public Boolean isStateTax {
        get{
            return opportunityObj.Billing_Address__r.State_Province__c == opportunityObj.Merchant__r.BillingState;       
        }
        set;
    }
    
    
    //Project + Export Process Related Fields
    public Map<Id, PriceBook_Product__c> priceBookProductMap{get;set;}
    
    //Export Process Related Fields
    public String billingName {get; private set;}
    
    public QuotationPDFController(ApexPages.StandardController controller) {
        quotation = (Quotation__c) controller.getRecord();
       	quotation = QuotationsSelector.getInstance().selectQuotationsByIds(new Set<Id>{quotation.Id})[0];
        //quotation = [SELECT Id, Name, Opportunity__c, RecordType.Name, CurrencyIsoCode, Effective_Start_Date__c, End_Date__c, Expected_Delivery_Time__c, Delivery_Remarks__c, Remarks__c, Freight__c, Opportunity__r.RecordType.Name, Opportunity__r.AccountId FROM Quotation__c WHERE Id =: quotation.Id];
       if (quotation.End_Date__c == null || quotation.Effective_Start_Date__c == null) {
            addErrorOnVfPage('Quotation Start/End Date is invalid.');
        } else { 
            if(quotation.RecordType.Name == 'Retail' || quotation.RecordType.Name == 'Project') {
                showDiscountColumn = false;
            }
            init();
        }
        if (!ApexPages.hasMessages()) {
            renderAs='PDF';
        } else { 
            renderAs='';
        }
    }
    
    private void init() {
        try {
            quotationLineItems = [SELECT Id, Name, PO_Product__c, Product__c, PriceBook_Product__c, Quantity__c, Quotation__c, Comments__c, Unit__c, Unit_Price__c, Total_Price__c, 
                                  Quotation__r.Opportunity__r.Account.Name, PO_Product__r.Product__r.Image__c, Product__r.Name, Product__r.Dimension_Length__c, Product__r.Dimension_Width__c, 
                                  Product__r.Dimension_Height__c, Product__r.Image__c, Product__r.Description__c, Product__r.Dimension_Unit__c, Product__r.Material__c,Product__r.Finish_1__c, Product__r.Finish_2__c, 
                                  Product__r.Finish_3__c, Product__r.Finish_4__c, Product__r.Product_SKU__c, Product__r.ProductCode, PO_Product__r.HST_Code__c, PO_Product__r.Product_Location__c,  PO_Product__r.Discount_Percentage__c, 
                                  PO_Product__r.Product__r.Id,  Product__r.HSN_Tax__r.Name,  PO_Product__r.Product__r.Name, PO_Product__r.Product__r.Material__c, PO_Product__r.Product__r.Description__c, PO_Product__r.Product__r.ProductCode,
                                  PO_Product__r.Product__r.Product_SKU__c, PriceBook_Product__r.Name, PriceBook_Product__r.Sampling_Charges__c, GST__c FROM Quotation_Line_Item__c WHERE Quotation__c =: quotation.Id];
            List<Id> priceBookIds = new List<Id>();
            for(Quotation_Line_Item__c quotationLineItem : quotationLineItems) {
                // All
                if(quotationLineItem.Product__r.Image__c != null) {
                    quotationLineItem.Product__r.Image__c = quotationLineItem.Product__r.Image__c.replace('alt', 'height="100" width="100" alt');
                }
                
                // Project and retail
                if(quotation.RecordType.Name == 'Retail' || quotation.RecordType.Name == 'Project') {
                    if(quotationLineItem.PO_Product__r.Discount_Percentage__c != 0.00) {
                        showDiscountColumn = true;
                    }
                }
                
                //Project and Export 
                if(quotation.RecordType.Name == 'Export' || quotation.RecordType.Name == 'Project') {
                    priceBookIds.add(quotationLineItem.PriceBook_Product__c);
                }
                
                if(quotationLineItem.Product__r.Material__c != null) {
                    quotationLineItem.Product__r.Material__c = breakMultiSelect(quotationLineItem.Product__r.Material__c);
                }
                
            }
            System.debug('priceBookIds ' + priceBookIds);
            // Export and Project
            if(quotation.RecordType.Name == 'Export' || quotation.RecordType.Name == 'Project') {
                priceBookProductMap = new Map<Id, PriceBook_Product__c>([SELECT Id, Product__r.Name, Effective_Start_Date__c, Effective_End_Date__c, 
                                                                         (SELECT Id, Min_Quantity__c, Max_Quantity__c, Price__c, Lead_Time_for_MOQ__c FROM PriceBook_Product_Entries__r) FROM PriceBook_Product__c WHERE Id IN: priceBookIds]);
                for(PriceBook_Product__c priceBook : priceBookProductMap.values()) {
                    if((quotation.Effective_Start_Date__c != null && quotation.Effective_Start_Date__c >= priceBook.Effective_Start_Date__c) && (quotation.End_Date__c !=null && quotation.End_Date__c <= priceBook.Effective_End_Date__c)) {
                        invalidQuotationProducts = '';
                    } else if(String.isBlank(invalidQuotationProducts)) {
                        invalidQuotationProducts = priceBook.Product__r.Name;
                    } else {
                        invalidQuotationProducts += ', ' + priceBook.Product__r.Name;
                    }
                }
            }
            customerAccount = [SELECT Id, Name, Pre_Carriage_By__c, Port_of_Loading__c, Port_Of_Discharge__c, Final_Destination__c, Place_Of_Receipt__c,
                               Country_Of_Final_Origin__c, Mode_of_Payment__c, Email__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, 
                               ShippingCountry, Phone, BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry, GST_Id__c, Terms_and_Conditions__c, 
                               CurrencyIsoCode FROM Account WHERE Id =:quotation.Opportunity__r.AccountId];
            opportunityObj = [SELECT Id, Name, Bank_Code__c, CloseDate, AccountId, Testing_Charges_Included__c, Contact_Person__c, PO_No__c, PO_Date__c, Incoterms__c,
                              CurrencyIsoCode, Freight__c, Freight_Amount__c, SAC_Code__c, Billing_Address__c, Delivery_Address__c, Billing_Address__r.GST_Id__c, 
                              Billing_Address__r.Name, Billing_Address__r.Street__c, Billing_Address__r.City__c, Billing_Address__r.State_Province__c, Billing_Address__r.Country__c, 
                              Billing_Address__r.Zip_Postal_Code__c, Billing_Address__r.State_Code__c, Delivery_Address__r.Name, Delivery_Address__r.Street__c, 
                              Delivery_Address__r.City__c, Delivery_Address__r.State_Province__c, Delivery_Address__r.Country__c, Delivery_Address__r.Zip_Postal_Code__c, 
                              Delivery_Address__r.State_Code__c, Notify_Address__r.Name, Notify_Address__r.Street__c, Notify_Address__r.City__c, 
                              Notify_Address__r.State_Province__c, Notify_Address__r.Country__c, Notify_Address__r.Zip_Postal_Code__c, 
                              Notify_Address__r.State_Code__c, Buyer_Address__r.Name, Buyer_Address__r.Street__c, Buyer_Address__r.City__c, 
                              Buyer_Address__r.State_Province__c, Buyer_Address__r.Country__c, Buyer_Address__r.Zip_Postal_Code__c, 
                              Buyer_Address__r.State_Code__c, Vendor_Code__c, Packing_Percentage__c, Transportation_Mode__c, 
                              Merchant__r.Name, Merchant__r.Email__c, Merchant__r.ShippingStreet, Merchant__r.ShippingCity, Merchant__r.ShippingPostalCode, Merchant__r.ShippingState, 
                              Merchant__r.ShippingCountry, Merchant__r.BillingStreet, Merchant__r.BillingCity, Merchant__r.BillingPostalCode,
                              Merchant__r.BillingState, Merchant__r.BillingCountry, Merchant__r.Website, Merchant__r.Company_Logo__c, Merchant__r.Phone,
                              Merchant__r.GST_Id__c, Merchant__r.PAN_Number__c, Merchant__r.Exporter_s_IEC_Code__c, Merchant__r.Exporters_s_Ref__c, 
                              Merchant__r.CENTRAL_EXCISE__c, Merchant__r.Export_Under_UT_1__c, Merchant__r.CIN_No__c, Contact_Person__r.RecordType.Name, 
                              Contact_Person__r.Company_Name__c, Contact_Person__r.Company_Street__c, Contact_Person__r.Company_City__c, 
                              Contact_Person__r.Company_State__c, Contact_Person__r.Company_Country__c, Contact_Person__r.Company_Zip_Postal_Code__c, 
                              Contact_Person__r.Name, Contact_Person__r.MailingStreet, Contact_Person__r.MailingCity, Contact_Person__r.MailingPostalCode, 
                              Contact_Person__r.MailingState, Contact_Person__r.MailingCountry, Contact_Person__r.Phone, Contact_Person__r.Email, 
                              Contact__r.Name, Contact__r.Phone, Contact__r.Email, Notify_Contact__r.Name, Notify_Contact__r.Phone, Notify_Contact__r.Email, 
                              Buyer_Contact__r.Name, Buyer_Contact__r.Phone, Buyer_Contact__r.Email, Owner.Name, Owner.Email, Owner.Phone,
                              Text_If_Packing_Freight_Charges_Is_Zero__c FROM Opportunity WHERE Id =: quotation.Opportunity__c];
            paymentTerms = [SELECT Name, Balance_Amount__c, Due_Date__c, Payment_Amount__c,
                            Payment_Percentage__c, Payment_Status_New__c, Total_Transaction_Amount__c FROM Payment_Term__c WHERE Opportunity__c =: opportunityObj.Id ORDER BY CreatedDate];
            System.debug('paymentTerms ' + paymentTerms);
            if(quotation.CurrencyIsoCode == 'INR') {
                currencySymbol = 'INR';
            } else {
                currencySymbol = CurrencyUtils.getSymbol(quotation.CurrencyIsoCode);
            }
            
            accessType = ApexPages.currentPage().getParameters().get('accessType');
            bankingDetails = [SELECT Bank_Details__c FROM Bank_Info__c WHERE CurrencyIsoCode = :opportunityObj.CurrencyIsoCode AND Account__c = :opportunityObj.Merchant__c LIMIT 1];
            if(quotation.RecordType.Name == 'Retail' || quotation.RecordType.Name == 'Project') {
                AKFD_Extra_Charges_Tax_Percentage__c akfdExtraChargesTaxPercentage = AKFD_Extra_Charges_Tax_Percentage__c.getOrgDefaults();
                extraChargesTaxPercentage = akfdExtraChargesTaxPercentage.Tax_Percentage__c.setScale(2);
            }
        } catch(Exception e) {
            System.debug('Exception'+e);
            String errorMessage = '';
            if(bankingDetails == null) {
                addErrorOnVfPage('Bank Info is not available.');
            } else {
                addErrorOnVfPage(e.getMessage());
            }
        }
        
    }
    
    
  
    
    /**
* Method to save quotation pdf to Opportunity attachments
*/
    public PageReference redirect() {
        if(accessType != null && accessType.trim().toLowerCase() == 'Preview') return null;
        if (!ApexPages.hasMessages()) {
            System.debug('Atttachment added');
            if(opportunityObj != null) {
                PageReference pdf;
                if( quotation.RecordType.Name == 'Export') {
                    pdf = Page.QuotationPDFExport;
                } else if(quotation.RecordType.Name == 'Retail') {
                    pdf = Page.QuotationPDFRetail;
                } else {
                    pdf = Page.QuotationPdfProject;
                }
                pdf.getParameters().put('id', quotation.Id);
                pdf.getParameters().put('accessType', 'Preview');
                pdf.setRedirect(true);
                
                //creates email attachment file
                Attachment attach = new Attachment();
                Blob body;
                try {
                    body = pdf.getContent();
                } catch (VisualforceException e) {
                    body = Blob.valueOf(e.getMessage());
                }
                
                attach.Body = body;
                attach.Name = quotation.Name +'(' + opportunityObj.Name + ') - Quotation.pdf';
                //attach.IsPrivate = false;
                attach.ParentId = opportunityObj.Id;
                insert attach;
                System.debug('RecordType ' + quotation.RecordType.Name);
                return new PageReference('/apex/shareDocument?opportunityId=' + opportunityObj.Id + '&attachmentId=' + attach.id + '&templateName=TemplateQuotation&documentId=' + quotation.Id + '&salesProcess=' + quotation.RecordType.Name + '&targetObjectId=' + opportunityObj.Contact_Person__r.Id + '&invalidQuotationProducts=' + invalidQuotationProducts);
            } else {
            	return new PageReference('/'+ quotation.Id);
            }
        } else {
        	return null;
        }
    }
    
    
    
    /**
* Method redirects to quotation page on click on back button
*/ 
    public PageReference goBack(){
        return new PageReference('/'+ quotation.Id);
    }  
    
    private String breakMultiSelect(String sourceString) {
        String targetString = '';
        String[] tokens = sourceString.split(';');
        for(String token : tokens) {
            targetString += token + ', ';
        }
        return targetString;
    }
    
    /**
* Method add Errors on VF Page which is passed as parameter
*/
    private void addErrorOnVfPage(String errorMessage){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMessage));
    }
}