public class DefaultPaymentTerms extends fflib_SObjectDomain {
    private Decimal totalPercentage{get; set;}
    public DefaultPaymentTerms(List<Projected_Stock__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new DefaultPaymentTerms(records);
        }
    }
    // Before Insert Trigger
    // This method ensures that if Payment Percentage is created, the total should be < 100
    public override void onBeforeInsert() {
    	List<Default_Payment_Term__c> allDefaultPaymentTerms = [Select Id, Name, Payment_Percentage__c From Default_Payment_Term__c];
        Decimal totalPercentage = 0.00;
        for(Default_Payment_Term__c defaultPaymentTerm : allDefaultPaymentTerms) {
            totalPercentage += defaultPaymentTerm.Payment_Percentage__c;
        }
        Decimal availablePercentage;
        for(Default_Payment_Term__c newDefaultPaymentTerm : (List<Default_Payment_Term__c>) records) {
            availablePercentage = 100.00 - totalPercentage;
            if(newDefaultPaymentTerm.Payment_Percentage__c > availablePercentage) {
                newDefaultPaymentTerm.Payment_Percentage__c.addError('Total Payment Percentage should not be greater than 100%. Available(' + availablePercentage + ')');
            }
        }
    }
    // Before Update Trigger
    // This method ensures that if Payment Percentage is updated, the total should be < 100
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) { 
    	List<Default_Payment_Term__c> allDefaultPaymentTerms = [Select Id, Name, Payment_Percentage__c From Default_Payment_Term__c];
        Decimal totalPercentage = 0.00;
        Map<Id, Default_Payment_Term__c> newDefaultPaymentTermRecords = new Map<Id, Default_Payment_Term__c>();
        for(Default_Payment_Term__c newDefaultPaymentTerm : (List<Default_Payment_Term__c>) records) {
        	newDefaultPaymentTermRecords.put(newDefaultPaymentTerm.Id, newDefaultPaymentTerm);
        }
        for(Default_Payment_Term__c defaultPaymentTerm : allDefaultPaymentTerms) {
            totalPercentage += defaultPaymentTerm.Payment_Percentage__c;
            if(existingRecords.get(defaultPaymentTerm.Id) != null && newDefaultPaymentTermRecords.get(defaultPaymentTerm.Id) != null) {
                totalPercentage -= ((Default_Payment_Term__c)existingRecords.get(defaultPaymentTerm.Id)).Payment_Percentage__c;
            }
        }
        Decimal availablePercentage;
        // Validation of Total Payment Percentage should be < 100
        for(Default_Payment_Term__c newDefaultPaymentTerm : (List<Default_Payment_Term__c>) records) {
            availablePercentage = 100.00 - totalPercentage;
            if(newDefaultPaymentTerm.Payment_Percentage__c > availablePercentage) {
                newDefaultPaymentTerm.Payment_Percentage__c.addError('Total Payment Percentage should not be greater than 100%. Available(' + availablePercentage + ')');
            }
        }
    }
}