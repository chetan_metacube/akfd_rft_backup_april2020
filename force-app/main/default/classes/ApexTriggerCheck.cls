public class ApexTriggerCheck {
    private static ApexTriggerCheck instance = null;
    /*Boolean variable can be private with getter setter or it can be public.*/
    public boolean isExecutinngFromApexCode;
    public boolean isFutureCalling;
    public boolean isExecutingFromAPIRequest;
    private ApexTriggerCheck() {
    }
    public static ApexTriggerCheck getInstance() {
        if(instance == null) {
            instance = new ApexTriggerCheck();
        }
        return instance;
    }
}