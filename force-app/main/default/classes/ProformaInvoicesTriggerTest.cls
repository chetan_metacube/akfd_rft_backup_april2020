@isTest()
public class ProformaInvoicesTriggerTest {
    @testSetup
    static void testSetup() {
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        
        List<Account> accounts=new List<Account>();
        Account accountCustomerType=new Account(Name='AccountTest',RecordTypeId=customerRecordTypeIdAccount,CurrencyIsoCode='USD');
        Account accountMerchantType=new Account(Name='AccountTest',RecordTypeId=merchantcustomerRecordTypeIdAccount);
        accounts.add(accountCustomerType);
        accounts.add(accountMerchantType);
        insert accounts;
        
        Contact newContact=new Contact(FirstName='Contact',LastName='Test',AccountId=accountCustomerType.Id,Email='test@test.com');
        insert newContact;
        
        Id exportRecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Opportunity newOpportunity1=new Opportunity(Name='Test Opportunity1',RecordTypeId=exportRecordTypeIdOpportunity,StageName='Client Setup',Merchant__c=accountMerchantType.Id,AccountId=accountCustomerType.Id,Contact_Person__c=newContact.Id,CloseDate=Date.today(),CurrencyIsoCode='USD', Freight_Amount__c = 10.00);
        insert newOpportunity1;
        Id projectRecordTypeIdOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        Opportunity newOpportunity2=new Opportunity(Name='Test Opportunity2',RecordTypeId=projectRecordTypeIdOpportunity,StageName='Client Setup',Merchant__c=accountMerchantType.Id,AccountId=accountCustomerType.Id,Contact_Person__c=newContact.Id,CloseDate=Date.today(),CurrencyIsoCode='USD', Freight_Amount__c = 10.00);

        Product2 product=new Product2(Name='Test Product',Brand__c = 'AKFD',Family='Furniture', Retail_Price__c = 0.00);
        insert product;
        
        PriceBook_Product__c priceBookObj=new PriceBook_Product__c(Customer__c=accountCustomerType.Id,Product__c=product.Id,Effective_Start_Date__c=Date.today(),Effective_End_Date__c=Date.today().addDays(365) ,Sampling_Charges__c=120.0);
        insert priceBookObj;
        
        PriceBook_Product_Entry__c priceBookEntryObj=new PriceBook_Product_Entry__c(PriceBook_Product__c=priceBookObj.Id,Min_Quantity__c=1,Max_Quantity__c=10,Price__c=145.00,Lead_Time_for_MOQ__c=15,Unit_of_Price__c='Each');
        insert  priceBookEntryObj;
        
        PO_Product__c desiredProductObj1=new PO_Product__c(Product__c=product.Id,Quantity__c=5,Opportunity__c=newOpportunity1.Id, Discount_Percentage__c = 0.00);
        insert desiredProductObj1;
        
                
        Id projectRecordTypeIdProformaInvoice = Schema.SObjectType.ProformaInvoice__c.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        ProformaInvoice__c proformaInvoice1 = new ProformaInvoice__c(Opportunity__c = newOpportunity1.Id, Status__c = 'New', Conversion_Rate__c = 10.00);
        List<ProformaInvoice__c> proformaInvoices = new List<ProformaInvoice__c>();
        proformaInvoices.add(proformaInvoice1);
        insert proformaInvoices;
        
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem1 = new Proforma_Invoice_Line_item__c(ProformaInvoice__c = proformaInvoice1.Id, Product__c = desiredProductObj1.Product__c, PO_Product__c = desiredProductObj1.Id, Quantity__c = 10, Required_Quantity__c = 15, Price__c = 10.00);
        
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = new List<Proforma_Invoice_Line_Item__c>();
        proformaInvoiceLineItems.add(proformaInvoiceLineItem1);
        insert proformaInvoiceLineItems;
        Payment_Term__c term1 = new Payment_Term__c(Name = 'Installment 1', Opportunity__c = newOpportunity1.Id, Payment_Percentage__c = 80.00);
        Payment_Term__c term2 = new Payment_Term__c(Name = 'Installment 2', Opportunity__c = newOpportunity1.Id, Payment_Percentage__c = 20.00);
        Database.saveResult[] result = Database.insert(new List<Payment_Term__c>{term1, term2}, false);
    }
    
    @isTest()
    static void TestOnBeforeUpdateExport() {
        Id exportRecordTypeIdProformaInvoice = Schema.SObjectType.ProformaInvoice__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        ProformaInvoice__c proformaInvoice = [Select Id, Status__c From ProformaInvoice__c Where RecordTypeId = :exportRecordTypeIdProformaInvoice LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        Database.saveResult[] result = Database.update(new List<ProformaInvoice__c>{proformaInvoice}, false);
    }
    
       
    @isTest()
    static void TestPIRevisionExport() {
        Id exportRecordTypeIdProformaInvoice = Schema.SObjectType.ProformaInvoice__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        ProformaInvoice__c proformaInvoice = [Select Id, Status__c, All_Items_Total_Quantity__c, All_Items_Total_Price__c From ProformaInvoice__c Where RecordTypeId = :exportRecordTypeIdProformaInvoice LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        Database.saveResult[] result = Database.update(new List<ProformaInvoice__c>{proformaInvoice}, false);
        Proforma_Invoice_Line_Item__c lineItem = [Select Id, Name, Required_Quantity__c, Price__c, Last_Updated_Price__c, Last_Updated_Quantity__c From Proforma_Invoice_Line_Item__c Where ProformaInvoice__c = :proformaInvoice.Id LIMIT 1];
        lineItem.Required_Quantity__c = lineItem.Required_Quantity__c + 1;
        lineItem.Price__c = lineItem.Price__c + 10;
        Database.saveResult[] resultLineItem = Database.update(new List<Proforma_Invoice_Line_Item__c>{lineItem}, false);
        lineItem = [Select Id, Name, Required_Quantity__c, Price__c, Last_Updated_Price__c, Last_Updated_Quantity__c From Proforma_Invoice_Line_Item__c Where ProformaInvoice__c = :proformaInvoice.Id LIMIT 1];
        system.debug('lineItem ' + lineItem);
        proformaInvoice = [Select Id, Status__c, All_Items_Total_Quantity__c, All_Items_Total_Price__c From ProformaInvoice__c Where Id = :proformaInvoice.Id];
        proformaInvoice.Status__c = 'Accepted';
        result = Database.update(new List<ProformaInvoice__c>{proformaInvoice}, false);
    }
    
    @isTest()
    static void TestOnBeforeDelete() {
        ProformaInvoice__c proformaInvoice = [Select Id, Status__c From ProformaInvoice__c LIMIT 1];
        Database.deleteResult[] result = Database.delete(new List<ProformaInvoice__c>{proformaInvoice}, false);
    }
}