@isTest
public class WarehouseProductsControllerTest {
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(customerAccounts, products, 1);
        List<PriceBook_Product_Entry__c> pricebookEntries = MockData.createPriceBookEntries(customerAccounts, products, priceBooks, 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(customerAccounts, opportunities, products, 1);
        
        List<Warehouse__c> warehouses = MockData.createWarehouses(1);
        MockData.createWarehouseLineItems(warehouses, products, 1);
        MockData.createProformaInvoices(opportunities, products, 1);
    }
    
    @isTest static void positiveTestCase_For_getProductWarehousesMethod() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        String result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
        System.assert(result != null);
        List<WarehouseProductsService.ProductData> productWarehouseDataList = (List<WarehouseProductsService.ProductData>)JSON.deserialize(result, List<WarehouseProductsService.ProductData>.class);
        System.assert(productWarehouseDataList[0].productWarehouses.size() > 0);
        System.assertEquals(productWarehouseDataList[0].productWarehouses[0].warehouseName, 'Test Warehouse01');
    }
    
    @isTest static void negativeTestCase_For_getProductWarehousesMethod_WhenUserDonotHaveAccess() { 
        User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' LIMIT 1];
        try {
            System.runAs(chatterFreeUser) {
                ProformaInvoice__c proformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c LIMIT 1];
                String result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
            }
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Insufficient Privileges.'));
        }
    }
    
    @isTest static void negativeTestCase_For_getProductWarehousesMethod_WhenPIisNotAcceptedYet() { 
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c LIMIT 1];
        try {
            String result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('PI is not Accepted yet. Product Allocation can be done only after PI gets Accepted.'));
        }
    }
    
    @isTest static void negativeTestCase_For_getProductWarehousesMethod_WhenPIIdIsNull() { 
        try {
            String result = WarehouseProductsController.getProductWarehouses(null);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Invalid proforma invoice id passed.'));
        }
    }
    
    @isTest static void positiveTestCase_For_allotStockMethod() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
         Proforma_Invoice_Line_Item__c proformaInvoiceLineItem = [SELECT Id, Name, Product__c, Required_Quantity__c FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id LIMIT 1];
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        Projected_Stock__c projectedStock = new Projected_Stock__c(Product__c = proformaInvoiceLineItem.Product__c, ProformaInvoice__c = proformaInvoice.Id, Required_Quantity__c = 5, Status__c = 'New');
        insert projectedStock;
        
        String result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
        System.assert(result != null);
        List<WarehouseProductsService.ProductData> productWarehouseDataList = (List<WarehouseProductsService.ProductData>)JSON.deserialize(result, List<WarehouseProductsService.ProductData>.class);
        WarehouseProductsService.ProductData productWarehouseData = productWarehouseDataList[0];
        productWarehouseData.bookedQuantity = 10;
        productWarehouseData.projectedStock = productWarehouseData.requiredQuantity - productWarehouseData.bookedQuantity;
        List<WarehouseProductsService.WarehouseWrapper> productWarehousesList = productWarehouseData.productWarehouses;
        WarehouseProductsService.WarehouseWrapper productWarehouse = productWarehousesList[0];
        productWarehouse.quantity = 10;
        productWarehouse.isSelected = true;
        
        WarehouseProductsController.allotStock(proformaInvoice.Id, JSON.serialize(productWarehouseDataList));
        Alloted_Stock__c bookedStock = [SELECT Id, Name, Stock__c, Product__c, ProformaInvoice__c, Product__r.Name, Warehouse__c, Warehouse__r.Name, ProformaInvoice__r.Name FROM Alloted_Stock__c WHERE ProformaInvoice__c =: proformaInvoice.Id]; 
        
        System.assert(bookedStock.Stock__c == 10);
        System.assert(bookedStock.Product__c == productWarehouseData.id);
        WarehouseProductsService.BookedStockWrapper bookedStockWrapper = new  WarehouseProductsService.BookedStockWrapper(bookedStock);
        System.assert(bookedStockWrapper.productId == bookedStock.Product__c);
    }
    
    
     @isTest static void negativeTestCase_For_allotStockMethod_WhenAllotmentAlreadyDone() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        
        String result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
        System.assert(result != null);
        List<WarehouseProductsService.ProductData> productWarehouseDataList = (List<WarehouseProductsService.ProductData>)JSON.deserialize(result, List<WarehouseProductsService.ProductData>.class);
        WarehouseProductsService.ProductData productWarehouseData = productWarehouseDataList[0];
        productWarehouseData.bookedQuantity = 10;
        productWarehouseData.projectedStock = productWarehouseData.requiredQuantity - productWarehouseData.bookedQuantity;
        List<WarehouseProductsService.WarehouseWrapper> productWarehousesList = productWarehouseData.productWarehouses;
        WarehouseProductsService.WarehouseWrapper productWarehouse = productWarehousesList[0];
        productWarehouse.quantity = 10;
        productWarehouse.isSelected = true;
        
         WarehouseProductsController.allotStock(proformaInvoice.Id, JSON.serialize(productWarehouseDataList));
         Alloted_Stock__c bookedStock = [SELECT Id, Name, Stock__c, Product__c, ProformaInvoice__c, Product__r.Name, Warehouse__c, Warehouse__r.Name, ProformaInvoice__r.Name FROM Alloted_Stock__c WHERE ProformaInvoice__c =: proformaInvoice.Id]; 
         
         System.assert(bookedStock.Stock__c == 10);
         System.assert(bookedStock.Product__c == productWarehouseData.id);
         
         try {
            result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Product allotment is already done for this Proforma Invoice.'));
        }
         
    }
    
    @isTest static void negativeTestCase_For_allotStockMethod_WhenBookedQuantityIsMoreThanCurrentStock() {
        ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
        proformaInvoice.Status__c = 'Accepted';
        update proformaInvoice;
        
        String result = WarehouseProductsController.getProductWarehouses(proformaInvoice.Id);
        System.assert(result != null);
        List<WarehouseProductsService.ProductData> productWarehouseDataList = (List<WarehouseProductsService.ProductData>)JSON.deserialize(result, List<WarehouseProductsService.ProductData>.class);
        WarehouseProductsService.ProductData productWarehouseData = productWarehouseDataList[0];
        productWarehouseData.bookedQuantity = 900;
        productWarehouseData.projectedStock = productWarehouseData.requiredQuantity - productWarehouseData.bookedQuantity;
        List<WarehouseProductsService.WarehouseWrapper> productWarehousesList = productWarehouseData.productWarehouses;
        WarehouseProductsService.WarehouseWrapper productWarehouse = productWarehousesList[0];
        productWarehouse.quantity = 1000;
        productWarehouse.isSelected = true;
        
        try {
         WarehouseProductsController.allotStock(proformaInvoice.Id, JSON.serialize(productWarehouseDataList));
         } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Booking quantity should not be greater than Current Stock of warehouse.'));
        }
    }
    
    @isTest static void negativeTestCase_For_allotStockMethod_WhenPiIdIsNull() {
        try {
            WarehouseProductsController.allotStock(null, '[{}]');
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Invalid proforma invoice id passed.'));
        }
    }
    
    @isTest static void negativeTestCase_For_allotStockMethod_WhenProductWarehouseStringIsNull() {
        try {
            ProformaInvoice__c proformaInvoice = [SELECT Id, Name, Status__c FROM ProformaInvoice__c LIMIT 1];
            WarehouseProductsController.allotStock(proformaInvoice.Id, null);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Invalid Product Wrapper passed.'));
        }
    }
    
    @isTest static void negativeTestCase_For_allotStockMethod_WhenParametersAreNull() {
        try {
            WarehouseProductsController.allotStock(null, null);
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Invalid Product Wrapper passed.'));
        }
    }
    
    
    
    

}