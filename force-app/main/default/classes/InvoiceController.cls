public class InvoiceController {
    
    // Method to get the Accepted PI's of an opportunity
	@AuraEnabled
    public static String getProformaInvoices(String opportunityId) {
        System.debug('controller called' + opportunityId);
        if(!String.isBlank(opportunityId)) {
            List<UserRecordAccess> userRecordsAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :opportunityId];
            //Check if user has edit access to the record
            if(userRecordsAccess != null && userRecordsAccess[0].HasEditAccess) {
                List<InvoicesService.ProformaInvoiceWrapper> proformaInvoiceList = InvoicesService.getProformaInvoiceWrapper(new List<Id>{opportunityId});
                if(proformaInvoiceList.size() > 0) {
                    return JSON.serialize(proformaInvoiceList);
                } else{
                    throw new AuraHandledException('Please create an Accepted PI before creating Invoice.');
                }
            } else {
                throw new AuraHandledException('Insufficient Privileges.');
            }
        } else {
            throw new AuraHandledException('Please pass an opportunity Id.');
        }
    }
    
    // Method to create the Invoice Record from Selected PI's
	@AuraEnabled
    public static String generateInvoice(String selectedProformaInvoicesWrappers) {
        System.debug('generate invoice called');
        List<InvoicesService.ProformaInvoiceLineItemWrapper> proformaInvoiceList = (List<InvoicesService.ProformaInvoiceLineItemWrapper>)JSON.deserialize(
               selectedProformaInvoicesWrappers, List<InvoicesService.ProformaInvoiceLineItemWrapper>.class);
        System.debug('proformaInvoiceList[0]'+proformaInvoiceList[0]);
        Id primaryProformaInvoiceId = proformaInvoiceList[0].proformaInvoiceId;
        Set<Id> proformaInvoiceIds = new Set<Id>();
        Set<Id> proformaInvoiceLineItemIds = new Set<Id>();
        Map<Id, Decimal> proformaInvoiceLineItemQuantity = new Map<Id, Decimal>();
        for(InvoicesService.ProformaInvoiceLineItemWrapper lineItem : proformaInvoiceList) {
            proformaInvoiceIds.add(lineItem.proformaInvoiceId);
            proformaInvoiceLineItemIds.add(lineItem.proformaInvoiceLineItemId);
            proformaInvoiceLineItemQuantity.put(lineItem.proformaInvoiceLineItemId, lineItem.Quantity);
        }
        
        Map<Id, ProformaInvoice__c> queriedProformaInvoicesMap= new Map<Id, ProformaInvoice__c>([Select Id, Name, Opportunity__r.AccountId, Opportunity__c, Opportunity__r.Name, Opportunity__r.PO_No__c, Country_Of_Final_Origin__c,Opportunity__r.Freight_Amount__c,Opportunity__r.Packing_Percentage__c,Opportunity__r.Transportation_Mode__c,Opportunity__r.Billing_Address__c, Opportunity__r.Notify_Address__c, Opportunity__r.Delivery_Address__c, Opportunity__r.Contact_Person__c, Opportunity__r.Notify_Contact__c, Opportunity__r.Contact__c, 
                                                              Final_Destination__c,Mode_of_Payment__c,
                                                              Freight__c,Advance_Payment__c,Packing_Charges__c,
                                                              Port_Of_Discharge__c,Place_Of_Receipt__c,
                                                              Port_of_Loading__c,Pre_Carriage_By__c,
                                                              Product_Liability_Insurance_Percentage__c,
                                                              Shipping_Charges__c,Trade_Discount__c,
                                                              Is_Revised__c, Product_Alloted__c,
                                                              CurrencyIsoCode, RecordType.Name,
                                                              (
                                                                  Select Id, Name,Product__r.Name,
                    											  Product__r.GST__c,
                                                                  Product__r.Image__c,
                                                                  SKU_Number__c,
                                                                  Product__r.Total_Stock__c,
                                                                  Product__r.Alloted_Stock__c,
                                                                  Quantity__c,Invoiced_Quantity__c,
                                                                  Balanced_Quantity__c,
                                                                  ProformaInvoice__c,
                                                                  ProformaInvoice__r.Name,
                                                                  ProformaInvoice__r.Opportunity__c,
                                                                  Required_Quantity__c,Unit__c,
                                                                  Price__c,Total_Price__c ,
                                                                  PO_Product__c,
                                                                  PO_Product__r.Discount_Percentage__c, 
                                                                  CurrencyIsoCode
                                                                  FROM ProformaInvoiceLineItems__r 
                                                                  Where Balanced_Quantity__c > 0 AND Id IN:proformaInvoiceLineItemIds
                                                              )
                                                              FROM ProformaInvoice__c WHERE Id IN :proformaInvoiceIds]);
        System.debug(proformaInvoiceList);
        System.debug('proformaInvoicesMap');
        System.debug(queriedProformaInvoicesMap);
        //List<Id> invoiceIds = InvoicesService.generate(queriedProformaInvoicesMap);
        for(ProformaInvoice__c proformaInvoice : queriedProformaInvoicesMap.values()) {
            for(Integer i = 0; i < proformaInvoice.ProformaInvoiceLineItems__r.size(); i++){
            	Proforma_Invoice_Line_Item__c lineItem = proformaInvoice.ProformaInvoiceLineItems__r.get(i);
                proformaInvoice.ProformaInvoiceLineItems__r.get(i).Quantity__c = proformaInvoiceLineItemQuantity.get(proformaInvoice.ProformaInvoiceLineItems__r.get(i).Id);
            }
        }
        List<InvoicesService.ProformaInvoicesIdsWrapper> mergeInvoicesProformaInvoices = new List<InvoicesService.ProformaInvoicesIdsWrapper>{new InvoicesService.ProformaInvoicesIdsWrapper(new List<Id>(proformaInvoiceIds), primaryProformaInvoiceId)};
        //List<Id> ids = InvoicesServiceImpl.generate1(mergeInvoicesProformaInvoices, queriedProformaInvoicesMap.values());
       	List<Id> invoiceIds = InvoicesService.createInvoices(mergeInvoicesProformaInvoices, queriedProformaInvoicesMap.values());
       	
        return invoiceIds[0];
    } 
}