@isTest
public class NumbersToWordsTest {
    public static testMethod void testConvert() {
        NumbersToWords util = new NumbersToWords();
        util.numberval = 1234567890.23;
        String result = util.convert();
        System.assertEquals('One  Crore Twenty Three Crore Forty Five  Lakh Sixty Seven  Thousand Eight  Hundred  Ninety  & Twenty Three ', result);
    }
}