public class RequestBodyXML {
    
    public enum APIType {PRODUCT, ACCOUNT, INVOICE, EXPORT_INVOICE}

    public methodCall methodCall;
    
    private final static String PRODUCT_API_PATH = 'bi.tally.integration.products';
    private final static String PRODUCT_API_METHOD = 'create_product';
    private final static String ACCOUNT_API_PATH = 'bi.tally.integration.ledgers';
    private final static String ACCOUNT_API_METHOD = 'create_ledger';
    private final static String INVOICE_API_PATH = 'bi.tally.integration.invoice';
    private final static String INVOICE_API_METHOD = 'create_invoice';
    private final static String EXPORT_INVOICE_API_PATH = 'bi.tally.integration.export.invoice';
    private final static String EXPORT_INVOICE_API_METHOD = 'export_invoice';
    
    
    public RequestBodyXML(APIType apiType){
        //populate some data
        //Create a book object
        String methodName = 'execute';
        Params params = new Params();
        List<Param> param = new List<Param>();
        param.add(new Param(new StrValue('salesforce')));
        param.add(new Param(new IntValue('1')));
        param.add(new Param(new StrValue('1')));
        switch on apiType {
            when PRODUCT {
                param.add(new Param(new StrValue(PRODUCT_API_PATH)));
                param.add(new Param(new StrValue(PRODUCT_API_METHOD)));
            }
            when ACCOUNT {
                param.add(new Param(new StrValue(ACCOUNT_API_PATH)));
                param.add(new Param(new StrValue(ACCOUNT_API_METHOD)));
            }
            when INVOICE {
                param.add(new Param(new StrValue(INVOICE_API_PATH)));
                param.add(new Param(new StrValue(INVOICE_API_METHOD)));
            }
            when EXPORT_INVOICE{
                param.add(new Param(new StrValue(EXPORT_INVOICE_API_PATH)));
                param.add(new Param(new StrValue(EXPORT_INVOICE_API_METHOD)));
            }
        }
        
        params.param = param;
        methodCall mCall = new methodCall();
        mCall.methodName = methodName;
        mCall.params = params;  
        //add catalog to library
        this.methodCall = mCall;
    }
    
    public void setValues(List<String> values) {
        this.methodCall.params.param.add(new Param(new Value(values)));
    }
    
    public String getXMLBody() {
        XMLSerializer serializer = new XMLSerializer();
        RequestBodyXML library = this;
        String serializedXML = serializer.serialize(library, true, null);
        serializedXML = serializedXML.replaceAll('strValue', 'string');
        serializedXML = serializedXML.replaceAll('intValue', 'int');
        return serializedXML;
    }
    
    public class MethodCall {
        public String methodName;
        public Params params;
    }
    
    public class Params {
        List<Param> param;
    }
    
    public class Param {
        public Object value;
        public Param(Object value) {
            this.value = value;
        }
    }
    
    public class StrValue {
        public Object strValue;
        public StrValue(Object strValue) {
            this.strValue = strValue;
        }
    }
    
    public class IntValue {
        public Object intValue;
        public IntValue(Object intValue) {
            this.intValue = intValue;
        }
    }
    
    public class Value {
        public Object value;
        public Value(Object value) {
            this.value = value;
        }
    }
    
    public class GSTDetails {
        public String gst_date;
        public String hsn_code;
        public String gst_taxable;
        public Decimal gst;
        public Decimal state_tax;
        
        public GSTDetails(String gst_date, String hsn_code, String gst_taxable, Decimal gst, Decimal state_tax) {
            this.gst_date = gst_date;
            this.hsn_code = hsn_code;
            this.gst_taxable = gst_taxable;
            this.gst = gst;
            this.state_tax = state_tax;
        }
    }
}