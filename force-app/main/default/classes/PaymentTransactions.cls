/*
 * Domain Class for Payment Transaction Trigger
 * Author : Gaurav Gupta and Chetan Sharma
 * Last Modified: 11 NOV 2018
*/
public class PaymentTransactions extends fflib_SObjectDomain {
    
    /* 
     * Parameterized Constructor 
     * Param - List of Payment Transaction  - equivalent to Trigger.New records
	*/
    public PaymentTransactions(List<Payment_Transaction__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new PaymentTransactions(records);
        }
    }
    
    private static Map<Id, Payment_Term__c> paymentTermsMap = null;
    private static Map<Id, Invoiced_Items__c> invoiceItems = null;
    
	/* 
     * Method - onBeforeInsert
     * Description - Method to execute when trigger is of type Before and Insert
     * To validate if new payment transaction is valid.
	*/
    public override void onBeforeInsert() {
        Set<Id> paymentTermsIds = new Set<Id>();    
        Set<Id> invoiceItemsIds = new Set<Id>();        
        Id transactionAdvanceRecordTypeId = Schema.SObjectType.Payment_Transaction__c.getRecordTypeInfosByName().get('Advance Payment').getRecordTypeId();
        Id transactionInvoiceRecordTypeId = Schema.SObjectType.Payment_Transaction__c.getRecordTypeInfosByName().get('Payment Against Invoice').getRecordTypeId();
        Id overpaidRecordTypeId = Schema.SObjectType.Payment_Term__c.getRecordTypeInfosByName().get('Over Paid Payment Terms').getRecordTypeId();
        
        //Filter the transaction if its invoice based transaction or independent.
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            paymentTermsIds.add(paymentTransaction.Payment_Term__c);
            if(paymentTransaction.Invoiced_Item__c != null) {
                invoiceItemsIds.add(paymentTransaction.Invoiced_Item__c);
            }
        }
        
        paymentTermsMap = new Map<Id, Payment_Term__c>([Select Id, Name, RecordTypeId, Conversion_Rate_To_INR__c, Dummy_Balance_Amount_v2__c, CurrencyIsoCode, Payment_Status_New__c, Dummy_Payment_Status__c, Payment_Amount__c, Balance_Amount__c, Opportunity__c, Opportunity__r.Name, Opportunity__r.RecordType.Name, Opportunity__r.Merchant__r.Name, Opportunity__r.Owner.Name, Opportunity__r.Owner.Email, Opportunity__r.PO_No__c, Opportunity__r.Account.Name, Opportunity__r.Account.Email__c From Payment_Term__c Where Id =:paymentTermsIds]);
        Map<Id, Invoiced_Items__c> invoiceItems = new Map<Id, Invoiced_Items__c>([Select Id, Name, Balance_Amount__c From Invoiced_Items__c Where Id in :invoiceItemsIds]);
        
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            if(paymentTransaction.Payment_Term__c != null) { 
                
                //Validate if the payment term of transaction is of installation type payment term. 
                if(paymentTermsMap.get(paymentTransaction.Payment_Term__c).RecordTypeId != overpaidRecordTypeId) {
                    //Validate if payment amount is positive
                    //Payment transaction is creating on empty payment term
                    //Payment term is fulfilled
                    //Payment amount is not greater than payment term balance amount. 
                    if(paymentTransaction.Amount__c <= 0) {
                        paymentTransaction.Amount__c.addError('Payment Amount should be positive and more than 0.');
                    } else if(paymentTermsMap.get(paymentTransaction.Payment_Term__c).Payment_Amount__c == 0) {
                        paymentTransaction.addError('PI does not exist for this opportunity.');
                    } else if(paymentTermsMap.get(paymentTransaction.Payment_Term__c).Balance_Amount__c == 0) {
                        paymentTransaction.addError('Payment Terms are fulfilled. No need to create more transactions.');
                    } else if(paymentTransaction.Amount__c > paymentTermsMap.get(paymentTransaction.Payment_Term__c).Balance_Amount__c) {
                        paymentTransaction.Amount__c.addError('Payment Amount should not be greater than Balance Amount of payment term(' + paymentTermsMap.get(paymentTransaction.Payment_Term__c).Balance_Amount__c + ').');
                    }
                    
                    paymentTransaction.CurrencyIsoCode = paymentTermsMap.get(paymentTransaction.Payment_Term__c).CurrencyIsoCode;
                    
                    //Validate if transaction is of advance transaction type.
                    if(paymentTransaction.Invoiced_Item__c == null) {
                        paymentTransaction.RecordTypeId = transactionAdvanceRecordTypeId;
                    } else {
                        if(invoiceItems.get(paymentTransaction.Invoiced_Item__c).Balance_Amount__c < paymentTransaction.Amount__c) {
                            paymentTransaction.Amount__c.addError('Payment Amount should not be greater than Balance Amount of invoice(' + invoiceItems.get(paymentTransaction.Invoiced_Item__c).Balance_Amount__c + ').');
                        } else {
                            paymentTransaction.RecordTypeId = transactionInvoiceRecordTypeId;
                        }
                    }
                    
                }
            }
        }
    }
    
    /* 
     * Method - onAfterInsert
     * Description - Method to execute when trigger is of type After and Insert
     * To update the settled amount related to the invoiced item.
	*/
    public override void onAfterInsert() {
        Set<Id> paymentTermsIds = new Set<Id>();
        Set<Id> invoiceItemsIds = new Set<Id>();        
        Id transactionAdvanceRecordTypeId = Schema.SObjectType.Payment_Transaction__c.getRecordTypeInfosByName().get('Advance Payment').getRecordTypeId();
        Id transactionInvoiceRecordTypeId = Schema.SObjectType.Payment_Transaction__c.getRecordTypeInfosByName().get('Payment Against Invoice').getRecordTypeId();
        Decimal balanceAmount = 0;

        //Filter the transaction if its invoice based transaction or independent.
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            paymentTermsIds.add(paymentTransaction.Payment_Term__c);
            if(paymentTransaction.Invoiced_Item__c != null) {
                invoiceItemsIds.add(paymentTransaction.Invoiced_Item__c);
            }
        }
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Payment_Term__c.SObjectType,
                    Invoiced_Items__c.SObjectType
                    }
        );
        
        Map<Id, Decimal> paymentTermsTotalTransaction = new Map<Id, Decimal>();
        invoiceItems = new Map<Id, Invoiced_Items__c>([Select Id, Name, Settled_Amount__c, Balance_Amount__c From Invoiced_Items__c Where Id in :invoiceItemsIds]);
        Decimal totalTransactionAmount = 0.00;
        
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            totalTransactionAmount = paymentTransaction.Amount__c;
            if(paymentTermsTotalTransaction.containsKey(paymentTransaction.Payment_Term__c)) {
                totalTransactionAmount += paymentTermsTotalTransaction.get(paymentTransaction.Payment_Term__c);
            }
            paymentTermsTotalTransaction.put(paymentTransaction.Payment_Term__c, totalTransactionAmount);
            paymentTermsMap.get(paymentTransaction.Payment_Term__c).Conversion_Rate_To_INR__c = paymentTransaction.Conversion_Rate_To_INR__c;
            if(paymentTransaction.RecordTypeId == transactionInvoiceRecordTypeId) {
                if(invoiceItems.get(paymentTransaction.Invoiced_Item__c) != null) {
                    invoiceItems.get(paymentTransaction.Invoiced_Item__c).Settled_Amount__c += paymentTransaction.Amount__c;
                } else {
                    paymentTransaction.addError('Something went wrong!!');
                }
            }
        }
        
        uow.registerDirty(invoiceItems.values());
        uow.registerDirty(paymentTermsMap.values());
        uow.commitWork();
        sendNotification(paymentTermsMap, records);
    }
    
    /* 
     * Method - onBeforeUpdate
     * Description - Method to execute when trigger is of type Before and Update
     * To validate if new payment transaction is valid.
	*/
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        Set<Id> paymentTermsIds = new Set<Id>();
        Set<Id> invoiceItemsIds = new Set<Id>();        
        Id transactionAdvanceRecordTypeId = Schema.SObjectType.Payment_Transaction__c.getRecordTypeInfosByName().get('Advance Payment').getRecordTypeId();
        Id transactionInvoiceRecordTypeId = Schema.SObjectType.Payment_Transaction__c.getRecordTypeInfosByName().get('Payment Against Invoice').getRecordTypeId();
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            paymentTermsIds.add(paymentTransaction.Payment_Term__c);
            if(paymentTransaction.Invoiced_Item__c != null) {
                invoiceItemsIds.add(paymentTransaction.Invoiced_Item__c);
            }
        }
        
        paymentTermsMap = new Map<Id, Payment_Term__c>([Select Id, Name, RecordTypeId, Conversion_Rate_To_INR__c, Dummy_Balance_Amount_v2__c, CurrencyIsoCode, Dummy_Payment_Status__c, Payment_Amount__c, Balance_Amount__c, Opportunity__c, Opportunity__r.Owner.Name, Opportunity__r.Owner.Email, Opportunity__r.PO_No__c, Opportunity__r.Account.Name From Payment_Term__c Where Id =:paymentTermsIds]);
        invoiceItems = new Map<Id, Invoiced_Items__c>([Select Id, Name, Settled_Amount__c, Balance_Amount__c, Opportunity__c From Invoiced_Items__c Where Id in :invoiceItemsIds]);
        Map<Id, Payment_Transaction__c> existingRecordsMap = (Map<Id, Payment_Transaction__c>)existingRecords;
        Decimal newBalanceAmount = 0.00;
        Decimal newBalanceAmountInvoice = 0.00;
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            newBalanceAmount = paymentTransaction.Balance_Amount__c + existingRecordsMap.get(paymentTransaction.Id).Amount__c;
           
            //Validate if payment amount is positive
            //Payment transaction is creating on empty payment term
            //Payment term is fulfilled
            //Payment amount is not greater than payment term balance amount. 
            if(paymentTransaction.Amount__c <= 0) {
                paymentTransaction.Amount__c.addError('Payment Amount should be positive and more than 0.');
            } else if(paymentTransaction.Amount__c > newBalanceAmount && paymentTransaction.Balance_Amount__c > 0) {
                paymentTransaction.Amount__c.addError('Payment Amount should not be greater than Balance Amount(' + newBalanceAmount + ').');
            }
            if(paymentTransaction.RecordTypeId == transactionAdvanceRecordTypeId && existingRecordsMap.get(paymentTransaction.Id).RecordTypeId != transactionAdvanceRecordTypeId) {
                paymentTransaction.Invoiced_Item__c = null;
            }
            
            if(paymentTransaction.Invoiced_Item__c != null) {
                newBalanceAmountInvoice = invoiceItems.get(paymentTransaction.Invoiced_Item__c).Balance_Amount__c + existingRecordsMap.get(paymentTransaction.Id).Amount__c;
                if(newBalanceAmountInvoice < paymentTransaction.Amount__c) {
                    paymentTransaction.Amount__c.addError('Payment Amount should not be greater than Balance Amount of invoice(' + newBalanceAmountInvoice + ').');
                } else if(paymentTermsMap.get(paymentTransaction.Payment_Term__c).Opportunity__c != invoiceItems.get(paymentTransaction.Invoiced_Item__c).Opportunity__c) {
                    paymentTransaction.Invoiced_Item__c.addError('Invoice Item should be of the same Opportunity\'s Invoice.');
                } else {
                    paymentTransaction.RecordTypeId = transactionInvoiceRecordTypeId;
                }
            }
        }
    }
    
    /* 
     * Method - onAfterUpdate
     * Description - Method to execute when trigger is of type After and Update
     * To update the settled amount related to the invoiced item.
	*/
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        Map<Id, Payment_Transaction__c> existingRecordsMap = (Map<Id, Payment_Transaction__c>)existingRecords;
        Set<Id> invoiceItemsIds = new Set<Id>();
        Set<Id> paymentTermsIds = new Set<Id>();
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            paymentTermsIds.add(paymentTransaction.Payment_Term__c);
            if(paymentTransaction.Invoiced_Item__c != null) {
                invoiceItemsIds.add(paymentTransaction.Invoiced_Item__c);
            }
            if(existingRecordsMap.get(paymentTransaction.Id).Invoiced_Item__c != null) {
                invoiceItemsIds.add(existingRecordsMap.get(paymentTransaction.Id).Invoiced_Item__c);
            }
        }
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Payment_Term__c.SObjectType,
                    Invoiced_Items__c.SObjectType
                    }
        );
        
        Map<Id, Decimal> paymentTermsTotalTransaction = new Map<Id, Decimal>();
        Decimal totalTransactionAmount = 0.00;
        Decimal newBalanceAmountInvoice = 0.00;
        
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            totalTransactionAmount = paymentTransaction.Amount__c - existingRecordsMap.get(paymentTransaction.Id).Amount__c;
            
            if(paymentTermsTotalTransaction.containsKey(paymentTransaction.Payment_Term__c)) {
                totalTransactionAmount += paymentTermsTotalTransaction.get(paymentTransaction.Payment_Term__c);
            }
            
            paymentTermsTotalTransaction.put(paymentTransaction.Payment_Term__c, totalTransactionAmount);
            paymentTermsMap.get(paymentTransaction.Payment_Term__c).Conversion_Rate_To_INR__c = paymentTransaction.Conversion_Rate_To_INR__c;
            if(paymentTransaction.Invoiced_Item__c != null) {
                if(existingRecordsMap.get(paymentTransaction.Id).Invoiced_Item__c == null) {
                    invoiceItems.get(paymentTransaction.Invoiced_Item__c).Settled_Amount__c += paymentTransaction.Amount__c;
                } else {
                    invoiceItems.get(paymentTransaction.Invoiced_Item__c).Settled_Amount__c -= existingRecordsMap.get(paymentTransaction.Id).Amount__c;
                    invoiceItems.get(paymentTransaction.Invoiced_Item__c).Settled_Amount__c += paymentTransaction.Amount__c;
                }
            } else if(existingRecordsMap.get(paymentTransaction.Id).Invoiced_Item__c != null) {
                invoiceItems.get(existingRecordsMap.get(paymentTransaction.Id).Invoiced_Item__c).Settled_Amount__c -= existingRecordsMap.get(paymentTransaction.Id).Amount__c;
            }
        }
        
        uow.registerDirty(invoiceItems.values());
        uow.registerDirty(paymentTermsMap.values());
        uow.commitWork();
    }
    
    /* 
     * Method - onBeforeDelete
     * Description - Method to execute when trigger is of type Before and Delete
     * To update the settled amount related to the invoiced item.
	*/
    public override void onBeforeDelete() {
        
        Set<Id> paymentTermsIds = new Set<Id>();
        Set<Id> invoiceItemsIds = new Set<Id>();
        Decimal totalTransactionAmount = 0.00;
        
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            paymentTermsIds.add(paymentTransaction.Payment_Term__c);
            if(paymentTransaction.Invoiced_Item__c != null) {
                invoiceItemsIds.add(paymentTransaction.Invoiced_Item__c);
            }
        }
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Payment_Term__c.SObjectType,
                    Invoiced_Items__c.SObjectType
                    }
        );
        
        Map<Id, Invoiced_Items__c> invoiceItems = new Map<Id, Invoiced_Items__c>([Select Id, Name, Settled_Amount__c, Balance_Amount__c From Invoiced_Items__c Where Id in :invoiceItemsIds]);
        Map<Id, Payment_Term__c> paymentTermsMap = new Map<Id, Payment_Term__c>([Select Id, Dummy_Balance_Amount_v2__c, Dummy_Payment_Status__c, Payment_Amount__c, Balance_Amount__c From Payment_Term__c Where Id =:paymentTermsIds]);
        Map<Id, Decimal> paymentTermsTotalTransaction = new Map<Id, Decimal>();
        
        for(Payment_Transaction__c paymentTransaction : (List<Payment_Transaction__c>) records) {
            totalTransactionAmount = paymentTransaction.Amount__c;
            if(paymentTermsTotalTransaction.containsKey(paymentTransaction.Payment_Term__c)) {
                totalTransactionAmount += paymentTermsTotalTransaction.get(paymentTransaction.Payment_Term__c);
            }
            paymentTermsTotalTransaction.put(paymentTransaction.Payment_Term__c, totalTransactionAmount);
            if(paymentTransaction.Invoiced_Item__c != null) {
                invoiceItems.get(paymentTransaction.Invoiced_Item__c).Settled_Amount__c -= paymentTransaction.Amount__c;
            }
        }
        
        uow.registerDirty(invoiceItems.values());
        uow.registerDirty(paymentTermsMap.values());
        uow.commitWork();
    }
    
    /*
     * Method - sendNotification
     * Description - Send email on creation of new transaction.
	*/
    private void sendNotification(Map<Id, Payment_Term__c> paymentTermsMap, List<Payment_Transaction__c> records) {
        AKFD_Configuration__c appConfig = AKFD_Configuration__c.getOrgDefaults(); 
        String[] toAddresses;   
        String subject;
        String body;
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        Payment_Term__c tempPaymentTerm = paymentTermsMap.get(records[0].Payment_Term__c);
        ProformaInvoice__c acceptedProformaInvoice;
        try{
            acceptedProformaInvoice = [SELECT Id, Name FROM ProformaInvoice__c Where Status__c = 'Accepted' AND Opportunity__c = :tempPaymentTerm.Opportunity__c];
        } catch(QueryException e) {
            system.debug('error ' + e);
        }
        if(acceptedProformaInvoice != null) {
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'supportteam@akfdstudio.com'];
            System.debug('OWEA ' + owea);
            for(Payment_Transaction__c transactionRecord : records) {
                mail = new Messaging.SingleEmailMessage();
                String poNumber =  paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.PO_No__c;
                subject = 'Receipt of payment for PI ' + acceptedProformaInvoice.Name + ', ' + paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Account.Name + '-' + paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.RecordType.Name;
                body = 'Greetings from '+paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Merchant__r.Name + ',<br/><br/>'
                    + 'We are in receipt of a payment of ' + transactionRecord.CurrencyIsoCode + ' ' + transactionRecord.Amount__c +'  vide the below.' + '<br/><br/>'
                    + 'Transaction Details:' + '<br/>'
                    + 'Unique Transaction Number: ' + transactionRecord.Unique_Transaction_Number__c + '<br/>'
                    + 'Customer: ' + paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Account.Name + '<br/>'
                    + 'Reference: ' + acceptedProformaInvoice.Name + ' ' + (poNumber!=null?poNumber:' ')  + '<br/><br/>'
                    + 'Thank you for the same.' + '<br/><br/>'
                    + 'We are now processing your order and will update you on the progress. Please note that the duration required for the fulfillment of your order, as indicated in the PI starts from today.' + '<br/>'
                    + 'In case there is any error in the details as mentioned above, please do not hesitate to get in touch with us.' + '<br/><br/>'
                    + 'Thanking you ' + '<br/>'
                    + paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Owner.Name;
                
                toAddresses = new String[] { appConfig.AKF_Process_Admin_Email__c, paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Owner.Email};   
                    if(paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Account.Email__c != null){
                        toAddresses.add(paymentTermsMap.get(transactionRecord.Payment_Term__c).Opportunity__r.Account.Email__c);
                    }
                System.debug('toAddresses');
                Integer index = 0;
                while(index < toAddresses.size()) {
                    if(toAddresses.get(index) == null) {
                        toAddresses.remove(index);
                    } else {
                        index++;
                    }
                }
                if(toAddresses == null || toAddresses.isEmpty()) {
                    return;        
                }
                mail.setToAddresses(toAddresses);
                mail.setSubject(subject);
                mail.setHtmlBody(body);
                if ( owea.size() > 0 ) { System.debug('Inside Sert Default');
                                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                                       }
                mails.add(mail);
            }
            if (!mails.isEmpty()) {
                Messaging.sendEmail(mails);
            }
            
        }
    }
}