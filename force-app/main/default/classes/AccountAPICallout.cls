public class AccountAPICallout {
    @InvocableMethod
    public static void upsertAccounts(List<AccountInput> accountInputs) {
        system.debug('accountInputs ' + accountInputs);
        if(accountInputs.size() == 1) {
            String accountXml = MyXmlPageController.getAccountXml(accountInputs[0].mAccount, accountInputs[0].addresses, accountInputs[0].contacts);
            System.debug('accountXml ' + accountXml);
            if(String.isNotBlank(accountXml)) {
                //Futureapexcallout.productCalloutApex(String.valueOf(RequestBodyXML.APIType.ACCOUNT), accountInputs[0].mAccount.Name, accountXml);
            }
        }
    }
    
    public class AccountInput {
        
        @InvocableVariable
        public Account mAccount;
        
        @InvocableVariable
        public List<Address__c> addresses;
        
        @InvocableVariable
        public List<Contact> contacts;
        
    }
}