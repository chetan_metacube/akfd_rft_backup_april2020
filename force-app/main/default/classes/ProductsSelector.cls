public class ProductsSelector extends fflib_SObjectSelector implements IProductsSelector {
    
    private static IProductsSelector instance = null;
    
    public static IProductsSelector getInstance() {
        if(instance == null) {
            instance = (IProductsSelector)Application.selector().newInstance(Product2.SObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Product2.Id,
            Product2.Name,
            Product2.Total_Stock__c,
            Product2.Minimum_Stock_Quantity__c,
            Product2.Root_Price__c,
            Product2.Product_SKU__c,
            Product2.Status__c,
            Product2.Unit__c,
            Product2.Family,
            Product2.Retail_Price__c,
            Product2.Alloted_Stock__c,
            Product2.Hidden_Total_Stock_Field__c
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return Product2.SObjectType;
    }
    
    public List<Product2> selectById(Set<ID> idSet) {
        return (List<Product2>) selectSObjectsById(idSet);
    }
    
    public List<Product2> selectProductsByName(Set<String> names) {
        fflib_QueryFactory query = newQueryFactory();
        query.setCondition('Name IN :names');
        query.addOrdering('Name', fflib_QueryFactory.SortOrder.ASCENDING);
        query.setLimit( Limits.getLimitQueryRows() );
        return Database.query(
            query.toSOQL()
        );
    }
}