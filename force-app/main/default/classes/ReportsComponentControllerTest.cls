@isTest
public class ReportsComponentControllerTest {
    @isTest(SeeAllData='true')
    static void testGetReportsWithBasicFilters() {
        List<String> reportNames = new List<String>();
        reportNames.add('Cash Outflow');
        reportNames.add('Cash Inflow');
        ReportsComponentController.BasicReportFilter basicReportFilter = new ReportsComponentController.BasicReportFilter(true, 'MONTH', '', '' ,'');
        String basicReportFilterJson = JSON.serialize(basicReportFilter);
        String customReportFilterJson = null;
        String reportResultJson = ReportsComponentController.getReports(reportNames, basicReportFilterJson, customReportFilterJson);
        ReportsComponentController.ReportResult reportResult = (ReportsComponentController.ReportResult)JSON.deserialize(reportResultJson, ReportsComponentController.ReportResult.class);
		System.assertNotEquals(null, reportResult);
    }
    
    @isTest(SeeAllData='true')
    static void testGetReportsWithCustomFilters() {
        List<String> reportNames = new List<String>();
        reportNames.add('Selling trend');
        ReportsComponentController.BasicReportFilter basicReportFilter = new ReportsComponentController.BasicReportFilter(true, 'YEAR', '2019', '' ,'');
        String basicReportFilterJson = JSON.serialize(basicReportFilter);
        ReportsComponentController.CustomReportFilter customReportFilter = new ReportsComponentController.CustomReportFilter(false, null, true, 'Tst Product');
        String customReportFilterJson = JSON.serialize(customReportFilter);
        String reportResultJson = ReportsComponentController.getReports(reportNames, basicReportFilterJson, customReportFilterJson);
        ReportsComponentController.ReportResult reportResult = (ReportsComponentController.ReportResult)JSON.deserialize(reportResultJson, ReportsComponentController.ReportResult.class);
        System.assertNotEquals(null, reportResult);
        
        customReportFilter = new ReportsComponentController.CustomReportFilter(false, null, true, null);
        customReportFilterJson = JSON.serialize(customReportFilter);
        reportResultJson = ReportsComponentController.getReports(reportNames, basicReportFilterJson, customReportFilterJson);
        reportResult = (ReportsComponentController.ReportResult)JSON.deserialize(reportResultJson, ReportsComponentController.ReportResult.class);
        System.assertNotEquals(null, reportResult);
    }
}