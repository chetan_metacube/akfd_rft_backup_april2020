/**
 * Used in DesiredProductsController.
 * Class contains Desired Products related functional logics.
 * Author: Chetan Sharma (1 March 2019)
 * Last Modified By: Chetan Sharma (17 June 2019)
 */
public class DesiredProductsService {
    private static IDesiredProductsService instance;
	
    /** 
    * Method contains logic to add desired products.
    * @param  Desired Products map
    * @return List of added desired products
    */ 
    public static List<PO_Product__c> addDesiredProducts(Map<Id, List<PO_Product__c>> desiredProducts) {
        
        return service().addDesiredProducts(desiredProducts);
        
    }
    
    
    private static IDesiredProductsService service() {
        
        if(instance == null) {
            instance = (IDesiredProductsService) Application.service().newInstance(IDesiredProductsService.class);
        }
        
        return instance;
         
    }
    /*
    public class OpportunitySpecificDesiredProducts { 
        public Opportunity opportunity {get;set;}
        public List<DesiredProduct> desiredProducts {get;set;}
        
        public OpportunitySpecificDesiredProducts(Opportunity opportunity, List<DesiredProduct> desiredProducts) {
            this.opportunity = opportunity;
            this.desiredProducts = desiredProducts;
        }
    }
    
    public class DesiredProduct {
        public Id Id {get;set;}
        public String Name {get;set;}
        public Id Product {get;set;}
        public Id OpportunityId {get;set;}
        public Decimal Quantity {get;set;}
    }
    */
}