@isTest
public class QuotationControllerTest {
	 @testSetup static void setup(){
        User adminUser = MockData.createUsers('System Administrator', 1)[0];
        User chatterFreeUser = MockData.createUsers('Read Only', 1)[0];
        
        List<Account> accountMerchantObj = MockData.createAccounts('Company/Consignor', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        List<Contact> contactObj = MockData.createContacts(accounts, 1);
        
        Lead_Source__c leadSourceObj = new Lead_Source__c(Name = 'Test exhibition');
        insert leadSourceObj;
        List<Opportunity> currentOpportunity = MockData.createOpportunities(accounts, accountMerchantObj, contactObj, new List<Lead_Source__c>{leadSourceObj}, 1, 'Export');
    	List<Product2> products = MockData.createProducts(1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(accounts, currentOpportunity, products, 1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        List<PriceBook_Product_Entry__c> priceBookEntries = MockData.createPriceBookEntries(accounts, products, priceBooks, 1);
         
     }
    
    @isTest static void testCreateQuotationMethod(){ 
    	Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].id;
        //FOr pricebookentries test class
        List<PriceBook_Product_Entry__c> pbEntry = PricebookProductEntriesSelector.getInstance().selectPricebookEntries();
        //
        PO_Product__c desiredProducts = [SELECT Name, Product__r.Name, Product__c, Quantity__c, Opportunity__c, 
                                               Product__r.GST__c, RecordType.Name, Retail_Price__c, Unit__c FROM PO_Product__c 
                                               WHERE Opportunity__c = :opportunityId LIMIT 1];
        //desiredProducts.Quantity__c = 10.00;
        String desiredProduct = '[{"Unit__c":"SQMT.","Retail_Price__c":'+desiredProducts.Retail_Price__c +
            ',"RecordType.Name":"'+desiredProducts.RecordType.Name+'","Product__r.GST__c":12,"Product__r.Image__c":"", "Opportunity__c":"'+desiredProducts.Opportunity__c +
            '","Quantity__c":2,"Product__c":"'+desiredProducts.Product__c+'","Product__r.Name":"'+desiredProducts.Product__r.Name+'","Name":"DP-0227","Id":"'+desiredProducts.Id+'"}]';
        String quotationId = QuotationController.createQuotation(opportunityId, desiredProduct);
        System.assert(quotationId != null);
    }
}