public interface IProductsSelector extends fflib_ISObjectSelector {
    List<Product2> selectProductsByName(Set<String> names);
    List<Product2> selectById(Set<ID> idSet);
}