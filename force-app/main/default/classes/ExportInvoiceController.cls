public with sharing class ExportInvoiceController {
    /**
    * Properties and variables
    */
    public List<String> headings { get; set; }    
    public List<Object> recordList { get; set; }   
    public List<Map<String, String>> recordsMap { get; set; }   
    public String newLine { get; set; }
    public String currentDateTime { get; set; }
    public Map<String, String> apiNamesWithColumnHeadrs { get; set; }
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    private final String KEY_EXPORT = 'Export';
    private final String KEY_INTERSTATE = 'Inter State Gst Sales';
    private final String KEY_LOCAL = 'GST SALES';
    
    /**
    * Constructor
    */
    public ExportInvoiceController() {   
        newLine = '\n';
        headings = new List<String>();    
        recordsMap = new List<Map<String, String>>();
        currentDateTime = '(' + System.Now().format('dd-MM-YYYY HH-mm a','IST') + ')';
        apiNamesWithColumnHeadrs = ExportToTallyUtils.invoiceColumnsMapping;
        headings = ExportToTallyUtils.invoiceColumnsKey;
        String query = 'SELECT ';
        Boolean isStateTax;
        Map<String, Schema.SObjectField> invoiceItemFieldsMap = Schema.getGlobalDescribe().get('Invoiced_Items__c').getDescribe().fields.getMap();
        Map<String, Schema.SObjectField> invoiceProductFieldsMap = Schema.getGlobalDescribe().get('Invoiced_Product__c').getDescribe().fields.getMap();
        List<String> invoicedItemsApiNames = new List<String>{'Id', 'Name', 'Invoice__r.Name', 'Invoice__r.RecordType.Name', 
                                                    'Invoice__r.Opportunity__r.Packing_Percentage__c', 'Invoice__c', 'Invoice__r.Product_Liability_Insurance__c',
                                                    'Invoice__r.Date__c', 'Invoice__r.Total_Amount__c', 'Invoice__r.Total_Taxable_Amount__c', 'Invoice__r.Net_Invoice_Amount__c',
                                                    'Invoice__r.Billing_Address__r.Street__c', 'Invoice__r.Billing_Address__r.City__c', 'Invoice__r.Packing_Percentage__c',
                                                    'Invoice__r.Billing_Address__r.State_Province__c', 'Invoice__r.Billing_Address__r.Country__c',
                                                    'Invoice__r.Billing_Address__r.Zip_Postal_Code__c', 'Invoice__r.Billing_Address__r.GST_Id__c',
                                                    'Invoice__r.Delivery_Address__r.Street__c', 'Invoice__r.Delivery_Address__r.City__c',
                                                    'Invoice__r.Delivery_Address__r.State_Province__c', 'Invoice__r.Delivery_Address__r.Country__c',
                                                    'Invoice__r.Delivery_Address__r.Zip_Postal_Code__c', 'Invoice__r.CurrencyIsoCode',
                                                    'Invoice__r.Freight_Amount__c', 'Invoice__r.Shipping_Charges__c', 'Invoice__r.Conversion_Rate_To_INR__c',
                                                    'Invoice__r.Packing_Charges__c', 'Invoice__r.Account__r.Name', 'Invoice__r.Account__r.GST_Id__c',
                                                    'Invoice__r.Trade_Discount__c', 'Invoice__r.Invoice_Amount_In_INR__c', 'Opportunity__r.Merchant__r.BillingState' };
                                                        
		List<String> invoicedProductsApiNames = new List<String>{'Product__r.Name', 
                                                     'Product__r.Narrative__c', 'Quantity__c', 'Product__r.HSN_Tax__r.Name', 'PO_Product__r.Discount_Percentage__c',
                                                     'Product__r.Unit__c', 'Unit_Price__c', 'Total_Price__c', 'GST__c', 'Product__r.Stock_Group__c', 'Taxable_Value__c' };
                                                         
        for(String apiName : invoicedItemsApiNames) {
            query += apiName + ', ';
        }    
        query += '( SELECT ';
        for(String apiName : invoicedProductsApiNames) {
            query += apiName + ', ';
        }  
        query = query.removeEnd(', ');
        query += ' FROM Invoiced_Products__r)';
        query += ' FROM Invoiced_Items__c Where Invoice__r.Status__c = \'Reviewed\' ORDER BY CreatedDate DESC LIMIT 50 ';
        List<Invoiced_Items__c> fieldDataMapping = Database.query(query);
        Map<String, String> data;
        String formattedDate = '';
        Date invoiceDate;
        Decimal packingCharges;
        Decimal freightCharges;
        Decimal additionalTaxAmount;
        Decimal totalDiscount;
        Decimal taxAmount;
        Integer productsListSize;
        Integer productCounter;
        Decimal discountPerProduct;
        List<Map<String, String>> record;
        for(Invoiced_Items__c item : fieldDataMapping) {
            record = new List<Map<String, String>>();
            isStateTax = item.Invoice__r.Billing_Address__r.State_Province__c == item.Opportunity__r.Merchant__r.BillingState;
            for(Invoiced_Product__c invoicedProduct : item.Invoiced_Products__r) {
                data = new Map<String, String>();
                for(String columnName : headings) {
                    if(invoicedItemsApiNames.contains(columnName)) {
                        if(columnName.contains('.')) {
                            data.put(columnName, String.valueOf(getFieldValue(item, columnName) == null ? '' : getFieldValue(item, columnName)));
                        } else {
                            data.put(columnName, String.valueOf(item.get(columnName) == null ? '' : item.get(columnName)));
                        }
                    } else if(invoicedProductsApiNames.contains(columnName)) {
                        if(columnName.contains('.')) {
                            data.put(columnName, String.valueOf(getFieldValue(invoicedProduct, columnName) == null ? '' : getFieldValue(invoicedProduct, columnName)));
                        } else {
                            data.put(columnName, String.valueOf(invoicedProduct.get(columnName) == null ? '' : invoicedProduct.get(columnName)));
                        }
                    } else {
                        data.put(columnName, '');
                    }
                }
                invoiceDate = item.Invoice__r.Date__c;
                formattedDate = DateTime.newInstance(invoiceDate.year(), invoiceDate.month(), invoiceDate.day()).format('d/MMM/YYYY');
                data.put('Invoice__r.Date__c', formattedDate);
                data.put('ShipmentGodown', 'DISPATCH GODOWN');
                data.put('SalesLedger', getSalesLedger(item.Invoice__r.RecordType.Name, isStateTax, invoicedProduct.GST__c, invoicedProduct.Product__r.Stock_Group__c));
                if(item.Invoice__r.RecordType.Name != 'Export') {
                    if(invoicedProduct.GST__c != null && invoicedProduct.GST__c != 0) {
                    	taxAmount = invoicedProduct.Taxable_Value__c * invoicedProduct.GST__c / 100;
                    }
                    discountPerProduct = invoicedProduct.PO_Product__r.Discount_Percentage__c * invoicedProduct.Unit_Price__c / 100;
                    data.put('Discount Percent', String.valueOf(invoicedProduct.PO_Product__r.Discount_Percentage__c.setScale(2)));
                    data.put('Discount Amount', String.valueOf(discountPerProduct.setScale(2)));
                    data.put('Sales Amount', String.valueOf(invoicedProduct.Taxable_Value__c.setScale(2)));
                    if(invoicedProduct.GST__c > 0) {
                        if(isStateTax) {
                            data.put('SGST ' + (invoicedProduct.GST__c / 2), String.valueOf((taxAmount / 2).setScale(2)));
                            data.put('CGST ' + (invoicedProduct.GST__c / 2), String.valueOf((taxAmount / 2).setScale(2)));
                        } else {
                            data.put('IGST ' + (invoicedProduct.GST__c), String.valueOf(taxAmount.setScale(2)));
                        }
                    }
                } else {
                    data.put('Sales Amount', String.valueOf(invoicedProduct.Taxable_Value__c.setScale(2)));
                }
                data.put('Billing_Address__r', getFullAddress(item.Invoice__r.Billing_Address__r));
                data.put('Delivery_Address__r', getFullAddress(item.Invoice__r.Delivery_Address__r));
                record.add(data);
            }
            if(item.Invoice__r.RecordType.Name != 'Export') {
                freightCharges = item.Invoice__r.Freight_Amount__c == null ? 0.00 : item.Invoice__r.Freight_Amount__c;
                if(item.Invoice__r.Packing_Percentage__c != null) {
                    packingCharges = item.Invoice__r.Packing_Percentage__c * item.Invoice__r.Total_Taxable_Amount__c / 100;
                }
                if(isStateTax) {
                    if(record[record.size() - 1].get('SGST 9.00') == '') {
                        additionalTaxAmount = (packingCharges * 18 / 100) + (freightCharges * 18 / 100);
                    } else {
                        additionalTaxAmount = (packingCharges * 18 / 100) + (freightCharges * 18 / 100) + (Double.valueOf(record[record.size() - 1].get('SGST 9.00')) * 2);
                    }
                    additionalTaxAmount = additionalTaxAmount.setScale(2);
                    record[record.size() - 1].put('LOCAL Freight_Amount__c', item.Invoice__r.Freight_Amount__c == null ? '0' : String.valueOf(freightCharges.setScale(2)));
                    record[record.size() - 1].put('LOCAL Packing_Percentage__c', item.Invoice__r.Packing_Percentage__c == null ? '0' : String.valueOf(packingCharges.setScale(2)));
                    record[record.size() - 1].put('SGST 9.00', String.valueOf(((additionalTaxAmount / 2).setScale(2))));
                    record[record.size() - 1].put('CGST 9.00', String.valueOf(((additionalTaxAmount / 2).setScale(2))));
                } else {
                    if(record[record.size() - 1].get('IGST 18.00') == '') {
                        additionalTaxAmount = (packingCharges * 18 / 100) + (freightCharges * 18 / 100);
                    } else {
                        additionalTaxAmount = (packingCharges * 18 / 100) + (freightCharges * 18 / 100) + Double.valueOf(record[record.size() - 1].get('IGST 18.00'));
                    }
                    additionalTaxAmount = additionalTaxAmount.setScale(2);
                    record[record.size() - 1].put('Intrastate Freight_Amount__c', item.Invoice__r.Freight_Amount__c == null ? '0' : String.valueOf(item.Invoice__r.Freight_Amount__c.setScale(2)));
                    record[record.size() - 1].put('Intrastate Packing_Percentage__c', item.Invoice__r.Packing_Percentage__c == null ? '0' : String.valueOf(packingCharges.setScale(2)));
                    record[record.size() - 1].put('IGST 18.00', String.valueOf(additionalTaxAmount.setScale(2)));
                }
            } else {
                packingCharges = item.Invoice__r.Packing_Charges__c == null ? 0.00 : item.Invoice__r.Packing_Charges__c;
                freightCharges = item.Invoice__r.Shipping_Charges__c == null ? 0.00 : item.Invoice__r.Shipping_Charges__c;
                record[record.size() - 1].put('Export Packing_Charges__c', item.Invoice__r.Packing_Charges__c == null ? '0' : String.valueOf(packingCharges.setScale(2)));
                record[record.size() - 1].put('Export Shipping_Charges__c', item.Invoice__r.Shipping_Charges__c == null ? '0' : String.valueOf(freightCharges.setScale(2)));
                totalDiscount = item.Invoice__r.Trade_Discount__c == null ? 0 : item.Invoice__r.Trade_Discount__c.setScale(2);
                totalDiscount += item.Invoice__r.Product_Liability_Insurance__c == null ? 0 : item.Invoice__r.Product_Liability_Insurance__c.setScale(2);
                record[record.size() - 1].put('Total Discount', String.valueOf(totalDiscount));
            }
            record[record.size() - 1].put('Total Invoice Amount', String.valueOf(item.Invoice__r.Net_Invoice_Amount__c.setScale(2)));
            record[record.size() - 1].put('Conversion Rate', String.valueOf(item.Invoice__r.Conversion_Rate_To_INR__c.setScale(2)));
            record[record.size() - 1].put('Invoice_Amount_In_INR__c', String.valueOf(item.Invoice__r.Invoice_Amount_In_INR__c.setScale(2)));
            recordsMap.addAll(record);
        }
                
    }              
    
    private static String getSalesLedger(String salesProcess, Boolean isStateTax, Decimal gstPercent, String productGroup) {
        String result = '';
        if(salesProcess.equals('Export')) {
            result += 'SALES EXPORT';
        } else {
            if(isStateTax) {
                result += 'GST SALES' + ' ' + (gstPercent == null ? '' : gstPercent == 0 ? 'EXEMPT' : '@' + String.valueOf(gstPercent.setScale(0)) + '%');
            } else {
                result += 'Inter State Gst Sales' + ' ' + (gstPercent == null ? '' : gstPercent == 0 ? 'Exempt' : '@' + String.valueOf(gstPercent.setScale(0)) + '%');
            }
        }
        result += productGroup == null || productGroup.equals('') ? '' : ' (' + productGroup + ')';
        return result;
    }
    
    private Object getFieldValue(SObject obj, String field) {
        if(obj == null) {
            return null;
        }
        if(field.contains('.')) {
            String nextField = field.substringAfter('.');
            String relation = field.substringBefore('.');
            return getFieldValue((SObject)obj.getSObject(relation), nextField);
        } else {
            return obj.get(field);
        }
    }
    
    private String getFullAddress(Address__c address) {
        String fullAddress = '', street, city, state, country, zip;
        if(address != null) {
            street = address.Street__c == null ? '' : address.Street__c + ', ';
            city = address.City__c == null ? '' : address.City__c + ', ';
            state = address.State_Province__c == null ? '' : address.State_Province__c + ', ';
            country = address.Country__c == null ? '' : address.Country__c + ' ';
            zip = address.Zip_Postal_Code__c == null ? '' : address.Zip_Postal_Code__c;
            fullAddress += street + city + state + country + zip;
        }
        return fullAddress;
    }
}