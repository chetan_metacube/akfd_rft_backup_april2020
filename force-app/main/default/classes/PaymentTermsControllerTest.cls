@isTest
public class PaymentTermsControllerTest {
    
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        
        List<Default_Payment_Term__c> defaultPaymentTerms = new List<Default_Payment_Term__c>();
        defaultPaymentTerms.add(new Default_Payment_Term__c(Name = 'Installment1', Payment_Percentage__c = 25.00));
        defaultPaymentTerms.add(new Default_Payment_Term__c(Name = 'Installment2', Payment_Percentage__c = 50.00));
        defaultPaymentTerms.add(new Default_Payment_Term__c(Name = 'Installment3', Payment_Percentage__c = 25.00));
        insert defaultPaymentTerms;
        
        List<Account> customerAccounts = MockData.createAccounts('Customer', 1);
        List<Account> companyConsignorAccounts = MockData.createAccounts('Company/Consignor', 1);
        List<Contact> contacts = MockData.createContacts(customerAccounts, 1);
        
        List<Lead_Source__c> leadSources = new List<Lead_Source__c>();
        leadSources.add(new Lead_Source__c(Name = 'Test Lead Source'));
        insert leadSources;
        
        List<Opportunity> opportunities = MockData.createOpportunities(customerAccounts, companyConsignorAccounts, contacts, leadSources, 1, 'Export');
                List<Product2> products = MockData.createProducts(1);
        
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(customerAccounts, products, 1);
        List<PriceBook_Product_Entry__c> pricebookEntries = MockData.createPriceBookEntries(customerAccounts, products, priceBooks, 1);
        List<PO_Product__c> desiredProducts = MockData.createDesiredProducts(customerAccounts, opportunities, products, 1);
        
        List<ProformaInvoice__c> proformaInvoices = MockData.createProformaInvoices(opportunities, products, 1);
        proformaInvoices = [SELECT Id, Name, Status__c FROM ProformaInvoice__c WHERE Id IN : proformaInvoices];
        proformaInvoices[0].Status__c = 'Accepted';
        update proformaInvoices;
    }
    
    @isTest static void positiveTestCaseFor_getPaymentTermsMethod() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        List<Payment_Term__c> paymentTerms = [SELECT Id, Name, Opportunity__c, Payment_Amount__c, Payment_Status_New__c, RecordTypeId, RecordType.Name, Payment_Percentage__c, Balance_Amount__c, Base_Amount_INR__c, Due_Date__c, Conversion_Rate_To_INR__c, CurrencyIsoCode FROM Payment_Term__c WHERE Opportunity__c =: opportunity.Id];
        //System.debug('Payment Terms ' + paymentTerms);
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        System.assert(result != null && result.length() > 0);
        PaymentTermsController.PaymentTermsWrapper paymentTermWrapper = (PaymentTermsController.PaymentTermsWrapper)JSON.deserialize(result, PaymentTermsController.PaymentTermsWrapper.class);
        System.assert(paymentTermWrapper.paymentTerms.size() == paymentTerms.size());
        String expectedCurrencySymbol = '';
        if(opportunity.CurrencyIsoCode == 'INR'){
            expectedCurrencySymbol = 'INR ';
        } else {
            expectedCurrencySymbol = CurrencyUtils.getSymbol(opportunity.CurrencyIsoCode);
        }
        System.assert(paymentTermWrapper.currencySymbol == expectedCurrencySymbol);
    }
    
    @isTest static void negativeTestCaseFor_getPaymentTermsMethod_WhenOppIdPassedNull() {
        try {
            PaymentTermsController.getPaymentTerms(null);
        } catch (Exception excp) {
            System.assert(excp.getMessage().contains('Invalid Opportunity Id entered.'));
        }
    }
    
     @isTest static void negativeTestCase_For_getPaymentTermsMethod_WhenUserDonotHaveAccess() { 
        User chatterFreeUser = [SELECT Id, LastName, FirstName, Alias, Email, Username, ProfileId FROM User WHERE Profile.Name = 'Read Only' LIMIT 1];
         try {
             System.runAs(chatterFreeUser) {
                 Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
                 List<Payment_Term__c> paymentTerms = [SELECT Id, Name, Opportunity__c, Payment_Amount__c, Payment_Status_New__c, RecordTypeId, RecordType.Name, Payment_Percentage__c, Balance_Amount__c, Base_Amount_INR__c, Due_Date__c, Conversion_Rate_To_INR__c, CurrencyIsoCode FROM Payment_Term__c WHERE Opportunity__c =: opportunity.Id];
                 //System.debug('Payment Terms ' + paymentTerms);
                 String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
             }
        } catch (Exception exc) {
            System.assert(exc.getMessage().contains('Insufficient Privileges.'));
        }
    }
    
    @isTest static void positiveTestCaseFor_updatePaymentTermsMethod() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        System.assert(result != null && result.length() > 0);
        PaymentTermsController.PaymentTermsWrapper paymentTermWrapper = (PaymentTermsController.PaymentTermsWrapper)JSON.deserialize(result, PaymentTermsController.PaymentTermsWrapper.class);
        for(Integer i = 0 ; i < paymentTermWrapper.paymentTerms.size() ; i++ ) {
            paymentTermWrapper.paymentTerms[i].Payment_Percentage__c -= 5;
        }
        System.assert(PaymentTermsController.updatePaymentTerm(opportunity.Id, JSON.serialize(paymentTermWrapper)));
    }
    
    @isTest static void negativeTestCaseFor_updatePaymentTermsMethod_WhenOppIdPassedNull() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        System.assert(result != null && result.length() > 0);
        PaymentTermsController.PaymentTermsWrapper paymentTermWrapper = (PaymentTermsController.PaymentTermsWrapper)JSON.deserialize(result, PaymentTermsController.PaymentTermsWrapper.class);
        for(Integer i = 0 ; i < paymentTermWrapper.paymentTerms.size() ; i++ ) {
            paymentTermWrapper.paymentTerms[i].Payment_Percentage__c -= 5;
        }
        try {
            PaymentTermsController.updatePaymentTerm(null, JSON.serialize(paymentTermWrapper));
        } catch (AuraHandledException aexc) {
            System.assert(aexc.getMessage().contains('Invalid opportunityId passed.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_updatePaymentTermsMethod_WhenWrapperIdPassedNull() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        try {
            PaymentTermsController.updatePaymentTerm(opportunity.Id, null);
        } catch (AuraHandledException aexc) {
            System.assert(aexc.getMessage().contains('Invalid paymentTermWrapperString passed.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_updatePaymentTermsMethod_WhenNoValueChanged() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        try {
            PaymentTermsController.updatePaymentTerm(opportunity.Id, result);
        } catch (AuraHandledException aexc) {
            System.assert(aexc.getMessage().contains('No Payment Term Value Changed.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_updatePaymentTermsMethod_WhenTotalPercentageGreaterThan100() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        System.assert(result != null && result.length() > 0);
        PaymentTermsController.PaymentTermsWrapper paymentTermWrapper = (PaymentTermsController.PaymentTermsWrapper)JSON.deserialize(result, PaymentTermsController.PaymentTermsWrapper.class);
        for(Integer i = 0 ; i < paymentTermWrapper.paymentTerms.size() ; i++ ) {
            paymentTermWrapper.paymentTerms[i].Payment_Percentage__c += 5;
        }
        
        try {
            PaymentTermsController.updatePaymentTerm(opportunity.Id, JSON.serialize(paymentTermWrapper));
        } catch (AuraHandledException aexc) {
            System.assert(aexc.getMessage().contains('Total Payment Percentage should be less than or equal to 100%.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_updatePaymentTermsMethod_WhenPercentageIsNegative() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        System.assert(result != null && result.length() > 0);
        PaymentTermsController.PaymentTermsWrapper paymentTermWrapper = (PaymentTermsController.PaymentTermsWrapper)JSON.deserialize(result, PaymentTermsController.PaymentTermsWrapper.class);
        for(Integer i = 0 ; i < paymentTermWrapper.paymentTerms.size() ; i++ ) {
            paymentTermWrapper.paymentTerms[i].Payment_Percentage__c = -15;
        }
        
        try {
            PaymentTermsController.updatePaymentTerm(opportunity.Id, JSON.serialize(paymentTermWrapper));
        } catch (AuraHandledException aexc) {
            System.assert(aexc.getMessage().contains('Payment term percentage should be greater than zero.'));
        }
    }
    
    @isTest static void negativeTestCaseFor_updatePaymentTermsMethod_WhenPaymentTermIsNotDue() {
        Opportunity opportunity = [Select Id, Name, CurrencyIsoCode FROM Opportunity LIMIT 1];
        List<Payment_Term__c> paymentTerms = [SELECT Id, Name, Opportunity__c, Payment_Amount__c, Payment_Status_New__c, RecordTypeId, RecordType.Name, Payment_Percentage__c, Balance_Amount__c, Base_Amount_INR__c, Due_Date__c, Conversion_Rate_To_INR__c, CurrencyIsoCode FROM Payment_Term__c WHERE Opportunity__c =: opportunity.Id];
        Payment_Transaction__c paymentTransaction = new Payment_Transaction__c( Amount__c = paymentTerms[0].Balance_Amount__c, Payment_Term__c = paymentTerms[0].Id);
        insert paymentTransaction;
        String result = PaymentTermsController.getPaymentTerms(opportunity.Id);
        System.assert(result != null && result.length() > 0);
        PaymentTermsController.PaymentTermsWrapper paymentTermWrapper = (PaymentTermsController.PaymentTermsWrapper)JSON.deserialize(result, PaymentTermsController.PaymentTermsWrapper.class);
        paymentTermWrapper.paymentTerms[0].Payment_Percentage__c -= 5;
        try {
            PaymentTermsController.updatePaymentTerm(opportunity.Id, JSON.serialize(paymentTermWrapper));
        } catch (AuraHandledException aexc) {
            System.assert(aexc.getMessage().contains('Payment Percentage can be edited for Due Payment Terms Only.'));
        }
    }
    
    
    
}