public class PriceBooks extends fflib_SObjectDomain {
    public PriceBooks(List<PriceBook_Product__c> records) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(records);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new PriceBooks(records);
        }
    }
    
    
    public override void onBeforeInsert() {
        List<ID> priceBookIds = new List<ID>();
        List<ID> customers = new List<ID>();
        for(PriceBook_Product__c priceBook : (List<PriceBook_Product__c>) records) {
            customers.add(priceBook.Customer__c);
            priceBookIds.add(priceBook.Id);
        }
        
        Map<Id, Account> accounts = new Map<Id, Account>([Select Id, CurrencyIsoCode From Account Where Id =:customers]);
        List<PriceBook_Product__c> pricebooks = [SELECT Id, Product__c, Customer__c from PriceBook_Product__c where Customer__c =:customers];
        
        for(PriceBook_Product__c priceBook : (List<PriceBook_Product__c>) records) {
            priceBook.CurrencyIsoCode = (accounts.get(priceBook.Customer__c)).CurrencyIsoCode;
            for(PriceBook_Product__c pb : pricebooks) {
                //check if pricebook for this account and product already exists, show error if true.
                if(priceBook.Customer__c == pb.Customer__c && priceBook.Product__c == pb.Product__c) {
                    priceBook.addError('Price Book has already been defined for this Account & Product.');
                }
            }
        }
    }
    
    
}