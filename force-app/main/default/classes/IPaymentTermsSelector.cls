public interface IPaymentTermsSelector extends fflib_ISObjectSelector {
	List<Payment_Term__c> selectInstallmentPaymentTermsByOpportunityIds(Set<Id> opportunityIds);
}