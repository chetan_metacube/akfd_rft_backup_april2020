public class Application {
    public static final fflib_Application.UnitOfWorkFactory UnitOfWork =  
        new fflib_Application.UnitOfWorkFactory(
            new List<SObjectType> {   
                Account.SObjectType,
                    Lead.SObjectType,
                    Case.SObjectType, 
                    Opportunity.SObjectType,
                    Contact.SObjectType,
                    Product2.SObjectType, 
                    PO_Product__c.SObjectType,
                    Quotation__c.SObjectType,
                    Quotation_Line_Item__c.SObjectType,
                    ProformaInvoice__c.SObjectType,
                    Proforma_Invoice_Line_Item__c.SObjectType,
                    Invoice__c.SObjectType,
                    Invoiced_Items__c.SObjectType,
                    Invoiced_Product__c.SObjectType
                    }
        );
    
    private static fflib_Application.SelectorFactory s_Selector;
    public static fflib_Application.SelectorFactory selector() {
        if (s_Selector == null) {
            s_Selector = new fflib_Application.SelectorFactory(
                new Map<SObjectType, Type> {
                    Case.SObjectType => CasesSelector.class,
                        //Lead.SObjectType => LeadsSelector.class,
                        Opportunity.SObjectType => OpportunitiesSelector.class,
                        Account.SObjectType => AccountsSelector.class,
                        Contact.SObjectType => ContactsSelector.class,
                        Lead.SObjectType => LeadsSelector.class,
                        Product2.SObjectType => ProductsSelector.class,
                        Pricebook_Product__c.SObjectType => PricebookProductsSelector.class,
                        Pricebook_Product_Entry__c.SObjectType => PricebookProductEntriesSelector.class,
                        Quotation__c.SObjectType => QuotationsSelector.class,
                        ProformaInvoice__c.SObjectType => ProformaInvoicesSelector.class,
                        Proforma_Invoice_Line_Item__c.SObjectType => ProformaInvoiceLineItemsSelector.class,
                        //Invoice__c.SObjectType => InvoicesSelector.class,
                        PO_Product__c.sObjectType => DesiredProductsSelector.class,
                        Invoiced_Items__c.SObjectType => InvoicedItemsSelector.class,
                        Invoiced_Product__c.SObjectType => InvoicedProductsSelector.class,
                        Alloted_Stock__c.SObjectType => AllotedStocksSelector.class,
                        Warehouse_Line_Item__c.SObjectType => WarehouseLineItemsSelector.class,
                        Payment_Term__c.SObjectType => PaymentTermsSelector.class,
                        AccountContactRelation.SObjectType => AccountContactRelationshipsSelector.class,
                        Projected_Stock__c.SObjectType => ProjectedStocksSelector.class
                        }
            );
        }
        
        return s_Selector;
    }
    
    private static fflib_Application.ServiceFactory s_Service;
    public static fflib_Application.ServiceFactory service() {
        if (s_Service == null) {
            s_Service = new fflib_Application.ServiceFactory(
                new Map<Type, Type> {
                    ILeadsService.class => LeadsServiceImpl.class,
                        ICasesService.class => CasesServiceImpl.class,
                        //IProformaInvoicesService.class => ProformaInvoicesServiceImpl.class,
                        IInvoicesService.class => InvoicesServiceImpl.class,
                        IQuotationsService.class => QuotationsServiceImpl.class, 
                        IProformaInvoicesService.class =>  ProformaInvoicesServiceImpl.class,
                        IDesiredProductsService.class => DesiredProductsServiceImpl.class,
                        IWarehouseProductsService.class => WarehouseProductsServiceImpl.class
                        }
            );
        }
        
        return s_Service;
    }
    
    
    // Configure and create the DomainFactory for this Application
    public static final fflib_Application.DomainFactory Domain = 
        new fflib_Application.DomainFactory(
            Application.s_Selector,
            new Map<SObjectType, Type> {
                ProformaInvoice__c.SObjectType => ProformaInvoices.Constructor.class,
                    PO_Product__c.SObjectType => DesiredProducts.Constructor.class
                    });
}