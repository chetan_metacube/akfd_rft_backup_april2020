public class WarehouseProductsServiceImpl implements IWarehouseProductsService {
    
    private static Id currentProformaInvoiceId {get;set;}
    
    public static  List<WarehouseProductsService.ProductData> getProductWarehousesByProformaInvoiceId(Id proformaInvoiceId) {
        //Validate Whether Proforma Invoice has already allocated stock / Accepted or not
        List<ProformaInvoice__c> proformaInvoices = (List<ProformaInvoice__c>)ProformaInvoicesSelector.getInstance().selectSObjectsById(new Set<Id>{proformaInvoiceId});
        System.debug('proformaInvoices@@' + proformaInvoices);   
        ProformaInvoice__c proformaInvoice = proformaInvoices.get(0);
        if(proformaInvoice.Status__c == null || !proformaInvoice.Status__c.equals('Accepted')) {
            throw getExceptionInstance('PI is not Accepted yet. Product Allocation can be done only after PI gets Accepted.');
        } else if(proformaInvoice.Product_Alloted__c) {
            throw getExceptionInstance('Product allotment is already done for this Proforma Invoice.');
        }
        
        //Soql query to get Proforma Invoice Line Item for specific proformaInvoiceId
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = (List<Proforma_Invoice_Line_Item__c>)ProformaInvoiceLineItemsSelector.getInstance().selectByProformaInvoiceIdWhichHasBalancedQuantity(new Set<Id>{proformaInvoiceId});
        System.debug('proformaInvoiceLineItems@@@' + proformaInvoiceLineItems);    
        Set<Id> productIds = new Set<Id>();
        
        /*
* Wrapping Product inside ProductWrapper - Respective required items from proforma invoice line item
* Adding wrapper to List of Product Wrapper
* Adding each product Id of proforma invoice line item to the list 
*/
        
        List<WarehouseProductsService.ProductWrapper> products = new List<WarehouseProductsService.ProductWrapper>();
        WarehouseProductsService.ProductWrapper wrapper;
        
        for(Proforma_Invoice_Line_Item__c lineItem : proformaInvoiceLineItems) {
            wrapper = new WarehouseProductsService.ProductWrapper(lineItem.Product__c, lineItem.Product__r.Name, lineItem.Balanced_Quantity__c, lineItem.Balanced_Quantity__c, 0.0, false);
            products.add(wrapper);
            productIds.add(lineItem.Product__c);
        }
        
        List<Id> warehouseIds = new List<Id>();
        WarehouseProductsService.WarehouseWrapper warehouseWrapper;
        List<WarehouseProductsService.WarehouseWrapper> warehouses;
        
        //Soql query to get Warehouse Line Item (Product-Warehouse) with alloted stock of specific Product respective with productIds list
        List<Warehouse_Line_Item__c> warehouseLineItems = (List<Warehouse_Line_Item__c>)WarehouseLineItemsSelector.getInstance().selectByProductIdsWhichHasCurrentStock(productIds);
        System.debug('warehouseLineItems@@@' + warehouseLineItems);    
        //System.debug('WarehouseLineItems ' + warehouseLineItems[0].Alloted_Stocks__r);
        /*
* Wrapping warehouse line item into WarehouseWrapper
* Adding wrapper to List of WarehouseWrapper 
* Adding alloted stock of warehouse product in AllotedStockMap
*/
        Map<Id, List<WarehouseProductsService.WarehouseWrapper>> productWarehouses = new Map<Id, List<WarehouseProductsService.WarehouseWrapper>>();
        for(Warehouse_Line_Item__c warehouseLineItem : warehouseLineItems) {
            warehouseWrapper = new WarehouseProductsService.WarehouseWrapper(warehouseLineItem);
            warehouseWrapper.allotments = getTotalStock(warehouseLineItem.Alloted_Stocks__r, proformaInvoiceId);
            warehouseWrapper.isZeroBooked = warehouseWrapper.allotments == 0.00 ? true : false;
            warehouseWrapper.currentAllotment = getCurrentPIStock(warehouseLineItem, proformaInvoiceId);
            warehouseWrapper.quantity = warehouseWrapper.currentAllotment;
            warehouseWrapper.allotStockId = warehouseLineItem.Id;
            if(productWarehouses.containsKey(warehouseLineItem.Product__c)) {
                warehouses = productWarehouses.get(warehouseLineItem.Product__c);
            } else {
                warehouses = new List<WarehouseProductsService.WarehouseWrapper>();
            }
            warehouses.add(warehouseWrapper);
            warehouseIds.add(warehouseLineItem.Warehouse__c);
            productWarehouses.put(warehouseLineItem.Product__c, warehouses);
        }
        List<WarehouseProductsService.ProductWarehouseWrapper> productWarehouseWrappers = new List<WarehouseProductsService.ProductWarehouseWrapper>();
        List<WarehouseProductsService.ProductData> productWarehouseData = new List<WarehouseProductsService.ProductData>();
        
        for(WarehouseProductsService.ProductWrapper productWrapper : products) {
            productWarehouseWrappers.add(new WarehouseProductsService.ProductWarehouseWrapper(productWrapper, productWarehouses.get(productWrapper.id)));
            productWarehouseData.add(new WarehouseProductsService.ProductData(productWrapper, productWarehouses.get(productWrapper.id)));
        }
        return productWarehouseData;
    }
    
    public static void allotStock(Id proformaInvoiceId, List<WarehouseProductsService.ProductData> productsData) {
        // Validate if booking quantity is greater than current stock of warehouse for that product.
        System.debug('productsData warehouse Wrapper Allot Stock' + productsData[0].productWarehouses);
        Map<Id, List<WarehouseProductsService.WarehouseWrapper>> productWarehouses = new Map<Id, List<WarehouseProductsService.WarehouseWrapper>>();
        // productWarehouse specific alloted stocks.
        Map<Id, List<WarehouseProductsService.BookedStockWrapper>> allotedStockMap = new Map<Id, List<WarehouseProductsService.BookedStockWrapper>>();
        List<Projected_Stock__c> allProjectedStocks = (List<Projected_Stock__c>)ProjectedStocksSelector.getInstance().selectByProformaInvoiceId(proformaInvoiceId);
        
        List<WarehouseProductsService.ProductWrapper> products = new List<WarehouseProductsService.ProductWrapper>();
        for(WarehouseProductsService.ProductData productData : productsData) {
            products.add(new WarehouseProductsService.ProductWrapper(productData.id, productData.name, productData.requiredQuantity, productData.projectedStock, productData.bookedQuantity, productData.isWarehouseAlloted));
            if(productData.productWarehouses != null) {
                productWarehouses.put(productData.id, productData.productWarehouses);
                for(WarehouseProductsService.WarehouseWrapper warehouseWrapper : productData.productWarehouses) {
                    allotedStockMap.put(warehouseWrapper.allotStockId, warehouseWrapper.allotedStocks);
                }
            }
        }
        
        // Set productWarehouses
        PageReference pageReference = null;
        List<WarehouseProductsService.WarehouseWrapper> warehouses = new List<WarehouseProductsService.WarehouseWrapper>();
        for(Id productId : productWarehouses.keySet()) {
            warehouses.addAll(productWarehouses.get(productId));
        }
        
        // Validate if booking quantity is greater than current stock of warehouse for that product.
        for(WarehouseProductsService.WarehouseWrapper wrapper : warehouses) {
            // Only selected ones are returned so no need to check isSelected
            if(wrapper.quantity > wrapper.warehouseLineItem.Current_Stock__c) {
                throw getExceptionInstance('Booking quantity should not be greater than Current Stock of warehouse.');
            }
        }
        
        List<WarehouseProductsService.BookedStockWrapper> allAllotedStock = new List<WarehouseProductsService.BookedStockWrapper>();
        for(Id allotedStockId : allotedStockMap.keySet()) {
            allAllotedStock.addAll(allotedStockMap.get(allotedStockId));
        }
        
        Map<Id, List<Alloted_Stock__c>> bookedStockOfProductCurrentProformaInvoice = new Map<Id, List<Alloted_Stock__c>>();
        Map<Id, List<Projected_Stock__c>> projectedStockOfProductCurrentProformaInvoice = new Map<Id, List<Projected_Stock__c>>();
        List<Projected_Stock__c> exisitingProjectedStock;
        
        //Wrapping product with its projected stock.
        for(Projected_Stock__c projectedStock : allProjectedStocks) {
            if(projectedStockOfProductCurrentProformaInvoice.containsKey(projectedStock.Product__c)) {
                exisitingProjectedStock = projectedStockOfProductCurrentProformaInvoice.get(projectedStock.Product__c);
            } else {
                exisitingProjectedStock = new List<Projected_Stock__c>();
            }
            exisitingProjectedStock.add(projectedStock);
            projectedStockOfProductCurrentProformaInvoice.put(projectedStock.Product__c, exisitingProjectedStock);
        }
        
        //Wrapping product with its booked stock.
        List<Alloted_Stock__c> bookedStocks;
        // Already Booked Stock for the current PI
        for(WarehouseProductsService.BookedStockWrapper bookedStockWrapper : allAllotedStock) {
            if(bookedStockWrapper.allotedStock.ProformaInvoice__c == proformaInvoiceId) {	
                if(bookedStockOfProductCurrentProformaInvoice.containsKey(bookedStockWrapper.allotedStock.Warehouse_Product__c)) {
                    bookedStocks = bookedStockOfProductCurrentProformaInvoice.get(bookedStockWrapper.allotedStock.Warehouse_Product__c);
                } else {
                    bookedStocks = new List<Alloted_Stock__c>();
                }
                bookedStocks.add(bookedStockWrapper.allotedStock);
                bookedStockOfProductCurrentProformaInvoice.put(bookedStockWrapper.allotedStock.Warehouse_Product__c, bookedStocks);
            }
        }
        System.debug('bookedStockOfProductCurrentProformaInvoice ' + bookedStockOfProductCurrentProformaInvoice);
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Projected_Stock__c.SObjectType,
                    Proforma_Invoice_Line_Item__c.SObjectType,
                    Payment_Term__c.SObjectType,
                    Opportunity.SObjectType,
                    Alloted_Stock__c.SObjectType,
                    ProformaInvoice__c.SObjectType
                    }
        );
        
        // Register upsert work
        UpsertUnitOfWorkAllotedStockHelper upsertAllotedStockWork = new UpsertUnitOfWorkAllotedStockHelper();
        UpsertUnitOfWorkProjectedStockHelper upsertProjectedStockWork = new UpsertUnitOfWorkProjectedStockHelper();
        uow.registerWork(upsertAllotedStockWork);
        uow.registerWork(upsertProjectedStockWork);
        
        List<Projected_Stock__c> projectedStocks = new List<Projected_Stock__c>();
        Projected_Stock__c newProjectedStock;
        Projected_Stock__c updatedInProductionProjectedStock;
        Boolean isInProductionProjectedStockExists;
        Boolean isNewProjectedStockExists;
        Decimal requiredQuantity;
        Decimal totalStockInProduction;
        
        /*
* Iteration to extract out the new projected stock, updated projected stock, projected stock to be delete
* If projected stock is with New status, it can be updated, if projected stock is with In Production status, it cannot be updated.
* Additional projected stock which is added (other than In Production state) will be considered as new projected stock.
* Any loose projected stock will be deleted.
*/
        for(WarehouseProductsService.ProductWrapper wrapper : products) {
            isNewProjectedStockExists = false;
            isInProductionProjectedStockExists = false;
            newProjectedStock = null;
            requiredQuantity = 0;
            totalStockInProduction = 0;
            if(wrapper.projectedStock > 0) {
                if(projectedStockOfProductCurrentProformaInvoice.get(wrapper.id) != null) {
                    exisitingProjectedStock = projectedStockOfProductCurrentProformaInvoice.get(wrapper.id);
                    for(Projected_Stock__c projectedStock : exisitingProjectedStock) {
                        if(projectedStock.Status__c == 'New') {
                            isNewProjectedStockExists = true;
                            newProjectedStock = projectedStock;
                        } else if(projectedStock.Status__c == 'In Production') {
                            system.debug('in prod');
                            system.debug('projectedStock.Required_Quantity__c ' + projectedStock.Required_Quantity__c);
                            isInProductionProjectedStockExists = true;
                            updatedInProductionProjectedStock = projectedStock;
                            totalStockInProduction += projectedStock.Required_Quantity__c;
                        }
                    }
                    requiredQuantity = wrapper.projectedStock - totalStockInProduction;
                    if(isInProductionProjectedStockExists) {
                        if(requiredQuantity <= 0) {
                            if(requiredQuantity == 0) {
                                //ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
                                //updatedInProductionProjectedStock.ProformaInvoice__c = null;
                                // uow.registerDirty(updatedInProductionProjectedStock);
                            } else {
                                updatedInProductionProjectedStock.Required_Quantity__c += requiredQuantity;
                                uow.registerDirty(updatedInProductionProjectedStock);
                                Projected_Stock__c newProjectedStockInProduction = new Projected_Stock__c();
                                newProjectedStockInProduction.Product__c = wrapper.id;
                                newProjectedStockInProduction.Required_Quantity__c = Math.abs(requiredQuantity);
                                newProjectedStockInProduction.Status__c = 'In Production';
                                uow.registerNew(newProjectedStockInProduction);
                            }
                            System.debug('isNewProjectedStockExists'+newProjectedStock);
                            if(isNewProjectedStockExists) {
                                uow.registerDeleted(newProjectedStock);
                            }
                        } else {
                            if(!isNewProjectedStockExists) {
                                newProjectedStock = new Projected_Stock__c();
                                newProjectedStock.Product__c = wrapper.id;
                                newProjectedStock.ProformaInvoice__c = proformaInvoiceId; 
                            }
                            newProjectedStock.Required_Quantity__c = requiredQuantity;
                        }
                    } else if(isNewProjectedStockExists){
                        newProjectedStock.Required_Quantity__c = requiredQuantity;
                    }
                } else {
                    newProjectedStock = new Projected_Stock__c();
                    newProjectedStock.Product__c = wrapper.id;
                    newProjectedStock.ProformaInvoice__c = proformaInvoiceId; 
                    newProjectedStock.Required_Quantity__c = wrapper.projectedStock;
                }
                if(newProjectedStock != null) {
                    projectedStocks.add(newProjectedStock);  
                }                 
            } else {
                if(projectedStockOfProductCurrentProformaInvoice.get(wrapper.id) != null) {
                    exisitingProjectedStock = projectedStockOfProductCurrentProformaInvoice.get(wrapper.id);
                    for(Projected_Stock__c projectedStock : exisitingProjectedStock) {
                        if(projectedStock.Status__c == 'New') {
                            uow.registerDeleted(projectedStock);
                        } else if(projectedStock.Status__c == 'In Production') {
                            system.debug('in prod');
                            system.debug('projectedStock.Required_Quantity__c ' + projectedStock.Required_Quantity__c);
                            isInProductionProjectedStockExists = true;
                            updatedInProductionProjectedStock = projectedStock;
                            totalStockInProduction += projectedStock.Required_Quantity__c;
                            /*ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
projectedStock.ProformaInvoice__c = null;
uow.registerDirty(projectedStock);*/
                        }
                    }
                    system.debug('totalStockInProduction ' + totalStockInProduction);
                        requiredQuantity = wrapper.projectedStock - totalStockInProduction;
                        system.debug('requiredQuantity ' + requiredQuantity);
                        if(isInProductionProjectedStockExists) {
                            if(requiredQuantity <= 0) {
                                if(requiredQuantity == 0) {
                                    //ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
                                   // updatedInProductionProjectedStock.ProformaInvoice__c = null;
                                	//uow.registerDirty(updatedInProductionProjectedStock);
                                } else {
                                    updatedInProductionProjectedStock.ProformaInvoice__c = null;
                                    uow.registerDirty(updatedInProductionProjectedStock);
                                    /*Projected_Stock__c newProjectedStockInProduction = new Projected_Stock__c();
                                    newProjectedStockInProduction.Product__c = wrapper.id;
                                    newProjectedStockInProduction.Required_Quantity__c = Math.abs(requiredQuantity);
                                    newProjectedStockInProduction.Status__c = 'In Production';
                                    uow.registerNew(newProjectedStockInProduction);*/
                                }
                            }
                        }
                }               
            }
        }
        System.debug('Final Projected Stocks ' + projectedStocks );
        if(projectedStocks.size() > 0) {
            try {
                upsertProjectedStockWork.registerProjectedStockUpsert(projectedStocks);
            } catch(DMLException ex) {
                throw getExceptionInstance('Something went wrong!!! ' + ex);
            }
        }
        
        List<Alloted_Stock__c> allotedStocks = new List<Alloted_Stock__c>();
        Alloted_Stock__c newAllotedStock;
        List<Alloted_Stock__c> existingAllotedStock;
        List<Alloted_Stock__c> allotedStockToBeDeleted = new List<Alloted_Stock__c>();
        /*
* Iteration to extract out the new booked stock, booked projected stock, booked stock to be delete
* Additional booked stock which is added will be considered as new booked stock.
* Any loose booked stock will be deleted.
*/
        System.debug('All product warehouses ' + productWarehouses);
        for(Id productId : productWarehouses.keySet()) {
            
            for(WarehouseProductsService.WarehouseWrapper warehousesWrapper : productWarehouses.get(productId)) {
                newAllotedStock = null;
                // If quantity is positive then add new stock else delete the booked stock
                if(warehousesWrapper.quantity > 0) {
                    System.debug('Quantity > 0 ');
                    // Get product and productWarehouse specific alloted Stock for current PI only
                    if(bookedStockOfProductCurrentProformaInvoice.get(warehousesWrapper.warehouseLineItem.Id) != null) {
                        existingAllotedStock = bookedStockOfProductCurrentProformaInvoice.get(warehousesWrapper.warehouseLineItem.Id);
                        for(Alloted_Stock__c allotedStock : existingAllotedStock) {
                            if(allotedStock.Warehouse_Product__c == warehousesWrapper.warehouseLineItem.Id) {
                                System.debug('Alloted Stock already Exists');
                                newAllotedStock = allotedStock;
                                // Check can we add break statement
                               // break;
                            }
                        }
                    }
                    
                    if(newAllotedStock == null) {                       
                        newAllotedStock = new Alloted_Stock__c();
                        newAllotedStock.ProformaInvoice__c = proformaInvoiceId;
                        newAllotedStock.Product__c = productId;
                        newAllotedStock.Warehouse_Product__c = warehousesWrapper.warehouseLineItem.Id;
                        newAllotedStock.Warehouse__c = warehousesWrapper.warehouseLineItem.Warehouse__c;
                    }
                    
                    newAllotedStock.Stock__c = warehousesWrapper.quantity;
                    allotedStocks.add(newAllotedStock);
                    System.debug('Added in alloted Stocks ' + allotedStocks);
                } else {
                    if(bookedStockOfProductCurrentProformaInvoice.get(warehousesWrapper.warehouseLineItem.Id) != null) {
                        uow.registerDeleted(bookedStockOfProductCurrentProformaInvoice.get(warehousesWrapper.warehouseLineItem.Id));
                    }  
                    System.debug('Alloted Stocks to be deleted ' + bookedStockOfProductCurrentProformaInvoice.get(warehousesWrapper.warehouseLineItem.Id));
                }
            }
        }
        System.debug('Final Alloted Stocks to be updated or created newly ' + allotedStocks);
        if(allotedStocks.size() > 0) {
            try {
                upsertAllotedStockWork.registerAllotedStockUpsert(allotedStocks);
            } catch(DMLException ex) {
                throw getExceptionInstance('Something went wrong!!! ' + ex);
            }
        }
        
        List<ProformaInvoice__c> proformaInvoices = (List<ProformaInvoice__c>)ProformaInvoicesSelector.getInstance().selectSObjectsById(new Set<Id>{proformaInvoiceId});
        proformaInvoices[0].Product_Alloted__c = true;
        uow.registerDirty(proformaInvoices[0]);
        try {
            ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
            uow.commitWork();
            
        } catch(Exception ex) {
            throw new AKFDException('Something went wrong!!! ' + ex);
        }
        pageReference = new PageReference('/' + proformaInvoiceId);
    }
    
    
    /**
* Method - getTotalStock
* Description - Return the total number of already booked stock 
*/
    private static Decimal getTotalStock(List<Alloted_Stock__c> allotedStocks, Id proformaInvoiceId){
        Decimal result = 0.00;
        for(Alloted_Stock__c allotedStock : allotedStocks) {
            if(allotedStock.ProformaInvoice__c != proformaInvoiceId) {
                result += allotedStock.Stock__c;
            }
        }
        return result;
    }
    
    /**
* Method - getCurrentPIStock
* Description - Return the number of already booked stock of warehouse
*/
    private static Decimal getCurrentPIStock(Warehouse_Line_Item__c warehouseLineItem, Id proformaInvoiceId){
        Decimal result = 0;
        for(Alloted_Stock__c allotedStock : warehouseLineItem.Alloted_Stocks__r) {
            if(allotedStock.ProformaInvoice__c == proformaInvoiceId) {
                result = allotedStock.Stock__c;
            }
        }
        return result;
    }
    
    
    /*
* Unit of work upsert helper class - UpsertUnitOfWorkAllotedStockHelper
*/
    public class UpsertUnitOfWorkAllotedStockHelper implements fflib_SObjectUnitOfWork.IDoWork {       
        public Database.UpsertResult[] Results {get; private set;}
        
        private List<Alloted_Stock__c> m_records;
        
        public UpsertUnitOfWorkAllotedStockHelper() {  
            m_records = new List<Alloted_Stock__c>();
        }
        
        public void registerAllotedStockUpsert(List<Alloted_Stock__c> records) {
            m_records.addAll(records);
        }
        
        public void doWork() {
            Results = Database.upsert(m_records, false);                
        }
    }
    
    /*
* Unit of work upsert helper class - UpsertUnitOfWorkProjectedStockHelper
*/
    public class UpsertUnitOfWorkProjectedStockHelper implements fflib_SObjectUnitOfWork.IDoWork {       
        public Database.UpsertResult[] Results {get; private set;}
        
        private List<Projected_Stock__c> m_records;
        
        public UpsertUnitOfWorkProjectedStockHelper() {  
            m_records = new List<Projected_Stock__c>();
        }
        
        public void registerProjectedStockUpsert(List<Projected_Stock__c> records) {
            m_records.addAll(records);
        }
        
        public void doWork() {
            Results = Database.upsert(m_records, false);                
        }
    }
    
    
    /**
* Method to getAuraExceptionClassInstance
* @param : error message
* @return AuraHandledException
*/
    private static AuraHandledException getExceptionInstance(String message) {
        AuraHandledException exceptionInstance = new AuraHandledException(message);
        exceptionInstance.setMessage(message);
        return exceptionInstance;
    }
    
    
}