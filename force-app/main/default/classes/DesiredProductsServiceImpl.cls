/**
 * Used in DesiredProductsService
 * Class contains implementation for DesiredProductsService
 * Author: Chetan Sharma (7 March 2019)
 * Last Modified By: Chetan Sharma (17 June 2019)
 */
public class DesiredProductsServiceImpl implements IDesiredProductsService{
    public static final String SOMETHING_WENT_WRONG = 'Something went wrong!!!';
    public static final String ALREADY_EXISTS = 'Some of the selected Product already exist in Desired Product list, Please check and reselect the product again.!!!';
    public static final String DECIMAL_VALUES_NOT_ALLOWED = 'Decimal values are not allowed for Each or Set Units.';
    
    /** 
    * Method has logic to insert desired products
    * @param selected products string and opportunityId. 
    * @return String result. 
    */
    public List<PO_Product__c> addDesiredProducts(Map<Id, List<PO_Product__c>> desiredProducts) {
        List<PO_Product__c> desiredProductsToBeInserted = new List<PO_Product__c>();
       
        if(desiredProducts != null && desiredProducts.size() > 0) {
            Set<Id> opportunityIds = desiredProducts.keySet();
            for(Id opportunityId : opportunityIds) {
                desiredProductsToBeInserted.addAll(desiredProducts.get(opportunityId)); 
            }
            
            if(isDesiredProductAlreadyExists(opportunityIds, desiredProductsToBeInserted, desiredProducts)) {
                throw new AKFDException(ALREADY_EXISTS);
            } else {
                try{
                    fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
                    uow.registerNew(desiredProductsToBeInserted);
                    uow.commitWork();
                } catch(fflib_SObjectUnitOfWork.UnitOfWorkException ex){
                    throw new AKFDException(SOMETHING_WENT_WRONG);
                } catch (Exception genericException) {
                    String errorMessage = '';
                    if (  genericException.getMessage().contains(DECIMAL_VALUES_NOT_ALLOWED)) {
                        errorMessage = DECIMAL_VALUES_NOT_ALLOWED;
                    } else {
                        errorMessage = SOMETHING_WENT_WRONG;
                    }
                    throw new AKFDException(errorMessage);
                }
            }
        }
        return desiredProductsToBeInserted;
    }
    
    /** 
    * Private Method has logic to check whether Desired Products already exists or not
    * @param Opportunity Id, Desired Products list (to be inserted and already exists ones), 
    * @return Boolean value 
    */
    private Boolean isDesiredProductAlreadyExists(Set<Id> opportunityIds, List<PO_Product__c> desiredProductsToBeInserted, Map<Id, List<PO_Product__c>> desiredProducts){
        List<PO_Product__c> existingDesiredProducts = new List<PO_Product__c>();
        Set<Id> productIds = new Set<Id>();
        for(PO_Product__c desiredProduct : desiredProductsToBeInserted) {
            productIds.add(desiredProduct.Product__c);
        }
        existingDesiredProducts = DesiredProductsSelector.getInstance().getRelatedDesiredProductsByOpportunityIds(opportunityIds);
        Map<Id,List<Id>> opportunitinesWithProductIds = new Map<Id,List<Id>>();
       	// validating whether desiredProducts already exists or not
        if(existingDesiredProducts.size() > 0) {
             //Generating Map of opportunity specific ProductIds
            for(PO_Product__c existingDesiredProduct : existingDesiredProducts) {
                if(opportunitinesWithProductIds.containsKey(existingDesiredProduct.Opportunity__c)) {
                    opportunitinesWithProductIds.get(existingDesiredProduct.Opportunity__c).add(existingDesiredProduct.Product__c);
                	System.debug('Already exists ' + opportunitinesWithProductIds);
                } else {
                    opportunitinesWithProductIds.put(existingDesiredProduct.Opportunity__c, new List<Id>{existingDesiredProduct.Product__c});
                	System.debug('New Entry ' + opportunitinesWithProductIds);
                }
            }
            System.debug('Opportunity Ids ' + opportunitinesWithProductIds);
            // Return true when desired product already exists.
            for(Id opportunityId :  opportunityIds) {
                System.debug('Desired Products ' +  desiredProducts.get(opportunityId));
                for(PO_Product__c desiredProduct : desiredProducts.get(opportunityId)){
                    if((opportunitinesWithProductIds.get(opportunityId)).contains(desiredProduct.Product__c)) {
                        System.debug('Desired Product Found');
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    
    /** 
    * Private Method has logic to check whether Desired Products already exists or not
    * @param Opportunity Id, Desired Products list (to be inserted and already exists ones), 
    * @return Boolean value 
    */
    private Boolean isDesiredProductAlreadyExistsNew(Set<Id> opportunityIds, List<PO_Product__c> desiredProductsToBeInserted, Map<Id, List<PO_Product__c>> opportunitySpecificDesiredProducts){
        List<PO_Product__c> existingDesiredProducts = new List<PO_Product__c>();
        existingDesiredProducts = DesiredProductsSelector.getInstance().getRelatedDesiredProductsByOpportunityIds(opportunityIds);
        
        return false;
    }
    
}