@isTest
public class PriceBooksTest {
    
    @isTest static void positiveTestCaseFor_beforeInsertPriceBooks() {
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        accounts = [SELECT Id, Name, CurrencyIsoCode FROM Account WHERE Id IN : accounts];
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        priceBooks = [SELECT Id, Name, CurrencyIsoCode FROM PriceBook_Product__c WHERE Customer__c =: accounts[0].Id];
        for(PriceBook_Product__c priceBook :  priceBooks) {
            System.assert(priceBook.CurrencyIsoCode == accounts[0].CurrencyIsoCode);
        }
    }
    
    @isTest static void negativTestCaseFor_beforeInsertPriceBooks_WhenPriceBookAlreadyCreated() {
        List<Account> accounts = MockData.createAccounts('Customer', 1);
        accounts = [SELECT Id, Name, CurrencyIsoCode FROM Account WHERE Id IN : accounts];
        List<Product2> products = MockData.createProducts(1);
        List<PriceBook_Product__c> priceBooks = MockData.createPriceBooks(accounts, products, 1);
        priceBooks = [SELECT Id, Name, CurrencyIsoCode FROM PriceBook_Product__c WHERE Customer__c =: accounts[0].Id];
        try {
            MockData.createPriceBooks(accounts, products, 1);
        } catch (Exception exec) { 
            System.assert(exec.getMessage().contains('Price Book has already been defined for this Account & Product.'));
        }
    }

}