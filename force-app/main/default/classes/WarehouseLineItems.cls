public class WarehouseLineItems extends fflib_SObjectDomain {
    public WarehouseLineItems(List<Warehouse_Line_Item__c> records) {
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new WarehouseLineItems(records);
        }
    
    }
    public override void onBeforeInsert() {
        Set<Id> warehouseProductsIds = new Set<Id>();
        Set<Id> warehouseIds = new Set<Id>();
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            if(warehouseProduct.Product__c == null) {
                warehouseProduct.Product__c.addError('Product is required.');
            } else { 
                warehouseProductsIds.add(warehouseProduct.Product__c);
                warehouseIds.add(warehouseProduct.Warehouse__c);
            }
        }
        List<Warehouse_Line_Item__c> existingWarehouseProducts = WarehouseLineItemsSelector.getInstance().selectByProductAndWarehouse(warehouseProductsIds, warehouseIds);
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            for(Warehouse_Line_Item__c existingWarehouseProduct : existingWarehouseProducts) {
                if(warehouseProduct.Product__c == existingWarehouseProduct.Product__c
                   && warehouseProduct.Warehouse__c == existingWarehouseProduct.Warehouse__c) {
                       warehouseProduct.addError('Product entry already exists in Warehouse.');
                   }
            }
        }
    }
    public override void onAfterInsert() {
        Set<Id> warehouseProductsIds = new Set<Id>();
        List<Id> warehouseIds = new List<Id>();
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            warehouseProductsIds.add(warehouseProduct.Product__c);
            warehouseIds.add(warehouseProduct.Warehouse__c);
        }
        Map<Id, Product2> products = new Map<Id, Product2>((List<Product2>)ProductsSelector.getInstance().selectSObjectsById(warehouseProductsIds));
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            if(products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c == null) {
                products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c = 0;
            } else {
                products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c += warehouseProduct.Current_Stock__c;
            }
        }
        if(products.size() > 0) {
            update products.values();
        }
    }
    
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) {
        Map<Id, Warehouse_Line_Item__c> existingRecordsMap = (Map<Id, Warehouse_Line_Item__c>) existingRecords;
        Set<Id> warehouseProductsIds = new Set<Id>();
        Set<Id> warehouseIds = new Set<Id>();
        List<Warehouse_Line_Item__c> warehouseProductWithUpdatedProduct = new List<Warehouse_Line_Item__c>();
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            if(warehouseProduct.Product__c == null) {
                warehouseProduct.Product__c.addError('Product is required.');
            } else if(warehouseProduct.Product__c != existingRecordsMap.get(warehouseProduct.Id).Product__c) {
                warehouseProductWithUpdatedProduct.add(warehouseProduct);
                warehouseProductsIds.add(warehouseProduct.Product__c);
                warehouseIds.add(warehouseProduct.Warehouse__c);
            } 
        }
        List<Warehouse_Line_Item__c> existingWarehouseProducts = WarehouseLineItemsSelector.getInstance().selectByProductAndWarehouse(warehouseProductsIds, warehouseIds);
        for(Warehouse_Line_Item__c warehouseProduct : warehouseProductWithUpdatedProduct) {
            for(Warehouse_Line_Item__c existingWarehouseProduct : existingWarehouseProducts) {
                if(warehouseProduct.Product__c == existingWarehouseProduct.Product__c
                   && warehouseProduct.Warehouse__c == existingWarehouseProduct.Warehouse__c) {
                       warehouseProduct.addError('Product entry already exists in Warehouse.');
                   }
            }
        }
    }
    
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        Map<Id, Warehouse_Line_Item__c> existingRecordsMap = (Map<Id, Warehouse_Line_Item__c>) existingRecords;
        Set<Id> warehouseProductsIds = new Set<Id>();
        List<Id> warehouseIds = new List<Id>();
        List<Warehouse_Line_Item__c> warehouseProductWithUpdatedProduct = new List<Warehouse_Line_Item__c>();
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            warehouseProductsIds.add(warehouseProduct.Product__c);
            warehouseIds.add(warehouseProduct.Warehouse__c);
        }
        Map<Id, Product2> products = new Map<Id, Product2>((List<Product2>)ProductsSelector.getInstance().selectSObjectsById(warehouseProductsIds));
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            if(products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c == null) {
                products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c = 0;
            }
            if(warehouseProduct.Product__c != existingRecordsMap.get(warehouseProduct.Id).Product__c) {
                products.get(existingRecordsMap.get(warehouseProduct.Id).Product__c).Hidden_Total_Stock_Field__c -= existingRecordsMap.get(warehouseProduct.Id).Current_Stock__c;
            } else {
                
                products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c -= existingRecordsMap.get(warehouseProduct.Id).Current_Stock__c;
            }
            products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c += warehouseProduct.Current_Stock__c;
        }
        
        if(products.size() > 0) {
            update products.values();
        }
    }
    
    
        
    public override void onBeforeDelete() {
        Set<Id> warehouseProductsIds = new Set<Id>();
        List<Id> warehouseIds = new List<Id>();
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            warehouseProductsIds.add(warehouseProduct.Product__c);
            warehouseIds.add(warehouseProduct.Warehouse__c);
        }
        Map<Id, Product2> products = new Map<Id, Product2>((List<Product2>)ProductsSelector.getInstance().selectSObjectsById(warehouseProductsIds));
        for(Warehouse_Line_Item__c warehouseProduct : (List<Warehouse_Line_Item__c>) records) {
            products.get(warehouseProduct.Product__c).Hidden_Total_Stock_Field__c -= warehouseProduct.Current_Stock__c;
        }
        if(products.size() > 0) {
            update products.values();
        }
    }
}