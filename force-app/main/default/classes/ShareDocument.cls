public class ShareDocument {
    public Id opportunityId {get;set;}
    public Id attachmentId {get;set;}
    public String templateName {get;set;}
    public Id documentId {get;set;}
    public String invalidQuotationProducts {get;set;}
    public String salesProcess {get;set;}
    public Id targetObjectId {get;set;}
    public String docketNumber {get;set;}
    public boolean isDocketNumberSharePage {get;set;}
    public ShareDocument() { 
        opportunityId = ApexPages.currentPage().getParameters().get('opportunityId');
        attachmentId = ApexPages.currentPage().getParameters().get('attachmentId');
        templateName = ApexPages.currentPage().getParameters().get('templateName');
        documentId = ApexPages.currentPage().getParameters().get('documentId');
        invalidQuotationProducts = ApexPages.currentPage().getParameters().get('invalidQuotationProducts');
        salesProcess = ApexPages.currentPage().getParameters().get('salesProcess');
        targetObjectId = ApexPages.currentPage().getParameters().get('targetObjectId');
        docketNumber = ApexPages.currentPage().getParameters().get('docketNumber');
        isDocketNumberSharePage = ApexPages.currentPage().getParameters().get('isDocketNumberSharePage') == null ? false : true;
    }
    
    
    
}