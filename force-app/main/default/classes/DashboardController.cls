/**
 * Class: DashboardController
 * Description :  
 * Created by: Rajesh @April/22/2018
 * Updated by :Yash @May/26/2018
 */
public class DashboardController {
    public static Map<Integer, String> mapMonth = new Map<Integer,String>{ 1 => 'JAN', 2 => 'FEB', 3 => 'MAR',
                                                                            4 => 'APR', 5 => 'MAY', 6 => 'JUN',
                                                                            7 => 'JUL', 8 => 'AUG', 9 => 'SEP',
                                                                            10 => 'OCT', 11 => 'NOV', 12 => 'DEC' };
    public static Map<Integer, String> mapQuarter = new Map<Integer,String>{ 1 => 'Quarter 1', 2 => 'Quarter 2', 3 => 'Quarter 3',
                                                                            4 => 'Quarter 4' };                                                 
    public Map<Integer, String> mapYear = new Map<Integer,String>();                                                    
    public String seletedFilter { get; set; }
    public String startRange { get; set; } 
    public String endRange { get; set; }
    public String chartType { get; set; }
    public String graphType { get; set; }
    public List<SelectOption> options { get; set; } 
    public List<ChartData> lstCompaire { get; set; }
    public List<ChartData> lstInflow { get; set; }
    public List<ChartData> lstOutflow { get; set; }
    public List<ChartData> lstDuePayments { get; set; }
    public List<ChartData> lstSellingTrend { get; set; }
    public List<ChartData> lstQuantityTrend { get; set; }
    public List<PieWedgeData> lstPieCompair { get; set; }
    //public String profileName { get; set; } 
    public static final String COMPARISON_GRAPH = 'Comparison Graph';
    public static final String DUE_PAYMENT = 'Due Payments';
    public static final String CASH_RECEIVED = 'Cash Received';
    public static final String CASH_OUTFLOW = 'Cash Outflow';
    public List<Id> allCaseReports{get;set;}
    public List<Id> allLeadReports{get;set;}
    public List<Id> allOpportunityReports{get;set;}
    public List<Id> allProductReports{get;set;}
    public Alloted_Stock__c allotedStockSource{get;set;}
    
    
    /**
     * Constructor
     */
    public DashboardController() {
        try{
        allotedStockSource = new Alloted_Stock__c();
        allotedStockSource.Product__c=[SELECT Id from product2 LIMIT 1].Id;
        allotedStockSource.Stock__c=100;
        //Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
       // profileName = p.name;
        
      //  if (profileName == 'System Administrator') {
            seletedFilter = 'Monthly';
            graphType = COMPARISON_GRAPH;
            getFilters();
            createYearMap();
            getChartData();  
            recivedPaymentGraph();
            //sellingTrendGraph();
       // }
        getReports();
        }
        catch(Exception e){
            
        }
    }
    
    private void getReports(){
        allCaseReports= new List<Id>();
        allLeadReports= new List<Id>();
        allOpportunityReports= new List<Id>();
        allProductReports= new List<Id>();
        for(Report report : [SELECT Id, FolderName, Name, Description,developerName FROM Report where FolderName='AKFD Dashboard' Order by Description])
        {
            if(report.Description.contains('Case'))
            {
                allCaseReports.add(report.Id);
            }
            if(report.Description.contains('Lead'))
            {
                allLeadReports.add(report.Id);
            }
            if(report.Description.contains('Opportunity'))
            {
                allOpportunityReports.add(report.Id);
            }
            if(report.Description.contains('Product'))
            {
                allProductReports.add(report.Id);
            }
        }
    }
    public void getFilters() {   
        options = new List<SelectOption>();
        options.add(new SelectOption('Monthly', 'Monthly'));
        options.add(new SelectOption('Quarterly', 'Quarterly')); 
        options.add(new SelectOption('Yearly', 'Yearly'));
        options.add(new SelectOption('Custom Range', 'Custom Range')); 
    }
    
    public void createYearMap() {  
        Integer currentYr = Integer.valueof(System.Today().year());
        mapYear.put(currentYr - 2, String.ValueOf(currentYr - 2));
        mapYear.put(currentYr - 1, String.ValueOf(currentYr - 1)); 
        mapYear.put(currentYr, String.ValueOf(currentYr)); 
        mapYear.put(currentYr + 1, String.ValueOf(currentYr + 1));
        mapYear.put(currentYr + 2, String.ValueOf(currentYr + 2)); 
    }
     
    public void recivedPaymentGraph() { 
        Map<Integer, Decimal> mapDue = new Map<Integer, Decimal>();
        lstDuePayments = new List<ChartData>();
        Map<Date, Decimal> mapCustomDue = new Map<Date, Decimal>();
        Integer currentYr = Integer.valueof(System.Today().year());
        String queryInFlow,queryOutFlow;
        if (seletedFilter == 'Monthly') { 
            
            for (sObject pt : [SELECT Calendar_Month(Transaction_Date__c) duration, SUM(Transaction_In_INR__c) totalAmount 
                               FROM Payment_Transaction__c  pt GROUP BY CALENDAR_YEAR(Transaction_Date__c), Calendar_Month(Transaction_Date__c) HAVING Calendar_Year(Transaction_Date__c) = :currentYr]) {
                mapDue.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));   
            }
            
            for (Integer month : mapMonth.keyset()) {
                    lstDuePayments.add(new ChartData(mapMonth.get(month), (mapDue.containsKey(month) ? mapDue.get(month) : 0) ,0));
            }
        } else if (seletedFilter == 'Yearly') {
            
            for (sObject pt : [SELECT Calendar_Year(Transaction_Date__c) duration, SUM(Transaction_In_INR__c) totalAmount FROM Payment_Transaction__c pt 
                                    GROUP BY Calendar_Year(Transaction_Date__c)]) {
                mapDue.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));   
            }  
            
            for (Integer yr : mapYear.keyset()) {
                    lstDuePayments.add(new ChartData(mapYear.get(yr), (mapDue.containsKey(yr) ? mapDue.get(yr) : 0), 0));
            } 
        } else if (seletedFilter == 'Quarterly') {
            
            for (sObject pt : [SELECT CALENDAR_QUARTER(Transaction_Date__c) duration, SUM(Transaction_In_INR__c) totalAmount FROM Payment_Transaction__c pt 
                                    GROUP BY CALENDAR_YEAR(Transaction_Date__c), CALENDAR_QUARTER(Transaction_Date__c) HAVING Calendar_Year(Transaction_Date__c) = :currentYr]) {
                mapDue.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));   
            }  
             
            for (Integer yr : mapQuarter.keyset()) {
                    lstDuePayments.add(new ChartData(mapQuarter.get(yr), (mapDue.containsKey(yr) ? mapDue.get(yr) : 0),0));
            } 
        } else if (seletedFilter == 'Custom Range') {
            date startDate = Date.parse(startRange); 
            date endDate = Date.parse(endRange);
            
            for (Payment_Transaction__c pt : [SELECT Transaction_Date__c, Transaction_In_INR__c FROM Payment_Transaction__c pt WHERE  Transaction_Date__c >:  Date.parse(startRange)
                                AND Transaction_Date__c <= : Date.parse(endRange)]) {
                mapCustomDue.put(Date.valueOf(pt.Transaction_Date__c), pt.Transaction_In_INR__c); 
            } 
             
            
            for (date d = Date.parse(startRange); d <= Date.parse(endRange); d=d.addDays(1)){  
                lstDuePayments.add(new ChartData(String.valueOf(d), (mapCustomDue.containsKey(d) ? mapCustomDue.get(d) : 0),0));
            } 
        }   
    }
    
    public void getChartData() { 
        Map<Integer, Decimal> mapInflow = new Map<Integer, Decimal>();
        Map<Integer, Decimal> mapOutflow = new Map<Integer, Decimal>();
        Map<Date, Decimal> mapCustomInflow = new Map<Date, Decimal>();
        Map<Date, Decimal> mapCustomOutflow = new Map<Date, Decimal>();
        lstCompaire = new List<ChartData>();
        String queryInFlow,queryOutFlow;
        Integer currentYr = Integer.valueof(System.Today().year()); 
         
        if (seletedFilter == 'Monthly') {
             
            for (sObject pt : [SELECT Calendar_Month(Due_Date__c) duration, SUM(Base_Amount_INR__c) totalAmount FROM Payment_Term__c pt  
                                    GROUP BY Calendar_Year(Due_Date__c), Calendar_Month(Due_Date__c) HAVING Calendar_Year(Due_Date__c) = :currentYr]) {
                mapInflow.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));    
            } 
         
            for (sObject pt : [SELECT Calendar_Month(Due_Date__c) duration, SUM(Amount__c) totalAmount FROM Cash_Outflow__c pt 
                                    GROUP BY Calendar_Year(Due_Date__c), Calendar_Month(Due_Date__c) HAVING Calendar_Year(Due_Date__c) = :currentYr]) {
                mapOutflow.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));   
            }
            
            for (Integer month : mapMonth.keyset()) {
                    lstCompaire.add(new ChartData(mapMonth.get(month), (mapInflow.containsKey(month) ? mapInflow.get(month) : 0)
                                                    ,(mapOutFlow.containsKey(month)) ? mapOutFlow.get(month) : 0));
            }
        } else if (seletedFilter == 'Yearly') {
            
            for (sObject pt : [SELECT Calendar_Year(Due_Date__c) duration, SUM(Base_Amount_INR__c) totalAmount FROM Payment_Term__c pt 
                                    GROUP BY Calendar_Year(Due_Date__c)]) {
                mapInflow.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));    
            } 
            
            for (sObject pt : [SELECT Calendar_Year(Due_Date__c) duration, SUM(Amount__c) totalAmount FROM Cash_Outflow__c pt 
                                    GROUP BY Calendar_Year(Due_Date__c)]) {
                mapOutflow.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));   
            }
            
            for (Integer yr : mapYear.keyset()) {
                    lstCompaire.add(new ChartData(mapYear.get(yr), (mapInflow.containsKey(yr) ? mapInflow.get(yr) : 0)
                                                    ,(mapOutFlow.containsKey(yr)) ? mapOutFlow.get(yr) : 0));
            } 
        } else if (seletedFilter == 'Quarterly') {
            
            for (sObject pt : [SELECT CALENDAR_QUARTER(Due_Date__c) duration, SUM(Base_Amount_INR__c) totalAmount FROM Payment_Term__c pt 
                                    GROUP BY Calendar_Year(Due_Date__c), CALENDAR_QUARTER(Due_Date__c) HAVING Calendar_Year(Due_Date__c) = :currentYr]) {
                mapInflow.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));    
            } 
            
            for (sObject pt : [SELECT CALENDAR_QUARTER(Due_Date__c) duration, SUM(Amount__c) totalAmount FROM Cash_Outflow__c pt 
                                    GROUP BY Calendar_Year(Due_Date__c), CALENDAR_QUARTER(Due_Date__c) HAVING Calendar_Year(Due_Date__c) = :currentYr]) {
                mapOutflow.put((Integer) pt.get('duration'), (Decimal) pt.get('totalAmount'));   
            }
            
            for (Integer yr : mapQuarter.keyset()) {
                    lstCompaire.add(new ChartData(mapQuarter.get(yr), (mapInflow.containsKey(yr) ? mapInflow.get(yr) : 0)
                                                    ,(mapOutFlow.containsKey(yr)) ? mapOutFlow.get(yr) : 0));
            } 
        } else if (seletedFilter == 'Custom Range') { 
            date startDate = Date.parse(startRange); 
            date endDate = Date.parse(endRange);
            
            for (SObject pt : [SELECT Due_Date__c,SUM(Base_Amount_INR__c) Amount FROM Payment_Term__c pt Where Due_Date__c >= :Date.parse(startRange)
                                AND Due_Date__c <= : Date.parse(endRange) Group by Due_Date__c]) {
                mapCustomInflow.put(Date.valueOf(pt.get('Due_Date__c')),(Decimal)pt.get('Amount')); 
            } 
             
            for (SObject pt : [SELECT Due_Date__c, SUM(Amount__c) Amount  FROM Cash_Outflow__c pt WHERE Due_Date__c >= :Date.parse(startRange)
                                AND Due_Date__c <= : Date.parse(endRange) Group by Due_Date__c]) {   
                mapCustomOutflow.put(Date.valueOf((pt.get('Due_Date__c'))),(Decimal) pt.get('Amount'));
            } 
            //System.assert(false,mapCustomOutflow);
            for (date d = Date.parse(startRange); d <= Date.parse(endRange); d=d.addDays(1)){  
                lstCompaire.add(new ChartData(String.valueOf(d), (mapCustomInflow.containsKey(d) ? mapCustomInflow.get(d) : 0)
                                                    ,(mapCustomOutflow.containsKey(d)) ? mapCustomOutflow.get(d) : 0));
            }
            
        }
          
        if (graphType == COMPARISON_GRAPH) { 
                Decimal inflow = 0;
                Decimal outFlow = 0;
                
                for (ChartData dt : lstCompaire) {
                    inflow += dt.inFlow; 
                    outFlow += dt.outFlow;
                }
                lstPieCompair = new List<PieWedgeData>();
                lstPieCompair.add(new PieWedgeData('Cash Inflow', inflow));
                lstPieCompair.add(new PieWedgeData('Cash Outflow', outFlow)); 
            }
    }
    
    //selling trend
    public void sellingTrendGraph(){

        Map<Integer, Decimal> mapQuantity = new Map<Integer, Decimal>(); 
        lstSellingTrend = new List<ChartData>();
        Map<Date, Decimal> mapSellingDates = new Map<Date, Decimal>();
        String queryInFlow,queryOutFlow;
        Integer currentYr = Integer.valueof(System.Today().year());
        
        if (seletedFilter == 'Monthly') { 
            
            for (sObject pt : [SELECT Calendar_Month(CreatedDate) duration, Count(Id) totalCount FROM Invoiced_Product__c  pt 
                               WHERE Product__c=:allotedStockSource.Product__c GROUP BY Calendar_Year(CreatedDate), Calendar_Month(CreatedDate) HAVING Calendar_Year(CreatedDate) = :currentYr]) {
                mapQuantity.put((Integer) pt.get('duration'), (Decimal) pt.get('totalCount'));   
            }
            
            for (Integer month : mapMonth.keyset()) {
                    lstSellingTrend.add(new ChartData(mapMonth.get(month), (mapQuantity.containsKey(month) ? mapQuantity.get(month) : 0) ,0));
            }
        } else if (seletedFilter == 'Yearly') {
            
            for (sObject pt : [SELECT Calendar_Year(CreatedDate) duration, Count(Id) totalCount FROM Invoiced_Product__c pt 
                                    WHERE Product__c=:allotedStockSource.Product__c GROUP BY Calendar_Year(CreatedDate)]) {
                mapQuantity.put((Integer) pt.get('duration'), (Decimal) pt.get('totalCount'));   
            }  
            
            for (Integer yr : mapYear.keyset()) {
                    lstSellingTrend.add(new ChartData(mapYear.get(yr), (mapQuantity.containsKey(yr) ? mapQuantity.get(yr) : 0), 0));
            } 
        } else if (seletedFilter == 'Quarterly') {
            
            for (sObject pt : [SELECT CALENDAR_QUARTER(CreatedDate) duration, Count(Id) totalCount FROM Invoiced_Product__c pt 
                                    WHERE Product__c=:allotedStockSource.Product__c GROUP BY  Calendar_Year(CreatedDate), CALENDAR_QUARTER(CreatedDate) HAVING Calendar_Year(CreatedDate) = :currentYr]) {
                mapQuantity.put((Integer) pt.get('duration'), (Decimal) pt.get('totalCount'));   
            }  
             
            for (Integer yr : mapQuarter.keyset()) {
                    lstSellingTrend.add(new ChartData(mapQuarter.get(yr), (mapQuantity.containsKey(yr) ? mapQuantity.get(yr) : 0),0));
            } 
        } else if (seletedFilter == 'Custom Range') {
            date startDate = Date.parse(startRange); 
            date endDate = Date.parse(endRange);
            DateTime startOfDT = DateTime.newInstance(startDate.year(),startDate.month(),startDate.day(),0,0,0);
			DateTime endOfDT = DateTime.newInstance(endDate.year(),endDate.month(),endDate.day(),23,59,59);
            


            for (SObject pt : [SELECT CreatedDate,Id FROM Invoiced_Product__c pt WHERE Product__c=:allotedStockSource.Product__c AND CreatedDate >= :startOfDT
                                AND CreatedDate <= : endOfDT]) {
                     DateTime dT = (DateTime)pt.get('CreatedDate');
                    Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());

                    if(mapSellingDates.containsKey(myDate)) {
                        mapSellingDates.put(myDate,mapSellingDates.get(myDate)+1);
                    } else {
                        mapSellingDates.put(myDate, 1);
                    }
                //mapCustomDue.put(Date.valueOf(pt.get('CreatedDate')), (Decimal) pt.get('totalCount')); 
            } 
             
            
            for (date d = Date.parse(startRange); d <= Date.parse(endRange); d=d.addDays(1)){  
                lstSellingTrend.add(new ChartData(String.valueOf(d), (mapSellingDates.containsKey(d) ? mapSellingDates.get(d) : 0),0));
            } 
        }
        
        //System.assert(false,lstSellingTrend);
    }
    
        
    //quantity trend
    public void quantityTrendGraph(){

        Map<Integer, Decimal> mapQuantity = new Map<Integer, Decimal>(); 
        lstQuantityTrend = new List<ChartData>();
        Map<Date, Decimal> mapSellingDates = new Map<Date, Decimal>();
        String queryInFlow,queryOutFlow;
        Integer currentYr = Integer.valueof(System.Today().year());
        
        if (seletedFilter == 'Monthly') { 
            
            for (sObject pt : [SELECT Calendar_Month(CreatedDate) duration, sum(Quantity__c) totalCount FROM Invoiced_Product__c pt 
                    GROUP BY Calendar_Year(CreatedDate), Calendar_Month(CreatedDate),Product__c Having sum(Quantity__c) > :allotedStockSource.Stock__c AND Calendar_Year(CreatedDate) = :currentYr]) {
                //mapQuantity.put((Integer) pt.get('duration'), (Decimal) pt.get('totalCount'));

                if(mapQuantity.containsKey((Integer) pt.get('duration')))
                    {
                        mapQuantity.put((Integer) pt.get('duration'),mapQuantity.get((Integer) pt.get('duration'))+1);
                    }
                    else
                    {
                        mapQuantity.put((Integer) pt.get('duration'), 1);
                    }



            }
            
            for (Integer month : mapMonth.keyset()) {
                    lstQuantityTrend.add(new ChartData(mapMonth.get(month), (mapQuantity.containsKey(month) ? mapQuantity.get(month) : 0) ,0));
            }
        } else if (seletedFilter == 'Yearly') {
            
            for (sObject pt : [SELECT Calendar_Year(CreatedDate) duration, sum(Quantity__c) totalCount FROM Invoiced_Product__c pt 
                                GROUP BY Calendar_Year(CreatedDate),Product__c Having sum(Quantity__c) > :allotedStockSource.Stock__c]) {
                if(mapQuantity.containsKey((Integer) pt.get('duration')))
                    {
                        mapQuantity.put((Integer) pt.get('duration'),mapQuantity.get((Integer) pt.get('duration'))+1);
                    }
                    else
                    {
                        mapQuantity.put((Integer) pt.get('duration'), 1);
                    }   
            }  
            
            for (Integer yr : mapYear.keyset()) {
                    lstQuantityTrend.add(new ChartData(mapYear.get(yr), (mapQuantity.containsKey(yr) ? mapQuantity.get(yr) : 0), 0));
            } 
        } else if (seletedFilter == 'Quarterly') {
            
            for (sObject pt : [SELECT CALENDAR_QUARTER(CreatedDate) duration, sum(Quantity__c) totalCount FROM Invoiced_Product__c pt 
                                    GROUP BY Calendar_Year(CreatedDate), CALENDAR_QUARTER(CreatedDate),Product__c Having sum(Quantity__c) > :allotedStockSource.Stock__c
                              		AND Calendar_Year(CreatedDate) = :currentYr]) {
                if(mapQuantity.containsKey((Integer) pt.get('duration')))
                    {
                        mapQuantity.put((Integer) pt.get('duration'),mapQuantity.get((Integer) pt.get('duration'))+1);
                    }
                    else
                    {
                        mapQuantity.put((Integer) pt.get('duration'), 1);
                    }   
            }  
             
            for (Integer yr : mapQuarter.keyset()) {
                    lstQuantityTrend.add(new ChartData(mapQuarter.get(yr), (mapQuantity.containsKey(yr) ? mapQuantity.get(yr) : 0),0));
            } 
        } else if (seletedFilter == 'Custom Range') {
            date startDate = Date.parse(startRange); 
            date endDate = Date.parse(endRange);
            DateTime startOfDT = DateTime.newInstance(startDate.year(),startDate.month(),startDate.day(),0,0,0);
			DateTime endOfDT = DateTime.newInstance(endDate.year(),endDate.month(),endDate.day(),23,59,59);



            Map<Id,Decimal> productQuantity=new Map<Id,Decimal>();
            for (SObject pt : [SELECT CreatedDate,Quantity__c,Product__c FROM Invoiced_Product__c 
                    pt WHERE CreatedDate >= :startOfDT AND CreatedDate <= : endOfDT]) {
                     DateTime dT = (DateTime)pt.get('CreatedDate');
                    Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
                    Id id=(Id)pt.get('Product__c');
                    Decimal quanity=(Decimal)pt.get('Quantity__c');

                    if(productQuantity.containsKey(id)){
                        productQuantity.put(id,productQuantity.get(id)+quanity);
                    }
                    else
                    {
                        productQuantity.put(id,quanity);
                    }


                    if(productQuantity.get(id) > allotedStockSource.Stock__c)
                    {


                        if(mapSellingDates.containsKey(myDate) )
                        {
                            mapSellingDates.put(myDate,mapSellingDates.get(myDate)+1);
                        }
                        else
                        {
                            mapSellingDates.put(myDate,1);
                        }
                    }
                //mapCustomDue.put(Date.valueOf(pt.get('CreatedDate')), (Decimal) pt.get('totalCount')); 
            } 
             
            
            for (date d = Date.parse(startRange); d <= Date.parse(endRange); d=d.addDays(1)){  
                lstQuantityTrend.add(new ChartData(String.valueOf(d), (mapSellingDates.containsKey(d) ? mapSellingDates.get(d) : 0),0));
            } 
        }
        
        //System.assert(false,lstSellingTrend);
    }
    




     // Wrapper class
    public class PieWedgeData { 
        public String name { get; set; }
        public Decimal data { get; set; }

        public PieWedgeData(String name, Decimal data) {
            this.name = name;
            this.data = data;
        }
    }
    // Wrapper class
    public class ChartData {
        public String legents { get; set; }
        public Decimal inFlow { get; set; }
        public Decimal outFlow { get; set; }
        
        public ChartData(String legents, Decimal inFlow, Decimal outFlow) {
            this.legents = legents;
            this.inFlow = inFlow;
            this.outFlow = outFlow; 
        }
    }
}