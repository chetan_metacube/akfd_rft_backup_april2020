public class MyXmlPageController {
    
    private static final String EMPTY_STRING = ' ';
    
    public MyXmlPageController() {
        
    }
    
    public static String getProductXml(Product2 product, HSN_Tax__c hsnTax) {
        RequestBodyXML xmlBody = new RequestBodyXML(RequestBodyXML.APIType.PRODUCT);
        List<String> productValues = new List<String>();
        productValues.add(product.Name == null ? EMPTY_STRING : product.Name);
        productValues.add(product.SKU_Number__c == null ? EMPTY_STRING : product.SKU_Number__c);
        productValues.add(product.Description__c == null ? EMPTY_STRING : product.Description__c);
        productValues.add(product.Stock_Group__c == null ? EMPTY_STRING : product.Stock_Group__c.equals('Mfg') ? 'Manufacturing' : product.Stock_Group__c.equals('Trg') ? 'Trading' : EMPTY_STRING);
        productValues.add(product.Brand__c == null ? EMPTY_STRING : product.Brand__c);
        productValues.add(product.Unit__c == null ? EMPTY_STRING : product.Unit__c);
        productValues.add(product.Root_Price_Updated_Date__c == null ? EMPTY_STRING : String.valueOf(product.Root_Price_Updated_Date__c).replace('-', ''));
        productValues.add(product.Retail_Price_Updated_Date__c == null ? EMPTY_STRING : String.valueOf(product.Retail_Price_Updated_Date__c).replace('-', ''));
        productValues.add((product.Root_Price__c == null ? EMPTY_STRING : String.valueOf(product.Root_Price__c)) + '/' + (product.Unit__c == null ? EMPTY_STRING : product.Unit__c));
        productValues.add((product.Retail_Price__c == null ? EMPTY_STRING : String.valueOf(product.Retail_Price__c)) + '/' + (product.Unit__c == null ? EMPTY_STRING : product.Unit__c));
        system.debug('myhsn ' + hsnTax);
        if(hsnTax != null) {
            //productValues.add(hsnTax.Tax__c == null || hsnTax.Tax__c == 0 ? 'Not Applicable' : 'Applicable');
            productValues.add('Applicable');
            productValues.add(JSON.serialize(new List<RequestBodyXML.GSTDetails> {
                new RequestBodyXML.GSTDetails(
                    product.HSN_Applicable_Date__c == null ? EMPTY_STRING : String.valueOf(product.HSN_Applicable_Date__c).replace('-', ''), 
                    hsnTax.Name == null ? EMPTY_STRING : String.valueOf(hsnTax.Name), 
                    hsnTax.Tax__c != null && hsnTax.Tax__c != 0.00 ? 'Taxable' : 'Exempt', 
                    hsnTax.Tax__c == null ? 0.00 : hsnTax.Tax__c, 
                    hsnTax.Tax__c == null ? 0.00 : hsnTax.Tax__c / 2)
                    }));
        } else {
            productValues.add('Not Applicable');
            productValues.add(JSON.serialize(new List<RequestBodyXML.GSTDetails>()));
        }
        System.debug('myproductValues ' + productValues);
        xmlBody.setValues(productValues);
        String result = xmlBody.getXMLBody();
        //system.assert(false, result);
        return result;
    }
    
    public static String getInvoiceXml(Invoice__c invoice, Address__c billingAddresses, Address__c deliveryAddresses, List<Invoiced_Product__c> invoicedProducts, Account partyAccount, Account merchantAccount, Opportunity mOpportunity, String invoiceType, ProformaInvoice__c proformaInvoice) {
        RequestBodyXML xmlBody = new RequestBodyXML(RequestBodyXML.APIType.INVOICE);
        Decimal totalInvoiceAmount = 0.00;
        List<String> invoiceValues = new List<String>();
        invoiceValues.add(String.valueOf(invoice.Date__c).replace('-', ''));
        system.debug('invoice ' + invoice.Billing_Address__r.Name);
        Map<String, String> billingAddress = new Map<String, String>();
        String billingAddress1 = billingAddresses.Name == null ? EMPTY_STRING : billingAddresses.Name;
        String billingAddress2 = billingAddresses.Street__c + ', ' + billingAddresses.City__c + ', ' + billingAddresses.State_Province__c + ' ' + billingAddresses.Zip_Postal_Code__c;
        billingAddress.put('address1', billingAddress1);
        billingAddress.put('address2', billingAddress2);
        invoiceValues.add(JSON.serialize(billingAddress));
        Map<String, String> deliveryAddress = new Map<String, String>();
        deliveryAddress.put('del_address1', deliveryAddresses == null || deliveryAddresses.Name == null ? EMPTY_STRING : deliveryAddresses.Name);
        deliveryAddress.put('del_address2', deliveryAddresses == null || deliveryAddresses.Street__c == null ? EMPTY_STRING : deliveryAddresses.Street__c);
        deliveryAddress.put('del_address3', deliveryAddresses == null || deliveryAddresses.City__c == null ? EMPTY_STRING : deliveryAddresses.City__c);
        deliveryAddress.put('del_address4', deliveryAddresses == null ? EMPTY_STRING : deliveryAddresses.State_Province__c + ' ' + deliveryAddresses.Zip_Postal_Code__c);
        deliveryAddress.put('del_address5', EMPTY_STRING);
        deliveryAddress.put('del_address6', EMPTY_STRING);
        deliveryAddress.put('del_address7', EMPTY_STRING);
        deliveryAddress.put('del_address8', EMPTY_STRING);
        deliveryAddress.put('del_address9', EMPTY_STRING);
        deliveryAddress.put('del_address10', EMPTY_STRING);
        invoiceValues.add(JSON.serialize(deliveryAddress));
        invoiceValues.add(billingAddresses.State_Province__c == null ? EMPTY_STRING : billingAddresses.State_Province__c);
        invoiceValues.add(EMPTY_STRING); //Narration
        invoiceValues.add(billingAddresses.Countries__c == null ? EMPTY_STRING : billingAddresses.Countries__c);
        invoiceValues.add(billingAddresses.GST_Id__c == null ? EMPTY_STRING : billingAddresses.GST_Id__c);
        invoiceValues.add(partyAccount.Name == null ? EMPTY_STRING : partyAccount.Name);
        invoiceValues.add(proformaInvoice.Name == null ? EMPTY_STRING : proformaInvoice.Name);
        invoiceValues.add(invoice.Name);
        invoiceValues.add(invoice.Transportation_Mode__c == null ? EMPTY_STRING : invoice.Transportation_Mode__c);
        invoiceValues.add(deliveryAddresses == null ? EMPTY_STRING : (deliveryAddresses.State_Province__c == null ? EMPTY_STRING : deliveryAddresses.State_Province__c) + ' ' + (deliveryAddresses.Zip_Postal_Code__c == null ? EMPTY_STRING : deliveryAddresses.Zip_Postal_Code__c));
        invoiceValues.add(String.valueOf(invoice.Date__c).replace('-', ''));
        invoiceValues.add(proformaInvoice.Date__c == null ? EMPTY_STRING : String.valueOf(proformaInvoice.Date__c).replace('-', ''));
        invoiceValues.add(proformaInvoice.Name == null ? EMPTY_STRING : proformaInvoice.Name);
        Boolean isStateTax = billingAddresses.State_Province__c == merchantAccount.BillingState;
        AdditionalChargesWrapper packingCharges = new AdditionalChargesWrapper();
        AdditionalChargesWrapper freightCharges = new AdditionalChargesWrapper();
        if(isStateTax) {
            packingCharges.name = 'PACKING CHARGE (GST 18%) REC';
            freightCharges.name = 'Freight Charges (GST @ 18%) Rec';
        } else {
            packingCharges.name = 'Packing Charges (IGST 18%) REC';
            freightCharges.name = 'Freight Charges (IGST @ 18%) Rec';
        }
        if(invoice.Packing_Percentage__c != null) {
            packingCharges.amount = (invoice.Packing_Percentage__c == null ? 0.00 : invoice.Packing_Percentage__c * invoice.Total_Taxable_Amount__c / 100).setScale(2);
            freightCharges.amount = (invoice.Freight_Amount__c == null ? 0.00 : invoice.Freight_Amount__c).setScale(2);
        }
        totalInvoiceAmount += packingCharges.amount;
        system.debug('packingCharges.amount ' + packingCharges.amount);
        system.debug('totalInvoiceAmount ' + totalInvoiceAmount);
        totalInvoiceAmount += freightCharges.amount;
        system.debug('freightCharges.amount ' + freightCharges.amount);
        system.debug('totalInvoiceAmount ' + totalInvoiceAmount);
        Map<Decimal, Decimal> salesTaxesWithTaxAmount = new Map<Decimal, Decimal>();
        invoiceValues.add(JSON.serialize(new List<AdditionalChargesWrapper> {packingCharges, freightCharges})); //packing

        List<InvoiceProductWrapper> invoiceProducts = new List<InvoiceProductWrapper>();
        InvoiceProductWrapper invoiceProduct;
        Decimal totalPrice;
        for(Invoiced_Product__c product : invoicedProducts) {
            invoiceProduct = new InvoiceProductWrapper();
            invoiceProduct.name = product.Product__r.Name;
            invoiceProduct.description = product.Product__r.Description__c == null ? EMPTY_STRING : product.Product__r.Description__c;
            invoiceProduct.rate = (product.Unit_Price__c).setScale(2) + '/' + product.Product__r.Unit__c;
            invoiceProduct.discount = (product.PO_Product__r.Discount_Percentage__c == null ? 0.00 : product.PO_Product__r.Discount_Percentage__c).setScale(2);
            totalPrice = (product.Unit_Price__c * product.Quantity__c).setScale(2);
            invoiceProduct.total = (totalPrice - (totalPrice * (product.PO_Product__r.Discount_Percentage__c == null ? 0.00 : product.PO_Product__r.Discount_Percentage__c) / 100)).setScale(2);
            invoiceProduct.quantity = (product.Quantity__c).setScale(2) + ' ' + product.Product__r.Unit__c;
            invoiceProduct.godown = 'DISPATCH GODOWN';
            invoiceProduct.batch = 'Primary Batch';
            invoiceProduct.gst = getSalesLedger(invoiceType, isStateTax, product.GST__c, product.Product__r.Stock_Group__c);		// GST ledger
            invoiceProduct.rate_with_tax = (product.Unit_Price__c + (product.Unit_Price__c * product.GST__c / 100)).setScale(2) + '/' + product.Product__r.Unit__c;
            invoiceProducts.add(invoiceProduct);
            if(product.GST__c > 0.00) {
                if(salesTaxesWithTaxAmount.containsKey(product.GST__c)) {
                    salesTaxesWithTaxAmount.put(product.GST__c, (salesTaxesWithTaxAmount.get(product.GST__c) + product.GST__c * invoiceProduct.total / 100).setScale(2));
                } else {
                    salesTaxesWithTaxAmount.put(product.GST__c, (product.GST__c * invoiceProduct.total / 100).setScale(2));
                }
            }
            totalInvoiceAmount += (invoiceProduct.total).setScale(2);
            system.debug('totalInvoiceAmount ' + totalInvoiceAmount);
        }
        //use from custom setting
        
        if(salesTaxesWithTaxAmount.containsKey(18.00)) {
            salesTaxesWithTaxAmount.put(18.00, (salesTaxesWithTaxAmount.get(18.00) + 18 * (packingCharges.amount + freightCharges.amount) / 100).setScale(2));
        } else {
            salesTaxesWithTaxAmount.put(18.00, (18 * (packingCharges.amount + freightCharges.amount) / 100).setScale(2));
        }
        
        List<GSTChargesWrapper> gstCharges = new List<GSTChargesWrapper>();
        GSTChargesWrapper gstWrapper;
        for(Decimal gstTax : salesTaxesWithTaxAmount.keySet()) {
            if(isStateTax) {
                gstWrapper = new GSTChargesWrapper('OUTPUT CGST @ ' + ((gstTax / 2).setScale(0)) + '%', (salesTaxesWithTaxAmount.get(gstTax) / 2).setScale(2), (gstTax / 2).setScale(2));
                gstCharges.add(gstWrapper);
                totalInvoiceAmount += gstWrapper.amount.setScale(2);
                gstWrapper = new GSTChargesWrapper('OUTPUT SGST @ ' + ((gstTax / 2).setScale(0)) + '%', (salesTaxesWithTaxAmount.get(gstTax) / 2).setScale(2), (gstTax / 2).setScale(2));
                gstCharges.add(gstWrapper);
                totalInvoiceAmount += gstWrapper.amount.setScale(2);
            } else {
                gstWrapper = new GSTChargesWrapper('Output Inter State Gst@ ' + (gstTax.setScale(0)) + '%', (salesTaxesWithTaxAmount.get(gstTax)).setScale(2), gstTax.setScale(2));
                gstCharges.add(gstWrapper);
                totalInvoiceAmount += gstWrapper.amount.setScale(2);
            }
            system.debug('totalInvoiceAmount ' + totalInvoiceAmount);
        }
        
        invoiceValues.add(JSON.serialize(gstCharges)); //gst
        
        invoiceValues.add(JSON.serialize(invoiceProducts)); //product
        Decimal roundOffValue = totalInvoiceAmount.setScale(0) - totalInvoiceAmount;
        invoiceValues.add(String.valueOf(roundOffValue.setScale(2))); //round off
        invoiceValues.add(String.valueOf(totalInvoiceAmount.setScale(0))); //total
        invoiceValues.add(invoiceType); //voucher type
        xmlBody.setValues(invoiceValues);
        String result = xmlBody.getXMLBody();
        return result;
    }
    
    public static String getExportInvoiceXml(Invoice__c invoice, Address__c billingAddresses, Address__c deliveryAddresses, List<Invoiced_Product__c> invoicedProducts, Account partyAccount, Account merchantAccount, Opportunity mOpportunity, String invoiceType, ProformaInvoice__c proformaInvoice) {
        RequestBodyXML xmlBody = new RequestBodyXML(RequestBodyXML.APIType.EXPORT_INVOICE);
        Decimal totalInvoiceAmount = 0.00;
        Decimal additionalAmount = 0.00;
        List<String> invoiceValues = new List<String>();
        invoiceValues.add(partyAccount.Name == null ? EMPTY_STRING : partyAccount.Name);
        invoiceValues.add(String.valueOf(invoice.Date__c).replace('-', ''));
        system.debug('invoice ' + invoice.Billing_Address__r.Name);
        Map<String, String> billingAddress = new Map<String, String>();
        String billingAddress1 = billingAddresses.Name == null ? EMPTY_STRING : billingAddresses.Name;
        String billingAddress2 = billingAddresses.Street__c + ', ' + billingAddresses.City__c + ', ' + billingAddresses.State_Province__c + ' ' + billingAddresses.Zip_Postal_Code__c;
        billingAddress.put('address1', billingAddress1);
        billingAddress.put('address2', billingAddress2);
        invoiceValues.add(JSON.serialize(billingAddress));
        invoiceValues.add(invoice.Name);
        invoiceValues.add(proformaInvoice.Name == null ? EMPTY_STRING : proformaInvoice.Name);
        invoiceValues.add(EMPTY_STRING); //Narration
        invoiceValues.add(billingAddresses.Countries__c == null ? EMPTY_STRING : billingAddresses.Countries__c);        
        invoiceValues.add(String.valueOf(invoice.Date__c).replace('-', ''));
        
        AdditionalChargesWrapper packingCharges = new AdditionalChargesWrapper();
        AdditionalChargesWrapper freightCharges = new AdditionalChargesWrapper();
        AdditionalChargesWrapper discount = new AdditionalChargesWrapper();
        packingCharges.name = 'PACKING CHARGE RECD. EXPORT';
        freightCharges.name = 'SHIPMENT & FORWARDING REC EXPORT';
        discount.name = 'Discount ($)';
        packingCharges.amount = invoice.Packing_Charges__c == null ? 0.00 : (invoice.Packing_Charges__c.setScale(2) * (invoice.Conversion_Rate_To_INR__c == null ? 1 : invoice.Conversion_Rate_To_INR__c)).setScale(2);
        freightCharges.amount = invoice.Shipping_Charges__c == null ? 0.00 : (invoice.Shipping_Charges__c.setScale(2) * (invoice.Conversion_Rate_To_INR__c == null ? 1 : invoice.Conversion_Rate_To_INR__c)).setScale(2);
        Decimal totalDiscount = invoice.Trade_Discount__c == null ? 0 : invoice.Trade_Discount__c.setScale(2);
        totalDiscount += invoice.Product_Liability_Insurance__c == null ? 0 : invoice.Product_Liability_Insurance__c.setScale(2);
                
        discount.amount = (totalDiscount  * (invoice.Conversion_Rate_To_INR__c == null ? 1 : invoice.Conversion_Rate_To_INR__c)).setScale(2);
        
        additionalAmount += packingCharges.amount;
        additionalAmount += freightCharges.amount;
        additionalAmount -= discount.amount;
        Map<Decimal, Decimal> salesTaxesWithTaxAmount = new Map<Decimal, Decimal>();
        
        List<ExportInvoiceProductWrapper> invoiceProducts = new List<ExportInvoiceProductWrapper>();
        ExportInvoiceProductWrapper invoiceProduct;
        Decimal totalPrice;
        for(Invoiced_Product__c product : invoicedProducts) {
            invoiceProduct = new ExportInvoiceProductWrapper();
            invoiceProduct.name = product.Product__r.Name;
            invoiceProduct.description = product.Product__r.Description__c == null ? EMPTY_STRING : product.Product__r.Description__c;
            invoiceProduct.rate = (product.Unit_Price__c  * (invoice.Conversion_Rate_To_INR__c == null ? 1 : invoice.Conversion_Rate_To_INR__c)).setScale(2) + '/' + product.Product__r.Unit__c;
            totalPrice = (product.Unit_Price__c * (invoice.Conversion_Rate_To_INR__c == null ? 1 : invoice.Conversion_Rate_To_INR__c)) * product.Quantity__c;
            invoiceProduct.total = totalPrice.setScale(2);
            invoiceProduct.quantity = product.Quantity__c + ' ' + product.Product__r.Unit__c;
            invoiceProduct.godown = 'DISPATCH GODOWN';
            invoiceProduct.batch = 'Primary Batch';
            invoiceProduct.ledger = getSalesLedger(invoiceType, false, product.GST__c, product.Product__r.Stock_Group__c);		// GST ledger
            invoiceProducts.add(invoiceProduct);
            totalInvoiceAmount += invoiceProduct.total;
        }
        
        Decimal totalWithoutAdditional = totalInvoiceAmount;
        invoiceValues.add(String.valueOf((additionalAmount + totalInvoiceAmount).setScale(2))); //total
        invoiceValues.add(String.valueOf(totalWithoutAdditional.setScale(2))); //round off
        invoiceValues.add(JSON.serialize(new List<AdditionalChargesWrapper> {packingCharges, freightCharges})); //packing
        invoiceValues.add(JSON.serialize(discount)); //discount
        invoiceValues.add(JSON.serialize(invoiceProducts)); //product
        xmlBody.setValues(invoiceValues);
        String result = xmlBody.getXMLBody();
        return result;
    }
    
    public static String getAccountXml(Account mAccount, List<Address__c> addresses, List<Contact> contacts) {
        RequestBodyXML xmlBody = new RequestBodyXML(RequestBodyXML.APIType.ACCOUNT);
        List<String> accountValues = new List<String>();
        system.debug('contacts ' + contacts);
        accountValues.add(mAccount.Name);
        accountValues.add(mAccount.Id);
        accountValues.add(mAccount.Group__c);
        Address__c billingAddress;
        
        system.debug('addresses ' + addresses);
        
        if(addresses != null) {
            for(Address__c address : addresses) {
                if('Billing'.equals(address.Address_Type__c)) {
                    billingAddress = address;
                    break;
                }
            }
        }

        Map<String, String> address = new Map<String, String>();
        if(billingAddress != null) {
            accountValues.add(billingAddress.Name);
            address.put('address1', billingAddress.Street__c == null ? EMPTY_STRING : billingAddress.Street__c);
            address.put('address2', billingAddress.City__c == null ? EMPTY_STRING : billingAddress.City__c);
            address.put('address3', EMPTY_STRING);
            address.put('address4', EMPTY_STRING);
            accountValues.add(JSON.serialize(address));
            accountValues.add(billingAddress.Countries__c == null ? EMPTY_STRING : billingAddress.Countries__c);
            accountValues.add(billingAddress.State_Province__c == null ? EMPTY_STRING : billingAddress.State_Province__c);
            accountValues.add(billingAddress.Zip_Postal_Code__c == null ? EMPTY_STRING : billingAddress.Zip_Postal_Code__c);
        } else {
            accountValues.add(EMPTY_STRING);
            address.put('address1', EMPTY_STRING);
            address.put('address2', EMPTY_STRING);
            address.put('address3', EMPTY_STRING);
            address.put('address4', EMPTY_STRING);
            accountValues.add(JSON.serialize(address));
            accountValues.add(EMPTY_STRING);
            accountValues.add(EMPTY_STRING);
            accountValues.add(EMPTY_STRING);
        }
        system.debug('address ' + address);
        
        List<String> contactName = new List<String>();
        List<String> contactNumber = new List<String>();
        String name;
        if(contacts != null && contacts.size() > 0) {
            for(Contact cnct : contacts) {
                //replace by contact.Name
                name = (cnct.FirstName == null ? '' : cnct.FirstName) + ' ' + (cnct.MiddleName == null ? '' : cnct.MiddleName) + ' ' + (cnct.LastName == null ? '' : cnct.LastName);
                contactName.add(name);
                contactNumber.add(cnct.Phone);
            }
            system.debug('contactName ' + contactName);
            system.debug('contactNumber ' + contactNumber);
            String contactNames = EMPTY_STRING + contactName;
            accountValues.add(contactName.contains(null) ? EMPTY_STRING : contactNames.subString(2, contactNames.length() - 1));
            String contactNumbers = EMPTY_STRING + contactNumber;
            accountValues.add(contactNumber.contains(null) ? EMPTY_STRING : contactNumbers.subString(2, contactNumbers.length() - 1));
        } else {
            accountValues.add(EMPTY_STRING);
            accountValues.add(EMPTY_STRING);
        }
        
        accountValues.add(mAccount.fax == null ? EMPTY_STRING : mAccount.fax);
        accountValues.add(mAccount.Mobile__c == null ? EMPTY_STRING : mAccount.Mobile__c);
        accountValues.add(mAccount.Email__c == null ? EMPTY_STRING : mAccount.Email__c);
        accountValues.add('Yes');
        accountValues.add(EMPTY_STRING);
        accountValues.add('No');
        String regType;
        switch on mAccount.Registration_Type__c {
            when 'Consumer (End User with no GST)'{
                regType = 'Consumer';
            }
            when 'Regular (having GST)' {
                regType = 'Regular';
            }
            when 'Unregistered (No GST company)' {
                regType = 'Unregistered';
            }
            when 'Composition' {
                regType = 'Composition';
            }
            when else {
                regType = 'Regular';
            }
        }
        accountValues.add(regType == null ? EMPTY_STRING : regType);
        accountValues.add(mAccount.PAN_Number__c == null ? EMPTY_STRING : mAccount.PAN_Number__c);
        accountValues.add(billingAddress == null || billingAddress.GST_Id__c == null ? EMPTY_STRING : billingAddress.GST_Id__c);
        system.debug('accountValues ' + JSON.serialize(accountValues));
        xmlBody.setValues(accountValues);
        String result = xmlBody.getXMLBody();
        return result;
    }
    
    private static String getSalesLedger(String salesProcess, Boolean isStateTax, Decimal gstPercent, String productGroup) {
        String result = '';
        if(salesProcess.equals('Export')) {
            result += 'SALES EXPORT';
        } else {
            if(isStateTax) {
                result += 'GST SALES' + ' ' + (gstPercent == null ? '' : gstPercent == 0.00 ? 'EXEMPT' : '@' + String.valueOf(gstPercent.setScale(0)) + '%');
            } else {
                result += 'Inter State Gst Sales' + ' ' + (gstPercent == null ? '' : gstPercent == 0.00 ? 'Exempt' : '@' + String.valueOf(gstPercent.setScale(0)) + '%');
            }
        }
        result += productGroup == null || productGroup.equals('') ? '' : ' (' + productGroup + ')';
        return result;
    }
    
    public class AdditionalChargesWrapper {
        public String name;		// Tax Ledger Name
        public Decimal amount;
        
        public AdditionalChargesWrapper() {
            name = '';
            amount = 0.00;
        }
        
        public AdditionalChargesWrapper(String name, Decimal amount) {
            this.name = name;
            this.amount = amount;
        }
    }
    
    public class GSTChargesWrapper {
        public String name;		// Tax Ledger Name
        public Decimal amount;
        public Decimal rate;
        
        public GSTChargesWrapper() {
            this.name = '';
            this.amount = 0.00;
            this.rate = 0.00;
        }
        
        public GSTChargesWrapper(String name, Decimal amount, Decimal rate) {
            this.name = name;
            this.amount = amount;
            this.rate = rate;
        }
    }
    
    public class InvoiceProductWrapper {
        public String name;
        public String description;
        public String rate;
        public Decimal discount;
        public Decimal total;
        public String quantity;
        public String godown;
        public String batch;
        public String gst;		// GST ledger
        public String rate_with_tax;
    }
    
    public class ExportInvoiceProductWrapper {
        public String name;
        public String description;
        public String rate;
        public Decimal total;
        public String quantity;
        public String godown;
        public String batch;
        public String ledger;		// GST ledger
    }
    
}