public class InvoicesServiceImpl implements IInvoicesService {
    private List<Alloted_Stock__c> allotedStocks {get;set;}
    public Map<Id,Decimal> allotedStockToProduct{get;set;}
    public static Map<Id, ProformaInvoice__c> proformaInvoiceMap; 
    /**
     * Get Proforma Invoices for opportunities
    **/
    public List<InvoicesService.ProformaInvoiceWrapper> getProformaInvoices(List<Id> opportunityIds) {
        proformaInvoiceMap = new Map<Id, ProformaInvoice__c>(ProformaInvoicesSelector.getInstance().selectProformaInvoicesWithLineItemsByOpportunityIds(new Set<Id>(opportunityIds)));
       
        // Soql query to get alloted stock of respective products and proforma inovices
        List<Id> proformaInvoiceIds = new List<Id>();
        Set<Id> productIds = new Set<Id>();
        List<Proforma_Invoice_Line_Item__c> listProformaLineItem = new List<Proforma_Invoice_Line_Item__c>();
        if(proformaInvoiceMap == null) {
            throw new AuraHandledException('No Accepted PI exists for this Opportunity.');
        }
        for(Id proformaInvoiceId : proformaInvoiceMap.keySet()) {
            if(proformaInvoiceMap.get(proformaInvoiceId).ProformaInvoiceLineItems__r.size() > 0) {
                proformaInvoiceIds.add(proformaInvoiceId);
                for(Proforma_Invoice_Line_Item__c proformaInvoiceLineItem : proformaInvoiceMap.get(proformaInvoiceId).ProformaInvoiceLineItems__r) {
                	productIds.add(proformaInvoiceLineItem.Product__c);
                	listProformaLineItem.add(proformaInvoiceLineItem);
                }
            }
        }
        allotedStocks = [Select Id, ProformaInvoice__c, Product__c, Product__r.Name, Stock__c, Warehouse_Product__c From Alloted_Stock__c Where ProformaInvoice__c =:proformaInvoiceIds And Product__c =:productIds];
        allotedStockToProduct = new Map<Id,Decimal>(); 
        for(Proforma_Invoice_Line_Item__c lineItem : listProformaLineItem) {
            allotedStockToProduct.put(lineItem.Id, getAllotedStock(lineItem.Product__c, lineItem.ProformaInvoice__c));
        }
        List<InvoicesService.ProformaInvoiceWrapper> proformaInvoiceWrappers = new List<InvoicesService.ProformaInvoiceWrapper>();
        for(Id proformaInvoiceId : proformaInvoiceMap.keySet()) {
            InvoicesService.ProformaInvoiceWrapper proformaInvoiceWrapper;
            List<InvoicesService.ProformaInvoiceLineItemWrapper> lineItemsWrapper = new List<InvoicesService.ProformaInvoiceLineItemWrapper>();
            for(Proforma_Invoice_Line_Item__c proformaInvoiceLineItem : proformaInvoiceMap.get(proformaInvoiceId).ProformaInvoiceLineItems__r) {
                InvoicesService.ProformaInvoiceLineItemWrapper lineItemWrapper = new InvoicesService.ProformaInvoiceLineItemWrapper(proformaInvoiceLineItem);
                lineItemWrapper.bookedQuantity = allotedStockToProduct.get(proformaInvoiceLineItem.id);
                lineItemsWrapper.add(lineItemWrapper);
            }
            if(proformaInvoiceMap.get(proformaInvoiceId).ProformaInvoiceLineItems__r.size() > 0) {
                proformaInvoiceWrapper = new InvoicesService.ProformaInvoiceWrapper(proformaInvoiceMap.get(proformaInvoiceId), lineItemsWrapper, proformaInvoiceMap.get(proformaInvoiceId).Opportunity__r.AccountId);
                proformaInvoiceWrappers.add(proformaInvoiceWrapper);
            }
        }
        
        return proformaInvoiceWrappers;
    }
    
    /**
    * Generate invoices for the given source records (so long as their domain classes implement ISupportInvoicing)
    **/
    public static List<Id> generate(List<InvoicesService.ProformaInvoicesIdsWrapper> mergeInvoicesProformaInvoices, List<ProformaInvoice__c> allProformaInvoices) {
        // Create unit of work to capture work and commit it under one transaction
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        
        // Invoicing Factory helps domain classes produce invoices
        InvoicesService.InvoiceFactory invoiceFactory = new InvoicesService.InvoiceFactory(uow);
        
        
        // Construct domain class capabile of performing invoicing
        fflib_ISObjectDomain domain = Application.Domain.newInstance(allProformaInvoices);
        if(domain instanceof InvoicesService.ISupportInvoicing) { 
            // Ask the domain object to generate its invoices
            InvoicesService.ISupportInvoicing invoicing = (InvoicesService.ISupportInvoicing) domain;
            invoicing.generate(invoiceFactory, mergeInvoicesProformaInvoices);	
            // Commit generated invoices to the database
            System.debug(uow);		
            uow.commitWork();
            
            //##############Send Emails Code Starts################### (Replace this code with optimal one).
            //1. Send Proforma Invoice creation email
            //Need to send List of emails to be sent
            EmailManager sendEmail = new EmailManager();
            List<Invoice__c> newlyInsertedInvoices = [SELECT Id, Name, Opportunity__c FROM Invoice__c WHERE Id IN :invoiceFactory.Invoices ];
            Set<Id> opportunityIds = new Set<Id>();
            for(Invoice__c invoice : newlyInsertedInvoices) {
                opportunityIds.add(invoice.Opportunity__c);
            }
            Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([Select Id, Name, Total_Received_Amount__c, Total_Settled_Amount__c, AccountId,Account.Name,RecordType.Name, Owner.Name, Owner.Email, CurrencyIsoCode,Transportation_Mode__c,Freight_Amount__c,
                                                                           Packing_Percentage__c, Billing_Address__c, Notify_Address__c, Delivery_Address__c, Buyer_Address__c, Contact_Person__c, Notify_Contact__c, Contact__c, Buyer_Contact__c From Opportunity Where Id IN : opportunityIds]);
            
            for(Invoice__c invoice : newlyInsertedInvoices) {
                sendEmail.sendNotification((SObject)invoice, opportunities.get(invoice.Opportunity__c));
            }
            
            //##############Send Emails Code ends###################
            
            // List of Invoices generated
            List<Id> invoiceIds = new List<Id>();
            for(Invoice__c invoice : invoiceFactory.Invoices) {
                invoiceIds.add(invoice.Id);
            }
            return invoiceIds;
        }
        
        // Related Domain object does not support the ability to generate invoices
        throw new AKFDException('Invalid source object for generating invoices.');
    }
    
    
    /*
    * Method getAllotedStock
    * Return Integer
    * Params productId of type ID
    * Params proformaInvoiceId of type Id
    * Method to get alloted stock for given product and proformaInvoice
    */
    private Decimal getAllotedStock(Id productId, Id proformaInvoiceId) {
        Decimal result = 0;
        for(Alloted_Stock__c allotedStock : allotedStocks) {
            if(allotedStock.Product__c == productId && allotedStock.ProformaInvoice__c == proformaInvoiceId) {
                result += allotedStock.Stock__c;
            }
        }
        return result;
    }
    
}