@isTest
Private Class DashboardControllerTest {
    @TestSetup static void setup(){
        AKFD_Extra_Charges_Tax_Percentage__c akfdExtraChargesTaxPercentage = AKFD_Extra_Charges_Tax_Percentage__c.getOrgDefaults();
        akfdExtraChargesTaxPercentage.Tax_Percentage__c = 18.00;
        upsert akfdExtraChargesTaxPercentage;
        
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        
        List<Account> acc=new List<Account>();
        Account accountCustomerObj = new Account(Name='AccountTestCustomer', RecordTypeId = customerRecordTypeIdAccount, CurrencyIsoCode = 'USD');
        Account accountMerchantObj = new Account(Name='AccountTestMerchant', RecordTypeId = merchantcustomerRecordTypeIdAccount , BillingState = 'Rajasthan');
        acc.add(accountCustomerObj);
        acc.add(accountMerchantObj);
        insert acc;
        
        List<Address__c> addresses = new List<Address__c>();
        Address__c billingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Billing', Countries__c = 'India', State__c = 'Rajasthan');
        Address__c shippingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Delivery', Countries__c = 'India', State__c = 'Rajasthan');
        addresses.add(billingAddress);
        addresses.add(shippingAddress);
        insert addresses;
       
        
        Id agentContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent Contact').getRecordTypeId();
        Contact agentContactObj = new Contact(LastName='Test Contact Last Name', AccountId=accountCustomerObj.Id,RecordTypeId=agentContactRecordTypeId,Email='test@test.com');
        insert agentContactObj;
         
        List<Opportunity> opportunities=new List<Opportunity>();
       	Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        opportunities.add(new Opportunity(Name = 'Test Opportunity', Contact_Person__c = agentContactObj.Id, Freight_Amount__c=20.00, CurrencyIsoCode = 'USD', AccountId = accountCustomerObj.Id, Merchant__c = accountMerchantObj.Id, recordTypeid = opportunityRecordTypeId, Billing_Address__c=billingAddress.Id, Delivery_Address__c = shippingAddress.Id, StageName = 'Client Setup', CloseDate=Date.Today().addDays(14)));
        insert opportunities;
      
        List<Product2> products = new  List<Product2>();
        products.add(new Product2(Name='New Chair1',Family='Accessory',Retail_Price__c=150.00,Material__c='Iron;Felt'));
        products.add(new Product2(Name='New Chair2',Family='Accessory',Retail_Price__c=180.00,Material__c='Iron'));
        insert products;
        
        Id desiredProductRecordTypeId = Schema.SObjectType.PO_Product__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        List<PO_Product__c> desiredProducts = new List<PO_Product__c>();
        desiredProducts.add(new PO_Product__c(Product__c = products[0].Id,RecordTypeId = desiredProductRecordTypeId, Lead_Time_In_Days__c = 15, Unit__c = 'Each', Retail_Price__c = 120.00, Quantity__c = 10, Status__c = 'New', Opportunity__c = opportunities[0].Id, SKU_Number__c = 'AKFD Test1'));
        insert desiredProducts;
         
        PriceBook_Product__c priceBookObj = new PriceBook_Product__c(Customer__c = accountCustomerObj.Id, Product__c = products[0].Id, Effective_Start_Date__c = Date.today(), Effective_End_Date__c = Date.today().addDays(365), Sampling_Charges__c = 120.0);
        insert priceBookObj;
        
        PriceBook_Product_Entry__c priceBookEntryObj = new PriceBook_Product_Entry__c(PriceBook_Product__c = priceBookObj.Id, Min_Quantity__c = 1, Max_Quantity__c = 100, Price__c = 145.00, Lead_Time_for_MOQ__c = 15, Unit_of_Price__c = 'Each');
        insert  priceBookEntryObj;
        
        //PI Creation
        Id performaInvoiceRecordTypeId = Schema.SObjectType.ProformaInvoice__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        ProformaInvoice__c proformaInvoice = new ProformaInvoice__c(Date__c = Date.today(),
                                                                    RecordTypeId = performaInvoiceRecordTypeId,
                                                                    Opportunity__c = opportunities[0].Id,
                                                                    CurrencyIsoCode = opportunities[0].CurrencyIsoCode,
                                                                    Conversion_Rate__c = 1,
                                                                    Final_Destination__c = 'INDIA',
                                                                    Mode_of_Payment__c = 'Cash',
                                                                    Freight__c = 'Temp',
                                                                    Advance_Payment__c = 50.00,
                                                                    Packing_Charges__c = 10,
                                                                    Port_Of_Discharge__c = 'INDIA' ,
                                                                    Place_Of_Receipt__c = 'INDIA' ,
                                                                    Port_of_Loading__c = 'INDIA' ,
                                                                    Pre_Carriage_By__c ='INDIA' ,
                                                                    Product_Liability_Insurance_Percentage__c = 8,
                                                                    Shipping_Charges__c = 5.00,
                                                                    Trade_Discount__c = 3.00
                                                                   );
        insert proformaInvoice;
        
        
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = new List<Proforma_Invoice_Line_Item__c>();
        Id performaInvoiceLineItemRecordTypeId = Schema.SObjectType.Proforma_Invoice_Line_Item__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        Proforma_Invoice_Line_Item__c proformaInvoiceLineItem;
        for(PO_Product__c desiredProduct : desiredProducts){
            proformaInvoiceLineItem = new Proforma_Invoice_Line_Item__c(  
                Product__c =desiredProduct.Product__c,
                RecordTypeId = performaInvoiceLineItemRecordTypeId,
                Required_Quantity__c = desiredProduct.quantity__c,
                Price__c = desiredProduct.Retail_Price__c,
                PO_Product__c = desiredProduct.Id,
                ProformaInvoice__c = proformaInvoice.Id,
                Unit__c = desiredProduct.Unit__c ,
                Stock_Till_date_Lead_time__c = desiredProduct.Lead_Time_In_Days__c,
                CurrencyIsoCode = opportunities[0].CurrencyIsoCode
            );
            proformaInvoiceLineItems.add(proformaInvoiceLineItem);
        } 
        insert proformaInvoiceLineItems;
        
        //Invoice creation
        Double conversionRate=1;
        Id invoiceRecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Invoice__c invoice = new Invoice__c(isMerged__c = false,
                                            Opportunity__c=opportunities[0].id,
                                            Date__c=Date.today(),
                                            RecordTypeId=invoiceRecordTypeId,
                                            Country_Of_Final_Origin__c = proformaInvoice.Country_Of_Final_Origin__c,
                                            CurrencyIsoCode = proformaInvoice.CurrencyIsoCode,
                                            Final_Destination__c = proformaInvoice.Final_Destination__c,
                                            Mode_of_Payment__c = proformaInvoice.Mode_of_Payment__c,
                                            Freight__c = proformaInvoice.Freight__c,
                                            Advance_Payment__c = proformaInvoice.Advance_Payment__c * conversionRate,
                                            Packing_Charges__c = proformaInvoice.Packing_Charges__c * conversionRate,
                                            Port_Of_Discharge__c = proformaInvoice.Port_Of_Discharge__c,
                                            Place_Of_Receipt__c = proformaInvoice.Place_Of_Receipt__c,
                                            Port_of_Loading__c = proformaInvoice.Port_of_Loading__c,
                                            Pre_Carriage_By__c = proformaInvoice.Pre_Carriage_By__c,
                                            Product_Liability_Insurance_Percentage__c = proformaInvoice.Product_Liability_Insurance_Percentage__c,
                                            Shipping_Charges__c = proformaInvoice.Shipping_Charges__c* conversionRate,
                                            Trade_Discount__c = proformaInvoice.Trade_Discount__c* conversionRate,
                                            Freight_Amount__c = opportunities[0].Freight_Amount__c,
                                            Transportation_Mode__c = opportunities[0].Transportation_Mode__c,
                                            Billing_Address__c = billingAddress.Id,
                                            Account__c = accountCustomerObj.Id
                                           );
        insert invoice;
        
        Id invoiceItemRecordTypeId = Schema.SObjectType.Invoiced_Items__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        Invoiced_Items__c invoiceItem = new Invoiced_Items__c(Invoice__c = invoice.id,
                                                Opportunity__c = opportunities[0].Id, 
                                                CurrencyIsoCode = invoice.CurrencyIsoCode,
                                                RecordTypeId = invoiceItemRecordTypeId,
                                                Settled_Amount__c=0.00);
        insert invoiceItem;
        
        List<Invoiced_Product__c> invoiceProducts = new List<Invoiced_Product__c>();
        Invoiced_Product__c invoiceProduct;
        Id invoicedProductsRecordTypeId = Schema.SObjectType.Invoiced_Product__c.getRecordTypeInfosByName().get('Export').getRecordTypeId();
        for(Proforma_Invoice_Line_Item__c piLineItem : proformaInvoiceLineItems) {
                    invoiceProduct = new Invoiced_Product__c(Product__c = piLineItem.Product__c,
                                                             Invoiced_Item__c = invoiceItem.Id,
                                                             RecordTypeId=invoicedProductsRecordTypeId,
                                                             Proforma_Invoice_Line_Item__c = piLineItem.Id,
                                                             PO_Product__c=piLineItem.PO_Product__c,
                                                             SKU_Number__c = piLineItem.SKU_Number__c,
                                                             Quantity__c = piLineItem.Required_Quantity__c,
                                                             Unit_Price__c = (piLineItem.Price__c * conversionRate),
                                                             CurrencyIsoCode = invoiceItem.CurrencyIsoCode
                                                             );
                    invoiceProducts.add(invoiceProduct);
        }
        insert invoiceProducts;
    }
    
    
    @isTest static void sellingTrendGraphWithQuarterlyFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        ApexPages.currentPage().getParameters().put('queryParam','sellingtrend');
        ApexPages.currentPage().getParameters().put('productId',productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Quarterly';
        dashboardController.sellingTrendGraph();
        //reportController.getSellingTrendReportData();
        System.assert(dashboardController.lstSellingTrend.size()>0);
        Test.stopTest();
    }
    
    @isTest static void sellingTrendGraphWithMonthlyFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','sellingtrend');
        ApexPages.currentPage().getParameters().put('productId',productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Monthly';
        dashboardController.sellingTrendGraph();
        System.assert(dashboardController.lstSellingTrend.size()>0);
        //reportController.getSellingTrendReportData();
        Test.stopTest();
    }
    
    @isTest static void sellingTrendGraphWithYearlyFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','sellingtrend');
        ApexPages.currentPage().getParameters().put('productId',productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Yearly';
        dashboardController.sellingTrendGraph();
        System.assert(dashboardController.lstSellingTrend.size()>0);
        //reportController.getSellingTrendReportData();
        Test.stopTest();
    }
    
    @isTest static void sellingTrendGraphWithDateFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','sellingtrend');
        ApexPages.currentPage().getParameters().put('productId',productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Custom Range';
        dashboardController.startRange = '05/19/2018';
        dashboardController.endRange = '05/30/2018';
        dashboardController.sellingTrendGraph();
        System.assert(dashboardController.lstSellingTrend.size()>0);
        //reportController.getSellingTrendReportData();
        Test.stopTest();
    }
    
    @isTest static void quantityTrendGraphWithQuarterlyFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','quantity');
        ApexPages.currentPage().getParameters().put('quantity','10');
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Quarterly';
        dashboardController.quantityTrendGraph();
        System.assert(dashboardController.lstQuantityTrend.size()>0);
        Test.stopTest();
    }
    
    @isTest static void quantityTrendGraphWithMonthlyFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','quantity');
        ApexPages.currentPage().getParameters().put('quantity','10');
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Monthly';
        dashboardController.quantityTrendGraph();
        System.assert(dashboardController.lstQuantityTrend.size()>0);
        Test.stopTest();
    }
    
    @isTest static void quantityTrendGraphWithYearlyFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','quantity');
        ApexPages.currentPage().getParameters().put('quantity','10');
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Yearly';
        dashboardController.quantityTrendGraph();
        System.assert(dashboardController.lstQuantityTrend.size()>0);
        Test.stopTest();
    }
    
    @isTest static void quantityTrendGraphWithDateFilter(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        ApexPages.currentPage().getParameters().put('queryParam','quantity');
        ApexPages.currentPage().getParameters().put('quantity','10');
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Custom Range';
        dashboardController.startRange = '05/19/2018';
        dashboardController.endRange = '05/30/2018';
        dashboardController.quantityTrendGraph();
        System.assert(dashboardController.lstQuantityTrend.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testRecivedPaymentGraphMonthly(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Monthly';
        dashboardController.recivedPaymentGraph();
        System.assert(dashboardController.lstDuePayments.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testRecivedPaymentGraphQuarterly(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Quarterly';
        dashboardController.recivedPaymentGraph();
        System.assert(dashboardController.lstDuePayments.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testRecivedPaymentGraphYearly(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Yearly';
        dashboardController.recivedPaymentGraph();
        System.assert(dashboardController.lstDuePayments.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testRecivedPaymentGraphCustomRange(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Custom Range';
        dashboardController.startRange = '05/19/2018';
        dashboardController.endRange = '05/30/2018';
        dashboardController.recivedPaymentGraph();
        System.assert(dashboardController.lstDuePayments.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testGetChartDataMonthly(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Monthly';
        dashboardController.getChartData();
        System.assert(dashboardController.lstCompaire.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testGetChartDataQuaterly(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Quaterly';
        dashboardController.getChartData();
        System.assert(dashboardController.lstCompaire.size()==0);
        Test.stopTest();
    }
    
    @isTest static void testGetChartDataYearly(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Yearly';
        dashboardController.getChartData();
        System.assert(dashboardController.lstCompaire.size()>0);
        Test.stopTest();
    }
    
    @isTest static void testGetChartDataCustomRange(){
        Test.startTest();
        Id productId=[SELECT Id FROM Product2 LIMIT 1].Id;
        System.debug('inside execution'+productId);
        DashboardController dashboardController= new DashboardController();
        dashboardController.seletedFilter='Custom Range';
        dashboardController.startRange = '05/19/2018';
        dashboardController.endRange = '05/30/2018';
        dashboardController.getChartData();
        System.assert(dashboardController.lstCompaire.size()>0);
        Test.stopTest();
    }
    
    
}