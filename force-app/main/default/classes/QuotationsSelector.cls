public class QuotationsSelector extends fflib_SObjectSelector implements IQuotationsSelector {
    private static IQuotationsSelector instance = null;
    
    public static IQuotationsSelector getInstance() {
        
        if(instance == null) { 
            instance = (IQuotationsSelector)Application.selector().newInstance(Quotation__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            	Quotation__c.Id, 
                Quotation__c.Name, 
                Quotation__c.Opportunity__c,
                Quotation__c.CurrencyIsoCode, 
                Quotation__c.Effective_Start_Date__c,
                Quotation__c.End_Date__c,
                Quotation__c.Expected_Delivery_Time__c,
                Quotation__c.Delivery_Remarks__c,
                Quotation__c.Remarks__c,
                Quotation__c.Freight__c
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return Quotation__c.sObjectType;
    }
    
    public List<Quotation__c> selectById(Set<ID> idSet) {
        return (List<Quotation__c>) selectSObjectsById(idSet);
    }
    public List<Quotation__c> selectQuotationsByIds(Set<Id> quotationIds) {
        System.debug('selector'+quotationIds);
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('RecordType.Name');
        query.selectField('Opportunity__r.RecordType.Name');
        query.selectField('Opportunity__r.AccountId');
        query.setCondition('Id IN :quotationIds');
        System.debug('selector'+query.toSOQL());
        return (List<Quotation__c>) Database.query( query.toSOQL() );
    }
    
    
}