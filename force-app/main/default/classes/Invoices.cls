/*
 * Domain Class for Invoice Trigger
 * Author : Gaurav Gupta
 * Last Modified: 29 Aug 2018
*/
public class Invoices extends fflib_SObjectDomain {
    
    /* 
     * Parameterized Constructor 
     * Param - List of Invoices  - equivalent to Trigger.New records
	*/
    public Invoices(List<Invoice__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new Invoices(records);
        }
    }
    
    /* 
     * Method - onBeforeUpdate
     * Param - Map of Invoices and Ids - equivalent to Trigger.oldMap
     * Description - Method to execute when trigger is of type Before and Update
	*/
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        validateContacts();
        Id exportInvoiceRecordType = Schema.SObjectType.Invoice__c.RecordTypeInfosByName.get('Export').RecordTypeId;
        Id projectInvoiceRecordType = Schema.SObjectType.Invoice__c.RecordTypeInfosByName.get('Project').RecordTypeId;
        Id retailInvoiceRecordType = Schema.SObjectType.Invoice__c.RecordTypeInfosByName.get('Retail').RecordTypeId;
        Id additionalPaymentTermRecordType = Schema.SObjectType.Payment_Term__c.RecordTypeInfosByName.get('Additional Payment Terms').RecordTypeId;
        Id installmentRecordTypeId = Schema.SObjectType.Payment_Term__c.RecordTypeInfosByName.get('Installment Payment Terms').RecordTypeId;
        Set<Id> opportunitiesId = new Set<Id>();
        Map<Id, Invoice__c> existingInvoiceRecords = (Map<Id, Invoice__c>)existingRecords;
        Map<Id, Invoice__c> reviewedInvoices = new Map<Id, Invoice__c>();
        
        // Filter and validate invoices. Reviewed invoices are stored separately.
        for(Invoice__c invoice : (List<Invoice__c>)records) {
            //Validate if invoice status is changed from Reviewed to other status or from new to reviewed.
            if(existingInvoiceRecords.get(invoice.Id).Status__c == 'Reviewed' && invoice.Status__c != 'Reviewed') {
                if(ApexTriggerCheck.getInstance() == null || ApexTriggerCheck.getInstance().isExecutinngFromApexCode == null) {
                    invoice.addError('Status cannot be changed from Reviewed.');
                }
            } else if(existingInvoiceRecords.get(invoice.Id).Status__c == 'New' && invoice.Status__c == 'Reviewed') {
                if(invoice.Net_Invoice_Amount__c < 0) {
                    if(invoice.RecordTypeId == exportInvoiceRecordType ) {
                        invoice.addError('Invoice cannot to Reviewed due to Product Liability.');
                    } else {
                        invoice.addError('Invoice cannot to Reviewed due to Discount Percentage.');
                    }
                } else {
                    reviewedInvoices.put(invoice.Id, invoice);
                }
            }
        }
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                ProformaInvoice__c.SObjectType,
                    Proforma_Invoice_Line_Item__c.SObjectType,
                    Invoiced_Product__c.SObjectType,
                    Payment_Term__c.SObjectType,
                    Alloted_Stock__c.SObjectType,
                    Warehouse_Line_Item__c.SObjectType
                    }
        );
        
        Map<Id, Invoiced_Items__c> invoicedItems = new Map<Id, Invoiced_Items__c>(InvoicedItemsSelector.getInstance().selectInvoicedItemsWithInvoicedProductsByInvoiceIds(reviewedInvoices.keySet()));
        
        Map<Id, List<Invoiced_Items__c>> invoiceWithLineItems = new Map<Id, List<Invoiced_Items__c>>();
        Map<Id, List<Invoiced_Product__c>> invoiceItemsWithProducts = new Map<Id, List<Invoiced_Product__c>>();
        List<Invoiced_Items__c> items;
        List<Invoiced_Product__c> products;
        Boolean isReviewed = false;
        
        //Creating invoice line item map, key -> invoice id, value -> list of invoice line item.
        //Create invoiced product map, key -> invoice item Id, value -> list of invoiced product.
        for(Invoiced_Items__c item : invoicedItems.values()) {
            if(invoiceWithLineItems.containsKey(item.Invoice__c)) {
                items = invoiceWithLineItems.get(item.Invoice__c);
            } else {
                items = new List<Invoiced_Items__c>();
            }
            items.add(item);
            System.debug('Invoiced Products ' + item.Invoiced_Products__r);
            for(Invoiced_Product__c invoicedProduct : item.Invoiced_Products__r) {
                if(invoiceItemsWithProducts.containsKey(invoicedProduct.Invoiced_Item__c)) {
                    products = invoiceItemsWithProducts.get(invoicedProduct.Invoiced_Item__c);
                } else {
                    products = new List<Invoiced_Product__c>();
                }
                products.add(invoicedProduct);
                invoiceItemsWithProducts.put(invoicedProduct.Invoiced_Item__c, products);
            }
            invoiceWithLineItems.put(item.Invoice__c, items);
            opportunitiesId.add(item.Opportunity__c);
        }

        List<Payment_Term__c> paymentTerms = PaymentTermsSelector.getInstance().selectInstallmentPaymentTermsByOpportunityIds(opportunitiesId);
        Map<Id, List<Payment_Term__c>> paymentTermsWithOpportunityMap = new Map<Id, List<Payment_Term__c>>();
        List<Payment_Term__c> terms;
        
        //Create payment terms with opportunity map, key -> opportunity id, value -> list of payment terms.
        for(Payment_Term__c paymentTerm : paymentTerms) {
            if(paymentTermsWithOpportunityMap.containsKey(paymentTerm.Opportunity__c)) {
                terms = paymentTermsWithOpportunityMap.get(paymentTerm.Opportunity__c);
            } else {
                terms = new List<Payment_Term__c>();
            }
            terms.add(paymentTerm);
            paymentTermsWithOpportunityMap.put(paymentTerm.Opportunity__c, terms);
        }

        AKFD_Extra_Charges_Tax_Percentage__c akfdExtraChargesTaxPercentage = AKFD_Extra_Charges_Tax_Percentage__c.getOrgDefaults();
        Decimal extraChargesTaxPercentage = akfdExtraChargesTaxPercentage.Tax_Percentage__c;
        
        Map<Id, Decimal> invoiceProductBaseAdditionalCharges = new Map<Id, Decimal>();
        Map<Id, Decimal> invoiceBasicPaymentAmount = new Map<Id, Decimal>();
        Map<Id, Boolean> isInvoicePriceChanged = new Map<Id, Boolean>();
        Map<Id, ProformaInvoice__c> proformaInvoicesOfInvoices = new Map<Id, ProformaInvoice__c>();
        Decimal productAdditionalCharge;
        Decimal productTradeDiscountValue;
        Decimal productTaxableAmount;
        Decimal totalTaxableAmount;
        Decimal deltaChangePrice;
        Decimal basicPaymentAMount;
        Decimal proformInvoiceItemAmount;
        Decimal invoiceProductAmount;
        Decimal packingPercentage;
        Decimal totalTax;
        List<Alloted_Stock__c> allotedStocks;
        Map<Id, Product2> allMasterProducts;
        Map<Id, Proforma_Invoice_Line_Item__c> updatedPILineItemsWithInvProductIds = new Map<Id, Proforma_Invoice_Line_Item__c>();
        Map<Id, Invoiced_Product__c> invoicedProducts = new Map<Id, Invoiced_Product__c>();
        Decimal packingCharge;
        
        //Loop to calculate the total invoice amount. Validate and calculate updated payment term amount, calculate additional payment term amount.
        for(Invoice__c invoice : reviewedInvoices.values()) {
            productAdditionalCharge = 0.00;
            totalTaxableAmount = 0.00;
            totalTax = 0.00;
            packingPercentage = 0.00;
            packingCharge = 0.00;
            //Traverse every invoice item to find updates.
            for(Invoiced_Items__c item : invoiceWithLineItems.get(invoice.Id)) {
                productTradeDiscountValue = 0.00;
                productTaxableAmount = 0.00;
                if(invoiceItemsWithProducts.get(item.Id) != null) {
                    //Traverse every invoiced product to get the changes in price or quantity.
                    for(Invoiced_Product__c invoicedProduct : invoiceItemsWithProducts.get(item.Id)) {
                        if(!proformaInvoicesOfInvoices.containsKey(item.Id)) {
                            proformaInvoicesOfInvoices.put(item.Id, invoicedProduct.Proforma_Invoice_Line_Item__r.ProformaInvoice__r);
                        }
                        
                        if(!invoiceBasicPaymentAmount.containsKey(item.Id)) {
                            invoiceBasicPaymentAmount.put(item.Id, invoicedProduct.Proforma_Invoice_Line_Item__r.ProformaInvoice__r.Total_Including_Taxes__c);
                        }
                        //Check if invoice is of export sales process or of project/retail.
                        //Code to update basicPaymentAmount
                        //Subtract total price according to PI and Add Total Price as per price on invoice product
                        if(invoice.RecordTypeId == exportInvoiceRecordType) {
                            if(invoicedProduct.Unit_Price__c != invoicedProduct.Proforma_Invoice_Line_Item__r.Price__c) {
                                basicPaymentAMount = invoiceBasicPaymentAmount.get(item.Id);
                                proformInvoiceItemAmount = invoicedProduct.Quantity__c * invoicedProduct.Proforma_Invoice_Line_Item__r.Price__c;
                                basicPaymentAMount -= proformInvoiceItemAmount;
                                invoiceProductAmount = invoicedProduct.Quantity__c * invoicedProduct.Unit_Price__c;
                                basicPaymentAMount += invoiceProductAmount;
                                invoiceBasicPaymentAmount.put(item.Id, basicPaymentAMount);
                                isInvoicePriceChanged.put(item.Id, true);
                            }
                            productTaxableAmount = invoicedProduct.Total_Price__c;
                            totalTaxableAmount += productTaxableAmount; 
                        } else {
                            if(invoicedProduct.Unit_Price__c != invoicedProduct.Proforma_Invoice_Line_Item__r.Price__c || invoicedProduct.Proforma_Invoice_Line_Item__r.GST__C != invoicedProduct.Product__r.GST__c) {
                                basicPaymentAMount = invoiceBasicPaymentAmount.get(item.Id);
                                proformInvoiceItemAmount = invoicedProduct.Quantity__c * invoicedProduct.Proforma_Invoice_Line_Item__r.Price__c;
                                proformInvoiceItemAmount -= proformInvoiceItemAmount * invoicedProduct.PO_Product__r.Discount_Percentage__c / 100;
                                proformInvoiceItemAmount += proformInvoiceItemAmount * invoicedProduct.Proforma_Invoice_Line_Item__r.GST__C / 100;
                                basicPaymentAMount -= proformInvoiceItemAmount;
                                invoiceProductAmount = invoicedProduct.Quantity__c * invoicedProduct.Unit_Price__c;
                                invoiceProductAmount -= invoiceProductAmount * invoicedProduct.PO_Product__r.Discount_Percentage__c / 100;
                                invoiceProductAmount += invoiceProductAmount * invoicedProduct.Product__r.GST__c / 100;
                                basicPaymentAMount += invoiceProductAmount;
                                invoiceBasicPaymentAmount.put(item.Id, basicPaymentAMount);
                                isInvoicePriceChanged.put(item.Id, true);
                            }
                            productTradeDiscountValue = invoicedProduct.Total_Price__c * invoicedProduct.PO_Product__r.Discount_Percentage__c / 100;
                            productTaxableAmount = invoicedProduct.Total_Price__c - productTradeDiscountValue;
                            totalTaxableAmount += productTaxableAmount; 
                        }
						totalTax += invoicedProduct.Tax_Amount__c;
                    }
                }
                packingPercentage = item.Invoice__r.Packing_Percentage__c;
            }
            packingCharge = totalTaxableAmount * packingPercentage / 100;
            if(extraChargesTaxPercentage == null || extraChargesTaxPercentage == 0.00) {
                extraChargesTaxPercentage = 1.00;
            }
            //invoice.Total_Product_Taxable_Value__c = totalTaxableAmount;
            //invoice.Total_Prdouct_GST__c = totalTax;
            productAdditionalCharge += packingCharge + packingCharge * extraChargesTaxPercentage / 100;
            invoiceProductBaseAdditionalCharges.put(invoice.Id, productAdditionalCharge);
        }
        
        allMasterProducts = new Map<Id, Product2>(getProductList(invoiceItemsWithProducts));
        allotedStocks = new List<Alloted_Stock__c>();
        Map<Id, ProformaInvoice__c> proformaInvoicesWithIds = new Map<Id, ProformaInvoice__c>(proformaInvoicesOfInvoices);
        allotedStocks = AllotedStocksSelector.getInstance().selectByPIProduct(proformaInvoicesWithIds.values(), allMasterProducts.keySet());
        List<Payment_Term__c> additionalPaymentTermsToBeInserted = new List<Payment_Term__c>();
        Decimal additionalAmount;
        Decimal productLiabilityValue;
        Payment_Term__c additionalPaymentTerm;
        Boolean isPriceChange;
        List<Payment_Term__c> paymentTermsToBeRevised = new List<Payment_Term__c>();
        List<Invoiced_Items__c> invoicedItemsWithPriceChanged = new List<Invoiced_Items__c>();
        List<ProformaInvoice__c> profomaInvoicesToBeUpdated = new List<ProformaInvoice__c>();
        List<Invoiced_Product__c> invoicedProductsToBeUpdated = new List<Invoiced_Product__c>();
        ProformaInvoice__c proformaInvoice;
        List<Messaging.SingleEmailMessage> warehouseEmailNotifications = new List<Messaging.SingleEmailMessage>();
        //Traverse every reviewed invoice to assign the additional payment term amount.
        for(Invoice__c invoice : reviewedInvoices.values()) {
            additionalAmount = invoice.Additional_Payment_Amount__c;
            /*if(invoice.RecordTypeId == exportInvoiceRecordType) {
                productLiabilityValue = invoice.Total_Amount__c * invoice.Product_Liability_Insurance_Percentage__c /100;
                additionalAmount += invoice.Packing_Charges__c; 
                additionalAmount += invoice.Shipping_Charges__c;
                additionalAmount -= invoice.Trade_Discount__c;
                additionalAmount -= productLiabilityValue;
            } else {
                additionalAmount += invoice.Freight_Amount__c + invoice.Freight_Amount__c * extraChargesTaxPercentage / 100;
                additionalAmount += invoiceProductBaseAdditionalCharges.get(invoice.Id);
            }*/
            
            /* Update Additional Payment Term Amount on Invoice - For Furthur use */
            //invoice.Additional_Payment_Term_Amount__c = additionalAmount;
            if(additionalAmount > 0) {
                additionalPaymentTerm = new Payment_Term__c(Name = 'Additional Payment Term (' + invoice.Name + ')', RecordTypeId = additionalPaymentTermRecordType, Opportunity__c = invoice.Opportunity__c);
                additionalPaymentTerm.Dummy_Payment_Amount__c = additionalAmount;
                additionalPaymentTerm.Dummy_Balance_Amount_v2__c = additionalAmount;
                additionalPaymentTerm.CurrencyIsoCode = invoice.CurrencyIsoCode;
                additionalPaymentTerm.Invoice__c = invoice.Id;
                additionalPaymentTermsToBeInserted.add(additionalPaymentTerm);
                
            } else if(additionalAmount < 0) {
                for(Invoiced_Items__c item : invoiceWithLineItems.get(invoice.Id)) {
                    if(item.Opportunity__c == invoice.Opportunity__c) {
                        paymentTermsToBeRevised.addAll(revisePaymentTerms(paymentTermsWithOpportunityMap.get(item.Opportunity__c), invoiceBasicPaymentAmount.get(item.Id) + additionalAmount));
                        proformaInvoice = proformaInvoicesOfInvoices.get(item.Id);
                        proformaInvoice.Total_Including_Taxes__c = invoiceBasicPaymentAmount.get(item.Id) + additionalAmount;
                        profomaInvoicesToBeUpdated.add(proformaInvoice);
                    }
                }
            }
        }
        
        //Traverse every invoiced item to get and update the updated invoiced product.
        for(Invoiced_Items__c item : invoicedItems.values()) {
            
            if(isInvoicePriceChanged.get(item.Id) != null && isInvoicePriceChanged.get(item.Id)) {
                invoicedItemsWithPriceChanged.add(item);
            }
			for(Invoiced_Product__c invoicedProduct : invoiceItemsWithProducts.get(item.Id)) {
                if(invoicedProduct.Quantity__c > invoicedProduct.Proforma_Invoice_Line_Item__r.Balanced_Quantity__c) {
                    reviewedInvoices.get(invoicedItems.get(item.Id).Invoice__c).addError('Invoiced quantity of a product should not be greater than balance quantity.');  
                }
                
                invoicedProduct.Proforma_Invoice_Line_Item__r.Invoiced_Quantity__c += invoicedProduct.Quantity__c;
                updatedPILineItemsWithInvProductIds.put(invoicedProduct.Id, invoicedProduct.Proforma_Invoice_Line_Item__r);
                if(invoicedProduct.Proforma_Invoice_Line_Item__r.GST__C != invoicedProduct.Product__r.GST__c) {
                    invoicedProduct.GST__c = invoicedProduct.Product__r.GST__c;
                    invoicedProductsToBeUpdated.add(invoicedProduct);
                }
            }
            
            invoicedProducts.putAll(invoiceItemsWithProducts.get(item.Id));
        }
        
        //Traverse all the invoice items with proced updated to revise the payment terms.
        for(Invoiced_Items__c item : invoicedItemsWithPriceChanged) {
            paymentTermsToBeRevised.addAll(revisePaymentTerms(paymentTermsWithOpportunityMap.get(item.Opportunity__c), invoiceBasicPaymentAmount.get(item.Id)));
            proformaInvoice = proformaInvoicesOfInvoices.get(item.Id);
            proformaInvoice.Total_Including_Taxes__c = invoiceBasicPaymentAmount.get(item.Id);
            profomaInvoicesToBeUpdated.add(proformaInvoice);
        }
        uow.registerDirty(updatedPILineItemsWithInvProductIds.values());
        uow.registerDirty(invoicedProductsToBeUpdated);
        List<Alloted_Stock__c> allotedStockToBeUpdated = new List<Alloted_Stock__c>();
        Map<String, List<Alloted_Stock__c>> consumedStocksOfAnInvoice = new Map<String, List<Alloted_Stock__c>>();
        updateWarehouse(updatedPILineItemsWithInvProductIds.values(), invoicedProducts.values(), uow ,allotedStocks, invoicedItems, reviewedInvoices, consumedStocksOfAnInvoice);
        if(additionalPaymentTermsToBeInserted.size() > 0) {
            uow.registerNew(additionalPaymentTermsToBeInserted);
        }
        system.debug('consumedStocksOfAnInvoice set ' + consumedStocksOfAnInvoice.keySet());
        uow.registerDirty(paymentTermsToBeRevised);
        uow.registerDirty(profomaInvoicesToBeUpdated);
        uow.commitWork();
        if(consumedStocksOfAnInvoice.size() > 0) { sendEmailNotification(consumedStocksOfAnInvoice); }
        
    }
        
    /* 
     * Method - onBeforeDelete
     * Description - Method to execute when trigger is of type Before and delete
	*/
    public override void onBeforeDelete() {
        for(Invoice__c invoice : (List<Invoice__c>) records) {
            if(invoice.Status__c == 'Reviewed') {
                invoice.addError('Reviewed Invoice Can\'t be delete.');
            }
        }
    }
    
    /*
     * Method - revisePaymentTerms
     * Params - List of payment terms, basic payment amount
     * Return - List of payment terms with revised amount
     * Description - Method to revise the basic payment terms with the basic payment amount
	*/
    private List<Payment_Term__c> revisePaymentTerms(List<Payment_Term__c> paymentTerms, Decimal basicPaymentAmount) {
        Decimal paymentTermAmount;
        List<Payment_Term__c> paymentTermsToBeUpdated = new List<Payment_Term__c>();
        for(Payment_Term__c paymentTerm : paymentTerms) {
            paymentTermAmount = basicPaymentAmount;
            paymentTermAmount *= paymentTerm.Payment_Percentage__c / 100;
            paymentTerm.Dummy_Payment_Amount__c = paymentTermAmount;
            paymentTermsToBeUpdated.add(paymentTerm);
        }
        return paymentTermsToBeUpdated;
    }
    
    /*
    * Method updateWarehosue
    * Return PageReference
    * Param - List<Proforma_Invoice_Line_Item__c>
    * Param - List<Invoiced_Product__c>
    * Param - fflib_SObjectUnitOfWork
    * Method to updated warehouse's current stock, allotedstock
    */
    private void updateWarehouse(List<Proforma_Invoice_Line_Item__c> updatedPILineItems, List<Invoiced_Product__c> invProducts, fflib_SObjectUnitOfWork uow, List<Alloted_Stock__c> allotedStocks, Map<Id, Invoiced_Items__c> invoicedItems, Map<Id, Invoice__c> reviewedInvoices, Map<String, List<Alloted_Stock__c>> consumedStocksOfAnInvoice) {
        List<Id> proformaInvoiceIds = new List<Id>();
        List<Id> proformaInvoiceProductIds = new List<Id>();
        Map<Id,Id> pILineItemsProducts = new Map<Id,Id>();
        Map<Id, Proforma_Invoice_Line_Item__c> pILineItems = new Map<Id,Proforma_Invoice_Line_Item__c>();
        Map<Id, Decimal> allotedProductQuantities = new Map<Id, Decimal>();
        Map<Id, List<Alloted_Stock__c>> allotedProducts = new Map<Id, List<Alloted_Stock__c>>();
        Map<Id, Integer> remainingAllotedProducts = new Map<Id, Integer>();
        List<Id> warehouseIds = new List<Id>();
        List<Alloted_Stock__c> tempAllotedStock;
        /*
        * Interation to get total alloted stock for proforma invoice line item product
        */
         System.debug('allotedStocks Before Loop ' + allotedStocks);
        for(Proforma_Invoice_Line_Item__c pILineItem : updatedPILineItems) {
            tempAllotedStock = new List<Alloted_Stock__c>();
            
            for(Alloted_Stock__c allotedStock : allotedStocks) {
                if(allotedStock.ProformaInvoice__c == pILineItem.ProformaInvoice__c && allotedStock.Product__c == pILineItem.Product__c) {
                    tempAllotedStock.add(allotedStock);
                    if(allotedProductQuantities.containsKey(pILineItem.Id)) {
                        allotedProductQuantities.put(pILineItem.Id, allotedProductQuantities.get(pILineItem.Id) + allotedStock.Stock__c);
                    } else {
                        allotedProductQuantities.put(pILineItem.Id, allotedStock.Stock__c);
                    }
                }
                warehouseIds.add(allotedStock.Warehouse_Product__c);
            }
            allotedProducts.put(pILineItem.Id, tempAllotedStock);
            System.debug('Alloted Products After Loop ' + allotedProducts);
        }
        System.debug('allotedProductQuantities After Loop ' + allotedProductQuantities);
        boolean error = false;
        for(Invoiced_Product__c invoicedProduct : invProducts) {
            system.debug('allotedProductQuantities ' + allotedProductQuantities);
            System.debug('invoicedProduct.Quantity__c' + invoicedProduct.Quantity__c);
            if(allotedProductQuantities.get(invoicedProduct.Proforma_Invoice_Line_Item__c) == null || invoicedProduct.Quantity__c > allotedProductQuantities.get(invoicedProduct.Proforma_Invoice_Line_Item__c)) {
                error = true;
                reviewedInvoices.get(invoicedItems.get(invoicedProduct.Invoiced_Item__c).Invoice__c).addError('Product quantity should not be greater than booked quantity.');
            }
        }
        Map<Id, Invoiced_Product__c> invoicedProducts = new Map<Id, Invoiced_Product__c>();
        Map<Id, Warehouse_Line_Item__c> warehouses = new Map<Id, Warehouse_Line_Item__c>((List<Warehouse_Line_Item__c>) WarehouseLineItemsSelector.getInstance().selectSObjectsById(new Set<Id>(warehouseIds)));
        List<Alloted_Stock__c> recordsToBeDeleted = new List<Alloted_Stock__c>();
        List<Alloted_Stock__c> recordsToBeUpdated = new List<Alloted_Stock__c>();
        Integer updatedStock;
        Integer remainingQuantity;
        List<Alloted_Stock__c> consumedStock;
        /*
        * Iteration to update warehouse's current stock and booked stock for respective product and proforma invoice
        */
       	String invoiceName;
        for(Invoiced_Product__c invoicedProduct : invProducts) {
            remainingQuantity = invoicedProduct.Quantity__c.intValue();
            invoiceName = invoicedProduct.Invoiced_Item__r.Invoice__c;
            for(Alloted_Stock__c allotedStock : allotedProducts.get(invoicedProduct.Proforma_Invoice_Line_Item__c)) {
                system.debug('allotedStock ' + allotedStock);
                if(remainingQuantity >= allotedStock.Stock__c.intValue()) {
                    remainingQuantity -= allotedStock.Stock__c.intValue();
                    warehouses.get(allotedStock.Warehouse_Product__c).Current_Stock__c -= allotedStock.Stock__c;
                    recordsToBeDeleted.add(allotedStock);
                    if(consumedStocksOfAnInvoice.containsKey(invoiceName)) {
                        consumedStock = consumedStocksOfAnInvoice.get(invoiceName);
                    } else {
                        consumedStock = new List<Alloted_Stock__c>();
                    }
                    consumedStock.add(allotedStock);
                    consumedStocksOfAnInvoice.put(invoiceName, consumedStock);
                } else {
                    warehouses.get(allotedStock.Warehouse_Product__c).Current_Stock__c -= remainingQuantity;
                    allotedStock.Stock__c -= remainingQuantity;
                    recordsToBeUpdated.add(allotedStock);
                    allotedStock.Stock__c = remainingQuantity;
                    if(consumedStocksOfAnInvoice.containsKey(invoiceName)) {
                        consumedStock = consumedStocksOfAnInvoice.get(invoiceName);
                    } else {
                        consumedStock = new List<Alloted_Stock__c>();
                    }
                    consumedStock.add(allotedStock);
                    consumedStocksOfAnInvoice.put(invoiceName, consumedStock);
                    break;
                }
            }
        }
        system.debug('consumedStocksOfAnInvoice ' + consumedStocksOfAnInvoice);
        List<Warehouse_Line_Item__c> warehousesToBeUpdated = new List<Warehouse_Line_Item__c>();
        for(Id warehouseId : warehouses.keySet()) {
            warehousesToBeUpdated.add(warehouses.get(warehouseId));
        }
        ApexTriggerCheck.getInstance().isExecutinngFromApexCode = true;
        uow.registerDirty(recordsToBeUpdated);
        uow.registerDeleted(recordsToBeDeleted);
        uow.registerDirty(warehousesToBeUpdated);
    }
    
    /*
     * Method - getProductList
     * Params - Map of invoice items with list of invoiced products
     * Return - List of Products
     * Description - Get master product list from the invoice products
	*/
    private List<Product2> getProductList(Map<Id, List<Invoiced_Product__c>> invoiceItemsWithProducts){
        List<Product2> productsList = new List<Product2>();
        for(Id invoiceItemId : invoiceItemsWithProducts.keySet()){
            for(Invoiced_Product__c invoicedProduct : invoiceItemsWithProducts.get(invoiceItemId)){
                productsList.add(invoicedProduct.Product__r); 
            }
        }
        return productsList;
    }
    
    private void validateContacts() {
        List<Id> accountIds = new List<Id>();
        List<AccountContactRelation> accountcontacts;
        Map<Id, Set<Id>> accountsContacts = new Map<Id, Set<Id>>();
        for(Invoice__c invoice : (List<Invoice__c>) records) {
            if(invoice.Account__c != null) {
                accountIds.add(invoice.Account__c);
            }
        }
        if(accountIds.size() > 0) {
            accountcontacts = AccountContactRelationshipsSelector.getInstance().selectByAccountId(new Set<Id>(accountIds));
        }
        if(accountcontacts != null && accountcontacts.size() > 0) {
            for(AccountContactRelation accountContact : accountcontacts) {
                if(!accountsContacts.containsKey(accountContact.AccountId)) {
                    accountsContacts.put(accountContact.AccountId, new Set<Id>{accountContact.ContactId});
                } else {
                    Set<Id> existingContacts = accountsContacts.get(accountContact.AccountId);
                    existingContacts.add(accountContact.ContactId);
                    accountsContacts.put(accountContact.AccountId, existingContacts);
                }
            }
        }
        
        for(Invoice__c invoice : (List<Invoice__c>) records) {
            if(invoice.Account__c != null) {
                Set<Id> existingContacts = accountsContacts.get(invoice.Account__c);
                if(existingContacts != null && existingContacts.size() > 0) {
                    if(invoice.Contact_Person__c != null && !existingContacts.contains(invoice.Contact_Person__c)) {
                        invoice.Contact_Person__c.addError('This contact is not related to this account');
                    } else if(invoice.Contact__c != null && !existingContacts.contains(invoice.Contact__c)) {
                        invoice.Contact__c.addError('This contact is not related to this account');
                    } else if(invoice.Notify_Contact__c != null && !existingContacts.contains(invoice.Notify_Contact__c)) {
                        invoice.Notify_Contact__c.addError('This contact is not related to this account');
                    } else if(invoice.Buyer_Contact__c != null && !existingContacts.contains(invoice.Buyer_Contact__c)) {
                        invoice.Buyer_Contact__c.addError('This contact is not related to this account');
                    }
                } else {
                    //un-comment if deepak sir identify this issue
                    invoice.Contact_Person__c.addError('This contact is not related to this account');
                }
            }
        }
    }
    
    public void sendEmailNotification(Map<String, List<Alloted_Stock__c>> consumedStocksOfAnInvoice) {
        AKFD_Configuration__c appConfig = AKFD_Configuration__c.getOrgDefaults(); 
        Set<String> toAddresses = new Set<String>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail;
        if(appConfig.AKF_Process_Admin_Email__c != null) {
            toAddresses.add(appConfig.AKF_Process_Admin_Email__c);
        }
        toAddresses.add('akshit.bhandari@metacube.com');
        toAddresses.add('deepak.sharma@metacube.com');
        //Setting default Email Address
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'supportteam@akfdstudio.com'];
        String subject;
        String body;
        String opportunityOwnerEmail = '';
        for(Invoice__c invoice : (List<Invoice__c>)records) {
            if(consumedStocksOfAnInvoice.get(invoice.Id) != null) {
                system.debug('invoice ' + invoice.Id);
                mail = new Messaging.SingleEmailMessage();
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                subject = invoice.Name + ': Consumed Product Stock from Warehouse';
                body = 'Dear User,<br/>' + '<br/>Invoice has been Created in Salesforce, following products stock has been reduced from the warehouse(s).<br/>You should Immediately reduce the product stock in Tally from the warehouse.' 
                    + ' <br/><br/>Here are the details:<br/><br/>'
                    + '<table style="border-collapse: collapse;" width="100%" border="1">' 
                    + '<thead><tr>'
                    + '<th style="padding: 8px; text-align: left;" align="left" width="15%">Invoice</th>'
                    + '<th style="padding: 8px; text-align: left;" align="left" width="30%">Product</th>'
                    + '<th style="padding: 8px; text-align: left;" align="left" width="20%">SKU</th>'
                    + '<th style="padding: 8px; text-align: left;" align="left" width="20%">Warehouse</th>'
                    + '<th style="padding: 8px; text-align: left;" align="left" width="15%">Stock</th>'
                    + '</tr></thead>'
                    + '<tbody>';
                system.debug('consumedStocksOfAnInvoice email ' + consumedStocksOfAnInvoice.keySet());
                system.debug('consumedStocksOfAnInvoice .get(invoice.Id) ' + consumedStocksOfAnInvoice.get(invoice.Id));
                for(Alloted_Stock__c allotedStock : consumedStocksOfAnInvoice.get(invoice.Id)) {
                    body += '<tr>';
                    body += '<td style="padding: 8px; text-align: left;" width="15%">' + invoice.Name + '</td>';
                    body += '<td style="padding: 8px; text-align: left;" width="30%">' + allotedStock.Product__r.Name + '</td>';
                    body += '<td style="padding: 8px; text-align: left;" width="20%">' + allotedStock.Product__r.SKU_Number__c + '</td>';
                    body += '<td style="padding: 8px; text-align: left;" width="20%">' + allotedStock.Warehouse__r.Name + '</td>';
                    body += '<td style="padding: 8px; text-align: left;" width="15%">' + allotedStock.Stock__c + '</td>';
                    body += '</tr>';
                }
                body += '</tbody></table> <br/>';
                body += '<br/><p>This is an automatically generated mail. Do not reply.</p>'; 
                body += '<br/>Thanks,'; 
                body += '<br/>AKFD';
                mail.setSubject(subject);
                //Set Email To Email parameters 
                mail.setToAddresses(new List<String>(toAddresses));   
                mail.setHtmlBody(body);
                mails.add(mail);
            }
        }
        if (!mails.isEmpty()) {
            Messaging.sendEmail(mails);
        }
    }
    
}