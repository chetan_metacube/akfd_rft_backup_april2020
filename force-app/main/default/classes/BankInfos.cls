public class BankInfos extends fflib_SObjectDomain {
	public BankInfos(List<Bank_Info__c> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new BankInfos(sObjectList);
        }
    }
    
    public override void onBeforeInsert() {
        Map<Id, Map<String,Bank_Info__c>> accountsBanksInfoMap = getAccountsBanksInfoMap((List<Bank_Info__c>) Records);
        
        for(Bank_Info__c bankInfo : (List<Bank_Info__c>) Records) {
            if(accountsBanksInfoMap.containsKey(bankInfo.Account__c)) {
                Map<String,Bank_Info__c> bankInfoMap = accountsBanksInfoMap.get(bankInfo.Account__c);
                if(bankInfoMap.containsKey(bankInfo.CurrencyIsoCode)) {
                    bankInfo.CurrencyIsoCode.adderror('Duplicate Bank Info, please enter Bank Info in another currency.');
                }
            }
        }
    }
    
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        Map<Id, Map<String,Bank_Info__c>> accountsBanksInfoMap = getAccountsBanksInfoMap((List<Bank_Info__c>) Records);
        for(Bank_Info__c bankInfo : (List<Bank_Info__c>) Records) {
            if(accountsBanksInfoMap.containsKey(bankInfo.Account__c)) {
                Map<String,Bank_Info__c> bankInfoMap = accountsBanksInfoMap.get(bankInfo.Account__c);
                if(bankInfoMap.containsKey(bankInfo.CurrencyIsoCode)) {
                    Bank_Info__c oldbankInfor = (Bank_Info__c)existingRecords.get(bankInfo.Id);
                    if(oldbankInfor.CurrencyIsoCode != bankInfo.CurrencyIsoCode){
                        bankInfo.CurrencyIsoCode.adderror('Duplicate Bank Info, please enter Bank Info in another currency.');
                    }
                }
            }
        }
    }
    
    private Map<Id, Map<String,Bank_Info__c>> getAccountsBanksInfoMap(List<Bank_Info__c> Records) {
        List<Id> accountIds = new List<Id>();
        for(Bank_Info__c bankInfo : Records){
            accountIds.add(bankInfo.Account__c);
        }
        List<Bank_Info__c> allBanksInfo = new List<Bank_Info__c>([SELECT Id,Name,CurrencyISOCode,Account__c FROM Bank_Info__c WHERE Account__c= :accountIds]);
        Map<Id, Map<String,Bank_Info__c>> accountsBanksInfoMap = new Map<Id, Map<String,Bank_Info__c>>();
        Map<String,Bank_Info__c> bankInfosMap;
        for(Bank_Info__c bankInfo : allBanksInfo) {
            if(accountsBanksInfoMap.containsKey(bankInfo.Account__c)) {
            	bankInfosMap = accountsBanksInfoMap.get(bankInfo.Account__c);
                if(!bankInfosMap.containsKey(bankInfo.CurrencyIsoCode)) {
                    bankInfosMap.put(bankInfo.CurrencyIsoCode, bankInfo);
                }
            } else {
                bankInfosMap = new Map<String,Bank_Info__c>();
                bankInfosMap.put(bankInfo.CurrencyIsoCode, bankInfo);
                accountsBanksInfoMap.put(bankInfo.Account__c, bankInfosMap);
            }
            
        }
        return accountsBanksInfoMap;
    }
}