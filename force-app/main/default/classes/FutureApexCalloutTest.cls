@isTest
public class FutureApexCalloutTest {
    @isTest static void testCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        String apiType = 'ACCOUNT';
        String entityName = 'Jaypore E-Commerce Pvt. Ltd. (SOR)';
        String xmlBody = '<methodCall><params><param><value><string>salesforce</string></value></param><param><value><int>1</int></value></param><param><value><string>1</string></value></param><param><value><string>bi.tally.integration.ledgers</string></value></param><param><value><string>create_ledger</string></value></param><param><value><value>Jaypore E-Commerce Pvt. Ltd. (SOR)</value><value>Online Debtors</value><value>Jaypore E-Commerce Pvt. Ltd.</value><value>{"address4":" ","address3":" ","address2":"PHASE - I,","address1":"B-64, OKHLA INDUSTRIAL AREA, 12365JHGF SITAPURA"}</value><value>India</value><value>Delhi</value><value>110020</value><value>Divye  Kaushik</value><value> </value><value> </value><value>09818559999</value><value>divye.kaushik@jaypore.com</value><value>Yes</value><value> </value><value>No</value><value>Regular</value><value>AACCJ7868E</value><value>07AACCJ7868E1ZB</value></value></param></params><methodName>execute</methodName></methodCall>';
        String actualValue;
        
        Test.startTest();
        // Verify response received contains fake values
        ApexTriggerCheck.getInstance().isFutureCalling = true;
        FutureApexCallout.productCalloutApex(apiType, entityName, xmlBody);
        Test.stopTest();
    }
    
}