public class InvoiceAPICallout {
    @InvocableMethod
    public static void insertInvoice(List<InvoiceInput> invoiceInputs) {
        system.debug('invoiceInputs ' + invoiceInputs);
        if(invoiceInputs.size() == 1) {
            Set<Id> itemIds = new Set<Id>();
            for(Invoiced_Items__c item : invoiceInputs[0].invoicedItems) {
                itemIds.add(item.Id);
            }
            List<String> invoicedProductsApiNames = new List<String>{'Product__r.Name', 
                'Product__r.Narrative__c', 'Product__r.Description__c', 'Quantity__c', 'Product__r.HSN_Tax__r.Name', 'PO_Product__r.Discount_Percentage__c',
                'Product__r.Unit__c', 'Unit_Price__c', 'Total_Price__c', 'GST__c', 'Product__r.Stock_Group__c' };
                    
            String query = 'SELECT ';
            for(String apiName : invoicedProductsApiNames) {
                query += apiName + ', ';
            }  
            String whereIds = '(';
            for(Id itemId : itemIds) {
                whereIds += '\'' + itemId + '\', ';
            } 
            whereIds = whereIds.removeEnd(', ');
            whereIds += ')';
            query = query.removeEnd(', ');
            query += ' FROM Invoiced_Product__c Where Invoiced_Item__c IN ' + whereIds;
            system.debug('query ' + query);
            List<Invoiced_Product__c> invoicedProducts = Database.query(query);
            //merge the below 2 queries
            Account merchantAccount = [Select Id, Name, BillingState From Account Where Id = :invoiceInputs[0].mOpportunity.Merchant__c];
            ProformaInvoice__c proformaInvoice = [Select Id, Name, Status__c, Date__c From ProformaInvoice__c Where Opportunity__c = :invoiceInputs[0].mOpportunity.Id And Status__c = 'Accepted'];
            String invoiceXml;
            Boolean isExportInvoice = 'Export'.equals(invoiceInputs[0].recordTypeName);
            if(isExportInvoice) {
                invoiceXml = MyXmlPageController.getExportInvoiceXml(invoiceInputs[0].invoice, invoiceInputs[0].billingAddress, invoiceInputs[0].deliveryAddress, invoicedProducts, invoiceInputs[0].partyAccount, merchantAccount, invoiceInputs[0].mOpportunity, invoiceInputs[0].recordTypeName, proformaInvoice);
            } else {
                invoiceXml = MyXmlPageController.getInvoiceXml(invoiceInputs[0].invoice, invoiceInputs[0].billingAddress, invoiceInputs[0].deliveryAddress, invoicedProducts, invoiceInputs[0].partyAccount, merchantAccount, invoiceInputs[0].mOpportunity, invoiceInputs[0].recordTypeName, proformaInvoice);
            } 
            System.debug('invoiceXml ' + invoiceXml);
            if(String.isNotBlank(invoiceXml)) {
                Futureapexcallout.productCalloutApex(isExportInvoice ? String.valueOf(RequestBodyXML.APIType.EXPORT_INVOICE) : String.valueOf(RequestBodyXML.APIType.INVOICE) , invoiceInputs[0].invoice.Name, invoiceXml);
            }
        }
    }
    
    public class InvoiceInput {
        
        @InvocableVariable
        public Invoice__c invoice;
        
        @InvocableVariable
        public List<Invoiced_Items__c> invoicedItems;
        
        @InvocableVariable
        public Address__c billingAddress;
        
        @InvocableVariable
        public Address__c deliveryAddress;
        
        @InvocableVariable
        public Opportunity mOpportunity;    
        
        @InvocableVariable
        public Account partyAccount;  
        
        @InvocableVariable
        public String recordTypeName;   
    }
}