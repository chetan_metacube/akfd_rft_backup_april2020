public interface IPricebookProductsSelector extends fflib_ISObjectSelector {
    
    //Opportunity getOpportunityWithByIdFieldSet(Id oppportunityId, Set<String> fieldSet);
    List<Pricebook_Product__c> selectPricebooksWithPricebookEntriesByAccountAndProductIds(Set<Id> accountIds, Set<Id> productIds);
}