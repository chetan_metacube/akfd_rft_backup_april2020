public class LeadsService {
    private static ILeadsService instance;
    // Method to get the lead record
    public static List<Lead> getLeadsById(Set<Id> ids) {
        
        return service().getLeadsById(ids);
        
    }
    // Method to upsert the lead record
    public static List<Lead> upsertLeads(List<Lead> leads) {
        
        return service().upsertLeads(leads);
        
    }
    // Method to convert lead to opportunity
    public static List<Opportunity> convertLeadToOpportunity(List<LeadsService.LeadConvertWrapper> leadData) {
        
        return service().convertLeadToOpportunity(leadData);
       
    }
    
    private static ILeadsService service() {
        
        if(instance == null) {
            instance = (ILeadsService) Application.service().newInstance(ILeadsService.class);
        }
        
        return instance;
        
    }
    
    public class LeadConvertWrapper {
        public Lead leadRecord;
        public String opportunityName;
        
        public LeadConvertWrapper(Lead leadRecord, String opportunityName) {
            this.leadRecord = leadRecord;
            this.opportunityName = opportunityName;
        }
    }
}