@isTest
public class DefaultPaymentTermTriggerTest {
    @testSetup
    static void testSetup() {
        List<Default_Payment_Term__c> terms = new List<Default_Payment_Term__c>();
        Default_Payment_Term__c term;
        for(Integer i = 0; i < 4; i++) {
            term = new Default_Payment_Term__c(Name = 'Installment ' + i, Payment_Percentage__c = 25.00);
            terms.add(term);
        }
        insert terms;
    }
    
    static testMethod void testBeforeInsert() {
        Default_Payment_Term__c term = new Default_Payment_Term__c(Name = 'Installment 1', Payment_Percentage__c = 25.00);
        try{
            insert term;
        } catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Total Payment Percentage should not be greater than 100%. Available(0.00)') ? true : false; 
            System.AssertEquals(expectedExceptionThrown, true);
        } 
    }
    
    static testMethod void testBeforeUpdate() {
        Default_Payment_Term__c term = [Select Id, Name, Payment_Percentage__c From Default_Payment_Term__c Limit 1];
        term.Payment_Percentage__c = 30.00;
        try{
            update term;
        } catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Total Payment Percentage should not be greater than 100%. Available(25.00)') ? true : false; 
            System.AssertEquals(expectedExceptionThrown, true);
        } 
    }
}