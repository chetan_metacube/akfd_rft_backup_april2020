public class InvoicedProductsSelector extends fflib_SObjectSelector implements IInvoicedProductsSelector {
    private static IInvoicedProductsSelector instance = null;
    
    public static IInvoicedProductsSelector getInstance() {
        
        if(instance == null) {
            instance = (IInvoicedProductsSelector)Application.selector().newInstance(Invoiced_Product__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            Invoiced_Product__c.Id,
                Invoiced_Product__c.Name,
                Invoiced_Product__c.Invoiced_Item__c,
                Invoiced_Product__c.Total_Price__c,
                Invoiced_Product__c.Proforma_Invoice__c,
                Invoiced_Product__c.Unit_Price__c,
                Invoiced_Product__c.Last_Unit_Price__c,
                Invoiced_Product__c.Quantity__c, 
                Invoiced_Product__c.Tax_Amount__c,
                Invoiced_Product__c.Product__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return Invoiced_Product__c.sObjectType;
    }
    
    public List<Invoiced_Product__c> selectInvoiceProductById() {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('Proforma_Invoice_Line_Item__r.ProformaInvoice__c');
        query.setLimit( Limits.getLimitQueryRows() );
        
        return (List<Invoiced_Product__c>) Database.query(
            query.toSOQL()
        );
    }
}