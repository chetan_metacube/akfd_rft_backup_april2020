public class AccountContactRelationshipsSelector extends fflib_SObjectSelector implements IAccountContactRelationshipsSelector{
	private static IAccountContactRelationshipsSelector instance = null;
    
    public static IAccountContactRelationshipsSelector getInstance() {
        
        if(instance == null) {
            instance = (IAccountContactRelationshipsSelector)Application.selector().newInstance(AccountContactRelation.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            AccountContactRelation.Id,
                AccountContactRelation.ContactId,
                AccountContactRelation.AccountId
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return AccountContactRelation.sObjectType;
    }
    
    public List<AccountContactRelation> selectById(Set<ID> idSet) {
        return (List<AccountContactRelation>) selectSObjectsById(idSet);
    }
    
    public List<AccountContactRelation> selectByAccountId(Set<Id> accountIds) {
        fflib_QueryFactory query = newQueryFactory();
        query.selectField('Contact.RecordType.Name');
        query.selectField('Contact.Name');
        query.selectField('Contact.Email');
        query.selectField('Contact.Department__c');
        query.selectField('Contact.RecordTypeId');
        
        query.setCondition('AccountId IN :accountIds');
        query.setLimit( Limits.getLimitQueryRows() );
        
        return (List<AccountContactRelation>) Database.query(
            query.toSOQL()
        );
    }
}