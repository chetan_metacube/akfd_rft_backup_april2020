public class AllotedStocksSelector extends fflib_SObjectSelector implements IAllotedStocksSelector {
    private static IAllotedStocksSelector instance = null;
    
    public static IAllotedStocksSelector getInstance() {
        
        if(instance == null) {
            instance = (IAllotedStocksSelector)Application.selector().newInstance(Alloted_Stock__c.sObjectType);
        }
        
        return instance;
    }
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> { 
            Alloted_Stock__c.Id,
                Alloted_Stock__c.Name,
                Alloted_Stock__c.Warehouse__c, 
                Alloted_Stock__c.Product__c, 
                Alloted_Stock__c.ProformaInvoice__c,
                Alloted_Stock__c.Warehouse_Product__c,
                Alloted_Stock__c.Stock__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return Alloted_Stock__c.sObjectType;
    }
    
    public List<Alloted_Stock__c> selectByPIProduct(List<ProformaInvoice__c> proformaInvoices, Set<Id> productIds) {
        fflib_QueryFactory query = newQueryFactory();
        
        query.selectField('Product__r.Name');
        query.selectField('Product__r.SKU_Number__c');
        query.selectField('Warehouse__r.Name');
        
        query.setCondition('ProformaInvoice__c IN :proformaInvoices AND Product__c IN :productIds');
        
        return (List<Alloted_Stock__c>) Database.query( query.toSOQL() );
    }
     
    
}