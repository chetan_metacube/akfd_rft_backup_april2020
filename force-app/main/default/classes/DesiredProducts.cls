public class DesiredProducts extends fflib_SObjectDomain implements IDesiredProducts, ProformaInvoicesService.ISupportProformaInvoicing, QuotationsService.ISupportQuotation {
    public DesiredProducts(List<PO_Product__c> records) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(records);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records) {
            return new DesiredProducts(records);
        }
    } 
    
    // Method to generate Quotation Record
    public void generateQuotations(QuotationsService.QuotationFactory quotationFactory, QuotationsService.DesiredProductsWithPriceBooksWrapper desiredProductwithPriceBooks) {
        for(QuotationsService.SelectedOpportunityProductsWrapper opportunityWrapper : desiredProductwithPriceBooks.opportunitySpecificRecords) {
            String productsWithNoImage = '';
            Opportunity opp = opportunityWrapper.OpportunityRecord;
            QuotationsService.Quotation quotation = new QuotationsService.Quotation();
            quotation.Opportunity = opportunityWrapper.OpportunityId;
            quotation.RecordTypeId = Schema.SObjectType.Quotation__c.getRecordTypeInfosByName().get(opp.RecordType.Name).getRecordTypeId();
            quotation.EffectiveStartDate = Date.newInstance(Date.today().year(), Date.today().month(),Date.today().day());
            quotation.CurrencyIsoCode = opp.CurrencyIsoCode;
            
            List<QuotationsService.QuotationLineItem> quotationLineItemWrapperList = new List<QuotationsService.QuotationLineItem>();
            for(QuotationsService.DesiredProduct selectedProduct : opportunityWrapper.SelectedProducts) {
                PriceBook_Product_Entry__c priceBookEntry = desiredProductwithPriceBooks.pricebooksEntries.get(selectedProduct.ProductId);
                QuotationsService.QuotationLineItem quotationLineItemWrapper = new QuotationsService.QuotationLineItem();
                quotationLineItemWrapper.ProductId = selectedProduct.ProductId;
                quotationLineItemWrapper.POProductId = selectedProduct.Id;
                quotationLineItemWrapper.RecordTypeId = Schema.SObjectType.Quotation_Line_Item__c.getRecordTypeInfosByName().get(opp.RecordType.Name).getRecordTypeId();
                quotationLineItemWrapper.GST = selectedProduct.ProductGST;          
                quotationLineItemWrapper.CurrencyIsoCode = opp.CurrencyIsoCode;
                if(opp.RecordType.Name != 'Retail') {
                    quotationLineItemWrapper.UnitPrice = priceBookEntry.Price__c;
                    quotationLineItemWrapper.PriceBookProductId = desiredProductwithPriceBooks.priceBooks.get(selectedProduct.ProductId).Id;
                    quotationLineItemWrapper.Unit = priceBookEntry.Unit_of_Price__c;
                  
                } else {
                   
                    quotationLineItemWrapper.UnitPrice = selectedProduct.RetailPrice;
                    quotationLineItemWrapper.Unit = selectedProduct.Unit;
                     
                }

                quotationLineItemWrapper.Quantity = selectedProduct.Quantity;
                quotationLineItemWrapperList.add(quotationLineItemWrapper);
                if(selectedProduct.ProductImage == null) {
                    if(String.isEmpty(productsWithNoImage)) { 
                        productsWithNoImage += selectedProduct.ProductName;
                    } else {
                        productsWithNoImage += ', ' + selectedProduct.ProductName;
                    }
                }
                
            }
            quotation.Items.addAll(quotationLineItemWrapperList);
    		quotationFactory.add(quotation);
       }
    }
    
    public void generateProformaInvoices(ProformaInvoicesService.ProformaInvoiceFactory proformaInvoiceFactory, ProformaInvoicesService.DesiredProductsWithPriceBooksWrapper desiredProductsWithPriceBooksWrapper) {
        Map<Id, PO_Product__c> recordsMap = new Map<Id, PO_Product__c>((List<PO_Product__c>) records);
        ProformaInvoicesService.ProformaInvoice proformaInvoice;
        ProformaInvoicesService.ProformaInvoiceLineItem proformaInvoiceLineItem;
        String recordTypeName = '';
        Map<Id, PriceBook_Product__c> priceBooks = desiredProductsWithPriceBooksWrapper.pricebooks;
        Map<Id, PriceBook_Product_Entry__c> pricebooksEntries = desiredProductsWithPriceBooksWrapper.pricebooksEntries;
        Map<Id, Map<Id, Id>> customersProductSpecificPriceBooks = desiredProductsWithPriceBooksWrapper.customersProductSpecificPriceBooks;
       
       
        /* Utilise InvoiceFactory to create invoices from Proforma Invoice details*/
        for(ProformaInvoicesService.DesiredProductsIdsWrapper desiredProductsIdsWrapper : desiredProductsWithPriceBooksWrapper.opportunitySpecificRecords) {
            //DesiredProductsService.OpportunitySpecificDesiredProducts opportunitySpecificDesiredProducts = proformaInvoiceWrapper.OpportunitySpecificDesiredProducts;
            proformaInvoice = new ProformaInvoicesService.ProformaInvoice();
            proformaInvoice.Opportunity = desiredProductsIdsWrapper.Opportunity.Id;
            recordTypeName = desiredProductsIdsWrapper.Opportunity.RecordType.Name;
            if(recordTypeName == 'Retail') {
                proformaInvoice.ConversionRate = 1.0;
                proformaInvoice.CurrencyIsoCode = desiredProductsIdsWrapper.CurrencyIsoCode;
            } else {
                proformaInvoice.ConversionRate = desiredProductsIdsWrapper.ConversionRate;
                proformaInvoice.CurrencyIsoCode = desiredProductsIdsWrapper.CurrencyIsoCode;
            }
            proformaInvoice.CurrentDate = Date.today();
            
            proformaInvoice.RecordTypeId = Schema.SObjectType.ProformaInvoice__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
            proformaInvoice.Items = new List<ProformaInvoicesService.ProformaInvoiceLineItem>();
            for(Id desiredProductId : desiredProductsIdsWrapper.desiredProducts) {
                proformaInvoiceLineItem = new ProformaInvoicesService.ProformaInvoiceLineItem();
                proformaInvoiceLineItem.CurrencyIsoCode =  proformaInvoice.CurrencyIsoCode;
                proformaInvoiceLineItem.DesiredProduct = desiredProductId;
                proformaInvoiceLineItem.GST = recordsMap.get(desiredProductId).Product__r.GST__c;
                proformaInvoiceLineItem.Product = recordsMap.get(desiredProductId).Product__c;
                proformaInvoiceLineItem.RecordTypeId = Schema.SObjectType.Proforma_Invoice_Line_Item__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
                proformaInvoiceLineItem.RequiredQuantity = recordsMap.get(desiredProductId).Quantity__c;
                
                 
                if(recordTypeName == 'Retail'){
                    proformaInvoiceLineItem.Unit = recordsMap.get(desiredProductId).Unit__c;
                    proformaInvoiceLineItem.StockTillDate = recordsMap.get(desiredProductId).Lead_Time_In_Days__c;
                    proformaInvoiceLineItem.Price = recordsMap.get(desiredProductId).Retail_Price__c;
                    System.debug('Unit Price = ' + proformaInvoiceLineItem.Price);
                } else { 
                    Id priceBookId = customersProductSpecificPriceBooks.get(recordsMap.get(desiredProductId).Opportunity__r.AccountId).get(recordsMap.get(desiredProductId).Product__c);
                    proformaInvoiceLineItem.Unit = priceBooks.get(priceBookId).Unit__c;//From PriceBook entries
                    proformaInvoiceLineItem.StockTillDate = pricebooksEntries.get(priceBookId).Lead_Time_for_MOQ__c;//From PriceBook entries
                    proformaInvoiceLineItem.Price = pricebooksEntries.get(priceBookId).Price__c * desiredProductsIdsWrapper.ConversionRate;   //From PriceBook entries
                    proformaInvoiceLineItem.SKUNumber = priceBooks.get(priceBookId).Client_SKU__c;  // FROM Pricebook
                }
                
                
                proformaInvoice.Items.add(proformaInvoiceLineItem);
            }
            proformaInvoiceFactory.add(proformaInvoice);
        }
        
    }
    
    
    
}