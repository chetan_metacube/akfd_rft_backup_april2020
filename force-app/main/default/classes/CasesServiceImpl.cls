/**
 * Used in CasesService
 * Class contains implementation of casesService
 * author: Chetan Sharma (15 Feb 2019)
 * Last Modified By: Chetan Sharma (17 June 2019)
 */
public class CasesServiceImpl implements ICasesService { 
    static final String CASE_DOESNOT_EXISTS = 'Case doesn\'t exist already';
    static final String SOMETHING_WENT_WRONG = 'Something went wrong!!!';
    
    /** 
    * Method return caseRecords 
    * @param caseIds. 
    * @return Case Records, for valid case ids. 
    */
    public List<Case> getCasesById(Set<Id> ids) {
        
        return CasesSelector.getInstance().selectCasesById(ids);
        
    }
    
     /** 
    * Method contains case conversion logic, returns lead Records generated from case conversion.
    * @param CaseConvertWrapper. 
    * @return Lead Record, for valid case convert wrapper data. 
    */
    public List<Lead> convertToLead(List<CasesService.CaseConvertWrapper> casesWithLeadData) { 
        List<Lead> leads = new List<Lead>();
        List<Case> casesToBeConverted = new List<Case>();
        Case caseTobeConverted;
        Lead newLead;
        //Set lead data
        for(CasesService.CaseConvertWrapper caseWrapper : casesWithLeadData) {
            caseTobeConverted = caseWrapper.caseRecord; 
            newLead =  new Lead( Title =  caseWrapper.leadTitle,
                                Lead_Source__c = caseWrapper.leadSource,
                                Account__c = caseTobeConverted.AccountId,
                                Company = caseTobeConverted.Account.Name,
                                //Set Contact details
                                Contact__c = caseTobeConverted.ContactId,
                                Salutation = caseTobeConverted.Contact.Salutation,
                                FirstName = caseTobeConverted.Contact.FirstName,
                                MiddleName = caseTobeConverted.Contact.MiddleName,
                                LastName = caseTobeConverted.Contact.LastName,
                                Suffix = caseTobeConverted.Contact.Suffix,
                                //Set other details on lead
                                Status = 'New',
                                Email = caseTobeConverted.Contact.Email,
                                OwnerId = caseTobeConverted.OwnerId,
                                Sales_Process__c = caseTobeConverted.Sales_Process__c,
                                Lead_Receipt_Date__c = Date.today());
            leads.add(newLead);
            caseTobeConverted.Status = 'Converted';
            casesToBeConverted.add(caseTobeConverted);
        }
        // Insert Lead and update case status as converted.
        try {
            fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
            uow.registerNew(leads);
            uow.registerDirty(casesToBeConverted);
            uow.commitWork();
        } catch(fflib_SObjectUnitOfWork.UnitOfWorkException ex){
            System.debug('Es ' + ex);
            if(ex.getMessage() == 'New records cannot be registered as dirty') {
                throw new AKFDException(CASE_DOESNOT_EXISTS);
            } else {
                throw new AKFDException(SOMETHING_WENT_WRONG);
            }
        } catch (Exception e) {
            throw new AKFDException(SOMETHING_WENT_WRONG);
        }
        
        return leads;
    }
   
}