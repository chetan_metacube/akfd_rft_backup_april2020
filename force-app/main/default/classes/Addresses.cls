public class Addresses extends fflib_SObjectDomain {
    static final String SOMETHING_WENT_WRONG = 'Something went wrong!!!';
    
    public Addresses(List<Address__c> sObjectList) {
        // Domain classes are initialised with lists to enforce bulkification throughout
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Addresses(sObjectList);
        }
    }
    
    
    public override void onBeforeInsert() {
        updateStateProvince();
    }
    
    
    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        updateStateProvince();
    }
    
    
    public override void onBeforeDelete() {
        // Creating accountIds set  
        Set<Id> addressRelatedAccountIds = new Set<Id>();
        for(Address__c address : (List<Address__c>)records) {
            addressRelatedAccountIds.add(address.Account__c);
        }
        // Query accounts related to address records and update accout fields accordingly
        Map<Id, Account> relatedAccouts = new Map<Id, Account>([SELECT Id, hasDeliveryAddress__c, hasBillingAddress__c FROM Account WHERE Id IN :addressRelatedAccountIds]);
        for(Address__c address : (List<Address__c>)records) {
            if(address.Address_Type__c == 'Billing') {
                relatedAccouts.get(address.Account__c).hasBillingAddress__c = false;
            } else if(address.Address_Type__c == 'Delivery') {
                relatedAccouts.get(address.Account__c).hasDeliveryAddress__c =  false;
            }
        }
        // Updating address related accounts
        try{
            fflib_ISObjectUnitOfWork uow = Application.unitOfWork.newInstance();
            uow.registerDirty(relatedAccouts.values());
            uow.commitWork();
        } catch(fflib_SObjectUnitOfWork.UnitOfWorkException ex){
            throw new AKFDException(SOMETHING_WENT_WRONG);
        } catch (Exception e) {
            throw new AKFDException(SOMETHING_WENT_WRONG);
        }
    }
    
    private void updateStateProvince() {
        // Updating address state/province
        for(Address__c addressObj : (List<Address__c>)records) {
            if(addressObj.State__c != null) {
                if(addressObj.State__c == 'Others'){
                    addressObj.State_Province__c = addressObj.State_Other_than_India__c;
                } else {
                    addressObj.State_Province__c = addressObj.State__c;
                }
            }
        }
    }
    
}