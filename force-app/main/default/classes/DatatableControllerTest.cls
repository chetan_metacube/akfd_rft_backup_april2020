@isTest
public class DatatableControllerTest {
    
    @testSetup static void setup() {
        MockData.createUsers('System Administrator', 1);
        MockData.createUsers('Read Only', 1);
        List<Account> accounts = MockData.createAccounts('Customer', 5);
    }
    
    @isTest static void positiveTestCase_For_getRecordsMethod() {
        DatatableController.DatatableDTO datatableDTO =  new DatatableController.DatatableDTO();
        datatableDTO.recordsPerPage = 5;
        datatableDTO.filter = ' RecordType.Name = \'Customer\' ';
        datatableDTO.objectApiName = 'Account';
        datatableDTO.objectFields = new List<String> {'Id', 'Name'};
            datatableDTO.currentOffset = 0;
        String result = DatatableController.getRecords(JSON.serialize(datatableDTO));
        System.assert(result.length() > 0);
        /*DatatableController.ResultWrapper resultWrapper = (DatatableController.ResultWrapper)JSON.deserialize(result, DatatableController.ResultWrapper.class);
        System.assert(resultWrapper.isResultFound);
        System.assertEquals(resultWrapper.records.size(), 5);
        System.assertEquals(resultWrapper.responseMessage, 'Result Found');
        System.assertEquals(resultWrapper.totalRecordCount, 5);*/
    }
    
    @isTest static void negativeTestCase_For_getRecordsMethod_WhenParameterPassedWasNull() { 
        try {
            String result = DatatableController.getRecords(null);
        } catch (Exception error) {
            System.debug('Error' + error.getMessage());
            System.assert(error.getMessage() != null);
        }
    }
    
    @isTest static void negativeTestCase_For_getRecordsMethod_WhenInvalidParameterPassed() {
        DatatableController.DatatableDTO datatableDTO =  new DatatableController.DatatableDTO();
        datatableDTO.recordsPerPage = 5;
        datatableDTO.filter = ' RecordType.Name = \'Customer\' ';
        datatableDTO.objectApiName = 'InvalidAccount';
        datatableDTO.objectFields = new List<String> {'Id', 'Name'};
            datatableDTO.currentOffset = 0;
        try {
            String result = DatatableController.getRecords(JSON.serialize(datatableDTO));
        } catch (Exception error) {
            System.debug('Error' + error.getMessage());
            System.assert(error.getMessage() != null);
        }
        
    }
    
}