/**
* Class contains ProformaInvoice related Component's logics
* @author : Chetan Sharma
* Last edited by : Chetan Sharma
*/
public class ProformaInvoiceController {
    /**
    * Method to get Opportunity Record by opportunityId
    * @param opportunityId
    * @return Opportunity Record
    */
    @AuraEnabled
    public static Opportunity getOpportunityRecordById(Id opportunityId) {
        if(opportunityId != null && opportunityId.getSobjectType() == Opportunity.sObjectType) {
            Opportunity opportunity = OpportunitiesSelector.getInstance().selectOpportunityWithPIsById(opportunityId);
            System.debug('Opportunity PIC ' + opportunity);
            if(opportunity.ProformaInvoices__r != null && opportunity.ProformaInvoices__r.size() > 0) {
                AuraHandledException auraException = new AuraHandledException('Accepted/Revised PI already exists.');
                auraException.setMessage('Accepted/Revised PI already exists.');
                throw auraException;
            }
            return opportunity; 
        } else {
            AuraHandledException auraException = new AuraHandledException('Invalid Opportunity Id.');
            auraException.setMessage('Invalid Opportunity Id.');
            throw auraException;
        }
    }
    
    /**
    * Method to get currencyIsoCode picklist
    * @return CurrencyIsoCodes JSON String 
    */
    @AuraEnabled
    public static String getCurrencyIsoCodes() {
        Map<String, String> pickListValues = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.CurrencyIsoCode.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        List<PicklistWrapper> picklistWrapper = new List<PicklistWrapper>(); 
        for( Schema.PicklistEntry picklistVal : picklistEntries ) {
            pickListValues.put(pickListVal.getLabel(), pickListVal.getValue());
            picklistWrapper.add(new PicklistWrapper(pickListVal.getLabel(), pickListVal.getValue()));
        } 
        return JSON.serialize(picklistWrapper);
    }
    
    /**
    * Method to generate proforma invoices
    * @params : opportunity, desiredProducts, conversion rate, currencyIsoCode
    * @return proformaInvoiceId  
    */
    @AuraEnabled
    public static Id generateProformaInvoice(Opportunity opportunity, List<PO_Product__c> desiredProducts, Double ConversionRate, String CurrencyIsoCode) {
        // Validate against valid values of parameters.
        if(Opportunity != null && desiredProducts != null && desiredProducts.size() > 0 && CurrencyIsoCode != null && ConversionRate != null && ConversionRate > 0) {
            // Validate quantities.
            UserRecordAccess userAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :opportunity.Id];
            if(userAccess != null && !userAccess.HasEditAccess) {
                //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Insufficient Privileges.'));
                throw getExceptionInstance('Insufficient Privileges.');
            }
            
            for(PO_Product__c desiredProduct : desiredProducts) {
                if( desiredProduct.Quantity__c < 0 ) {
                    throw getExceptionInstance('Invalid quantity entered. Quantity should be greater than zero.');
                    //throw new AuraHandledException('Invalid quantity entered. Quantity should be greater than zero.');
                }
            }
            // Set desired products data.
            Map<Id, PO_Product__c> queriedDesiredProducts =  new Map<Id, PO_Product__c>(DesiredProductsSelector.getInstance().selectDesiredProductsByIds( (new Map<Id, PO_Product__c>(desiredProducts)).keySet()));
            for(PO_Product__c desiredProduct : desiredProducts){
                queriedDesiredProducts.get(desiredProduct.Id).Quantity__c = desiredProduct.Quantity__c;
            }
            desiredProducts = queriedDesiredProducts.values();
            List<ProformaInvoicesService.ProformaInvoicesWrapper> proformaInvoiceWrappers = new List<ProformaInvoicesService.ProformaInvoicesWrapper>();
            proformaInvoiceWrappers.add(new ProformaInvoicesService.ProformaInvoicesWrapper(opportunity, desiredProducts, ConversionRate, CurrencyIsoCode));
            try {
                return ProformaInvoicesService.generateProformaInvoice(proformaInvoiceWrappers).get(0);
            } catch (AKFDException e) {
                System.debug('Exception message ' + e.getMessage());
                String errorMessage = (String)((Map<String, Object>)JSON.deserializeUntyped(e.getMessage())).get(Opportunity.Id);
                throw getExceptionInstance(errorMessage);
            }
        } else {
            throw getExceptionInstance('Something Went Wrong!!!');
        }
        
    }
    
    private static AuraHandledException getExceptionInstance(String message) {
        AuraHandledException exceptionInstance = new AuraHandledException(message);
        exceptionInstance.setMessage(message);
        return exceptionInstance;
    }
    
    // Inner class for handling picklist wrapper
    public class PicklistWrapper {
        public String label {get;set;}
        public String value {get;set;}
         
        public PicklistWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    
}