@RestResource(urlMapping='/api/productStock/')
global class ImportStockWebService {
    @HttpPost
    global static String updateProductWarehouse() {
        RestRequest request = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        
        
        WarehouseProductWrapper wrapper;
        String response;
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Warehouse_Line_Item__c.SObjectType,
                    API_Log__c.SObjectType,
                    Attachment.SObjectType
                    }
        );
        ApexTriggerCheck.getInstance().isExecutingFromAPIRequest = true;
        UpsertUnitOfWorkWarehouseLineItemHelper upsertWarehouseLineItemWork = new UpsertUnitOfWorkWarehouseLineItemHelper();
        uow.registerWork(upsertWarehouseLineItemWork);
        
        try {
            wrapper = (WarehouseProductWrapper)JSON.deserialize(request.requestBody.toString(), WarehouseProductWrapper.class);
            if(String.isBlank(wrapper.ProductName)){
                throw new StringException('Product Name should not be empty!');
            }
            
            if(String.isBlank(wrapper.ProductSKU)) {
                throw new StringException('Product SKU should not be empty!');
            } 
            
            if(wrapper.Stock == null) {
                throw new StringException('Stock should not be empty!');
            } else if(wrapper.Stock < 0) {
                throw new StringException('Stock should be positive!');
            }
            
            if(String.isBlank(wrapper.Warehouse)) {
                throw new StringException('Warehouse Name should not be empty!');
            }
            
            Product2 product = [Select Id, Name, SKU_Number__c, (Select Id, Warehouse__r.Name, Current_Stock__c From Warehouse_Products__r), (Select Id, Warehouse__r.Name, Stock__c From Alloted_Stocks__r) From Product2 Where Name =:wrapper.ProductName And SKU_Number__c = :wrapper.ProductSKU];
            system.debug('');
            List<Warehouse__c> warehouse = [Select Id, Name From Warehouse__c Where Name = :wrapper.Warehouse];
            if(warehouse.isEmpty()) {
                throw new StringException('Warehouse with specified name doesn\'t exist.');
            }
            
            Id warehouseId = warehouse[0].Id;
            
            List<Warehouse_Line_Item__c> warehouseProducts;
            List<Alloted_Stock__c> allotedStocks;
            Decimal totalBookedStock = 0.00;
            
            if(!product.Alloted_Stocks__r.isEmpty()) {
                allotedStocks = product.Alloted_Stocks__r;
                for(Alloted_Stock__c allotedStock : allotedStocks) {
                    if(wrapper.Warehouse.equals(allotedStock.Warehouse__r.Name)) {
                        totalBookedStock += allotedStock.Stock__c;
                    }
                }
            }
            
            if(wrapper.Stock < totalBookedStock) {
                throw new StringException('Imported stock cannot be below booked stock.');
            }
            
            Warehouse_Line_Item__c invetoryToBeUpserted;
            
            if(!product.Warehouse_Products__r.isEmpty()) {
                warehouseProducts = product.Warehouse_Products__r;
                for(Warehouse_Line_Item__c warehouseProduct : warehouseProducts) {
                    if(warehouseId == warehouseProduct.Warehouse__c) {
                        invetoryToBeUpserted = warehouseProduct;
                        invetoryToBeUpserted.Current_Stock__c = wrapper.Stock;
                    }
                }
            }
            
            if(invetoryToBeUpserted == null) {
                invetoryToBeUpserted = new Warehouse_Line_Item__c();
                invetoryToBeUpserted.Product__c = product.Id;
                invetoryToBeUpserted.Warehouse__c = warehouseId;
                invetoryToBeUpserted.Current_Stock__c = wrapper.Stock;
            }

            system.debug('invetoryToBeUpserted ' + invetoryToBeUpserted);
            upsertWarehouseLineItemWork.registerProductStockUpsert(new List<Warehouse_Line_Item__c> {invetoryToBeUpserted});
            response = 'Success!';
            
            EmailManager.sendEmail('UAT ImportStockWebService Success!', response + '<br/><br/>' + wrapper, new List<String>{ 'deepak.sharma@metacube.com'});
        } catch(QueryException queryException) {
            response = 'Product with specified combination of name and sku doesn\'t exist.';
            system.debug('response ' + response);
            EmailManager.sendEmail('UAT ImportStockWebService Error!', 'Error Message: \'' + response + '\'' + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
        } catch(StringException stringException) {
            
            response = stringException.getMessage();
            system.debug('response ' + response);
            EmailManager.sendEmail('UAT ImportStockWebService Error!', response + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
            
        } catch (Exception e) {
            
            response = 'Something went wrong, API request failed!';
            system.debug('response ' + response);
            EmailManager.sendEmail('UAT ImportStockWebService Error!', response + '<br/><br/>' + wrapper, new List<String>{'deepak.sharma@metacube.com'});
        }
        
        Id callinsRecordTypeId = Schema.SObjectType.API_Log__c.getRecordTypeInfosByName().get('Callins').getRecordTypeId();
        API_Log__c apiLog = new API_Log__c();
        apiLog.Name = 'Import Warehouse Stock';
        apiLog.Status__c = response.equals('Success!') ? 'Success' : 'Failed';
        apiLog.Type__c = 'STOCK';
        apiLog.Message__c = response;
        apiLog.Request_Body__c = JSON.serialize(wrapper);
        apiLog.RecordTypeId = callinsRecordTypeId;
        system.debug('apiLog ' + apiLog);
        uow.registerNew(apiLog);
        
        Attachment requestBodyAttachment = new Attachment();
        requestBodyAttachment.Body = Blob.valueOf(JSON.serialize(wrapper));
        requestBodyAttachment.Name = 'Request Body.JSON';
        requestBodyAttachment.IsPrivate = false;
        system.debug('requestBodyAttachment ' + requestBodyAttachment);
        
        uow.registerNew(requestBodyAttachment, Attachment.ParentId, apiLog);
        
        Attachment responseBodyAttachment = new Attachment();
        responseBodyAttachment.Body = Blob.valueOf(JSON.serialize(response));
        responseBodyAttachment.Name = 'Response Body.JSON';
        responseBodyAttachment.IsPrivate = false;
        uow.registerNew(responseBodyAttachment, Attachment.ParentId, apiLog);
        system.debug('responseBodyAttachment ' + responseBodyAttachment);
        uow.commitWork();
        
        return response;
    }
    
    @TestVisible class WarehouseProductWrapper {
        public String ProductName;
        public String ProductSKU;
        public Decimal Stock;
        public String Warehouse;
    }
    
    /*
    * Unit of work upsert helper class - UpsertUnitOfWorkWarehouseLineItemHelper
    */
    public class UpsertUnitOfWorkWarehouseLineItemHelper implements fflib_SObjectUnitOfWork.IDoWork {       
        public Database.UpsertResult[] Results {get; private set;}
        
        private List<Warehouse_Line_Item__c> m_records;
        
        public UpsertUnitOfWorkWarehouseLineItemHelper() {  
            m_records = new List<Warehouse_Line_Item__c>();
        }
        
        public void registerProductStockUpsert(List<Warehouse_Line_Item__c> records) {
            m_records.addAll(records);
        }
        
        public void doWork() {
            Results = Database.upsert(m_records, false);                
        }
    }
}