@isTest
public class ExportAccountControllerTest {
    @TestSetup static void setup() {
        AKFD_Extra_Charges_Tax_Percentage__c akfdExtraChargesTaxPercentage = AKFD_Extra_Charges_Tax_Percentage__c.getOrgDefaults();
        akfdExtraChargesTaxPercentage.Tax_Percentage__c = 18.00;
        upsert akfdExtraChargesTaxPercentage;
        
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        
        List<Account> acc=new List<Account>();
        Account accountCustomerObj = new Account(Name='AccountTestCustomer', RecordTypeId = customerRecordTypeIdAccount, CurrencyIsoCode = 'USD');
        Account accountMerchantObj = new Account(Name='AccountTestMerchant', RecordTypeId = merchantcustomerRecordTypeIdAccount , BillingState = 'Rajasthan');
        acc.add(accountCustomerObj);
        acc.add(accountMerchantObj);
        insert acc;
        
        List<Address__c> addresses = new List<Address__c>();
        Address__c billingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Billing', Countries__c = 'India', State__c = 'Rajasthan');
        Address__c shippingAddress = new Address__c(Account__c =accountCustomerObj.Id, Address_Type__c = 'Delivery', Countries__c = 'India', State__c = 'Rajasthan');
        addresses.add(billingAddress);
        addresses.add(shippingAddress);
        insert addresses;
        
        
        Id agentContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent Contact').getRecordTypeId();
        Contact agentContactObj = new Contact(LastName='Test Contact Last Name', AccountId=accountCustomerObj.Id,RecordTypeId=agentContactRecordTypeId,Email='test@test.com');
        insert agentContactObj;
    }
    
    @isTest static void testExportAccountsCsv() {
        ExportAccountController controller = new ExportAccountController();
    }
}