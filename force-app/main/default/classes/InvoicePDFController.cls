public class InvoicePDFController {
    public Opportunity opportunityObj {get;set;}
    public Account customerAccount {get;set;}
    public Invoice__c invoice {get;set;}
    public List<Invoiced_Product__c> invoicedProducts {get;set;}
    public String currencySymbol {get;set;}
    
    public String gstTotalWord {get; private set;}
    public String gstTotalText {get; private set;}
    
    public String billingName {get; private set;}
    public String renderAs {get;set; }
    public List<String> bankingDetails {get; private set;}
    public Decimal packingCharges {get;set;}
    private String accessType {get;set;}
    private String docketNumber {get;set;}
    public Decimal total {get;set;}
    public Decimal grandTotal{get;set;}
     public Boolean showDiscountColumn{get;set;}
    
    //Export and Project
    public List<Payment_Term__c> paymentTerms{get;set;}
    public Account consignorAccount {get;set;}
    
    // Project and Retail
    public String grandTotalText{get; private set;}
    public Decimal totalSgst{get;set;}
    public Decimal totalCgst{get;set;}
    public Decimal totalIgst{get;set;}
    public Decimal freightAmountTaxValue{get;set;}
    public Decimal packingAmountTaxValue{get;set;}
    public Decimal extraChargesTaxPercentage{get;set;}
    public Boolean isStateTax{
        get{ 
            return invoice.Billing_Address__r.State_Province__c == opportunityObj.Merchant__r.BillingState;        
        }
        set;
    }
    
    // Export
    
    public List<String> bankingBeneficiaryDetail {get; private set;}
    //public Boolean showTradeDiscount{get;set;}
    public Boolean showLiabilityInsurance{get;set;}
    public Boolean isAgent{
        get{
            return (opportunityObj.Contact_Person__r.RecordType.Name == 'Agent Contact');
        }
        set;
    }
    
    public InvoicePDFController(ApexPages.StandardController controller) {
        invoice = (Invoice__c) controller.getRecord();
        invoice = [SELECT Name, RecordType.Name, Opportunity__c, Opportunity__r.AccountId, Opportunity__r.PO_NO__c, Opportunity__r.PO_Date__c, 
                   Advance_Payment__c, Country_Of_Final_Origin__c, Date__c, Delivery_Date__c, CurrencyIsoCode, Export_Under_Bond__c, 
                   Final_Destination__c, Freight__c, Mode_of_Payment__c, Name_of_Shipper__c, Net_Total__c, Packing_Charges__c, Packing_Percentage__c, 
                   Port_Of_Discharge__c, Place_Of_Receipt__c, Port_of_Loading__c, Pre_Carriage_By__c, Product_Liability_Insurance__c, Product_Liability_Insurance_Percentage__c, 
                   Ship_Date__c, Shipping_Charges__c, Total_Amount__c, Total_Quantity__c, Trade_Discount__c, Delivery_Remarks__c, Transportation_Mode__c, Freight_Amount__c, Total_Settled_Amount__c, 
                   Billing_Address__r.GST_Id__c, Billing_Address__r.Name, Billing_Address__r.Street__c, Billing_Address__r.City__c, Billing_Address__r.State_Province__c, Billing_Address__r.Country__c, Billing_Address__r.Zip_Postal_Code__c, Billing_Address__r.State_Code__c, 
                   Delivery_Address__r.Name, Delivery_Address__r.Street__c, Delivery_Address__r.City__c, Delivery_Address__r.State_Province__c, Delivery_Address__r.Country__c, Delivery_Address__r.Zip_Postal_Code__c, Delivery_Address__r.State_Code__c, 
                   Buyer_Address__r.Name, Buyer_Address__r.Street__c, Buyer_Address__r.City__c, Buyer_Address__r.State_Province__c, Buyer_Address__r.Country__c, Buyer_Address__r.Zip_Postal_Code__c, Buyer_Address__r.State_Code__c, Notify_Address__r.Name, Notify_Address__r.Street__c, 
                   Notify_Address__r.City__c, Notify_Address__r.State_Province__c, Notify_Address__r.Country__c, Notify_Address__r.Zip_Postal_Code__c, Notify_Address__r.State_Code__c, Contact_Person__r.Name, Contact_Person__r.Phone, 
                   Contact_Person__r.Email, Contact__r.Name, Contact__r.Phone, Contact__r.Email, Notify_Contact__r.Name, Notify_Contact__r.Phone, Notify_Contact__r.Email, Buyer_Contact__r.Name, Buyer_Contact__r.Phone, Buyer_Contact__r.Email, 
                   Opportunity__r.Merchant__r.Bank_Details__c, Net_Invoice_Amount__c FROM Invoice__c WHERE Id =: invoice.id];
        showDiscountColumn = false;
        showLiabilityInsurance = false;
        init();
        if (!ApexPages.hasMessages()) {
            renderAs = 'PDF';
        } else { 
            renderAs = '';
        }
     }
    
    /*
* Fetch values for the Invoice like, consignor, customer, opportunity etc,
*/
    private void init() {
        try {
            Set<String> opportunityFieldSet = new Set<String> { 'Id', 'Name', 'Bank_Code__c', 'CloseDate', 'AccountId', 'Testing_Charges_Included__c', 'Contact_Person__c', 'PO_No__c', 'PO_Date__c', 'Incoterms__c',
                'CurrencyIsoCode', 'Freight__c', 'Freight_Amount__c', 'SAC_Code__c', 'Billing_Address__c', 'Delivery_Address__c', 'Billing_Address__r.GST_Id__c', 
                'Billing_Address__r.Name', 'Billing_Address__r.Street__c', 'Billing_Address__r.City__c', 'Billing_Address__r.State_Province__c', 'Billing_Address__r.Country__c', 
                'Billing_Address__r.Zip_Postal_Code__c', 'Billing_Address__r.State_Code__c', 'Delivery_Address__r.Name', 'Delivery_Address__r.Street__c', 
                'Delivery_Address__r.City__c', 'Delivery_Address__r.State_Province__c', 'Delivery_Address__r.Country__c', 'Delivery_Address__r.Zip_Postal_Code__c', 
                'Delivery_Address__r.State_Code__c', 'Notify_Address__r.Name', 'Notify_Address__r.Street__c', 'Notify_Address__r.City__c', 
                'Notify_Address__r.State_Province__c', 'Notify_Address__r.Country__c', 'Notify_Address__r.Zip_Postal_Code__c', 
                'Notify_Address__r.State_Code__c', 'Buyer_Address__r.Name', 'Buyer_Address__r.Street__c', 'Buyer_Address__r.City__c', 
                'Buyer_Address__r.State_Province__c', 'Buyer_Address__r.Country__c', 'Buyer_Address__r.Zip_Postal_Code__c', 
                'Buyer_Address__r.State_Code__c', 'Vendor_Code__c', 'Packing_Percentage__c', 'Transportation_Mode__c', 
                'Merchant__r.Name', 'Merchant__r.Email__c', 'Merchant__r.ShippingStreet', 'Merchant__r.ShippingCity', 'Merchant__r.ShippingPostalCode', 'Merchant__r.ShippingState', 
                'Merchant__r.ShippingCountry', 'Merchant__r.BillingStreet', 'Merchant__r.BillingCity', 'Merchant__r.BillingPostalCode',
                'Merchant__r.BillingState', 'Merchant__r.BillingCountry', 'Merchant__r.Website', 'Merchant__r.Company_Logo__c', 'Merchant__r.Phone',
                'Merchant__r.GST_Id__c', 'Merchant__r.PAN_Number__c', 'Merchant__r.Exporter_s_IEC_Code__c', 'Merchant__r.Exporters_s_Ref__c', 
                'Merchant__r.CENTRAL_EXCISE__c', 'Merchant__r.Export_Under_UT_1__c', 'Merchant__r.CIN_No__c', 'Contact_Person__r.RecordType.Name', 
                'Contact_Person__r.Company_Name__c', 'Contact_Person__r.Company_Street__c', 'Contact_Person__r.Company_City__c', 
                'Contact_Person__r.Company_State__c', 'Contact_Person__r.Company_Country__c', 'Contact_Person__r.Company_Zip_Postal_Code__c', 
                'Contact_Person__r.Name', 'Contact_Person__r.MailingStreet', 'Contact_Person__r.MailingCity', 'Contact_Person__r.MailingPostalCode', 
                'Contact_Person__r.MailingState', 'Contact_Person__r.MailingCountry', 'Contact_Person__r.Phone', 'Contact_Person__r.Email', 
                'Contact__r.Name', 'Contact__r.Phone', 'Contact__r.Email', 'Notify_Contact__r.Name', 'Notify_Contact__r.Phone', 'Notify_Contact__r.Email', 
                'Buyer_Contact__r.Name', 'Buyer_Contact__r.Phone', 'Buyer_Contact__r.Email', 'Owner.Name', 'Owner.Email', 'Owner.Phone',
                'Text_If_Packing_Freight_Charges_Is_Zero__c'};
                    opportunityObj = [SELECT Id, Name, Bank_Code__c, CloseDate, AccountId, Testing_Charges_Included__c, Contact_Person__c, PO_No__c, PO_Date__c, Incoterms__c,
                                      CurrencyIsoCode, Freight__c, Freight_Amount__c, SAC_Code__c, Billing_Address__c, Delivery_Address__c, Billing_Address__r.GST_Id__c, 
                                      Billing_Address__r.Name, Billing_Address__r.Street__c, Billing_Address__r.City__c, Billing_Address__r.State_Province__c, Billing_Address__r.Country__c, 
                                      Billing_Address__r.Zip_Postal_Code__c, Billing_Address__r.State_Code__c, Delivery_Address__r.Name, Delivery_Address__r.Street__c, 
                                      Delivery_Address__r.City__c, Delivery_Address__r.State_Province__c, Delivery_Address__r.Country__c, Delivery_Address__r.Zip_Postal_Code__c, 
                                      Delivery_Address__r.State_Code__c, Notify_Address__r.Name, Notify_Address__r.Street__c, Notify_Address__r.City__c, 
                                      Notify_Address__r.State_Province__c, Notify_Address__r.Country__c, Notify_Address__r.Zip_Postal_Code__c, 
                                      Notify_Address__r.State_Code__c, Buyer_Address__r.Name, Buyer_Address__r.Street__c, Buyer_Address__r.City__c, 
                                      Buyer_Address__r.State_Province__c, Buyer_Address__r.Country__c, Buyer_Address__r.Zip_Postal_Code__c, 
                                      Buyer_Address__r.State_Code__c, Vendor_Code__c, Packing_Percentage__c, Transportation_Mode__c, 
                                      Merchant__r.Name, Merchant__r.Email__c, Merchant__r.ShippingStreet, Merchant__r.ShippingCity, Merchant__r.ShippingPostalCode, Merchant__r.ShippingState, 
                                      Merchant__r.ShippingCountry, Merchant__r.BillingStreet, Merchant__r.BillingCity, Merchant__r.BillingPostalCode,
                                      Merchant__r.BillingState, Merchant__r.BillingCountry, Merchant__r.Website, Merchant__r.Company_Logo__c, Merchant__r.Phone,
                                      Merchant__r.GST_Id__c, Merchant__r.PAN_Number__c, Merchant__r.Exporter_s_IEC_Code__c, Merchant__r.Exporters_s_Ref__c, 
                                      Merchant__r.CENTRAL_EXCISE__c, Merchant__r.Export_Under_UT_1__c, Merchant__r.CIN_No__c, Contact_Person__r.RecordType.Name, 
                                      Contact_Person__r.Company_Name__c, Contact_Person__r.Company_Street__c, Contact_Person__r.Company_City__c, 
                                      Contact_Person__r.Company_State__c, Contact_Person__r.Company_Country__c, Contact_Person__r.Company_Zip_Postal_Code__c, 
                                      Contact_Person__r.Name, Contact_Person__r.MailingStreet, Contact_Person__r.MailingCity, Contact_Person__r.MailingPostalCode, 
                                      Contact_Person__r.MailingState, Contact_Person__r.MailingCountry, Contact_Person__r.Phone, Contact_Person__r.Email, 
                                      Contact__r.Name, Contact__r.Phone, Contact__r.Email, Notify_Contact__r.Name, Notify_Contact__r.Phone, Notify_Contact__r.Email, 
                                      Buyer_Contact__r.Name, Buyer_Contact__r.Phone, Buyer_Contact__r.Email, Owner.Name, Owner.Email, Owner.Phone,
                                      Text_If_Packing_Freight_Charges_Is_Zero__c FROM Opportunity WHERE Id =: invoice.Opportunity__c];
            invoicedProducts = [SELECT Invoiced_Item__r.Opportunity__r.PO_No__c, Product__r.Name, GST__c, Product__r.Description__c, Product__r.Product_SKU__c,
                                Proforma_Invoice_Line_Item__r.SKU_Number__c, Proforma_Invoice_Line_Item__r.Unit__c, Product__r.Material__c, Product__r.Image__c,
                                PO_Product__c ,PO_Product__r.HST_Code__c, SKU_Number__c, PO_Product__r.Product_Location__c, PO_Product__r.Discount_Percentage__c, Product__r.HSN_Tax__r.Name,
                                Product__r.Finish_1__c, Product__r.Finish_2__c, Product__r.Finish_3__c, Product__r.Finish_4__c, Product__r.Dimension_Length__c, Product__r.Dimension_Width__c, Product__r.Dimension_Height__c,
                                Product__r.Dimension_Unit__c, Quantity__c, Unit_Price__c, Total_Price__c, Unit__c FROM Invoiced_Product__c WHERE Invoiced_Item__r.Invoice__c =: invoice.Id];
            System.debug('Invoic id  ' + invoice.Id);
            
            System.debug('Invoiced Products ' + invoicedProducts);
            System.debug('Invoiced Products size' + invoicedProducts.size());
            System.debug('Invoiced Products ' + invoicedProducts[0].PO_Product__c);
            System.debug('Invoiced Products discount PErcentage ' + invoicedProducts[0].PO_Product__r.Discount_Percentage__c);
            
            customerAccount = [SELECT Id, Name, Pre_Carriage_By__c, Port_of_Loading__c, Port_Of_Discharge__c, Final_Destination__c, Place_Of_Receipt__c, Country_Of_Final_Origin__c,
                               Mode_of_Payment__c, Email__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, ShippingCountry, Phone, BillingStreet, BillingCity, BillingPostalCode, 
                               BillingState, BillingCountry, GST_Id__c, Terms_and_Conditions__c, CurrencyIsoCode FROM Account WHERE Id =: invoice.Opportunity__r.AccountId];
           /* consignorAccount = [SELECT Id, Name, Email__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, ShippingCountry, BillingStreet, BillingCity, BillingPostalCode, BillingState, 
                                BillingCountry, Phone, GST_Id__c, CIN_No__c, Exporter_s_IEC_Code__c, Exporters_s_Ref__c, CENTRAL_EXCISE__c, Export_Under_UT_1__c FROM Account WHERE Id =: invoice.Opportunity__r.Merchant__c];*/
            billingName = customerAccount.Name;
            total = 0.00;
            Decimal totalGST = 0.00;
            totalSgst = 0.0;
            totalCgst = 0.0;
            totalIgst = 0.0;
            Decimal totalTaxableValue = 0.0;
            for(Invoiced_Product__c invoicedProduct : invoicedProducts) {
                if(invoicedProduct.Product__r.Image__c != null) {
                    invoicedProduct.Product__r.Image__c = invoicedProduct.Product__r.Image__c.replace('alt','height="50" width="50" alt');
                }
                if(invoicedProduct.Product__r.Material__c != null){
                    invoicedProduct.Product__r.Material__c = breakMultiSelect(invoicedProduct.Product__r.Material__c);
                }
                if(invoicedProduct.PO_Product__r.Discount_Percentage__c != 0.00){
                    showDiscountColumn = true;
                }
                System.debug('Total price ' + invoicedProduct.Total_Price__c);
                System.debug('Discount Percentage ' + invoicedProduct.PO_Product__r.Discount_Percentage__c);
                Decimal totalEffectivePrice = invoicedProduct.Total_Price__c - (invoicedProduct.Total_Price__c * invoicedProduct.PO_Product__r.Discount_Percentage__c) / 100;
                totalTaxableValue += totalEffectivePrice.setScale(2);
                if(invoice.RecordType.Name == 'Retail' || invoice.RecordType.Name == 'Project') {
                    if(isStateTax) {
                        Decimal sgst = (totalEffectivePrice * invoicedProduct.GST__c / 2) / 100;
                        System.debug('sgst' + sgst);
                        System.debug('totalSgst' + totalSgst);
                        totalSgst += sgst.setScale(2);
                        Decimal cgst = (totalEffectivePrice * invoicedProduct.GST__c / 2) / 100;
                        totalCgst += cgst.setScale(2);
                    } else {
                        Decimal igst = (totalEffectivePrice * invoicedProduct.GST__c) / 100;
                        totalIgst += igst.setScale(2);
                    }
                }
                
            }
            
            if(invoice.CurrencyIsoCode == 'INR'){
                currencySymbol = 'INR';
            } else {
                currencySymbol = CurrencyUtils.getSymbol(invoice.CurrencyIsoCode);
            }
            packingCharges = (invoice.Packing_Percentage__c * totalTaxableValue / 100).setScale(2);
            Bank_Info__c bankInfo = [SELECT Bank_Details__c FROM Bank_Info__c WHERE CurrencyIsoCode =: invoice.CurrencyIsoCode AND Account__c =: opportunityObj.Merchant__c LIMIT 1];
            if( bankInfo.Bank_Details__c != null) {
                bankInfo.Bank_Details__c = bankInfo.Bank_Details__c.replaceAll('<[^>]+>',' ');
                bankingDetails = bankInfo.Bank_Details__c.split(',');
            }
            accessType = ApexPages.currentPage().getParameters().get('accessType');
            docketNumber = ApexPages.currentPage().getParameters().get('docketNumber');
            
            if(invoice.RecordType.Name == 'Export') {
                //show Discount
                if(invoice.Trade_Discount__c != 0.00){
                    showDiscountColumn = true;
                }
                //show Liability insurance
                if(invoice.Product_Liability_Insurance_Percentage__c != 0.00){
                    showLiabilityInsurance = true;
                }
                
            }
            
            if(invoice.RecordType.Name == 'Export') {
                if(invoice.Opportunity__r.Merchant__r.Bank_Details__c != null){
                    bankingbeneficiaryDetail = invoice.Opportunity__r.Merchant__r.Bank_Details__c.split(',');
                }
            }
            
            if(invoice.RecordType.Name == 'Export' || invoice.RecordType.Name == 'Retail') {
                //if  notify contact is agent type then pick company details from agent's record
                if(opportunityObj.Contact_Person__r.RecordType.Name == 'Agent Contact') {
                    billingName = opportunityObj.Contact_Person__r.Company_Name__c;
                    customerAccount.BillingStreet = opportunityObj.Contact_Person__r.Company_Street__c;
                    customerAccount.BillingCity = opportunityObj.Contact_Person__r.Company_City__c;
                    customerAccount.BillingState = opportunityObj.Contact_Person__r.Company_State__c;
                    customerAccount.BillingPostalCode = opportunityObj.Contact_Person__r.Company_Zip_Postal_Code__c;
                    customerAccount.BillingCountry = opportunityObj.Contact_Person__r.Company_Country__c;
                }
                paymentTerms = [SELECT Id, Name, Balance_Amount__c, Due_Date__c, Payment_Amount__c, Payment_Percentage__c, Payment_Status_New__c,
                                Total_Transaction_Amount__c FROM Payment_Term__c WHERE Opportunity__c =: opportunityObj.Id ORDER BY CreatedDate];
                
            }
            
            if(invoice.RecordType.Name == 'Project' || invoice.RecordType.Name == 'Retail') {
                AKFD_Extra_Charges_Tax_Percentage__c akfdExtraChargesTaxPercentage = AKFD_Extra_Charges_Tax_Percentage__c.getOrgDefaults();
                System.debug('Extra Percentage ' + akfdExtraChargesTaxPercentage);
                extraChargesTaxPercentage = akfdExtraChargesTaxPercentage.Tax_Percentage__c.setScale(2);
                System.debug('invoice Freight_Amount__c ' + invoice);
                System.debug('extraChargesTaxPercentage ' + extraChargesTaxPercentage);
                freightAmountTaxValue = (Decimal)(invoice.Freight_Amount__c * extraChargesTaxPercentage / 100);
                packingAmountTaxValue = (Decimal)(packingCharges * extraChargesTaxPercentage / 100);
                 if(isStateTax) {
                    totalCgst = totalCgst + freightAmountTaxValue / 2 + packingAmountTaxValue / 2;
                    totalSgst = totalSgst + freightAmountTaxValue / 2 + packingAmountTaxValue / 2;
                } else {
                    totalIgst = totalIgst + freightAmountTaxValue + packingAmountTaxValue;
                }
            }
           
            totalGST = totalSgst + totalCgst + totalIgst;
            total = (totalTaxableValue + packingCharges + invoice.Freight_Amount__c).setScale(2);
            grandTotal = (total + totalSgst + totalIgst + totalCgst).setScale(2);
            NumbersToWords convertToWord = new NumbersToWords();
            //for converting grand total to words
           	grandTotalText = convertToWord.convert((Decimal)(invoice.Net_Invoice_Amount__c - invoice.Total_Settled_Amount__c));
            //for converting total GST to words
            gstTotalText = convertToWord.convert((Decimal)totalGST);
        } catch(Exception e) {
            System.debug('Stack Trace ' + e.getStackTraceString());
            System.debug('Exception ' + e);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,
                                                            'Bank Info is not available.');
            ApexPages.addMessage(myMsg);
        }
    }


   /**
* Method to save Invoice pdf to Opportunity attachments
*/
    public PageReference redirect() {
        if(accessType != null && accessType.trim().toLowerCase() == 'Preview') return null;
        
        if (!ApexPages.hasMessages()) {
            if(opportunityObj != null) {
                PageReference pdf;
                if( invoice.RecordType.Name == 'Export') {
                    pdf = Page.InvoicePDFExport;
                } else if(invoice.RecordType.Name == 'Retail') {
                    pdf = Page.InvoicePDFRetail;
                } else {
                    pdf = Page.InvoicePDFProject;
                }
                pdf.getParameters().put('id', invoice.Id);
                pdf.getParameters().put('accessType', 'Preview');
                pdf.setRedirect(true);
                
                //creates email attachment file
                Attachment attach = new Attachment();
                Blob body;
                try {
                    body = pdf.getContent();
                } catch (VisualforceException e) {
                    body = Blob.valueOf(e.getMessage());
                }
                
                attach.Body = body;
                attach.Name = invoice.Name +'(' + opportunityObj.Name + ') - Invoice.pdf';
                attach.IsPrivate = false;
                attach.ParentId = opportunityObj.Id;
                insert attach;
                System.debug('RecordType ' + invoice.RecordType.Name);
                String path = '/apex/shareDocument?opportunityId=' + opportunityObj.Id + '&attachmentId=' + attach.id + '&templateName=TemplateFinalInvoice&documentId=' + invoice.Id + '&salesProcess=' + invoice.RecordType.Name + '&targetObjectId=' + opportunityObj.Contact_Person__r.Id;
                path = ('ShareDocketNumber'.equals(accessType))? path + '&isDocketNumberSharePage=true&docketNumber=' + docketNumber : path ;
                return new PageReference(path);
            }
            return new PageReference('/' + invoice.Id);
        }
        return null;
    }    
     /**
    * Method return PageReference Object to Invoice Detail Page
    */
    public PageReference goBack(){
        return new PageReference('/'+ invoice.Id);
    }
    
    // Function to break the multiselect field
    private String breakMultiSelect(String source) {
        String target = '';
        String[] tokens = source.split(';');
        for(String token : tokens) {
            target+=token + ', ';
        }
        return target;
    }
   /* 
    @AuraEnabled
    public static String getProformaInvoicesOfOpportunity(String opportunityId) {
        return null;
    }*/
    
}