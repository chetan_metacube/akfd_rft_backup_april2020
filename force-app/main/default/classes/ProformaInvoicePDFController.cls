/**
 * Class contains PI pdf related code
 * @author : Chettan Sharma
 * Last Edited By : Yash Surana on 25 July 2019
*/
public class ProformaInvoicePDFController {
    public Opportunity opportunityObj {get; private set;}
    public ProformaInvoice__c proformaInvoice {get; private set;}
    public List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems {get; private set;}
    public Account customerAccount {get; private set;}
    public String currencySymbol {get; private set;}
    public List<Payment_Term__c> paymentTerms{get;set;}
    public List<String> bankingDetails {get; private set;}
    public String renderAs {get;set; }
    private String accessType = 'Preview';
    
    // Retail and Project 
    public Boolean showDiscountColumn {get;set;}
    public Decimal extraChargesTaxPercentage {get;set;}
    public Boolean isStateTax{
        get{ 
            return opportunityObj.Billing_Address__r.State_Province__c == opportunityObj.Merchant__r.BillingState;        
        }
        set;
    }
    
    //Export 
    public String paymentTermsCurrencySymbol {get; private set;}
    public String netTotalWord {get; private set;}
    public List<String> bankingbeneficiaryDetail {get; private set;}
    public Boolean showTradeDiscount{get;set;}
    public Boolean showLiabilityInsurance{get;set;}
    
    public ProformaInvoicePdfController(ApexPages.StandardController controller) {
        proformaInvoice = (ProformaInvoice__c) controller.getRecord();
        proformaInvoice = [SELECT Id, Name, Opportunity__c, RecordType.Name, RecordTypeId, Delivery_Date__c, Pre_Carriage_By__c, Port_of_Loading__c, Port_Of_Discharge__c, 
                           Opportunity__r.AccountId, Opportunity__r.PO_NO__c, Opportunity__r.PO_Date__c, Opportunity__r.Merchant__r.Bank_Details__c, Date__c, Trade_Discount__c,
                           Final_Destination__c, Remarks__c, Delivery_Remarks__c, Place_Of_Receipt__c, Country_Of_Final_Origin__c, Mode_of_Payment__c, Freight__c, All_Items_Total_Price__c, Shipping_Charges__c, Product_Liability_Insurance__c, Packing_Charges__c, Product_Liability_Insurance_Percentage__c, CurrencyIsoCode, Net_Total__c FROM ProformaInvoice__c WHERE Id =: proformaInvoice.Id];
        showTradeDiscount = false;
        showLiabilityInsurance = false;
        init();
        if (!ApexPages.hasMessages()){
            renderAs = 'PDF';
        } else { 
            renderAs = '';
        }
    }
    
    /*
*   Fetch values for the Proforma Invoice like, consignor, customer, opportunity etc,
*/
    private void init() {
        try {
            proformaInvoiceLineItems = [SELECT Id, Name, GST__c, PO_Product__r.HST_Code__c, ProformaInvoice__c, Product__c, Required_Quantity__c, Price__c, Unit__c,
                                        Total_Price__c, Quantity__c, SKU_Number__c, Stock_Till_date_Lead_time__c, Product__r.Name, Product__r.Material__c, Product__r.Dimension_Length__c,
                                        Product__r.Dimension_Width__c , Product__r.Dimension_Height__c, Product__r.Finish_1__c , Product__r.Finish_2__c ,
                                        Product__r.Finish_3__c , Product__r.Finish_4__c , Product__r.Product_SKU__c, Product__r.ProductCode, Product__r.Description__c,
                                        Product__r.Dimension_Unit__c, Product__r.Image__c, PO_Product__r.Product__r.Image__c, PO_Product__r.Product_Location__c, 
                                        PO_Product__r.Discount_Percentage__c, PO_Product__r.Product__r.Id, Product__r.HSN_Tax__r.Name, PO_Product__r.Product__r.Name, 
                                        PO_Product__r.Product__r.Material__c, PO_Product__r.Product__r.Description__c, PO_Product__r.Product__r.ProductCode 
                                        FROM Proforma_Invoice_Line_Item__c WHERE ProformaInvoice__c =: proformaInvoice.Id];
            for(Proforma_Invoice_Line_Item__c proformaInvoiceLineItem : proformaInvoiceLineItems) {
                if(proformaInvoiceLineItem.Product__r.Image__c != null) {
                    proformaInvoiceLineItem.Product__r.Image__c=proformaInvoiceLineItem.Product__r.Image__c.replace('alt','height="25" width="25" alt');
                }
                if(proformaInvoiceLineItem.PO_Product__r.Discount_Percentage__c != 0.00) {
                    showDiscountColumn = true;
                }
                if(proformaInvoiceLineItem.Product__r.Material__c != null) {
                    proformaInvoiceLineItem.Product__r.Material__c = breakMultiSelect(proformaInvoiceLineItem.Product__r.Material__c);
                }
            }
            customerAccount = [SELECT Id, Name, Pre_Carriage_By__c, Port_of_Loading__c, Port_Of_Discharge__c, Final_Destination__c, Place_Of_Receipt__c, Country_Of_Final_Origin__c,
                               Mode_of_Payment__c, Email__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState, ShippingCountry, Phone, BillingStreet, BillingCity, BillingPostalCode, 
                               BillingState, BillingCountry, GST_Id__c, Terms_and_Conditions__c, CurrencyIsoCode FROM Account WHERE Id =: proformaInvoice.Opportunity__r.AccountId];
            opportunityObj = [SELECT Id, Name, Bank_Code__c, CloseDate, AccountId, Testing_Charges_Included__c, Contact_Person__c, PO_No__c, PO_Date__c, Incoterms__c,
                              CurrencyIsoCode, Freight__c, Freight_Amount__c, SAC_Code__c, Billing_Address__c, Delivery_Address__c, Billing_Address__r.GST_Id__c, 
                              Billing_Address__r.Name, Billing_Address__r.Street__c, Billing_Address__r.City__c, Billing_Address__r.State_Province__c, Billing_Address__r.Country__c, 
                              Billing_Address__r.Zip_Postal_Code__c, Billing_Address__r.State_Code__c, Delivery_Address__r.Name, Delivery_Address__r.Street__c, 
                              Delivery_Address__r.City__c, Delivery_Address__r.State_Province__c, Delivery_Address__r.Country__c, Delivery_Address__r.Zip_Postal_Code__c, 
                              Delivery_Address__r.State_Code__c, Notify_Address__r.Name, Notify_Address__r.Street__c, Notify_Address__r.City__c, 
                              Notify_Address__r.State_Province__c, Notify_Address__r.Country__c, Notify_Address__r.Zip_Postal_Code__c, 
                              Notify_Address__r.State_Code__c, Buyer_Address__r.Name, Buyer_Address__r.Street__c, Buyer_Address__r.City__c, 
                              Buyer_Address__r.State_Province__c, Buyer_Address__r.Country__c, Buyer_Address__r.Zip_Postal_Code__c, 
                              Buyer_Address__r.State_Code__c, Vendor_Code__c, Packing_Percentage__c, Transportation_Mode__c, 
                              Merchant__r.Name, Merchant__r.Email__c, Merchant__r.ShippingStreet, Merchant__r.ShippingCity, Merchant__r.ShippingPostalCode, Merchant__r.ShippingState, 
                              Merchant__r.ShippingCountry, Merchant__r.BillingStreet, Merchant__r.BillingCity, Merchant__r.BillingPostalCode,
                              Merchant__r.BillingState, Merchant__r.BillingCountry, Merchant__r.Website, Merchant__r.Company_Logo__c, Merchant__r.Phone,
                              Merchant__r.GST_Id__c, Merchant__r.PAN_Number__c, Merchant__r.Exporter_s_IEC_Code__c, Merchant__r.Exporters_s_Ref__c, 
                              Merchant__r.CENTRAL_EXCISE__c, Merchant__r.Export_Under_UT_1__c, Merchant__r.CIN_No__c, Contact_Person__r.RecordType.Name, 
                              Contact_Person__r.Company_Name__c, Contact_Person__r.Company_Street__c, Contact_Person__r.Company_City__c, 
                              Contact_Person__r.Company_State__c, Contact_Person__r.Company_Country__c, Contact_Person__r.Company_Zip_Postal_Code__c, 
                              Contact_Person__r.Name, Contact_Person__r.MailingStreet, Contact_Person__r.MailingCity, Contact_Person__r.MailingPostalCode, 
                              Contact_Person__r.MailingState, Contact_Person__r.MailingCountry, Contact_Person__r.Phone, Contact_Person__r.Email, 
                              Contact__r.Name, Contact__r.Phone, Contact__r.Email, Notify_Contact__r.Name, Notify_Contact__r.Phone, Notify_Contact__r.Email, 
                              Buyer_Contact__r.Name, Buyer_Contact__r.Phone, Buyer_Contact__r.Email, Owner.Name, Owner.Email, Owner.Phone,
                              Text_If_Packing_Freight_Charges_Is_Zero__c FROM Opportunity WHERE Id =: proformaInvoice.Opportunity__c];
            paymentTerms = [SELECT Id, Name, Balance_Amount__c, Due_Date__c, Payment_Amount__c, Payment_Percentage__c, Payment_Status_New__c, 
                            Total_Transaction_Amount__c FROM Payment_Term__c WHERE Opportunity__c =: opportunityObj.Id ORDER BY CreatedDate];
            if(opportunityObj.CurrencyIsoCode == 'INR') {
                currencySymbol = 'INR';
            } else {
                currencySymbol = CurrencyUtils.getSymbol(opportunityObj.CurrencyIsoCode);
            }
            accessType = ApexPages.currentPage().getParameters().get('accessType');
            //gets the banking details for PDF
            Bank_Info__c bankingDetail = [SELECT Bank_Details__c FROM Bank_Info__c WHERE CurrencyIsoCode =: opportunityObj.CurrencyIsoCode AND Account__c = :opportunityObj.Merchant__c LIMIT 1];
            if(bankingDetail.Bank_Details__c != null){
                bankingDetail.Bank_Details__c = bankingDetail.Bank_Details__c.replaceAll('<[^>]+>',' ');
                bankingDetails = bankingDetail.Bank_Details__c.split(',');
            }
            if(proformaInvoice.RecordType.Name == 'Retail' || proformaInvoice.RecordType.Name == 'Project') {
                AKFD_Extra_Charges_Tax_Percentage__c akfdExtraChargesTaxPercentage = AKFD_Extra_Charges_Tax_Percentage__c.getOrgDefaults();
                extraChargesTaxPercentage = akfdExtraChargesTaxPercentage.Tax_Percentage__c.setScale(2);
            }
            if(proformaInvoice.RecordType.Name == 'Export') {
                if(proformaInvoice.Opportunity__r.Merchant__r.Bank_Details__c != null){
                    bankingbeneficiaryDetail = proformaInvoice.Opportunity__r.Merchant__r.Bank_Details__c.split(',');
                }
                //show Discount
                if(proformaInvoice.Trade_Discount__c != 0.00){
                    showTradeDiscount = true;
                } 
                //show Liability insurance
                if(proformaInvoice.Product_Liability_Insurance_Percentage__c != 0.00){
                    showLiabilityInsurance = true;
                }
                //for converting the net total to words
                NumbersToWords convertToWords = new NumbersToWords();
                this.netTotalWord = convertToWords.convert((Decimal)(proformaInvoice.Net_Total__c).setScale(2));
            }
        } catch(Exception e) {
            System.debug('Exception ' + e);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,
                                                            'Bank Info is not available.');
            ApexPages.addMessage(myMsg);
        }
        
    }
    
    private String breakMultiSelect(String source) {
        String target = '';
        String[] tokens = source.split(';');
        for(String token : tokens) {
            target+=token + ', ';
        }
        return target;
    }
    
    public PageReference goBack(){
        return new PageReference('/'+ proformaInvoice.Id);
    }
    
       /**
* Method to save quotation pdf to Opportunity attachments
*/
    public PageReference redirect() {
        if(accessType != null && accessType.trim().toLowerCase() == 'Preview') return null;
        
        if (!ApexPages.hasMessages()) {
            if(opportunityObj != null) {
                PageReference pdf;
                if( proformaInvoice.RecordType.Name == 'Export') {
                    pdf = Page.ProformaInvoicePDFExport;
                } else if(proformaInvoice.RecordType.Name == 'Retail') {
                    pdf = Page.ProformaInvoicePDFRetail;
                } else {
                    pdf = Page.ProformaInvoicePDFProject;
                }
                pdf.getParameters().put('id', proformaInvoice.Id);
                pdf.getParameters().put('accessType', 'Preview');
                pdf.setRedirect(true);
                pdf.setAnchor('PDF');
                
                //creates email attachment file
                Attachment attach = new Attachment();
                Blob body;
                try {
                    body = pdf.getContent();
                } catch (VisualforceException e) {
                    body = Blob.valueOf(e.getMessage());
                }
                
                attach.Body = body;
                attach.Name = proformaInvoice.Name +'(' + opportunityObj.Name + ') - ProformaInvoice.pdf';
                attach.IsPrivate = false;
                attach.ParentId = opportunityObj.Id;
                insert attach;
                System.debug('RecordType ' + proformaInvoice.RecordType.Name);
                return new PageReference('/apex/shareDocument?opportunityId=' + opportunityObj.Id + '&attachmentId=' + attach.id + '&templateName=TemplateProformaInvoice&documentId=' + proformaInvoice.Id + '&salesProcess=' + proformaInvoice.RecordType.Name + '&targetObjectId=' + opportunityObj.Contact_Person__r.Id);
            }
            return new PageReference('/' + proformaInvoice.Id);
        }
        return null;
    }
    
    

}