@isTest
public class BankInfoTriggerTest {
    @isTest static void bankInfoTest(){
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Id merchantcustomerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company/Consignor').getRecordTypeId();
        
        List<Account> acc=new List<Account>();
        Account accountCustomerObj = new Account(Name='AccountTestCustomer', RecordTypeId = customerRecordTypeIdAccount, CurrencyIsoCode = 'USD');
        Account accountMerchantObj = new Account(Name='AccountTestMerchant',CurrencyIsoCode='INR', RecordTypeId = merchantcustomerRecordTypeIdAccount , BillingState = 'Rajasthan', Bank_Details__c = 'Test bank Details');
        acc.add(accountCustomerObj);
        acc.add(accountMerchantObj);
        insert acc;
        Bank_Info__c bankInfo = new Bank_Info__c(Account__c=accountMerchantObj.Id, CurrencyIsoCode='USD',Bank_Details__c='<p><span style="font-size: 14px; color: rgb(43, 40, 38);">This is usd Currency</span></p>');
        insert bankInfo;
        bankInfo.Bank_Details__c='INR';
        update bankInfo;
    }
}