public class ProformaInvoices extends fflib_SObjectDomain implements IProformaInvoices, InvoicesService.ISupportInvoicing {
    public ProformaInvoices(List<ProformaInvoice__c> records){
        super(records);
        Configuration.disableTriggerCRUDSecurity();
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> records){
            return new ProformaInvoices(records);
        }
    }
    
    public override void onBeforeUpdate(Map<Id, SObject> existingRecords) { 
        System.debug('ON Before Update');
        Map<Id, ProformaInvoice__c> existingRecordsMap = (Map<Id, ProformaInvoice__c>) existingRecords;
        List<Id> opportunitiesId = new List<Id>();
        List<Id> proformaInvoicesId = new List<Id>();
        Map<Id, ProformaInvoice__c> newAcceptedProformaInvoices = new Map<Id, ProformaInvoice__c>();
        List<ProformaInvoice__c> revisedProformaInvoices = new List<ProformaInvoice__c>();
        system.debug('Trigger ');
        for(ProformaInvoice__c proformaInvoice : (List<ProformaInvoice__c>) records) {
            if(proformaInvoice.Status__c == 'Accepted') {
                newAcceptedProformaInvoices.put(proformaInvoice.Id, proformaInvoice);
                opportunitiesId.add(proformaInvoice.Opportunity__c);
            } else if(proformaInvoice.Status__c == 'Revised') {
                revisedProformaInvoices.add(proformaInvoice);
            } else {
                if(existingRecordsMap.get(proformaInvoice.Id).Status__c.equals('Accepted')) {
                    proformaInvoice.Status__c.addError('Proforma Invoice Status Can\'t be change from Accepted.' );
                } else if(existingRecordsMap.get(proformaInvoice.Id).Status__c.equals('Revised') && proformaInvoice.Status__c != 'Accepted') {
                    proformaInvoice.Status__c.addError('Proforma Invoice Status Can\'t be change from Revised.' );
                }
            }
        }
        system.debug('before revise ' +opportunitiesId);
        for(ProformaInvoice__c proformaInvoice : revisedProformaInvoices) {
            system.debug('inside revise');
            if(!existingRecordsMap.get(proformaInvoice.Id).Status__c.equals('Revised')) {
                proformaInvoice.addError('Proforma Invoice Status Can\'t be changed to Revised. It\'s system generated.');
            }
        }
        
        List<ProformaInvoice__c> existingAcceptedProformaInvoices = ProformaInvoicesSelector.getInstance().selectAcceptedProformaInvoicesByOpportunityIds(new Set<Id>(opportunitiesId));
        List<Id> opportunitiesIdWithAcceptedPI = new List<Id>();
        for(ProformaInvoice__c proformaInvoice : existingAcceptedProformaInvoices) {
            opportunitiesIdWithAcceptedPI.add(proformaInvoice.Opportunity__c);
        }
        
        Boolean isPriceChanged;
        Boolean isQuantityChanged;
        Boolean isPIRevised;    
        system.debug('before new appcepted');
        for(ProformaInvoice__c proformaInvoice : newAcceptedProformaInvoices.values()) {
            system.debug('inside new appcepted');
            isPriceChanged = existingRecordsMap.get(proformaInvoice.Id).All_Items_Total_Price__c != proformaInvoice.All_Items_Total_Price__c;
            isQuantityChanged = existingRecordsMap.get(proformaInvoice.Id).All_Items_Total_Quantity__c != proformaInvoice.All_Items_Total_Quantity__c;
            isPIRevised = isPriceChanged || isQuantityChanged;
            if(!existingRecordsMap.get(proformaInvoice.Id).Status__c.equals('Accepted')) {
                system.debug('opportunitiesIdWithAcceptedPI ' + opportunitiesIdWithAcceptedPI);
                system.debug('proformaInvoice.Opportunity__c ' + proformaInvoice.Opportunity__c);
                system.debug('opportunitiesIdWithAcceptedPI.contains(proformaInvoice.Opportunity__c) ' + opportunitiesIdWithAcceptedPI.contains(proformaInvoice.Opportunity__c));
                system.debug('isPIRevised ' + isPIRevised);
                if(opportunitiesIdWithAcceptedPI.contains(proformaInvoice.Opportunity__c) && !isPIRevised) {
                    system.debug('opportunitiesIdWithAcceptedPI.contains(proformaInvoice.Opportunity__c) ' + opportunitiesIdWithAcceptedPI.contains(proformaInvoice.Opportunity__c));
                    proformaInvoice.Status__c.addError('Accepted PI already exists, there can be only one Accepted PI.' );
                    newAcceptedProformaInvoices.remove(proformaInvoice.Id);
                    opportunitiesId.remove(opportunitiesId.indexOf(proformaInvoice.Opportunity__c));
                } else {
                    proformaInvoicesId.add(proformaInvoice.Id);
                }
            }
        }
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Payment_Term__c.SObjectType,
                    Opportunity.SObjectType,
                    Proforma_Invoice_Line_Item__c.SObjectType
                    }
        );
        system.debug('opportunitiesId ' + opportunitiesId);
        Id installmentRecordTypeId = Schema.SObjectType.Payment_Term__c.RecordTypeInfosByName.get('Installment Payment Terms').RecordTypeId;
        Id exportRecordTypeId = Schema.SObjectType.ProformaInvoice__c.RecordTypeInfosByName.get('Export').RecordTypeId;
        system.debug('installmentRecordTypeId ' + installmentRecordTypeId);
        List<Payment_Term__c> paymentTerms = new List<Payment_Term__c>();
        Map<Id,Opportunity> opportunities = new Map<Id, Opportunity>();
        if(proformaInvoicesId.size() > 0) {
            paymentTerms = PaymentTermsSelector.getInstance().selectInstallmentPaymentTermsByOpportunityIds(new Set<Id>(opportunitiesId));
            opportunities = new Map<Id, Opportunity>((List<Opportunity>)OpportunitiesSelector.getInstance().selectSObjectsById(new Set<Id>(opportunitiesId)));
        }
        List<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItems = ProformaInvoiceLineItemsSelector.getInstance().selectByProformaInvoices(new Set<Id>(proformaInvoicesId));
        Map<Id, Decimal> proformaInvoiceUpdatedTotalAmount = new Map<Id, Decimal>();
        Map<Id, Decimal> proformaInvoiceTaxableAmount = new Map<Id, Decimal>();
        Map<Id, List<Proforma_Invoice_Line_Item__c>> proformaInvoiceLineItemsMap = new Map<Id, List<Proforma_Invoice_Line_Item__c>>();
        List<Proforma_Invoice_Line_Item__c> items;
        system.debug('proformaInvoiceLineItems ' + proformaInvoiceLineItems);
        for(Proforma_Invoice_Line_Item__c item : proformaInvoiceLineItems) {
            if(proformaInvoiceLineItemsMap.containsKey(item.ProformaInvoice__c)) {
                items = proformaInvoiceLineItemsMap.get(item.ProformaInvoice__c);
            } else {
                items = new List<Proforma_Invoice_Line_Item__c>();
            }
            items.add(item);
            proformaInvoiceLineItemsMap.put(item.ProformaInvoice__c, items);
        }
        Decimal updatedAmount;
        Decimal totalUpdatedAmount;
        Decimal totalTaxableAmount;
        Decimal taxableAmount;
        Decimal proformInvoiceItemAmount;
        Decimal invoiceProductAmount;
        Integer updatedQuantity;
        for(ProformaInvoice__c proformaInvoice : newAcceptedProformaInvoices.values()) {
            totalUpdatedAmount = 0.00;
            totalTaxableAmount = 0.00;
            isPIRevised = false;
            if(proformaInvoiceLineItemsMap.get(proformaInvoice.Id) != null) {
                if(proformaInvoice.RecordTypeId == exportRecordTypeId) {
                    if(existingRecordsMap.get(proformaInvoice.Id).Status__c == 'New') {
                        system.debug('new');
                        for(Proforma_Invoice_Line_Item__c lineItem : proformaInvoiceLineItemsMap.get(proformaInvoice.Id)) {
                            taxableAmount = lineItem.Total_Price__c;
                            totalUpdatedAmount += taxableAmount;
                        }
                        system.debug('totalupdate new export ' + totalUpdatedAmount);
                    } else if(existingRecordsMap.get(proformaInvoice.Id).Status__c == 'Revised') {
                        system.debug('revise');
                        for(Proforma_Invoice_Line_Item__c lineItem : proformaInvoiceLineItemsMap.get(proformaInvoice.Id)) {
                            isQuantityChanged = lineItem.Last_Updated_Quantity__c != lineItem.Required_Quantity__c;
                            isPriceChanged = lineItem.Last_Updated_Price__c != lineItem.Price__c;
                            isPIRevised = isQuantityChanged || isPriceChanged;
                            if(isPIRevised) {
                                taxableAmount = proformaInvoice.Total_Including_Taxes__c;
                                taxableAmount -= (lineItem.Last_Updated_Quantity__c - lineItem.Invoiced_Quantity__c) * lineItem.Last_Updated_Price__c;
                                taxableAmount += (lineItem.Required_Quantity__c - lineItem.Invoiced_Quantity__c) * lineItem.Price__c;
                                proformaInvoice.Total_Including_Taxes__c = taxableAmount;
                            } else {
                                taxableAmount = proformaInvoice.Total_Including_Taxes__c;
                            }
                            totalUpdatedAmount = taxableAmount;
                        }
                        system.debug('totalupdate revise export ' + totalUpdatedAmount);
                    }
                } else {
                    if(existingRecordsMap.get(proformaInvoice.Id).Status__c == 'New') {
                        system.debug('new');
                        for(Proforma_Invoice_Line_Item__c lineItem : proformaInvoiceLineItemsMap.get(proformaInvoice.Id)) {
                            taxableAmount = lineItem.Total_Price__c - lineItem.Total_Price__c * (lineItem.PO_Product__r.Discount_Percentage__c/100);
                            updatedAmount = taxableAmount;
                            updatedAmount += taxableAmount * (lineItem.GST__c/100);
                            system.debug('updatedAmount ' + updatedAmount);
                            totalUpdatedAmount += updatedAmount;
                        }
                        system.debug('totalupdate new P/R ' + totalUpdatedAmount);
                    } else if(existingRecordsMap.get(proformaInvoice.Id).Status__c == 'Revised') {
                        for(Proforma_Invoice_Line_Item__c lineItem : proformaInvoiceLineItemsMap.get(proformaInvoice.Id)) {
                            isQuantityChanged = lineItem.Last_Updated_Quantity__c != lineItem.Required_Quantity__c;
                            isPriceChanged = lineItem.Last_Updated_Price__c != lineItem.Price__c;
                            isPIRevised = isQuantityChanged || isPriceChanged;
                            if(isPIRevised) {
                                taxableAmount = proformaInvoice.Total_Including_Taxes__c;
                                proformInvoiceItemAmount = (lineItem.Last_Updated_Quantity__c - lineItem.Invoiced_Quantity__c) * lineItem.Last_Updated_Price__c;
                                proformInvoiceItemAmount -= proformInvoiceItemAmount * (lineItem.PO_Product__r.Discount_Percentage__c/100);
                                proformInvoiceItemAmount += proformInvoiceItemAmount * (lineItem.GST__c/100);
                                taxableAmount -= proformInvoiceItemAmount;
                                proformInvoiceItemAmount = (lineItem.Required_Quantity__c - lineItem.Invoiced_Quantity__c) * lineItem.Price__c;
                                proformInvoiceItemAmount -= proformInvoiceItemAmount * (lineItem.PO_Product__r.Discount_Percentage__c/100);
                                proformInvoiceItemAmount += proformInvoiceItemAmount * (lineItem.GST__c/100);
                                taxableAmount += proformInvoiceItemAmount;                                
                                proformaInvoice.Total_Including_Taxes__c = taxableAmount;
                            } else {
                                taxableAmount = proformaInvoice.Total_Including_Taxes__c;
                            }
                            totalUpdatedAmount = taxableAmount;
                        }
                        
                        system.debug('totalupdate revise P/R ' + totalUpdatedAmount);
                    }
                }
                //proformaInvoiceTaxableAmount.put(proformaInvoice.Id, totalTaxableAmount);
                proformaInvoiceUpdatedTotalAmount.put(proformaInvoice.Id, totalUpdatedAmount);
            }
        }
        Decimal totalAmount = 0;
        Map<Id, List<Payment_Term__c>> paymentTermsWithOpportunityMap = new Map<Id, List<Payment_Term__c>>();
        List<Payment_Term__c> terms;
        system.debug('paymentTerms ' + paymentTerms);
        for(Payment_Term__c paymentTerm : paymentTerms) {
            if(paymentTermsWithOpportunityMap.containsKey(paymentTerm.Opportunity__c)) {
                terms = paymentTermsWithOpportunityMap.get(paymentTerm.Opportunity__c);
            } else {
                terms = new List<Payment_Term__c>();
            }
            terms.add(paymentTerm);
            paymentTermsWithOpportunityMap.put(paymentTerm.Opportunity__c, terms);
        }
        system.debug('paymentTermsWithOpportunityMap ' + paymentTermsWithOpportunityMap);
        List<Payment_Term__c> paymentTermsToBeUpdated = new List<Payment_Term__c>();
        List<Opportunity> opportunitiesToBeUpdated = new List<Opportunity>();
        Set<Proforma_Invoice_Line_Item__c> proformaInvoiceLineItemsToBeUpdated = new Set<Proforma_Invoice_Line_Item__c>();
        Decimal paymentTermAmount;
        for(ProformaInvoice__c proformaInvoice : newAcceptedProformaInvoices.values()) {
            isPriceChanged = existingRecordsMap.get(proformaInvoice.Id).All_Items_Total_Price__c != proformaInvoice.All_Items_Total_Price__c;
            isQuantityChanged = existingRecordsMap.get(proformaInvoice.Id).All_Items_Total_Quantity__c != proformaInvoice.All_Items_Total_Quantity__c;
            isPIRevised = isPriceChanged || isQuantityChanged;
            system.debug('Accepted');
            if(isPIRevised) {
                proformaInvoice.Status__c = 'Revised';
                proformaInvoice.Is_Revised__c = true;
                if(isQuantityChanged) {
                    proformaInvoice.Product_Alloted__c = false;
                }
            } else {
                if(opportunities != null && opportunities.get(proformaInvoice.Opportunity__c) != null) {
                    opportunities.get(proformaInvoice.Opportunity__c).CurrencyIsoCode = proformaInvoice.CurrencyIsoCode;
                    opportunities.get(proformaInvoice.Opportunity__c).Freight_Amount__c *= proformaInvoice.Conversion_Rate__c;
                    opportunitiesToBeUpdated.add(opportunities.get(proformaInvoice.Opportunity__c));
                }
                if(proformaInvoiceUpdatedTotalAmount.get(proformaInvoice.Id) != null) {
                    totalAmount = proformaInvoiceUpdatedTotalAmount.get(proformaInvoice.Id);
                    system.debug('totalAmount ' + totalAmount);
                    proformaInvoice.Total_Including_Taxes__c = totalAmount;
                    proformaInvoice.Is_Revised__c = false;
                }
                if(proformaInvoiceLineItemsMap.get(proformaInvoice.Id) != null) {
                    for(Proforma_Invoice_Line_Item__c lineItem : proformaInvoiceLineItemsMap.get(proformaInvoice.Id)) {
                        if(lineItem.Last_Updated_Quantity__c != lineItem.Required_Quantity__c) {
                            lineItem.Last_Updated_Quantity__c = lineItem.Required_Quantity__c;
                            proformaInvoiceLineItemsToBeUpdated.add(lineItem);
                        }
                        if(lineItem.Last_Updated_Price__c != lineItem.Price__c) {
                            lineItem.Last_Updated_Price__c = lineItem.Price__c;
                            proformaInvoiceLineItemsToBeUpdated.add(lineItem);
                        }
                    }
                }
                if(paymentTermsWithOpportunityMap.get(proformaInvoice.Opportunity__c) != null) {
                    for(Payment_Term__c paymentTerm : paymentTermsWithOpportunityMap.get(proformaInvoice.Opportunity__c)) {
                        paymentTermAmount = totalAmount;
                        //totalAmount += proformaInvoiceTaxableAmount.get(proformaInvoice.Id) * opportunities.get(paymentTerm.Opportunity__c).Packing_Percentage__c / 100;
                        //totalAmount += opportunities.get(proformaInvoice.Opportunity__c).Freight_Amount__c;
                        system.debug('paymentTermAmount' + paymentTermAmount);
                        paymentTermAmount *= paymentTerm.Payment_Percentage__c / 100;
                        system.debug('paymentTermAmount percent' + paymentTermAmount);
                        paymentTerm.Dummy_Payment_Amount__c = paymentTermAmount;
                        paymentTerm.Dummy_Balance_Amount_v2__c = paymentTermAmount;
                        paymentTerm.CurrencyIsoCode = proformaInvoice.CurrencyIsoCode;
                        paymentTermsToBeUpdated.add(paymentTerm);
                    }
                }
            }
        }
        system.debug('paymentTermsToBeUpdated ' + paymentTermsToBeUpdated);
        system.debug('opportunitiesToBeUpdated ' + opportunitiesToBeUpdated);
        system.debug('proformaInvoiceLineItemsToBeUpdated ' + proformaInvoiceLineItemsToBeUpdated);
        uow.registerDirty(paymentTermsToBeUpdated);
        uow.registerDirty(opportunitiesToBeUpdated);
        uow.registerDirty(new List<Proforma_Invoice_Line_Item__c>(proformaInvoiceLineItemsToBeUpdated));
        uow.commitWork();
    }
    
    public override void onAfterUpdate(Map<Id, SObject> existingRecords) {
        
    }
    
    public override void onBeforeDelete() {
        for(ProformaInvoice__c proformaInvoice : (List<ProformaInvoice__c>) records) {
            if(proformaInvoice.Status__c == 'Accepted') {
                proformaInvoice.addError('Accepted Proforma Invoice Can\'t be delete.');
            } else if(proformaInvoice.Status__c == 'Revised') {
                proformaInvoice.addError('Revised Proforma Invoice Can\'t be delete.');
            }
        }
    }
    
    public void generate(InvoicesService.InvoiceFactory invoiceFactory, List<InvoicesService.ProformaInvoicesIdsWrapper> mergeInvoicesProformaInvoicesIds) {
        Map<Id, ProformaInvoice__c> recordsMap = new Map<Id, ProformaInvoice__c>((List<ProformaInvoice__c>) records);
        InvoicesService.Invoice invoice;
        InvoicesService.InvoicedItem invoiceItem;
        InvoicesService.InvoicedProduct invoicedProduct;
        //invoice.Lines = new List<InvoicesService.InvoiceLine>();
        /* Utilise InvoiceFactory to create invoices from Proforma Invoice details*/
        for(InvoicesService.ProformaInvoicesIdsWrapper proformaInvoicesIdsWrapper : mergeInvoicesProformaInvoicesIds) {
            System.debug('+++++++++++++++++++++proformaInvoicesIdsWrapper'+proformaInvoicesIdsWrapper);
            invoice = new InvoicesService.Invoice();
            invoice.Account = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.AccountId;
            
            invoice.isMerged = proformaInvoicesIdsWrapper.proformaInvoicesIds.size() > 1 ? true : false;
            invoice.Opportunity = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__c;
            invoice.InvoiceDate = Date.today();
            String proformaInvoiceRecordTypeName = Schema.getGlobalDescribe().get('ProformaInvoice__c').getDescribe().getRecordTypeInfosById().get(recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).RecordTypeId).getName();
            invoice.RecordTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(proformaInvoiceRecordTypeName).getRecordTypeId();
            invoice.CountryOfFinalOrigin = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Country_Of_Final_Origin__c;
            invoice.CurrencyIsoCode = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).CurrencyIsoCode;
            invoice.FinalDestination = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Final_Destination__c;
            invoice.ModeOfPayment = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Mode_of_Payment__c;
            invoice.Freight = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Freight__c;
            invoice.AdvancePayment = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Advance_Payment__c;
            invoice.PackingCharges = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Packing_Charges__c;
            invoice.PortOfDischarge = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Port_Of_Discharge__c;
            invoice.PlaceOfReceipt = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Place_Of_Receipt__c;
            invoice.PortOfLoading = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Port_of_Loading__c;
            invoice.PreCarriageBy = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Pre_Carriage_By__c;
            invoice.ProductLiabilityInsurancePercentage = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Product_Liability_Insurance_Percentage__c;
            invoice.ShippingCharges = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Shipping_Charges__c;
            invoice.TradeDiscount = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Trade_Discount__c;
            invoice.FreightAmount = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Freight_Amount__c;
            invoice.PackingPercentage = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Packing_Percentage__c;
            invoice.TransportationMode = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Transportation_Mode__c;
            /*added for address implementation*/
            invoice.BillingAddress = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Billing_Address__c;
            invoice.NotifyAddress = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Notify_Address__c;
            invoice.DeliveryAddress = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Delivery_Address__c;
            invoice.ContactPerson = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Contact_Person__c;
            invoice.NotifyContact = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Notify_Contact__c;
            invoice.Contact = recordsMap.get(proformaInvoicesIdsWrapper.primaryProformaInvoiceId).Opportunity__r.Contact__c;
            
            for(Id proformaInvoiceId : proformaInvoicesIdsWrapper.proformaInvoicesIds) {
                invoiceItem = new InvoicesService.InvoicedItem();
                invoiceItem.Opportunity = recordsMap.get(proformaInvoiceId).Opportunity__c;
                invoiceItem.CurrencyIsoCode = recordsMap.get(proformaInvoiceId).CurrencyIsoCode;
                invoiceItem.RecordTypeId = Schema.SObjectType.Invoiced_Items__c.getRecordTypeInfosByName().get(proformaInvoiceRecordTypeName).getRecordTypeId();
                invoiceItem.SettledAmount = 0.00;
                System.debug('proformaInvoiceLineItem'+recordsMap.get(proformaInvoiceId));
                System.debug('proformaInvoiceLineItem'+recordsMap.get(proformaInvoiceId).ProformaInvoiceLineItems__r);
                for(Proforma_Invoice_Line_Item__c proformaInvoiceLineItem : recordsMap.get(proformaInvoiceId).ProformaInvoiceLineItems__r) {
                    invoicedProduct = new InvoicesService.InvoicedProduct();
                    System.debug('invoicedProduct'+invoicedProduct);
                    invoicedProduct.Product = proformaInvoiceLineItem.Product__c;
                    invoicedProduct.GST = proformaInvoiceLineItem.Product__r.GST__c;
                    invoicedProduct.RecordTypeId = Schema.SObjectType.Invoiced_Product__c.getRecordTypeInfosByName().get(proformaInvoiceRecordTypeName).getRecordTypeId();
                    invoicedProduct.ProformaInvoiceLineItem = proformaInvoiceLineItem.Id;
                    invoicedProduct.POProduct = proformaInvoiceLineItem.PO_Product__c;
                    invoicedProduct.SKUNumber = proformaInvoiceLineItem.SKU_Number__c;
                    invoicedProduct.Quantity = proformaInvoiceLineItem.Quantity__c;
                    invoicedProduct.Unit = proformaInvoiceLineItem.Unit__c;
                    invoicedProduct.UnitPrice = proformaInvoiceLineItem.Price__c;
                    invoicedProduct.InvoicedProductDiscount = proformaInvoiceLineItem.PO_Product__r.Discount_Percentage__c;
                    invoicedProduct.CurrencyIsoCode = proformaInvoiceLineItem.CurrencyIsoCode;
                    System.debug('invoicedProduct'+invoicedProduct);
                    System.debug('invoiceItem'+invoiceItem);
                    invoiceItem.InvoicedProducts.add(invoicedProduct);
                    System.debug('invoiceItem'+invoiceItem);
                }
                System.debug('invoiceItem'+invoiceItem);
                invoice.Items.add(invoiceItem);
                System.debug('invoice'+invoice);
            }
            
            
            invoiceFactory.add(invoice);
        }
    }
    
}