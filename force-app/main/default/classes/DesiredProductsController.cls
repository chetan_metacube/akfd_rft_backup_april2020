/**
 * Used in AddDesiredProductsComponent
 * Class for Desired Products related functionality 
 * author: Chetan Sharma (15 Feb 2019)
 * Last Modified By: Chetan Sharma (17 June 2019)
 */
public class DesiredProductsController {
  /** 
    * Method has logic to insert desired products
    * @param selected products string and opportunityId. 
    * @return String result. 
    */
    @AuraEnabled
    public static String insertDesiredProducts(String selectedProductsString, Id opportunityId) {
        // 1. Validate String is not blank and generate wrapper
        if(String.isNotBlank(selectedProductsString) &&  opportunityId != null && opportunityId.getSobjectType() == Schema.Opportunity.getSObjectType()) {
            String resultMessage = '';  
            System.debug('SelectedProductString ' + selectedProductsString);
            Set<SelectedProductsWrapper> selectedProducts = new Set<SelectedProductsWrapper>();
            /*
            for(Object selectedProduct : (List<Object>)JSON.deserializeUntyped((selectedProductsString))) {
                selectedProducts.add(new SelectedProductsWrapper(selectedProduct));
            }*/
            selectedProducts.addAll((List<SelectedProductsWrapper>)JSON.deserialize(selectedProductsString, List<SelectedProductsWrapper>.class));
            
            // 2. Validate if list of selected products is empty
            if(selectedProducts.size() == 0) {
                resultMessage = 'Please add atleast one product.';
                throw new AuraHandledException(resultMessage);
            } else {
                // 3. If all good add it into desired Products
                UserRecordAccess userAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :opportunityId];
                if(userAccess != null && !userAccess.HasEditAccess) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Insufficient Privileges.'));
                    throw new AuraHandledException('Insufficient Privileges.');
                }
                Set<String> fieldSet = new Set<String> {'RecordType.Name', 'Account.Applicable_Discount_Percentage__c', 'CurrencyIsoCode'};
                Opportunity opportunity = OpportunitiesSelector.getInstance().getOpportunityWithByIdFieldSet(opportunityId, fieldSet);
                List<PO_Product__c> desiredProducts = new List<PO_Product__c>();
                PO_Product__c newDesiredProduct;
                Decimal discountPercentage = 0.00;
                Id desiredProductRecordTypeId = Schema.SObjectType.PO_Product__c.getRecordTypeInfosByName().get(opportunity.RecordType.Name).getRecordTypeId();
                for(SelectedProductsWrapper selectedProduct : selectedProducts) {
                    discountPercentage =  (opportunity.RecordType.Name == 'Export') ? 0.0 : opportunity.Account.Applicable_Discount_Percentage__c;
                    newDesiredProduct = new PO_Product__c(Product__c = selectedProduct.id,
                                                          Opportunity__c = opportunityId,
                                                          Quantity__c = selectedProduct.quantity,
                                                          Unit__c = selectedProduct.unit,
                                                          Discount_Percentage__c = discountPercentage,
                                                          recordTypeId = desiredProductRecordTypeId,
                                                          CurrencyIsoCode = opportunity.CurrencyIsoCode);
                    desiredProducts.add(newDesiredProduct);
                }
                // 4. Insert into database
                try {
                    Map<Id, List<PO_Product__c>> opportunitySpecificDesiredProducts = new Map<Id, List<PO_Product__c>>();
                    opportunitySpecificDesiredProducts.put(opportunityId, desiredProducts);
                    DesiredProductsService.addDesiredProducts(opportunitySpecificDesiredProducts);
                    resultMessage = 'Success : Desired Products added successfully.';
                } catch (Exception e) {
                    resultMessage = e.getMessage();
                    throw new AuraHandledException(resultMessage);
                }
            }
            return resultMessage;
        } else if(String.isBlank(selectedProductsString)) { 
            throw new AuraHandledException('Please select at least one product.');
        } else {
            throw new AuraHandledException('Invalid Opportunity Id entered.');
        }
    }
    
    @AuraEnabled
    public static String getProductsByIDs(ID idInstance){
        if(String.isNotBlank(idInstance) && idInstance.getSobjectType() == Schema.Product2.getSObjectType()){
            Set<ID> ids = new Set<ID>();
            ids.add(idInstance);
            Product2 productInstance = ProductsSelector.getInstance().selectById(ids)[0];
            SelectedProductsWrapper selectedWrapper = new SelectedProductsWrapper(productInstance);
            return JSON.serialize(selectedWrapper);
        }
        return null;
    }
    
    public class SelectedProductsWrapper {
        public String id {get;set;} 
        public String name {get;set;}
        public String productSKU {get;set;}
        public String status {get;set;}
        public String family {get;set;}
        public String unit {get;set;}
        public Decimal quantity {get;set;}
        
        public SelectedProductsWrapper() {
            
        }
      
        public SelectedProductsWrapper(Product2 product) { 
            this.id = product.id;
            this.name = product.name;
            this.productSKU = product.Product_SKU__c; 
            this.status = product.Status__c; 
            this.family = product.Family; 
            this.unit = product.Unit__c; 
            this.quantity = 0; 
        }
		
    }
}