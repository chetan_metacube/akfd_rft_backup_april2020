@isTest
public class AccountAPICalloutTest {
	@isTest
    static void upsertAccountsTestMethod(){
        Id customerRecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        // Create Account
        Account accountCustomerType = new Account(Name='AccountTest',RecordTypeId=customerRecordTypeIdAccount,CurrencyIsoCode='USD', Group__c ='SUNDRY DEBTORS');
        insert accountCustomerType;
        Id agentContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agent Contact').getRecordTypeId();
        Contact agentContactObj = new Contact(LastName = 'Test Contact Last Name', AccountId = accountCustomerType.Id, RecordTypeId = agentContactRecordTypeId, Email = 'test@test.com');
        insert agentContactObj;
        List<Address__c> addresses = new List<Address__c>();
        Address__c billingAddress = new Address__c(Account__c = accountCustomerType.Id, Name='Billing Address', Address_Type__c = 'Billing', Countries__c = 'India', State__c = 'Rajasthan');
        Address__c shippingAddress = new Address__c(Account__c = accountCustomerType.Id, Address_Type__c = 'Delivery', Countries__c = 'India', State__c = 'Rajasthan');
        addresses.add(billingAddress);
        addresses.add(shippingAddress);
        insert addresses;
        AccountAPICallout.AccountInput accountInput = new AccountAPICallout.AccountInput();
        accountInput.mAccount = accountCustomerType;
        accountInput.addresses = addresses;
        accountInput.contacts = new List<Contact>{agentContactObj};
        AccountAPICallout.upsertAccounts(new List<AccountAPICallout.AccountInput>{accountInput});
        
    }
}