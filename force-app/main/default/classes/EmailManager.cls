/** 
 * Class for handling the Emails sent 
 * Author : Chetan Sharma And Yash Surana
*/
public class EmailManager {
	private Map<String, String> sObjectTypeNameMap;
    private String opportunityOwnerEmail {get;set;}
    
    public EmailManager(){
        sObjectTypeNameMap = new Map<String,String>();
        sObjectTypeNameMap.put('Quotation__c', 'Quotation');
        sObjectTypeNameMap.put('ProformaInvoice__c', 'Proforma Invoice');
        sObjectTypeNameMap.put('Invoice__c', 'Invoice');
    }
    
    
    
    /**
     * Send Mail to list of related persons
     */
    public static void sendEmail(String subject, String message, List<String> emailIds) {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Setting default Email Address
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'supportteam@akfdstudio.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        String[] toAddresses = emailIds;
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        //Set Email Body
        String body = ''; 
        body += message;  
        body += '<br/><p>This is an automatically generated mail. Do not reply.</p>'; 
        body += '<br/><br/> Regards,'; 
        body += '<br/>AKFD';
        //Set Email To Email parameters 
        mail.setToAddresses(toAddresses);   
        mail.setHtmlBody(body);
        
        //Add Email to mail list
        mailList.add(mail);
        if (!mailList.isEmpty()) {
            Messaging.sendEmail(mailList);
        }
    }
    
    
    /**
     * Method used to send email notification on Quotation/PI/Invoice creation to user.
	*/
    public void sendNotification(SObject ObjRef, Opportunity opportunityObj){
       	String sObjectTypeName = objRef.getSObjectType().getDescribe().getName();
        //Getting default admin email address and generating toAddress string
        AKFD_Configuration__c appConfig = AKFD_Configuration__c.getOrgDefaults(); 
        List<String> toAddresses = new List<String>();
        if(appConfig.AKF_Process_Admin_Email__c != null){
            toAddresses.add(appConfig.AKF_Process_Admin_Email__c);
        }
        if(opportunityObj.Owner.Email != null){
            toAddresses.add(opportunityObj.Owner.Email);
        }
        System.debug('Opportunity Owner Email Address ' + opportunityObj.Owner.Email);
        String documentName = ''; 
        if( sObjectTypeName == 'Quotation__c' ) {
            documentName = ((Quotation__c)ObjRef).Name;
        } else if( sObjectTypeName == 'Invoice__c' ) {
            documentName = ((Invoice__c)ObjRef).Name;
        } else if( sObjectTypeName == 'ProformaInvoice__c' ){
            documentName = ((ProformaInvoice__c)ObjRef).Name;
        }
        
        System.debug('toAddresses'+toAddresses);
        String subject = sObjectTypeNameMap.get(sObjectTypeName) + ' ' + documentName + ' has been created ( ' + opportunityObj.Account.Name + ' ) - ' + opportunityObj.RecordType.Name;
        String message = '<p>Dear ' + opportunityObj.Owner.Name + ',</p><br/>'; 
        message+= 'You have created a ' + sObjectTypeNameMap.get(sObjectTypeName)+ ' <b>' + documentName + '</b> on date ' + Date.today().format() + ' for ' + opportunityObj.Account.Name + '.<br />';
        sendEmail(subject, message, toAddresses);
    }
    
    /**
     * Method used to send email notification on Quotation/PI/Invoice creation to user when some product do not have images.
	 */
    public void sendNoImageNotification(Opportunity opportunity, String productsWithNoImage) {
        //Getting default admin email address and generating toAddress string
        AKFD_Configuration__c appConfig = AKFD_Configuration__c.getOrgDefaults(); 
        List<String> toAddresses = new List<String>();
        if(appConfig.AKF_Process_Admin_Email__c != null){
            toAddresses.add(appConfig.AKF_Process_Admin_Email__c);
        }
        if(opportunity.Owner.Email != null){
            toAddresses.add(opportunity.Owner.Email);
        }
        String subject = 'Master Product - No Image Found ( ' + opportunity.Account.Name + ' )';
        String message = '<p>Dear User,</p><br/>'; 
        message+= 'No Image is found in Master Product for below Product: <br/><b>' + productsWithNoImage + '</b>.<br/>';
        message+= 'Kindly add the product image in Master product.';
        System.debug('toAddresses'+toAddresses);
        sendEmail(subject, message, toAddresses);
    }
    
    /**
    * Send Docket Number Email to related persons
    */
    public void sendDocketNumberEmail(String emailTemplateName, Id recordId, List<String> recipientEmails, Id targetObjectId) {
        EmailTemplate emailTemplate = [SELECT Id, Name FROM EmailTemplate WHERE Name =:emailTemplateName AND isActive = true LIMIT 1];
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        OrgWideEmailAddress[] owea = [SELECT Id from OrgWideEmailAddress WHERE Address = 'supportteam@akfdstudio.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
        }
        mail.setTemplateId(emailTemplate.Id);
        String[] toAddresses = recipientEmails;
        mail.setToAddresses(toAddresses);
        mail.setTargetObjectId(targetObjectId); 
        mail.setTreatTargetObjectAsRecipient(false);
        
        mailList.add(mail);
        mail.setWhatId(recordId); 
        if (!mailList.isEmpty()) {
            Messaging.sendEmail(mailList);
        }
    }
    
   
     public void sendEmailUsingEmailTemplate(Id documentId, Id attachmentId, String templateName, List<String> recipientEmails, Id targetObjectId){
         EmailTemplate emailTemplate = [SELECT Id, Name FROM EmailTemplate WHERE Name =: templateName and isActive = true Limit 1];
         Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
         OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'supportteam@akfdstudio.com'];
         if ( owea.size() > 0 ) {
             email.setOrgWideEmailAddressId(owea.get(0).Id);
         }
         email.setSaveAsActivity(false);
         email.setTemplateId(emailTemplate.Id);
         Attachment attachment = [SELECT Id, Name, Body FROM Attachment WHERE Id =: attachmentId]; 
         Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
         efa.setFileName(attachment.Name);
         efa.setBody(attachment.Body);
         email.setTargetObjectId(targetObjectId); 
         email.setTreatTargetObjectAsRecipient(false);
         email.setWhatId(documentId); 
         email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
         email.setToAddresses(recipientEmails); 
         System.debug('recipientEmails ' + recipientEmails);
         Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email}); 
     }
}