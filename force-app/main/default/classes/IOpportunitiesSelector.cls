public interface IOpportunitiesSelector extends fflib_ISObjectSelector {
    
    Opportunity getOpportunityWithByIdFieldSet(Id oppportunityId, Set<String> fieldSet);
    
    Opportunity selectOpportunityWithPIsById(Id oppportunityId);
    
    List<Opportunity> selectOpportunitiesByIds(Set<Id> opportunityIds);
}