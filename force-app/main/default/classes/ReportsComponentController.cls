public class ReportsComponentController {
    
    @AuraEnabled
    public static String getReports(List<String> reportNames, String basicReportFilterJson, String customReportFilterJson) {
        BasicReportFilter basicReportFilter = (BasicReportFilter)JSON.deserialize(basicReportFilterJson, BasicReportFilter.class);
        CustomReportFilter customReportFilter;
        
        if(String.isNotEmpty(customReportFilterJson)) {
            customReportFilter = (CustomReportFilter)JSON.deserialize(customReportFilterJson, CustomReportFilter.class);
        }
        
        List <Report> reportList = [ SELECT Id, Name, DeveloperName FROM Report where Name IN :reportNames ];
        String reportId = (String)reportList.get(0).get('Id');
        
        Reports.ReportDescribeResult describe;
        Reports.ReportMetadata reportMetadata;
        Reports.ReportResults result;
        MAP<String,Reports.ReportFact> reportFact;
        Reports.Dimension dimension;
        Map<String, List<ReportResult>> reportsResult = new Map<String, List<ReportResult>>();
        List<ReportResult> reportResult;
        Decimal value;
        List<String> recordYears;
        String label;
        
        for(Report reportRecord : reportList) {
            describe = Reports.ReportManager.describeReport(reportRecord.Id);
            reportMetadata = describe.getReportMetadata();
            
            if(basicReportFilter.isFilterApplied) {
                reportMetadata = setBasicReportFilter(reportMetadata, basicReportFilter);
            }
            
            if(customReportFilter !=null && customReportFilter.isProductFilter && String.isNotBlank(customReportFilter.productFilter)) {
                for(Reports.ReportFilter filter : reportMetadata.getReportFilters()) {
                    if(filter.getColumn().contains('Product__c.Name')) {
                        filter.setOperator('contains');
                        filter.setValue(customReportFilter.productFilter);
                    }
                }
            }
            result = Reports.ReportManager.runReport(reportRecord.Id, reportMetadata, false); 
            reportFact = result.getFactMap();
            dimension = result.getGroupingsDown();
            reportResult = new List<ReportResult>();
            recordYears = new List<String>();
            
            if(dimension.getGroupings().isEmpty()) {
                value = (Decimal)reportFact.get('T!T').getAggregates().get(0).getValue();
                recordYears = new List<String>{basicReportFilter.filterYear};
                reportResult.add(new ReportResult(null, null, recordYears, reportRecord.Id));
            } else {
                if(basicReportFilter.isFilterApplied) {
                    recordYears = getRecordYears(dimension.getGroupings());
                }
                for(Reports.GroupingValue groupingValue : dimension.getGroupings()) {
                    value = (Decimal)reportFact.get(groupingValue.getKey() + '!T').getAggregates().get(0).getValue();
                    if(basicReportFilter.isFilterApplied && basicReportFilter.filterType.equals('DAY')) {
                        label = parseDate(groupingValue.getLabel());
                    } else {
                        label = groupingValue.getLabel();                        
                    }
                    value = value.setScale(2);
                    reportResult.add(new ReportResult(label, String.valueOf(value), recordYears, reportRecord.Id));
                }
                
            }
            reportsResult.put(reportRecord.Name, reportResult);
        }
        
        return JSON.serialize(reportsResult);
    }
    
    private static Reports.ReportMetadata setBasicReportFilter(Reports.ReportMetadata reportMetadata, BasicReportFilter basicReportFilter) {
        
        for(Reports.GroupingInfo groupInfo : reportMetadata.getGroupingsDown()) {
            if(groupInfo.getName().contains('Date')) {
                groupInfo.setDateGranularity(basicReportFilter.filterType);
            }
        }
        
        Reports.StandardDateFilter standardDateFilter;
        Integer diffYear;
        
        if(String.isNotEmpty(basicReportFilter.startDate) && String.isNotEmpty(basicReportFilter.endDate)) {
            
            standardDateFilter = reportMetadata.getStandardDateFilter();
            standardDateFilter.setStartDate(basicReportFilter.startDate);
            standardDateFilter.setEndDate(basicReportFilter.endDate);
            
        } else if(String.isNotEmpty(basicReportFilter.filterYear) && basicReportFilter.filterYear.isNumeric()){
            
            diffYear = System.today().year() - Integer.valueOf(basicReportFilter.filterYear);
            for(Reports.ReportFilter filter : reportMetadata.getReportFilters()) {
                if(filter.getColumn().contains(reportMetadata.getStandardDateFilter().getColumn())) {
                    filter.setOperator('equals');
                    filter.setValue(getRelativeYearFromYearDifference(diffYear));
                }
            }
            
        }
        
        return reportMetadata;
    }
    
    private static String getRelativeYearFromYearDifference(Integer diffYear) {
        String relativeYear = '';
        if(diffYear > 1) {
            relativeYear = 'LAST ' + diffYear + 'YEARS';
        } else if(diffYear < -1) {
            relativeYear = 'NEXT ' + diffYear + 'YEARS';
        } else if(diffYear == 1) {
            relativeYear = 'LAST YEAR';
        } else if(diffYear == -1) {
            relativeYear = 'NEXT YEAR';
        } else {
            relativeYear = 'THIS YEAR';
        }
        
        return relativeYear;
    }
    
    private static String parseDate(String dateString) {
        String resultDate = '';
        Date convertedDate = Date.parse(dateString);
        resultDate += convertedDate.month() + '/' + convertedDate.day() + '/' + convertedDate.year();
        return resultDate;
    }
    
    private static List<String> getRecordYears(List<Reports.GroupingValue> groupingValues) {
        List<String> result = new List<String>();
        Map<Integer, String> maxMinMap = new Map<Integer, String>();
        for(Reports.GroupingValue groupingValue : groupingValues) {
            maxMinMap.put(Integer.valueOf(groupingValue.getLabel().right(4)), groupingValue.getLabel());
        }
        List<Integer> minMaxYear = new List<Integer>();
        minMaxYear.addAll(maxMinMap.keySet());
        for(Integer index = minMaxYear.get(0); index <= minMaxYear.get(minMaxYear.size() -1); index++) {
            result.add(String.valueOf(index));
        }
        return result;
    }
    
    public class ReportResult {
        String key;
        String value;
        List<String> years;
        Id reportId;
        
        public ReportResult(String key, String value, List<String> years, Id reportId) {
            this.key = key;
            this.value = value;
            this.years = years;
            this.reportId = reportId;
        }
    }
    
    public class BasicReportFilter {
        Boolean isFilterApplied;
        String filterType;
        String filterYear;
        String startDate;
        String endDate;
        
        public BasicReportFilter(Boolean isFilterApplied, String filterType ,String filterYear, String startDate, String endDate) {
            this.isFilterApplied = isFilterApplied;
            this.filterType = filterType;
            this.filterYear = filterYear;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
    
    public class CustomReportFilter {
        Boolean isQuantityFilter;
        Decimal quantityFilter;
        Boolean isProductFilter;
        String productFilter;
        
        public CustomReportFilter(Boolean isQuantityFilter, Decimal quantityFilter ,Boolean isProductFilter, String productFilter) {
            this.isQuantityFilter = isQuantityFilter;
            this.quantityFilter = quantityFilter;
            this.isProductFilter = isProductFilter;
            this.productFilter = productFilter;
        }
    }
}