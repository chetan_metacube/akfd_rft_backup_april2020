public interface IAllotedStocksSelector extends fflib_ISObjectSelector {
    
    List<Alloted_Stock__c> selectByPIProduct(List<ProformaInvoice__c> proformaInvoices, Set<Id> productIds);
   
}