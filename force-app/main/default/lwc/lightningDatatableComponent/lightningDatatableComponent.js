import LightningDatatable from 'lightning/datatable';
import imageTableControl from './imageTableControl.html';

export default class LightningDatatableComponent extends LightningDatatable  {
    static customTypes = {
        image: {
            template: imageTableControl
        }
    };
}