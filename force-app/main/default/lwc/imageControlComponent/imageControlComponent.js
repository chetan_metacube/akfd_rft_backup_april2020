import { LightningElement,api } from 'lwc';

export default class ImageControlComponent extends LightningElement {
    @api url;
    @api altText;
}