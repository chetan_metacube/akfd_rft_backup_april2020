trigger InvoicedProductTrigger on Invoiced_Product__c (before update) {
    List<Id> productIds = new List<Id>();
    List<Id> proformaInvoiceIds = new List<Id>(); 
    for(Invoiced_Product__c invoicedProduct : Trigger.New) {
        productIds.add(invoicedProduct.Product__c);
        proformaInvoiceIds.add(invoicedProduct.Proforma_Invoice__c);
    }
    List<Alloted_Stock__c> allotedStocks = [Select Id, Product__c, ProformaInvoice__c, Stock__c From Alloted_Stock__c Where Product__c IN :productIds AND ProformaInvoice__c IN :proformaInvoiceIds];
    Map<Id, Alloted_Stock__c> invoicedProductAllotedStock = new Map<Id, Alloted_Stock__c>();
    for(Invoiced_Product__c invoicedProduct : Trigger.New) {
        for(Alloted_Stock__c allotedStock : allotedStocks) {
            if(allotedStock.Product__c == invoicedProduct.Product__c && allotedStock.ProformaInvoice__c == invoicedProduct.Proforma_Invoice__c) {
                if(invoicedProduct.Quantity__c > allotedStock.Stock__c) {
                    invoicedProduct.Quantity__c.addError('Invoiced Quantity should not be greater than product\'s booked quantity.(Booked Quantity: ' + allotedStock.Stock__c + ')');
                }
            }
        }
        //invoicedProduct.Last_Unit_Price__c = Trigger.oldMap.get(invoicedProduct.Id).Unit_Price__c;
    }
    System.debug('InvoicedProductTrigger ');
    
}