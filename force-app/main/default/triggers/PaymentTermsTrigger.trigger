trigger PaymentTermsTrigger on Payment_Term__c (before insert, before update, after update, before delete) {
	fflib_SObjectDomain.triggerHandler(PaymentTerms.class);
}