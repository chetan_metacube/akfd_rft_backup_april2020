trigger DefaultPaymentTermTrigger on Default_Payment_Term__c (before insert, before update) {
	fflib_SObjectDomain.triggerHandler(DefaultPaymentTerms.class);
}