trigger PaymentTransactionTrigger on Payment_Transaction__c (before insert, before update, after insert, after update, before delete) {
    fflib_SObjectDomain.triggerHandler(PaymentTransactions.class);
}