/*
* Trigger to update product's total stock field on insertion and updation of current stock of product warehouse
* Author : Yash Surana 
* Last Modified: 14 Mar 2019
*/
trigger WarehouseLineItemTrigger on Warehouse_Line_Item__c (before insert, before update, after insert, after update, before delete) {
    fflib_SObjectDomain.triggerHandler(WarehouseLineItems.class);
}