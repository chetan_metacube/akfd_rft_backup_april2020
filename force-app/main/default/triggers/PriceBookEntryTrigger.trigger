trigger PriceBookEntryTrigger on PriceBook_Product_Entry__c (before insert) {
    fflib_SObjectDomain.triggerHandler(PriceBookEntries.class);
}