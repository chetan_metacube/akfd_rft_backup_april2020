trigger ProformaInvoiceTrigger on ProformaInvoice__c (before insert, after insert, before update, after update, before delete) {
    fflib_SObjectDomain.triggerHandler(ProformaInvoices.class);
}