/*
 * Trigger to fire Email on addition of new projected stock 
 * Author : Gaurav Gupta , Yash Surana
 * Last Modified: 14 Mar 2019
*/
trigger ProjectedStockTrigger on Projected_Stock__c (before insert, after insert, before update, after update, before delete) {
    fflib_SObjectDomain.triggerHandler(ProjectedStocks.class);
}