trigger BankInfoTrigger on Bank_Info__c (before insert, before update) {
	fflib_SObjectDomain.triggerHandler(BankInfos.class);
}