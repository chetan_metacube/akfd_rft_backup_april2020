trigger CashOutflowTrigger on Cash_Outflow__c (before insert) {
    fflib_SObjectDomain.triggerHandler(CashOutflows.class);
}