trigger InvoiceTrigger on Invoice__c(after update, before delete) {
    fflib_SObjectDomain.triggerHandler(Invoices.class);
}