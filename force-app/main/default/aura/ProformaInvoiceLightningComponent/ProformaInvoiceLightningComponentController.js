({
    // Method to initialize the lightning component
    initAction : function(component, event, helper) {
        component.set('v.columns', 
                      [
                          {label: 'Id', fieldName: 'Id', type: 'text', hidden: true},
                          {label: 'Name', fieldName: 'Name', type: 'text', sortable: false},
                          {label: 'Product Name', fieldName: 'Product__r.Name', type: 'text', sortable: false},
                          {label: 'Product Id', fieldName: 'Product__c', type: 'text', hidden : true, sortable: false},
                          {label: 'Required Quantity', fieldName: 'Quantity__c', type: 'number', editable: true},
                          {label: 'Opportunity', fieldName: 'Opportunity__c', type: 'text', hidden: true},
                          {label: 'Opportunity Account', fieldName: 'Opportunity__r.AccountId', type: 'text', hidden: true},
                          {label: 'Product Image', fieldName: 'Product__r.Image__c', type: 'image'},
                          {label: 'Product GST', fieldName: 'Product__r.GST__c', type: 'number', hidden: true},
                          {label: 'Record Type', fieldName: 'RecordType.Name', type: 'text', hidden: true},
                          {label: 'Retail Price', fieldName: 'Retail_Price__c', type: 'number', hidden: true},
                          {label: 'Unit', fieldName: 'Unit__c', type: 'text', hidden: true}
                      ]);
        var columnss = component.get("v.columns");
        component.set("v.conversionRate", 1.0);
        component.set('v.objectApiName', 'PO_Product__c');
        
        var getOpportunityAction = component.get("c.getOpportunityRecordById");
        getOpportunityAction.setParams({"opportunityId" : component.get('v.recordId')});
        
        getOpportunityAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                if( response.getReturnValue() == null){
                    component.set("v.errorMessage",  "Insufficient privileges.");
                } else {
                    component.set("v.alreadyHasAcceptedProformaInvoice", false);
                    component.set("v.opportunity", response.getReturnValue());
                    component.set("v.accountCurrencyIsoCode", response.getReturnValue().Account.CurrencyIsoCode);
                    component.set("v.currencyIsoCode", component.get("v.accountCurrencyIsoCode"));
                    if(response.getReturnValue().RecordType.Name == 'Retail' || response.getReturnValue().RecordType.Name == 'Project') {
                        component.set("v.isExportProcess", false);
                    }
                }
            } else if(state === "ERROR") {
                if(response.getError()[0].message == 'Accepted/Revised PI already exists.') {
                    component.set("v.alreadyHasAcceptedProformaInvoice", true);
                    component.set("v.errorMessage", 'Accepted/Revised PI already exists.');
                }
            }
            component.set("v.loadBody", true);
        });
        $A.enqueueAction(getOpportunityAction); 
    },
    
    // Method to change current currency on Proforma Invoice, which will later reflet on opportunity if PI accepted.
    changeCurrency : function(component, event, helper) {
        // Get currency options and set to currecny options attribute
        let options = component.set("v.options");
        if(options == undefined || options == null || options.length == 0) {
            var getCurrencyFieldAction = component.get("c.getCurrencyIsoCodes");
            getCurrencyFieldAction.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") { 
                    if( response.getReturnValue() == null) {
                        component.set("v.errorMessage",  "Insufficient privileges.");
                    } else { 
                        var searchResult = JSON.parse(response.getReturnValue());
                        var options = [];
                        for(var index = 0 ; index < searchResult.length ; index++) {
                            var option = {};
                            option.label = searchResult[index].label;
                            option.value =  searchResult[index].value;
                            options.push(option);
                        }
                        component.set("v.options", options);
                        component.set("v.accountCurrency", new Map(options.map(i => [i.value, i.label])).get(component.get("v.accountCurrencyIsoCode")));
                        component.set('v.showChangeCurrencySection', true);
                    }
                } else {
                     component.set("v.errorMessage",  response.getError()[0].message);
                }
            });
            $A.enqueueAction(getCurrencyFieldAction); 
        } else {
            component.set('v.showChangeCurrencySection', true);
        }
    },
    
    // Method to create proforma invoice with selected desired products.
    createProformaInvoice : function(component, event, helper) {
        component.set("v.discardDraftvalues", true);
        var childComponent = component.find('productsTable');
        var selectedProducts = childComponent.get('v.tempSelectedRecords');
        // Validating Selected Products Array.
        if( typeof(selectedProducts) != 'undefined' && selectedProducts.length > 0) {
            // Validate against valid quantity of desired products.
            for(var index = 0; index < selectedProducts.length ; index++ ) {
                if(selectedProducts[index].Quantity__c <= 0) {
                    component.set('v.errorMessage' , 'Invalid quantity entered. Quantity should be greater than zero.');
                	return;
                }	
            }
            var createProformaInvoiceAction = component.get("c.generateProformaInvoice");
            createProformaInvoiceAction.setParams({"opportunity" : component.get('v.opportunity'), 
                                                   "desiredProducts" : selectedProducts, 
                                                   "ConversionRate" : component.get("v.conversionRate"), 
                                                   "CurrencyIsoCode" : component.get("v.currencyIsoCode")});
            createProformaInvoiceAction.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") { 
                    if( response.getReturnValue() == null){
                        component.set("v.pageMessage",  "Insufficient privileges.");
                    } else { 
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": response.getReturnValue(),
                            "slideDevName": "related"
                        });
                        navEvt.fire();
                    }
                } else if(state === 'ERROR'){
                    component.set('v.errorMessage' , response.getError()[0].message);
                }
                
            });
            $A.enqueueAction(createProformaInvoiceAction); 
        } else {
            component.set('v.errorMessage' , 'Please select atleast one Desired Product.');
        }
        
    },
    
    // Method to save selected currency by User temporarily and set the related variable to hide the section.
    saveCurrency : function(component, event, helper) {
        var conversionRate = component.get("v.conversionRate");
        if(conversionRate > 0.0 ) {
            component.set("v.showChangeCurrencySection", false);
            component.set("v.currencyIsoCode", component.get("v.selectedValue"));
            component.set("v.isModalOpen", false);
            component.set("v.errorMessage", "");
        } else {
            component.set("v.errorMessage", "Conversion rate should be greater than zero.");
        }
        
    },
    
    // Method to reset user selected currency, Conversion rate etc from change currency section.
    cancel : function(component, event, helper) {
        component.set("v.showChangeCurrencySection", false);
        component.set("v.isModalOpen", false);
        component.set("v.currencyIsoCode", component.get("v.accountCurrencyIsoCode"));
        component.set("v.selectedValue", component.get("v.accountCurrencyIsoCode"));
        component.set("v.conversionRate", 1);
        component.set("v.errorMessage", "");
    }
})