({
    // init Helper method to initialize lightning component 
    init : function(component) {
        var initAction = component.get("c.getProductWarehouses");
        initAction.setParams({"proformaInvoiceId" : component.get("v.recordId")});
        
        initAction.setCallback(this, function(response) {
            var state = response.getState();
            // Handle states 
            if (state === "SUCCESS") { 
                if( response.getReturnValue() == null){
                    component.set("v.errorMessage", response.getReturnValue());
                } else { 
                    var productWarehouseWrappers = JSON.parse(response.getReturnValue());
                    console.log('productWarehouseWrappers' + productWarehouseWrappers);
                    component.set("v.productWarehouseWrappers", productWarehouseWrappers);
                    
                    var allotments = new Map();
                    for(var index = 0 ; index < productWarehouseWrappers.length ; index++) {
                        for(var innerIndex = 0 ;  productWarehouseWrappers[index].productWarehouses != null && innerIndex < productWarehouseWrappers[index].productWarehouses.length ; innerIndex++) {
                            console.log('productWarehouses ', productWarehouseWrappers[index].productWarehouses[innerIndex]);
                            allotments.set(productWarehouseWrappers[index].productWarehouses[innerIndex].allotStockId, productWarehouseWrappers[index].productWarehouses[innerIndex].allotedStocks);
                        }
                    }
                    component.set("v.allotments", allotments);
                    var activeSections = [];
                    activeSections.push('section');
                    component.set("v.activeSections", activeSections);
                }
            } else if(state === "ERROR") {
                component.set("v.errorMessage", response.getError()[0].message);
                component.set("v.isAlloted", true);
            }
        });
        $A.enqueueAction(initAction);
    },
    
    // AllotStock helper method to finally allot stock from current stock and to allot projected stock. 
    allotStock : function(component, tempProductWarehouseWrappers) {
        var allotStockAction = component.get("c.allotStock");
        allotStockAction.setParams({"proformaInvoiceId" : component.get("v.recordId"), 
                                    "productWarehousesWrapperString" : JSON.stringify(tempProductWarehouseWrappers)});
        allotStockAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                window.open("/lightning/r/ProformaInvoice__c/" + component.get("v.recordId")+"/view" , "_self");
            } else if(state === "ERROR") {
                component.set("v.errorMessage", response.getError()[0].message);
                component.find("AddButton").getElement().disabled = false;
                component.find("ResetButton").getElement().disabled = false;
                component.find("CancelButton").getElement().disabled = false;
            }
        });
        $A.enqueueAction(allotStockAction);
    },
    
	// Method to show alloted stock for current product.
    getAllotedStock : function(component, event, helper) {
        var selectedAllotmentId = component.get("v.selectedAllotmentId");
        var currentProformaInvoiceId = component.get("v.recordId");
        var allotments = component.get("v.allotments");
        var allotedStock = allotments.get(selectedAllotmentId);
       
        // Splice current Proforma Invoice entry
        for(var index = 0; index < allotedStock.length ; index++) {
            if(allotedStock[index].proformaInvoiceId == currentProformaInvoiceId) {
                allotedStock.splice(index, 1); 
                break;
            }
        }
        component.set("v.allotedStocks", allotedStock);
    },
    
    // Method to show row actions at the time of component initailization.
    getRowActions : function (component, row, doneCallback) {
        var actions = [{
            'label': row['allotments'].toString(10),
            'iconName': 'utility:preview',
            'name': 'show_details'
        }];
        
        // Simulate a trip to the server
        setTimeout($A.getCallback(function () {
            doneCallback(actions);
        }), 200);
        
       
    }
})