({
    // Method to initailize controller.
    doInit : function(component, event, helper) {
        // Set all actions and columns.
        var actions = [
            { label: 'View Stock', name: 'viewStock', 'iconName': 'utility:preview' }
        ];
        var rowActions = helper.getRowActions.bind(this, component);
        component.set("v.warehouseTableColumns", 
                      [
                          {label: 'Warehouse Name', fieldName: "warehouseName", type: 'text', sortable: false},
                          {label: 'Location', fieldName: 'warehouseLocation', type: 'text', sortable: false , target : '_black', tooltip : 'Navigate'},
                          {label: 'Current Stock', fieldName: 'currentStock', type: 'number', sortable: false},
                          {label: 'Booked Stock', type: 'button', initialWidth: 150, class : 'slds-button', typeAttributes:
                           { label: { fieldName: 'allotments'}, title: 'View already booked quantities details.', name: 'view_details', iconName: 'utility:info' , disabled: {fieldName: 'isZeroBooked'}, iconPosition : 'right', 'class' : 'slds-button'}},
                          {label: 'Currently Booked', fieldName: 'currentAllotment', type: 'number'},
                          {label: 'Booking Quantity', fieldName: 'quantity', type: 'number', editable: true},
                      ]);
        component.set("v.projectedStockTableColumns", 
                      [
                          {label: 'Product Name', fieldName: "name", type: 'text', sortable: true},
                          {label: 'Projected Stock', fieldName: 'projectedStock', type: 'number', sortable: false },
                          {label: 'Warehouse Alloted', fieldName: 'isWarehouseAlloted', type: 'boolean'}
                      ]);
        component.set("v.bookedStockTableColumns", 
                      [
                          {label: 'Product', fieldName:"productUrl", type: 'url', typeAttributes: { label: { fieldName: 'productName' }, target: '_blank'}},
                          {label: 'PI', fieldName:"proformaInvoiceUrl", type: 'url', typeAttributes: { label: { fieldName: 'proformaInvoiceName' }, target: '_blank'}},
                          {label: 'Warehouse', fieldName:"warehouseUrl", type: 'url', typeAttributes: { label: { fieldName: 'warehouseName' }, target: '_blank'}},
                          {label: 'Stock', fieldName: 'stock', type: 'number'}
                      ]);
        // Call helper method which will get data from apex method.
        helper.init(component);
    },
    
    // Method to handle desired product row click and get current product alloted stock
    handleValueChange : function (component, event, helper) {
        var clickedProductWarehouseRow = component.get("v.clickedProductWarehouseRow");
        
        if(clickedProductWarehouseRow != undefined && clickedProductWarehouseRow.length > 0) {
            component.set("v.selectedAllotmentId", JSON.parse(clickedProductWarehouseRow).warehouseLineItem.Id);
        }
        
        helper.getAllotedStock(component, event, helper);
    },
    
    // Method contains logics related to warehouse bookings
    bookWarehouseQuantities : function(component, event, helper) {
        // If user is editing datatable and at the same time clicks on book stock button then value in current editing field is incorrect.
        // So we have to reset datatable draft values
        component.set("v.discardDraftvalues", true);
        var productWarehouseWrappers = component.get("v.productWarehouseWrappers");
        // Temporarily hold the warehouse wrapper data, because in case of reset we have to reset all values. 
        component.set("v.tempProductWarehouseWrappers" , JSON.parse(JSON.stringify( productWarehouseWrappers )));
        // Get all productdata tables.
        var productData  = component.find("productData");
        var productDataMap = new Map();
        // Set data in product data map, where key is product name and key is product data.
        if(productData != undefined && productData.length == undefined) {
            productDataMap.set(productData.get("v.records")[0].productName, productData);
        } else if(productData != undefined && productData.length > 0) {
            for(var index = 0 ; index < productData.length ; index++) {
                productDataMap.set(productData[index].get("v.records")[0].productName, productData[index]); 
            }
        }
        
        // Iterate products with warehouse entries and get selected warehouses and related data out of it.
        for(var index = 0 ; index < productWarehouseWrappers.length ; index++) {
            var selectedWarehouses = {};
            var allRecords = {};
            
            // Get selected records and all records
            if(productDataMap.get(productWarehouseWrappers[index].name) != undefined) {
                selectedWarehouses = productDataMap.get(productWarehouseWrappers[index].name).get("v.tempSelectedRecords");
                allRecords = productDataMap.get(productWarehouseWrappers[index].name).get("v.records");
            }
            
            var productWarehouses = selectedWarehouses;
            var productBookedQuantity = 0.0;
            // Iterate and validate selected warehouses against quantity.
            for(var i = 0 ; i < selectedWarehouses.length ; i++) {
                if(selectedWarehouses[i].quantity > 0 ) {
                    if(selectedWarehouses[i].currentStock - selectedWarehouses[i].allotments >= selectedWarehouses[i].quantity) {
                        productBookedQuantity += parseFloat(selectedWarehouses[i].quantity);
                        selectedWarehouses[i].isSelected = true;
                    } else {
                        component.set("v.errorMessage" , "Maximum booking quantity can be " + (selectedWarehouses[i].currentStock - selectedWarehouses[i].allotments) + " only for " + selectedWarehouses[i].productName + " at " + selectedWarehouses[i].warehouseName + ".");
                        return;
                    }
                } else {
                    component.set("v.errorMessage" , "Invalid Quantity Entered. Booking quantity should be greater than zero.");
                    return;
                }
            } 
            
            // Remove warehouse product entries which are not selected
            if(productDataMap.get(productWarehouseWrappers[index].name) != undefined) {
               productDataMap.get(productWarehouseWrappers[index].name).set("v.records", selectedWarehouses);
            }
			
            // Validate BookedQuantity against RequiredQuantity.
			if(productBookedQuantity > productWarehouseWrappers[index].requiredQuantity ) {
                component.set("v.errorMessage" , "Booking Quantity should be less than or equal to Required Quantity.");
                return;
            }
            
            // Set the final values in table if all validations passed.
            productWarehouseWrappers[index].bookedQuantity = productBookedQuantity;
            productWarehouseWrappers[index].projectedStock = productWarehouseWrappers[index].requiredQuantity - productWarehouseWrappers[index].bookedQuantity;
            productWarehouseWrappers[index].productWarehouses = productWarehouses;
            
            productWarehouseWrappers[index].isWarehouseAlloted =  productBookedQuantity > 0 ? true : false;
        }
        
        // Set all values if all validations passed.
        component.get("v.warehouseTableColumns")[component.get("v.warehouseTableColumns").length-1].editable = false;
        component.set("v.productWarehouseWrappers" , productWarehouseWrappers);
        component.set("v.isStockBooked" , true);
        component.set("v.isAlloted", true);
        component.set("v.hideCheckboxColumn" , true);
        component.set("v.emptyListMessage" , "No Stock Booked for this product");
        var warehouseTableColumns = component.get("v.warehouseTableColumns");
        warehouseTableColumns[5].editable = false;
        component.set("v.warehouseTableColumns", warehouseTableColumns);
        component.set("v.errorMessage" , "");
    },
    
    // Method to reset all warehouse bookings with initail ones.
    resetWarehouseBookings : function(component, event, helper) {
        component.set("v.productWarehouseWrappers" , component.get("v.tempProductWarehouseWrappers"));
        component.set("v.isStockBooked" , false);
        component.set("v.isAlloted", false);
        component.set("v.errorMessage" , '');
        var warehouseTableColumns = component.get("v.warehouseTableColumns");
        warehouseTableColumns[5].editable = true;
        component.set("v.hideCheckboxColumn" , false);
        component.set("v.emptyListMessage" , "No Stock Available for this product");
    },
    
    // Method to finally make apex controller call to allot booked and projected stocks to related desired products.
    allotStocksForDesiredProducts : function(component, event, helper) {
        component.find("AddButton").getElement().disabled = true;
        component.find("ResetButton").getElement().disabled = true;
        component.find("CancelButton").getElement().disabled = true;
       
        //Assign Final values
        var productWarehouseWrappers = component.get("v.productWarehouseWrappers");
        var tempProductWarehouseWrappers = component.get("v.tempProductWarehouseWrappers");
        
        
        // Setting all selected warehouse product related data
        for(var index = 0 ; index < tempProductWarehouseWrappers.length ; index++) {
            var allProductWarehouses = tempProductWarehouseWrappers[index].productWarehouses;
            var selectedProductWarehouses = productWarehouseWrappers[index].productWarehouses;
            var selectedProductWarehousesMap;
            if(selectedProductWarehouses.length == undefined) {
                selectedProductWarehousesMap = new Map();
            } else {
                selectedProductWarehousesMap = new Map(selectedProductWarehouses.map(obj => [ obj.allotStockId, obj ]));
            }
            if(allProductWarehouses != null) {
                for(var i = 0 ;  i < allProductWarehouses.length ; i++ ) {
                    if(selectedProductWarehousesMap.has(allProductWarehouses[i].allotStockId)) {
                        allProductWarehouses[i].quantity = selectedProductWarehousesMap.get(allProductWarehouses[i].allotStockId).quantity;
                        allProductWarehouses[i].isSelected = true;
                    } else {
                        allProductWarehouses[i].quantity = 0.0;
                    }
                }
            }    
            tempProductWarehouseWrappers[index].productWarehouses = allProductWarehouses;
            tempProductWarehouseWrappers[index].bookedQuantity = productWarehouseWrappers[index].bookedQuantity;
            tempProductWarehouseWrappers[index].projectedStock = productWarehouseWrappers[index].projectedStock;
            tempProductWarehouseWrappers[index].isWarehouseAlloted = selectedProductWarehousesMap.size > 0; 
        }
        helper.allotStock(component, tempProductWarehouseWrappers);
    },
    
    // Method to redirect to previous page.
    cancel : function (component, helper, event ) {
        window.open("/lightning/r/ProformaInvoice__c/" + component.get("v.recordId") + "/view" , "_self");
    },
    
    // Method to show allotment modal. 
    showAllotments: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        helper.getAllotedStock(component, event, helper);
        component.set("v.isOpen", true);
    },
    
    // Method to hide allotment model.
    hideAllotments: function(component, event, helper) {
        // for Hide/Close Model, set the "isOpen" attribute to "False"  
        component.set("v.isOpen", false);
    },
    
    // Method to handle delete action.
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'delete':
                helper.removeProduct(component, row);
                break;
        }
    }
    
    
})