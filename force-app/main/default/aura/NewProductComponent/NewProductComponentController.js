({
    clearError :  function(component, event, helper) {
        component.set("v.errorMessage", "");
        
    },
    
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    handleSuccess : function(component, event, helper) {
        component.set("v.isOpen", false);
        var newProductID = event.getParams().response.id;
        var cmpEvent = component.getEvent("addNewProduct"); 
        cmpEvent.setParams({"newProduct" : newProductID}); 
        cmpEvent.fire();
    },
    
    handleError : function(component, event, helper) {
        component.set("v.errorMessage", event.getParams().detail); 
    }
    
})