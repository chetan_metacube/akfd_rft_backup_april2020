({
    init : function(component, event, helper) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd='0'+dd;
        } 
        if(mm<10) {
            mm='0'+mm;
        } 
        today = yyyy + '-' + mm + '-'+ dd;
        var opportunityId;
        var actionPaymentTerms = component.get("c.getPaymentTerm");
        actionPaymentTerms.setParams({"termId" : component.get("v.recordId")});
        var actionInvoiceItems = component.get("c.getInvoicedItems");
        actionPaymentTerms.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                opportunityId = response.getReturnValue()[0].Opportunity__c;
                component.set("v.paymentTermName", response.getReturnValue()[0].Name);
                component.set("v.transaction.Amount__c", response.getReturnValue()[0].Balance_Amount__c);
                component.set("v.transaction.Conversion_Rate_To_INR__c", '1.00');
                component.set("v.transaction.Payment_Term__c", response.getReturnValue()[0].Id);
                component.set("v.transaction.Transaction_Date__c",today);
                if(response.getReturnValue()[0].RecordType.Name != 'Additional Payment Terms') {
                    actionInvoiceItems.setParams({"opportunityId" : opportunityId});
                    $A.enqueueAction(actionInvoiceItems);
                } else {
                    component.set("v.additional", true);
                }
            }
            else {
                //console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(actionPaymentTerms);
        // Add callback behavior for when response is received
        actionInvoiceItems.setCallback(this, function(response) {
            let invoices = [];
            var state = response.getState();
            if (state === "SUCCESS") {
                invoices = response.getReturnValue();
                for (let i = 0; i < invoices.length; i++) { 
                    invoices[i].Total_Price__c = parseFloat(invoices[i].Total_Price__c).toFixed(2);
                    invoices[i].Settled_Amount__c = parseFloat(invoices[i].Settled_Amount__c).toFixed(2);
                    invoices[i].Balance_Amount__c = parseFloat(invoices[i].Balance_Amount__c).toFixed(2);
                }
                component.set("v.invoices", invoices);
            }
            else {
                //console.log("Failed with state: " + state);
            }
        });
        
    },
    closeModal: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    save :function(component, event, helper) {
        let transactionAmount = component.get("v.transaction.Amount__c");
		let isAdditional = component.get("v.additional");
        if(transactionAmount == '' || transactionAmount <= 0) {
            component.set("v.pageMessage","Payment Amount should be positive and more than 0.");
            return;
        }
        if(component.get("v.related") == true) {
            //console.log('true');
            if(component.get("v.transaction.Invoiced_Item__c") == "") {
                component.set("v.pageMessage","Please select atleast one invoice.");
                return;
            }
        } else {
            //console.log('false');
            component.set("v.transaction.Invoiced_Item__c", "");
        }
        component.set("v.pageMessage","");
        //console.log('createTransaction');
        let tran = JSON.stringify(component.get("v.transaction"));
        //console.log('v.transaction ', tran);
        //console.log('v.transaction invoice ', component.get("v.transaction.Invoiced_Item__c"));
        let action = component.get("c.createTransaction");
        action.setParams({
            "data": tran
        });
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                //console.log('Transaction SUCCESS');
                /*var redUrl ="https://mydev--dev.lightning.force.com/one/one.app#/sObject/" + response.getReturnValue().Id + '/view';
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": redUrl
                });
                urlEvent.fire();*/
                
                let toastParams = {
                    title: "Success",
                    message: "New Transaction has been created.", // Default error message
                    type: "success"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                $A.get("e.force:closeQuickAction").fire();
                toastEvent.fire();
            } else if(state === "ERROR") {
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                //console.error(message);
                component.set("v.pageMessage",message);
            } else {
                //console.log('Transaction failed');
            }
        });
        $A.enqueueAction(action);
    },
    
    handleError :function(errorMessage) {
        component.set("v.pageMessage",errorMessage);
    },
    
    handleSuccess :function(toastParams) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        $A.get("e.force:closeQuickAction").fire();
        toastEvent.fire();
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
    ,
    
    goToUrl:function(component,event,helper){
        //console.log('Enter Here');
        var evt = $A.get("e.force:navigateToComponent");
        //console.log('evt'+evt);
        evt.setParams({
            componentDef: "c:InvoicedItemLookup",
            //componentAttributes :{ //your attribute}
        });
       
    evt.fire();
    },
    openmodal: function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    }

})