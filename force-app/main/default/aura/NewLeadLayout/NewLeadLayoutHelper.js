({
    // method to handle edit lead
    initialize : function(component, recordId){
        var getLeadAction = component.get("c.getData");
        //get existing lead data
        getLeadAction.setParams({"leadId" : recordId});
        getLeadAction.setCallback(this, function(response) {
            var state = response.getState();
            //set lead data if reponse is success
            if (state === "SUCCESS") {
                var resultData = JSON.parse(response.getReturnValue());
                var status = JSON.parse(resultData.status);
                component.set("v.options", status);
                if(recordId != null) {
                    component.set("v.LeadObj", resultData.lead);
                    component.set("v.selectedValue",  resultData.Status);
                } else {
                    var userId = $A.get("$SObjectType.CurrentUser.Id");
                    component.set("v.LeadObj.OwnerId", userId);
                    component.set("v.selectedValue",  "New");
                }
           } else if(state === "ERROR") {
                component.set("v.pageMessage", response.getError()[0].message);
                component.set("v.hasAccess]", false); 
            }
            component.set("v.loaded", true);
        });
        $A.enqueueAction(getLeadAction); 
    },
    // Method to Upsert lead data
    save : function(component, leadObj){
        var saveLeadAction = component.get("c.saveLead");
        saveLeadAction.setParam("leadObject", JSON.stringify(leadObj));
        saveLeadAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 var event = $A.get( 'e.force:navigateToSObject' );
                 event.setParams({
                            'recordId' : response.getReturnValue().Id
                        }).fire();
            } else if(state === "ERROR") {
                component.set("v.pageMessage", response.getError()[0].message); 
            }
        });
        $A.enqueueAction(saveLeadAction); 
    },
    //Method to check and handle errors if any
	handleErrors : function(component, leadObj) {
        component.set("v.pageMessage", "");
        this.checkAndAddErrors(component, "Title", leadObj.Title);
        this.checkAndAddErrors(component, "Account", leadObj.Account__c);
        this.checkAndAddErrors(component, "Contact", leadObj.Contact__c);
        this.checkAndAddErrors(component, "Lead Source", leadObj.Lead_Source__c);
        this.checkAndAddErrors(component, "Lead Receipt Date", leadObj.Lead_Receipt_Date__c);
        this.checkAndAddErrors(component, "Sales Process", leadObj.Sales_Process__c);
        
    },
    //Method to check for errors and add error on component
    checkAndAddErrors : function(component, fieldName, fieldValue) {
        var errorMessage = "";
        if(typeof(fieldValue) == "undefined"){
            errorMessage = "Please add " + fieldName + "\n";
        } else if(fieldValue.trim().length == 0) {
            errorMessage = "Please enter valid " + fieldName + "\n";
        }
        component.set("v.pageMessage",  component.get("v.pageMessage") + errorMessage);
    }
})