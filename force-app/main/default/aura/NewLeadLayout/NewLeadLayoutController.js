({
    doInit: function(component, event, helper) {
        // Prepare a new record from template
        var recordId = component.get("v.recordId");
        var cmpTarget = component.find('NatureOfQuery');
        $A.util.addClass(cmpTarget, 'NatureOfQuery');
        //check if lead record is existing
        helper.initialize(component, recordId);
    },
    // validates lead records and call save method
	save : function(component, event, helper) {
        //Apply validation on Lead Title, Account, Contact, LeadSource, Sales Process
        var leadObj = component.get("v.LeadObj");
        helper.handleErrors(component, leadObj);
        //save the record if no errors
        if(component.get("v.pageMessage").length == 0){
            console.log('in save');
            leadObj.Status = component.get("v.selectedValue");
            helper.save(component,leadObj);
        }
	},
    // closes the model and redirects to lead detail page 
    closeModel : function(component) {
        var leadHomeEvent;
        if(component.get("v.recordId") == null) {
            leadHomeEvent = $A.get("e.force:navigateToObjectHome");
            leadHomeEvent.setParams({
                "scope": "Lead"
            }).fire();
        } else {
            leadHomeEvent = $A.get( 'e.force:navigateToSObject' );
            leadHomeEvent.setParams({
                'recordId' : component.get("v.recordId")
            }).fire();
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        console.log('show spinner');
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        console.log('hide spinner');
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})