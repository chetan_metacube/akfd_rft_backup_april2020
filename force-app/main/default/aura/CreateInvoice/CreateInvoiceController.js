({
    // Method called when component is initiated
    init : function(component, event, helper) {
        console.log('init called');
        component.set('v.columns', 
                      [
                          {label: 'Product Name', fieldName: 'productName', type: 'text', sortable: false},
                          {label: 'Balanced Quantity', fieldName: 'balancedQuantity', type: 'number', sortable: false},
                          {label: 'Current Stock', fieldName: 'currentStock', type: 'number', hidden : false, sortable: false},
                          {label: 'Booked Stock', fieldName: 'bookedStock', type: 'number'},
                          {label: 'Booked Quantity(PI)', fieldName: 'bookedQuantity', type: 'number', hidden: false},
                          {label: 'Invoice Quantity', fieldName: 'Quantity', type: 'number', hidden: false, editable: true},
                          {label: 'Product Price', fieldName: 'productPrice', type: 'number', hidden: false}
                          
                      ]);
        helper.getProformaInvoice(component, component.get("v.recordId"));
        debugger;
    },
    // Method called for Invoice creation
    createInvoice : function(component, event, helper) {
        component.set("v.discardDraftvalues", true);
        component.set("v.errorMessage", '');
        var proformaInvoices = component.get("v.data");
        var validSelectedProducts = [];
        for(var index = 0 ; index < proformaInvoices.length ; index++) {
            var selectedProducts;
            // check length of the PI's returned
            // In case of single PI object is returned
            // In case of merged PI array of PI's is returned
            if(proformaInvoices.length == 1) {
                selectedProducts = component.find("proformaInvoicesTable").get("v.tempSelectedRecords");
            } else {
                selectedProducts = component.find("proformaInvoicesTable")[index].get("v.tempSelectedRecords");
            }
            //check if a product is selected
            if(selectedProducts.length == 0) {
                var error;
                if(index == 0) {
                    error = [{'message' : 'Please select a Desired Product from primary PO.'}];
                } else {
                    error = [{'message' : 'Please select a Desired Product from secondary PO.'}];
                }
                helper.handleError(component, error);
                break;
            } else {
                for(var i = 0 ; i < selectedProducts.length ; i++) {
                    //check for required quantity > 0
                    if(selectedProducts[i].Quantity <= 0) {
                        // Error message similar as aura exception from apex
                        var error = [{'message' : 'Quantity entered should be greater than 0 for ' + selectedProducts[i].proformaInvoiceName}];
                        helper.handleError(component, error);
                        //component.set("v.errorMessage", 'Quantity entered should be greater than 0.');
                        break;
                    } else if(selectedProducts[i].Quantity > selectedProducts[i].balancedQuantity) {
                        // validate if entered invoice quantity is greater than balanced quantity
                        var error = [{'message' : 'Invoice Quantity cannot be greater than Required Quantity for ' + selectedProducts[i].proformaInvoiceName}];
                        helper.handleError(component, error);
                        //component.set("v.errorMessage", 'Invoice Quantity cannot be greater than Required Quantity.');
                        break;
                    } else if(selectedProducts[i].Quantity > selectedProducts[i].bookedQuantity) {
                        //validate if entered invoice quantity is greater than booked quantity
                        var error = [{'message' : 'Invoice Quantity cannot be greater than Booked Quantity for ' + selectedProducts[i].proformaInvoiceName}];
                        helper.handleError(component, error);
                        //component.set("v.errorMessage","Invoice Quantity cannot be greater than Booked Quantity.");
                        break;
                    } else {
                        validSelectedProducts.push(selectedProducts[i]);
                    }
                }
            }
        }
        
        if(!component.get("v.errorMessage").length > 0) {
            // call helper for creating an Invoice
            debugger;
            helper.createInvoice(component, validSelectedProducts);
        }
    },
    //Method to merge PO and get details of the PO
    mergePO : function(component, event, helper) {
        component.set("v.errorMessage","");
        if(component.get("v.mergedOpportunity").length > 0) {
            for(var i = 0; i < component.get("v.data").length; i++) {
                let data = component.get("v.data")[i];
                var opportunityIds = [];
                opportunityIds.push(data.proformaInvoice.Opportunity__c);
            }
            //Check if same opportunity PO is merged twice
            if(opportunityIds.includes(component.get("v.mergedOpportunity"))) {
                component.set("v.errorMessage","Same Opportunity cannot be merged.");
                component.set("v.hideMergedOpportunitySection", true);
            } else {
                helper.getProformaInvoice(component, component.get("v.mergedOpportunity"));
                component.set("v.hideMergedOpportunitySection", true);
            }
        } else {
            component.set("v.errorMessage","Please select an opportunity.");
        }
    },
    showMergePO : function(component, helper) {
        component.set("v.errorMessage","");
        component.set("v.mergedOpportunity","")
        var data = component.get("v.data");
        component.set("v.currency", data[0].proformaInvoice.CurrencyIsoCode);
        component.set("v.recordTypeName", data[0].proformaInvoice.RecordType.Name);
        component.set("v.hideMergedOpportunitySection", false);
    },
    hideMergePO : function(component) {
        component.set("v.errorMessage","");
        component.set("v.hideMergedOpportunitySection", true);
    },
    backToOpportunity : function(component, event, helper) {
        window.open("/lightning/r/Opportunity/"+component.get("v.recordId")+"/view","_self");
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})