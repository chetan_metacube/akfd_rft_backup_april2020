({
    //Method to get Proforma Invoice and its products for an opportunity
    getProformaInvoice : function(component, opportunityId) {
        console.log('helper called');
    	var actionGetProformaInvoice = component.get("c.getProformaInvoices");
        actionGetProformaInvoice.setParams({"opportunityId" : opportunityId});
        actionGetProformaInvoice.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var parsedData = JSON.parse(response.getReturnValue());
                if(component.get("v.data").length == 0) {
                    component.set("v.accountId", parsedData[0].accountId);
                    component.set("v.data", parsedData);
                } else {
                    var data = component.get("v.data");
                    data.push(parsedData[0]);
                    component.set("v.data", data);
                }
            } else {

                component.set("v.hasPIError", true);
                this.handleError(component, response.getError());
            }
        });
        $A.enqueueAction(actionGetProformaInvoice);
	},
    //Method to create invoice by calling the controller function with selected products
    createInvoice : function(component, selectedProducts) {
        console.log('Hlper create Invoice' + selectedProducts);
        var selectedProformaInvoiceLineItems = JSON.stringify(selectedProducts);
        var actionGenerateInvoice = component.get("c.generateInvoice");
        actionGenerateInvoice.setParams({"selectedProformaInvoicesWrappers" : selectedProformaInvoiceLineItems});
        actionGenerateInvoice.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                window.open("/lightning/r/Invoice__c/" + response.getReturnValue() + "/view", "_self");
            }
        });
		$A.enqueueAction(actionGenerateInvoice);      
    },
    handleError : function(component, errors) {
        let message = 'Unknown error'; // Default error message
        
        // Retrieve the error message sent by the server
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        // Display the message
        component.set("v.errorMessage", message);
    }
})