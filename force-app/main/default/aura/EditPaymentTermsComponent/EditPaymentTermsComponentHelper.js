({
    getPaymentTerms : function(component) {
        let getPaymentTermsAction = component.get("c.getPaymentTerms");
        getPaymentTermsAction.setParams({"opportunityId" : component.get("v.recordId") });
        getPaymentTermsAction.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") { 
                if( response.getReturnValue() == null){
                    component.set("v.errorMessage", 'Error : ' + response.getReturnValue());
                } else { 
                    let paymentTermWrapper = JSON.parse(response.getReturnValue());
                    component.set("v.paymentTermWrappers",  paymentTermWrapper );
                    let paymentTerms = JSON.parse(JSON.stringify( paymentTermWrapper.paymentTerms ));
                    for(let index = 0 ; index < paymentTerms.length ; index++ ) {
                        paymentTerms[index].Payment_Amount__c = paymentTermWrapper.currencySymbol + ' ' + paymentTerms[index].Payment_Amount__c;
                    }
                    component.set("v.paymentTerms", paymentTerms);
                }
            } else if(state === "ERROR") {
                component.set("v.errorMessage", 'Error : ' + response.getError()[0].message);
            }
        });
        $A.enqueueAction(getPaymentTermsAction); 
    },
    
    // Method to make apex controller call to update payment terms
    saveHelper : function(component, event, paymentTerms) {
        /* Set variable values */
        let paymentTermWrappers = component.get("v.paymentTermWrappers");
        let paymentTermWrapper = paymentTermWrappers[0];
        paymentTermWrapper.paymentTerms = paymentTerms;
        
        /* Controller action instantiation and variable initialization */
        let updatePaymentTermsAction = component.get("c.updatePaymentTerm");
        updatePaymentTermsAction.setParams({ "opportunityId" : component.get("v.recordId"),
                                            "paymentTermWrapperString" : JSON.stringify(paymentTermWrapper)});
        
        /* CallBack to controller method */
        updatePaymentTermsAction.setCallback(this, function(response) {
            let state = response.getState();
            // Handle response states.
            if (state === "SUCCESS") {
                window.open("/lightning/r/Opportunity/" + component.get("v.recordId") + "/view" , "_self");
            } else if(state === "ERROR") {
                this.reset(component, event);
                component.set("v.errorMessage", response.getError()[0].message);
            }
        });
        $A.enqueueAction(updatePaymentTermsAction);
    },
    
    // Method to reset all payment terms data with initail ones
    reset : function(component, event) { 
        let paymentTerms = component.get("v.paymentTerms");
        let paymentTermWrappers = component.get("v.paymentTermWrappers");
        for(let index = 0 ; index < paymentTerms.length ; index++ ) {
            paymentTerms[index].Payment_Percentage__c = paymentTermWrappers[0].paymentTerms[index].Payment_Percentage__c;
        }
        component.set("v.errorMessage" , "");
        component.set("v.paymentTerms", paymentTerms);
    }
})