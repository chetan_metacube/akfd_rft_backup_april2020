({
    // Method to initialize component.
    init : function (component, event, helper) {
        component.set('v.paymentTermsListColumns', 
                      [		
                          {label: 'Payment Term Name', fieldName: 'Name', type: 'text', sortable: true},
                          {label: 'PAYMENT STATUS', fieldName: 'Payment_Status_New__c', type: 'text'},
                          {label: 'PAYMENT AMOUNT', fieldName: 'Payment_Amount__c', type: 'text'},
                          {label: 'PAYMENT PERCENTAGE', fieldName: 'Payment_Percentage__c', type: 'number', editable:true}
                      ]);
        
        helper.getPaymentTerms(component);
    },
    
    // Merhod to save user edited payment terms
    savePaymentTerms : function (component, event, helper) {
       /* Set variable values */
        let paymentTerms =  JSON.parse(JSON.stringify(component.get("v.paymentTerms")));
        let paymentTermWrappers = component.get("v.paymentTermWrappers");
        let totalPercentage = 0.0;
        component.set("v.errorMessage" , "");
        
        /* Checking against PaymentPercentage validations */
        for(let index = 0 ; index < paymentTerms.length ; index++ ) {
            if(paymentTermWrappers[0].paymentTerms[index].Payment_Percentage__c != paymentTerms[index].Payment_Percentage__c) {
                if(paymentTerms[index].Payment_Percentage__c <= 0) {
                    component.set("v.errorMessage" , "Payment term percentage should be greater than zero.");
                    return; 
                } else if(paymentTerms[index].Payment_Status_New__c != 'Due') {
                    component.set("v.errorMessage" , "Payment Percentage can be edited for Due Payment Terms Only.");
                    return; 
                } 
            }
            paymentTerms[index].Payment_Amount__c = parseFloat(paymentTerms[index].Payment_Amount__c.replace(paymentTermWrappers[0].currencySymbol + ' ' , ""));
            totalPercentage += parseFloat(paymentTerms[index].Payment_Percentage__c);
        }
        // Check whether payment percentage should be between 0 to 100 (boundary in cluded)
        if(totalPercentage > 100.00 || totalPercentage <= 0.00) {
            component.set("v.errorMessage" , 'Total Payment Percentage should be less than or equal to 100%.');
            return; 
        }
        helper.saveHelper(component, event, paymentTerms);
    },
    
    //Method to reset all component data with previous one.
    reset : function (component, event, helper) {
       helper.reset(component, event);
    },
    
    // Method to redirect to opportunity detail page
    cancel : function (component, event, helper) {
        window.open("/lightning/r/Opportunity/" + component.get("v.recordId") + "/view" , "_self");
    }
    

    
    
})