({
	removeProduct : function(component, row) {
		var rows = component.get('v.desiredProducts');
        var rowIndex = rows.indexOf(row);
        rows.splice(rowIndex, 1);
        component.set('v.desiredProducts', rows);
    }
})