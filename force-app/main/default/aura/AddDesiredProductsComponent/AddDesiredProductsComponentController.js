({
    init : function (component, event, helper) {
        component.set('v.productListcolumns', 
                      [		
                          {label: 'Id', fieldName: 'Id', type: 'text', hidden: true},
                          {label: 'Name', fieldName: 'Name', type: 'text', sortable: true},
                          {label: 'Product SKU', fieldName: 'Product_SKU__c', type: 'text'},
                          {label: 'UNIT', fieldName: 'Unit__c', type: 'text'},
                          {label: 'Status', fieldName: 'Status__c', type: 'picklist'},
                          {label: 'Category', fieldName: 'Family', type: 'picklist'}
                      ]);
        component.set('v.objectApiName', 'Product2');
        component.set('v.hideCheckboxColumn', false);
        component.set('v.allowSearching', true);
        component.set('v.tableHeader', '');
        component.set('v.parentLoaded', true);
        component.set('v.filterData', ' Opportunity__c = \''+ component.get('v.recordId') +'\' ');
        
        //Parameters of desired Product list
        var actions = [
            { label: 'Delete', name: 'delete' }
        ];
        component.set('v.desiredPorductListColumns', 
                      [
                          {label: 'Name', fieldName: 'Name', type: 'text'},
                          {label: 'Product SKU', fieldName: 'Product_SKU__c', type: 'text'},
                          {label: 'UNIT', fieldName: 'Unit__c', type: 'text'},
                          {label: 'Status', fieldName: 'Status__c', type: 'picklist'},
                          {label: 'Category', fieldName: 'Family', type: 'picklist'},
                          {label: 'Quantity', fieldName: 'Quantity__c', type: 'number', editable : true},
                          { type: 'action', typeAttributes: { rowActions: actions } }
                          
                      ]);
    },
    
    addSelectedProducts: function (component, event) {
        var childComponent = component.find('productsTable');
        var selectedProducts = childComponent.get('v.tempSelectedRecords');
        console.log('selectedProducts@@@' , selectedProducts);
        if(selectedProducts == 'undefined' || selectedProducts.length == 0 ) {
            component.set('v.errorMessageInHeader', 'Please select atleast one product.');
            component.set("v.errorMessage", "");
            return;
        }
        
        component.set('v.errorMessageInHeader', "");
        component.set("v.errorMessage", "");
        
        var desiredProducts = component.get("v.desiredProducts");
        var newlySelectedProducts = [];
        for(var i = 0; i < selectedProducts.length ; i++) {
            if(desiredProducts.indexOf(selectedProducts[i]) == -1) {
                selectedProducts[i].Quantity__c = 0;
                newlySelectedProducts.push(selectedProducts[i]);
            } else {
                component.set("v.errorMessageInHeader", "Some of the selected product(s) are already added.");
                component.set("v.errorMessage", "");
                return;
            }
        }
        for(var i = 0; i < newlySelectedProducts.length ; i++) {
            desiredProducts.push(newlySelectedProducts[i]);
        }
        
        component.set("v.desiredProducts", desiredProducts);
        console.log('test', component.get("v.desiredProducts"));
        childComponent.set('v.tempSelecteRecords', null);
    },
    
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'delete':
                helper.removeProduct(component, row);
                break;
        }
    },
    
    addNewProductToSelectedList: function (component, event, helper) {
        var productID = event.getParam("newProduct");
        var getNewProduct = component.get("c.getProductsByIDs");
        getNewProduct.setParams({"idInstance" : productID });
        getNewProduct.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var desiredProducts = component.get("v.desiredProducts");
                var newProduct = JSON.parse(response.getReturnValue());
                newProduct["category"] = newProduct["family"];
                newProduct = Object.fromEntries(   Object.entries(newProduct).map(([k, v]) => [k.toLowerCase(), v]) );
                var newProductToAdd = {};
                for(var i = 0 ; i <  component.get('v.desiredPorductListColumns').length -1 ; i++ ){
                    var row = component.get('v.desiredPorductListColumns')[i];
                    newProductToAdd[row.fieldName] = newProduct[row.label.toLowerCase().replace(/\s/g, "")];
                }
                desiredProducts.push(newProductToAdd);
                component.set("v.desiredProducts",desiredProducts);
            }else if(state === "ERROR") {
                component.set("v.errorMessage", response.getError()[0].message);
                component.set("v.errorMessageInHeader", "");
            }
        });
        $A.enqueueAction(getNewProduct); 
        
    },
    
    handleSaveEdition: function (component, event, helper) {
        if(!component.get("v.discardDraftvalues")) {
            var draftValues = event.getParam('draftValues');
            var desiredProducts = component.get("v.desiredProducts");
            var selectedProductsList = component.get("v.selectedProducts");
            for(var i = 0 ; i < draftValues.length ; i++) {
                desiredProducts[parseInt(draftValues[i].id.substr(4), 10)].Quantity__c = draftValues[i].Quantity__c;
            }
        }
        component.find("desiredProducts").set("v.draftValues", null);
        component.set("v.discardDraftvalues", false);
    },
    
    addDesiredProducts: function (component, event) {
        component.set("v.discardDraftvalues", true);
        var desiredProducts = component.get("v.desiredProducts");
        if(desiredProducts.length > 0) {
            var desiredProductsList = [];
            var errorMessage = '';
            for(var i = 0 ; i < desiredProducts.length ; i++) {
                if(desiredProducts[i].Quantity__c != null && desiredProducts[i].Quantity__c > 0 ){
                    var desiredProduct = {};
                    desiredProduct.id = desiredProducts[i].Id;
                    desiredProduct.name = desiredProducts[i].Name;
                    desiredProduct.stockKeepingUnit = desiredProducts[i].StockKeepingUnit;
                    desiredProduct.status = desiredProducts[i].Status__c;
                    desiredProduct.category = desiredProducts[i].Family;
                    desiredProduct.unit = desiredProducts[i].Unit__c;
                    desiredProduct.quantity = desiredProducts[i].Quantity__c;
                    desiredProductsList.push(desiredProduct);
                } else { 
                    errorMessage = 'Quantity should be positive.';
                    break;
                }
            }
            
            if(errorMessage.length == 0) {
                var selectedProductsString = JSON.stringify(desiredProductsList);
                var addDesiredProductAction = component.get("c.insertDesiredProducts");
                addDesiredProductAction.setParams({"opportunityId" : component.get("v.recordId"), 
                                                   "selectedProductsString" : selectedProductsString});
                addDesiredProductAction.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        if( response.getReturnValue() == null){
                            component.set("v.errorMessage", response.getReturnValue());
                            component.set("v.errorMessageInHeader", "");
                        } else { 
                            component.set("v.result", response.getReturnValue());
                            window.open("/lightning/r/Opportunity/" + component.get("v.recordId") + "/view" , "_self");
                        }
                    } else if(state === "ERROR") {
                        component.set("v.errorMessage", response.getError()[0].message);
                        component.set("v.errorMessageInHeader", "");
                    }
                });
                $A.enqueueAction(addDesiredProductAction); 
            } else {
                component.set("v.errorMessage", errorMessage);
                component.set("v.errorMessageInHeader", "");
            }
        } else {
            component.set("v.errorMessage", "Please add atleast one product.");
            component.set("v.errorMessageInHeader", "");
        }
    },
    
})