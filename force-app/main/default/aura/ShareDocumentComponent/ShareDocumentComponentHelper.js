({
	initHelper : function(component, event) {
        console.log("possibleRecipientssas " );
        var getPossibleRecipientsAction = component.get("c.getPossibleRecipients");
        
        getPossibleRecipientsAction.setParams({"opportunityId" : component.get("v.recordId"), "documentId" : component.get("v.documentId")});
        getPossibleRecipientsAction.setCallback(this, function(response) { 
            var state = response.getState();
            if (state === "SUCCESS") {  
                if( response.getReturnValue() == null){
                    component.set("v.errorMessage", 'Error : ' + response.getReturnValue());
                } else { 
                    var result = JSON.parse(response.getReturnValue());
                    component.set("v.possibleRecipients", result);
                    component.set("v.isDocumentAlreadyShared", result.isDocumentAlreadyShared);
               }
            } else if(state === "ERROR") {
                component.set("v.errorMessage", 'Error : ' + response.getError()[0].message);
             }
        });
        $A.enqueueAction(getPossibleRecipientsAction);
    },
    
    removeCustomEmail : function(component, row) {
		var rows = component.get('v.customEmails');
        var rowIndex = rows.indexOf(row);
        rows.splice(rowIndex, 1);
        component.set('v.customEmails', rows);
    },
    
    shareDocumentHelper : function(component, event, allRecipientsEmails) {
        var data = {};
        console.log('recipientEmails' + allRecipientsEmails)
        data.recipientEmails = allRecipientsEmails;
        console.log('Sales Process ' +  component.get("v.salesProcess"));
        data.salesProcess = component.get("v.salesProcess");
        data.attachmentId = component.get("v.attachmentId");
        data.templateName = component.get("v.templateName");
        data.docketNumber = component.get("v.docketNumber");
        data.documentId = component.get("v.documentId");
        data.targetObjectId = component.get("v.targetObjectId");
        data.isDocketNumberSharePage = component.get("v.isDocketNumberSharePage");
        data.isDocumentAlreadyShared = component.get("v.isDocumentAlreadyShared");
        data.isDocketNumberUpdated = (component.get("v.docketNumber") != component.get("v.initialDocketNumber"));
            
        var shareDocumentAction = component.get("c.share");
        shareDocumentAction.setParam("data",  JSON.stringify(data));
        shareDocumentAction.setCallback(this, function(response) { 
            var state = response.getState();
            if (state === "SUCCESS") {  
                if( response.getReturnValue() == null){
                    component.set("v.errorMessage", 'Error : ' + response.getReturnValue());
                } else { 
                    component.set("v.successMessage", response.getReturnValue());
                    component.set("v.warningMessage", "");
                }
            } else if(state === "ERROR") {
                component.set("v.errorMessage", 'Error : ' + response.getError()[0].message);
                component.set("v.warningMessage", "");
            }
        });
        $A.enqueueAction(shareDocumentAction);

        
    }
   
})