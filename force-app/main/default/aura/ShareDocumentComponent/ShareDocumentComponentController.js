({
    doInit : function(component, event, helper) {
        component.set("v.initialDocketNumber", component.get("v.docketNumber"));
        
        var actions = [
            { label: 'Delete', name: 'delete' }
        ];
        component.set('v.relatedContactsTableColumns', 
                      [
                          {label: 'Name', fieldName: "name", type: 'text', sortable: false},
                          {label: 'Email', fieldName: 'email', type: 'text', sortable: false},
                          {label: 'Type', fieldName: "recordTypeName", type: 'text', sortable: false},
                          {label: 'Department', fieldName: 'department', type: 'text'}
                      ]);
        component.set('v.usersTableColumns', 
                      [
                          {label: 'Name', fieldName: "Name", type: 'text', sortable: false},
                          {label: 'Email', fieldName: 'Email', type: 'text', sortable: false , target : '_black', tooltip : 'Navigate'},
                          {label: 'Role', fieldName: 'UserRole.Name', type: 'text', sortable: false}
                      ]);
        component.set('v.customEmailsColumns', 
                      [
                          {label: 'Email', fieldName: 'Email', type: 'text', sortable: false },
                          { type: 'action', typeAttributes: { rowActions: actions } }
                      ]);
        helper.initHelper(component, event);
        //debugger;
        var activeSections = [];
        activeSections.push("A");
        component.set("v.activeSections", activeSections);
        console.log('Active sections ' + component.get("v.activeSections"));
        
    },
    
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'delete':
                helper.removeCustomEmail(component, row);
                break;
        }
    },
    
    addToCustomEmailList : function(component, event, helper) {
        var customEmail = component.find("customEmail");
        
        if( customEmail.get("v.validity").valid ) {
            var tempCustomEmail = {};
            tempCustomEmail.Email = customEmail.get("v.value").trim();
            if(tempCustomEmail.Email.length == 0 ) {
                alert('Empty email entered.');
                return;
            }
            var customEmails = component.get("v.customEmails");
            var alreadyExists = false;
            for( var index = 0 ; index < customEmails.length ; index++ ) {
                if(customEmails[index].Email == tempCustomEmail.Email ) {
                    alreadyExists = true;
                    break;
                }
            }
            
            if(!alreadyExists) {
                customEmails.push(tempCustomEmail);
                component.set("v.customEmails", customEmails);
            } else {
                alert('Email already added');
            }
            
        } else {
             customEmail.set("v.messageWhenPatternMismatch", "Invalid email address.") ;
         }
    },
    
    shareDocument : function(component, event, helper) {
        var realtedContactsData = component.find("RelatedContactsData");
        var selectedRelatedContacts = realtedContactsData.get('v.tempSelectedRecords');
        var internalUsersData = component.find("InternalUsersData");
        var selectedInternalUsers = internalUsersData.get('v.tempSelectedRecords');
        var customEmails = component.get("v.customEmails");
        var docketNumber = component.get("v.docketNumber");
        
        if(JSON.parse(component.get("v.isDocketNumberSharePage")) && !docketNumber) {
            var errorMessage = "Please Add the Docket number.";
            component.set("v.errorMessage", errorMessage);
            return;
        }
        
        var allRecipientsEmails = [];
        if(selectedRelatedContacts.length == 0 && selectedInternalUsers.length == 0) {
            var errorMessage = "Primary Sharing list cannot be empty. Please select an email Id from Primary List.";
            component.set("v.errorMessage", errorMessage);
            return;
        }

        for(var index = 0 ; index < selectedRelatedContacts.length ; index++) { 
            allRecipientsEmails.push(selectedRelatedContacts[index].email);
        }
        
        for(var index = 0 ; index < selectedInternalUsers.length ; index++) { 
            allRecipientsEmails.push(selectedInternalUsers[index].Email);
        }
        
        for(var index = 0 ; index < customEmails.length ; index++) { 
            allRecipientsEmails.push(customEmails[index].Email);
        }
        component.set("v.allRecipientsEmails", allRecipientsEmails);
        
        var isDocumentAlreadyShared = component.get("v.isDocumentAlreadyShared");
        if(isDocumentAlreadyShared) {
            component.set("v.warningMessage", "Document has already been shared. Do you want to share again?");
            component.set("v.allRecipientsEmails", allRecipientsEmails);
        } else {
            helper.shareDocumentHelper(component, event, allRecipientsEmails); 
        }
    },
    
    redirectToDetailPage : function(component, event, helper) {
        window.open("/" + component.get("v.recordId") , "_self");
    },
    
    goBack : function(component, event, helper) {
        window.open("/" + component.get("v.recordId") , "_self");
    },
    
    shareAfterWarning : function(component, event, helper) {
        helper.shareDocumentHelper(component, event, component.get("v.allRecipientsEmails")); 
    },
    
    goToSharePage : function(component, event, helper) {
        component.set("v.warningMessage", "");
    },
    
    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
    }
    
})