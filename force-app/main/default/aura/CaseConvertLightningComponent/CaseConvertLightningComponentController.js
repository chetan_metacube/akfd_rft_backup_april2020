({
    // Method to initailize component
	doInit : function(component, event, helper) {
		helper.getCase(component);
	},
    
    //Method helps to convert lead
    convertToLead : function(component, event, helper) {
        helper.handleErrors(component);
        //If Error already exist then don't allow to convert
        if(component.get("v.pageMessage") != undefined && component.get("v.pageMessage").length == 0 && component.get("v.showForm")) {
            //helper.handleErrors(component);
            if(component.get("v.pageMessage").length == 0){
                helper.convert(component);
            }
        }
    },
    
    //Method to perform close action
    closeModal : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})