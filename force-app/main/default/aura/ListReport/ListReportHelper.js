({
	getReportList: function(component) {
        var action = component.get('c.getReports');
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.reports', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
      }
})