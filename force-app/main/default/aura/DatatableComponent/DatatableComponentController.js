({
    // Method to initilize lightning component.
    init : function (component, event, helper) {
        // If user need DatatableComponent to QueryData then this parameter will be true.
        if(component.get('v.queryData') == true) {
            // Set Variables
            let objectApiName =  component.get("v.objectApiName");
            // ObjectApiName validation check
            if(objectApiName != undefined && objectApiName != null && objectApiName.length > 0) {
                component.set('v.noOfRecords', 0);
                component.set('v.offset', 0);
                component.set('v.searchTermString', '');
                component.set('v.options', [ {'label' : '5', 'value' : 5},
                                            {'label' : '10', 'value' : 10},
                                            {'label' : '20', 'value' : 20},
                                            {'label' : '30', 'value' : 30}]);
                let columns = component.get('v.columns');
                let fieldApiNames = [];
                let imageFieldName = '';
                // Remove hidden fields to query
                for( let i = 0 ; i < columns.length ; i++) {
                    fieldApiNames.push(columns[i].fieldName);
                    if(columns[i].type == 'image')
                    {
                        imageFieldName = columns[i].fieldName;
                    }
                    if(columns[i].hasOwnProperty('hidden')) {
                        columns.splice(i--, 1);
                    }
                }
                // Check whether there is any layout visible field entered or not in columns.
                component.set('v.fieldApiNames', fieldApiNames);
                component.set('v.imageFieldName', imageFieldName);
                if(fieldApiNames != undefined && fieldApiNames != null && fieldApiNames.length > 0) {
                    helper.getData(component, component.get('v.offset'));
                } else {
                    component.set("v.errorMessage", "Please enter some visible fields to show in datatable.");
                }
            } else {
                component.set("v.errorMessage", "ObjectApiName is invalid.");
            }
        }
    },
    
    // Method to Search Records from datatable.
    searchRecords : function (component, event, helper) { 
        // Get search string and get records accordingly.
        let searchString = component.find('searchTerm').get('v.value');
        if(searchString != undefined && searchString.length > 0) {
            component.set("v.searchTermString", component.find('searchTerm').get('v.value'));
            helper.getData(component, 0);
            if(component.get("v.errorMessage") == undefined || component.get("v.errorMessage").length == 0 ) {
                component.set("v.offset", 0);
            }
        } else {
            alert("Please enter search term.");  
        }
    },
    
    // Method to reset table data.
    reset: function (component, event, helper) {
        // Reset search term and getData to reset table data
        var queryTerm = component.find('searchTerm');
        queryTerm.set('v.value', '');
        component.set('v.searchTermString', '');
        helper.getData(component, 0);
        if(component.get("v.errorMessage") == undefined || component.get("v.errorMessage").length == 0 ) {
            component.set("v.offset", 0);
        }
    },
    
    // Method to update selected Rows count on datatable.
    updateSelectedRowsCount: function (component, event) {
        let selectedRows = event.getParam('selectedRows');
        component.set('v.selectedRowsCount', selectedRows.length);
        component.set("v.tempSelectedRecords", event.getParam('selectedRows'));
    },
        
    // Method to search records when enter key is pressed.
    handleKeyUp : function (component, event, helper) {
        var isEnterKeyPressed = event.keyCode === 13;
        if (isEnterKeyPressed) {
            component.set("v.searchTermString", component.find('searchTerm').get('v.value'));
            helper.getData(component, 0);
            if(component.get("v.errorMessage") == undefined || component.get("v.errorMessage").length == 0 ) {
                component.set("v.offset", 0);
            }
        }
    },
    
    // Method to handle editing in row column
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        component.set("v.actionRowRecord", JSON.stringify(row));
        component.set("v.actionPerformed", true);
    },
    
    // Update table according to noOfRecords changed
    updateTable : function (component, event, helper) {
        // Set table parameter
        component.set("v.recordsPerPage",  parseInt(component.get("v.recordsPerPage"), 10));
        component.set("v.maxRowSelection",  parseInt(component.get("v.recordsPerPage"), 10));
        component.set("v.searchTermString", component.find('searchTerm').get('v.value'));
        helper.getData(component, 0);
        if(component.get("v.errorMessage") == undefined || component.get("v.errorMessage").length == 0 ) {
            component.set("v.offset", 0);
        }
    },
    
    // Get next noOfRecords 
    getNext : function (component, event, helper) {
        component.set('v.offset', component.get('v.offset') + component.get('v.recordsPerPage'));
        helper.getData(component, component.get('v.offset'));
        if(component.get("v.errorMessage") != undefined && component.get("v.errorMessage").length > 0 ) {
            component.set('v.offset', component.get('v.offset') - component.get('v.recordsPerPage'));
        }
    },
    
    // Get previous noOfRecords 
    getPrevious: function (component, event, helper) {
        component.set('v.offset', component.get('v.offset') - component.get('v.recordsPerPage'));
        helper.getData(component, component.get('v.offset'));
        if(component.get("v.errorMessage") != undefined && component.get("v.errorMessage").length > 0 ) {
            component.set('v.offset', component.get('v.offset') + component.get('v.recordsPerPage'));
        }
        
    },
    
    // Method to sort table data
    sort: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // Assign the latest attribute with the sorted column fieldName and sorted direction
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    
    // Method to handle row save action
    handleSaveEdition: function (component, event, helper) {
        debugger;
        if(!component.get("v.discardDraftvalues")) {
            let draftValues = event.getParam('draftValues');
            let selectedProductsList = component.get("v.records");
            for(let i = 0 ; i < draftValues.length ; i++) {
                let keys = Object.keys(draftValues[i]);
                for(let keyIndex = 0 ; keyIndex < keys.length ; keyIndex++ ) {
                    if((keys[keyIndex]).toUpperCase() != "id".toUpperCase()){ 
                        selectedProductsList[parseInt(draftValues[i]["id"].substr(4), 10)][keys[keyIndex]] = draftValues[i][keys[keyIndex]];
                    }
                }
            }
        }
        component.find("genericDataTable").set("v.draftValues", null);
        component.set("v.discardDraftvalues", false);
        
    },
    
    // Method to handle cancel action=
    handleCancelEdition: function (component, event, helper) {
        var draftValues = event.getParam('draftValues');
    }
    
    
})