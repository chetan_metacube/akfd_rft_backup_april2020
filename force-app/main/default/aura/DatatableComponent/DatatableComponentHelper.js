({
    getData : function(component, offset) {
        var getDataAction = component.get("c.getRecords");
        var datatableDTO = {
            "recordsPerPage" :  component.get("v.recordsPerPage"),
            "objectApiName" : component.get("v.objectApiName"),
            "objectFields" : component.get("v.fieldApiNames"),
            "filter" : component.get("v.filter"),
            "searchTerm" : component.get("v.searchTermString"),
            "currentOffset" : offset 
        };
        
        getDataAction.setParams({"datatableDTOString" : JSON.stringify(datatableDTO) });
        getDataAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                if( response.getReturnValue() == null){
                    component.set("v.errorMessage",  "Insufficient privileges.");
                } else { 
                    var searchResult = JSON.parse(response.getReturnValue());
                    var imageFieldName= component.get("v.imageFieldName");
                    if(imageFieldName != '' && searchResult.records) {
                       searchResult.records.forEach(function(item){
                           if(item[imageFieldName]) {
                               var imageHtml = new DOMParser().parseFromString(item[imageFieldName], "text/xml");
                               var imageTag = imageHtml.getElementsByTagName('p')[0].innerHTML.replace(/&amp;/g, '&'); 
                               var regularExp = /<img[^>]+src="([^">]+)/g;
                               var url = regularExp.exec(imageTag);
                               item[imageFieldName] = url[1];
                           } else {
                               item[imageFieldName] = component.get("v.noImageURL");
                           }
                       });
                    }
                    component.set("v.records", searchResult.records);

                    component.set("v.noOfRecords", searchResult.totalRecordCount);
                }
            } else {
                component.set("v.errorMessage", response.getError()[0].message);
            }
        });
        $A.enqueueAction(getDataAction); 
        
    },
    
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.records");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse));
        component.set("v.records", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }

})