({
    init : function(component) {
        component.set('v.columns', 
                      [
                          {label: 'Id', fieldName: 'Id', type: 'text', hidden : true},
                          {label: 'Desired Product Number', fieldName: 'Name', type: 'text', sortable: false},
                          {label: 'Product Name', fieldName: 'Product__r.Name', type: 'text', sortable: false},
                          {label: 'Product Id', fieldName: 'Product__c', type: 'text', hidden : true, sortable: false},
                          {label: 'Required Quantity', fieldName: 'Quantity__c', type: 'number', editable: true},
                          {label: 'Opportunity', fieldName: 'Opportunity__c', type: 'text', hidden: true},
                          {label: 'Product Image', fieldName: 'Product__r.Image__c', type: 'text', hidden: true},
                          {label: 'Product GST', fieldName: 'Product__r.GST__c', type: 'number', hidden: true},
                          {label: 'Record Type', fieldName: 'RecordType.Name', type: 'text', hidden: true},
                          {label: 'Retail Price', fieldName: 'Retail_Price__c', type: 'number', hidden: true},
                          {label: 'Unit', fieldName: 'Unit__c', type: 'text', hidden: true}
                          
                      ]);
        component.set('v.parentLoaded', true);
    },
    //Save Method to validate errors and call helper
    save : function(component, event, helper) {
        component.set("v.discardDraftvalues", true);
        component.set("v.pageMessage", '');
        var selectedRecords = component.find('dataTable').get('v.tempSelectedRecords');
        //check if no product is selected
        if(selectedRecords.length == 0) {
            component.set("v.pageMessage", 'Please select a Desired Product.');
        } else {
            for(var i = 0; i < selectedRecords.length; i++) {
                //check if quantity is greater than 0
                if(selectedRecords[0].Quantity__c <= 0) {
                    component.set("v.pageMessage", 'Quantity entered should be greater than 0.');
                }
            }
            if(!component.get("v.pageMessage").length > 0) {
                helper.save(component);
            }
        }
    },
    // Method to close modal
    closeModal: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})