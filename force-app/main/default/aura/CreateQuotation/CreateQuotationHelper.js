({
	save : function( component) {
		// create quotation action method
        var actionCreateQuotation = component.get("c.createQuotation");
        var desiredProducts =  JSON.stringify(component.find('dataTable').get('v.tempSelectedRecords'));
        console.log(component.find('dataTable').get('v.tempSelectedRecords'));
        // action method's callback
        actionCreateQuotation.setParams({"opportunityId" : component.get("v.recordId"), 
                                         "desiredProducts" : desiredProducts});
        actionCreateQuotation.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //$A.get("e.force:closeQuickAction").fire();
                window.open("/lightning/r/Quotation__c/" + response.getReturnValue() + "/view", "_self");
            } else if(state === "ERROR") {
                this.handleError(component, response.getError());
            }
        });
        $A.enqueueAction(actionCreateQuotation);
	},
    
    handleError : function(component, errors) {
        var message = '';
        
        // Retrieve the error message sent by the server
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        // Display the message
        component.set("v.pageMessage", message);
    }
})