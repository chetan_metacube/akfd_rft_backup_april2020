({
	init : function (component, event, helper) {
       /* component.set('v.columns', 
                      [
                          {label: 'Id', fieldName: 'Id', type: 'text', sortable: true, hidden: true},
                          {label: 'Name', fieldName: 'Name', type: 'text', sortable: true, hidden: true},
                          {label: 'AKFD SKU', fieldName: 'StockKeepingUnit', type: 'text'},
                          {label: 'UNIT', fieldName: 'Unit__c', type: 'text'},
                          {label: 'Status', fieldName: 'Status__c', type: 'picklist'},
                          {label: 'Category', fieldName: 'Family', type: 'picklist'}
                      ]);
                     */ 
         component.set('v.columns', 
                      [
                          {label: 'Id', fieldName: 'Id', type: 'text', hidden: true},
                          {label: 'Name', fieldName: 'Name', type: 'text', sortable: false},
                          {label: 'Product Name', fieldName: 'Product__r.Name', type: 'text', sortable: false},
                          {label: 'Product Id', fieldName: 'Product__c', type: 'text', hidden : true, sortable: false},
                          {label: 'Quantity', fieldName: 'Quantity__c', type: 'number', editable: true},
                          {label: 'Opportunity', fieldName: 'Opportunity__c', type: 'text', hidden: true},
                          {label: 'Opportunity Account', fieldName: 'Opportunity__r.AccountId', type: 'text'},
                          {label: 'Product Image', fieldName: 'Product__r.Image__c', type: 'text', hidden: true},
                          {label: 'Product GST', fieldName: 'Product__r.GST__c', type: 'number', hidden: true},
                          {label: 'Record Type', fieldName: 'RecordType.Name', type: 'text', hidden: true},
                          {label: 'Retail Price', fieldName: 'Retail_Price__c', type: 'number', hidden: true},
                          {label: 'Unit', fieldName: 'Unit__c', type: 'text', hidden: true}
                          
                      ]);
                      
        /*
         component.set('v.columns', 
                      [
                          {label: 'Id', fieldName: 'Id', type: 'text'},
                          {label: 'Name', fieldName: 'Name', type: 'text', sortable: false},
                          {label: 'Quantity', fieldName: 'Quantity__c', type: 'text', editable: true},
                          {label: 'Product Name', fieldName: 'Product__r.Name', type: 'text', sortable: false}
                          
                      ]);
          */            
        console.log('comumns' + component.get('v.columns')[0].fieldName);
        component.set('v.objectApiName', 'PO_Product__c');
        component.set('v.hideCheckboxColumn', false);
        component.set('v.allowSearching', true);
        component.set('v.tableHeader', 'Select Desired Products');
        component.set('v.parentLoaded', true);
        component.set('v.filterData', ' Opportunity__c = \'0069D0000028y8bQAA\' ');
        console.log('filterData ' + component.get('v.filterData'));
        
        
        
    },
})