({
    // Method to initailize lead. 
    init : function(component, event, helper) {
        helper.getLead(component, event, helper);
    },
    
    // Method to close modal.
    closeModal : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    // Method to convert lead.
    convert : function(component, event, helper) {
        // Apply validations
        let opportunityName = component.get("v.opportunityName");
        let leadRecord = component.get("v.lead");
        if(opportunityName === '' || opportunityName === undefined) {
            component.set("v.pageMessage" , "Opportunity name should not be empty.");
            return;
        }
        if(leadRecord.Merchant__c == '' || leadRecord.Merchant__c == undefined) {
            component.set("v.pageMessage" , "Please select company.");
            return;
        }
        component.set("v.pageMessage" , "");
        
        // If all validations passed make apex call to convert lead.
        helper.convertLeadToOpportunity(component, event, helper);
        
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
})