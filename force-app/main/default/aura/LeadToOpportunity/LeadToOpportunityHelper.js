({
    // Method to get lead data from backend
    getLead : function(component, event, helper) {
        var leadId;
        var actionGetLead = component.get("c.getData");
        actionGetLead.setParams({"leadId" : component.get("v.recordId")});
        actionGetLead.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {	
                var resultData = JSON.parse(response.getReturnValue());
                if(resultData.lead.Status == 'Qualified') {
                    component.set("v.isValid", false);
                    component.set("v.pageMessage", 'Qualified leads cannot be converted to Opportunity.');
                }
                component.set("v.lead", resultData.lead);
            } else if(state === "ERROR") {
                helper.handleError(component, response.getError());
                component.set("v.isValid", false);
            } else {
                component.set("v.pageMessage", "Something went wrong!!!");
            }
        });
        $A.enqueueAction(actionGetLead);
    },
    
    // Method to convert lead to opportunity
    convertLeadToOpportunity : function(component, event, helper) {
        let action = component.get("c.createOpportunity");
        action.setParams({
            "lead": JSON.stringify(component.get("v.lead")),
            "opportunityName": component.get("v.opportunityName")
        });
        action.setCallback(this, function(response){
            let state = response.getState();
            console.log('Request Response state '  + state);
            if (state === "SUCCESS") {
                var sObectEvent = $A.get("e.force:navigateToSObject");
                sObectEvent .setParams({
                    "recordId": response.getReturnValue().Id  ,
                    "slideDevName": "Details"
                });
                sObectEvent.fire(); 
            } else if(state === "ERROR") {
                helper.handleError(component, response.getError());
            } else {
                component.set("v.pageMessage", "Something went wrong!!!");
            }
        });
        $A.enqueueAction(action);
    },
    
    // Method to handle errors 
    handleError : function(component, errors) {
        let message = 'Unknown error'; // Default error message
        
        // Retrieve the error message sent by the server
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        
        // Display the message
        component.set("v.pageMessage", message);
    }
})