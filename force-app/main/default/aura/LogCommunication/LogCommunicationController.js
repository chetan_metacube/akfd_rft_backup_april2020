({
    //Method called on component inititalization
	init : function(component, event, helper) {
        helper.getCommunicationFieldValues(component, event, helper);
	},
    //Method which validates Communication record and calls helper to insert it
    save : function(component, event, helper) {
        if((component.get("v.communication.Contact__c") === '' || component.get("v.communication.Contact__c") === undefined)) {
            component.set("v.contactLookupError", true);
            return;
        } else {
            component.set("v.contactLookupError", false);
            component.set("v.communication.Type__c", component.get("v.selectedType"));
            component.set("v.communication.Opportunity_Stages__c", component.get("v.selectedStage"));
            helper.save(component,event, helper);
        }
	},
    //Method to close the Modal
    closeModal: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        document.getElementById("Accspinner").style.display = "block";

    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        document.getElementById("Accspinner").style.display = "none";

    }
})