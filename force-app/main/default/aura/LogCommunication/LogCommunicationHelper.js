({
    // Method to get the picklist values for communication
    getCommunicationFieldValues : function(component, event, helper) {
        console.log('helper called');
		var actionCommunicationValues = component.get("c.getCommunicationFieldValues");
        actionCommunicationValues.setParams({"opportunityId" : component.get("v.recordId")});
        actionCommunicationValues.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                if( response.getReturnValue() != null){
                    var picklistResults = JSON.parse(response.getReturnValue());
                    //setting the picklist values
                    component.set("v.communication.Type__c",  picklistResults.communicationType);
                    component.set("v.communication.Opportunity_Stages__c",  picklistResults.opportunityStage);
                    component.set("v.communication.AccountName",  picklistResults.currentOpportunity.Account.Name);
                    component.set("v.communication.OpportunityName",  picklistResults.currentOpportunity.Name);
                    component.set("v.communication.Account__c",  picklistResults.currentOpportunity.AccountId);
                    component.set("v.communication.Opportunity__c", component.get("v.recordId"));
                }
            } else {
                this.handleError(component, response.getError());
            }
        });
        $A.enqueueAction(actionCommunicationValues);
    },
    //Method to create communication record
    save : function(component, event, helper) {
        if(component.find("type").get("v.value") === '') {
            component.set("v.communication.Type__c",'Feedback');
        } else {
            component.set("v.communication.Type__c",  component.find("type").get("v.value"));
        }
        if(component.find("stage").get("v.value") === '') {
            component.set("v.communication.Opportunity_Stages__c",'Client Setup');
        } else {
            component.set("v.communication.Opportunity_Stages__c",  component.find("stage").get("v.value"));
        }
        var actionSaveCommunication = component.get("c.saveCommunication");
        var communicationObject =  JSON.stringify(component.get("v.communication"));
        console.log(communicationObject);
        actionSaveCommunication.setParams({"communication" : communicationObject});
        actionSaveCommunication.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                if( response.getReturnValue() != null){
                    let toastParams = {
                    title: "Success",
                    message: "New Communication has been created.", // Default error message
                    type: "success"
                };
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                $A.get("e.force:closeQuickAction").fire();
                toastEvent.fire();
                }
            } else {
                this.handleError(component, response.getError());
            }
        });
        $A.enqueueAction(actionSaveCommunication);
    },
    
    handleError : function(component, errors) {
        let message = 'Unknown error'; // Default error message
        
        // Retrieve the error message sent by the server
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        console.log('messgae'+message);
        // Display the message
        component.set("v.pageMessage", message);
    }
})