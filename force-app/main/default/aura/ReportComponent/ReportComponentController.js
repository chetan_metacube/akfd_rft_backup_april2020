({
    afterScriptsLoaded : function(component, event, helper) {
        var filterTypes = component.get("v.filterTypes");
        filterTypes = [ 
            {id: 'MONTH', label: 'Monthly'},
            {id: 'QUARTER', label: 'Quarterly'},
            {id: 'YEAR', label: 'Yearly'},
            {id: 'DAY', label: 'Custom Range'}
        ];
        var filterYears = component.get("v.filterYears");
        filterYears.push('All Years');
        component.set("v.filterYears", filterYears);
        component.set("v.isLoading", true);
        component.set("v.filterTypes", filterTypes);
        component.set("v.ready", true);
        component.set("v.chartMap", new Map());
        helper.getChart(component, event, helper);
    },
    applyFilter: function(component, event, helper) {
        if(component.get("v.isProductFilter") && (component.get("v.productFilter") ===	 null || component.get("v.productFilter") === "")) {
            component.set("v.productLookupError", true);
            return;
        } else {
            component.set("v.productLookupError", false);
        }
        if(component.get("v.isProductFilter") && (component.get("v.productFilter") !==	 null || component.get("v.productFilter") !== "")) {
            if(component.get("v.productFilter") !== component.get("v.selectedProduct")) {
                component.set("v.seletedFilter", 'MONTH');
                component.set("v.yearFilter", 'All Years');
            }
        }
        if(component.get("v.seletedFilter") === 'DAY') {
            var startDate = component.get("v.startDate");
            var endDate = component.get("v.endDate");
            var endDateInputField = component.find("endDate");
            
            if(endDate < startDate) {
                endDateInputField.setCustomValidity('End date should not be less than start date.');	
                endDateInputField.reportValidity();
                return;
            } else {
                endDateInputField.setCustomValidity('');	
                endDateInputField.reportValidity();
            }
        }
        component.set("v.ready", true);
        component.set("v.isLoading", true);
        helper.getChart(component, event, helper); 
    },
    viewReport: function(component, event, helper) {
        var recordId = event.getSource().get("v.name");
        var url = "/" + recordId;
        if(component.get("v.isProductFilter")) {
            var productName = component.get("v.productFilter");
            url = "/lightning/r/Report/" + recordId + "/view?fv1=" + productName;
        }
        window.open(url, '_blank');
    },
    onRender: function(component, event, helper) {
        console.log('hello');
    }
    
})