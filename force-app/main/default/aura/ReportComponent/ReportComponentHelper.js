({
    
    
    getChart : function (component, event, helper) {
        var ready = component.get("v.ready");
        var isQuantityFilter = component.get("v.isQuantityFilter");
        var isProductFilter = component.get("v.isProductFilter");
        var quantityFilter = component.get("v.quantityFilter");
        var productFilter = component.get("v.productFilter");
        var yearFilter = component.get("v.yearFilter");
        var filterType = component.get("v.seletedFilter");
        var filter = component.get("v.filter");
        var filterYears = component.get("v.filterYears");
        var startDate = component.get("v.startDate");
        var endDate = component.get("v.endDate");
        var reportNames = component.get("v.reportNames");
        var chartType = component.get("v.chartType");
        var reportIds = component.get("v.reportIds");
        reportIds = [];
        if (ready === false) {
            return;
        }
        
        if(filterType !== 'DAY') {
            startDate = "";
            endDate = "";
        } else {
            yearFilter = "";
        }
        
        var action = component.get("c.getReports");
        
        var basicReportFilter = {
            "startDate": startDate,
            "isFilterApplied": filter,
            "filterYear": yearFilter,
            "filterType": filterType,
            "endDate": endDate
        };
        
        var customReportFilter = {
            "isQuantityFilter": false, 
            "quantityFilter": 0.00, 
            "isProductFilter": false,
            "productFilter": ""
        };
        
        var colors = ['#36a2eb', 
                      '#ff6384', 
                      '#76ded9',
                      '#f67019',
                      '#f53794',
                      '#537bc4',
                      '#acc236',
                      '#166a8f',
                      '#00a950',
                      '#58595b',
                      '#8549ba',
                      '#4dc9f6'
                     ];
        
        if(isQuantityFilter || isProductFilter) {

            customReportFilter = {
                "isQuantityFilter": isQuantityFilter, 
                "quantityFilter": quantityFilter, 
                "isProductFilter": isProductFilter,
                "productFilter": productFilter
            };
            
            action.setParams({"reportNames": reportNames, "basicReportFilterJson" : JSON.stringify(basicReportFilter), "customReportFilterJson" : JSON.stringify(customReportFilter)});
        
        } else {
            
            action.setParams({"reportNames": reportNames, "basicReportFilterJson" : JSON.stringify(basicReportFilter)});
        
        }

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var reportResultData = JSON.parse(response.getReturnValue());
                var dataSets = [];
                var dataSet;
                var chartData = [];
                var chartLabels = [];
                var years = [];
                var value;
                var values = new Map();
                var valueMap;
                Object.keys(reportResultData).forEach(function(key) {
                    component.set("v.isLoading", false);
                    value = reportResultData[key];
                    valueMap = new Map();
                    var reportId;
                    for(var i = 0; i < (value.length); i++){
                        console.log('value[i].value ', value[i].value);
                        if(value[i].key !== null && value[i].value !== null) {
                            valueMap.set(value[i].key, value[i].value);
                            if(!chartLabels.includes(value[i].key)) {
                                chartLabels.push(value[i].key);
                            }
                        }
                        years = value[i].years;
                        reportId = value[i].reportId;
                    }
                    reportIds.push({value:reportId, key:key});
                    values.set(key, valueMap);
                });
                console.log('reportIds ', reportIds);
                component.set("v.reportIds", reportIds);
                let date1, date2;
                let dates = [];
                var sortedLabels = [];
                var realtedQuatersYearsLabels = new Map();
                var dateString;
                console.log('chartLabels before ', chartLabels);
                if(filter) {
                    if(filterType === 'MONTH') {
                        chartLabels.forEach(function(lable) {
                            date1 = new Date(lable);
                            dates.push(date1);
                        });
                        dates = dates.sort((a,b)=> a-b);
                        dates.forEach(function(date) {
                            dateString = date.toLocaleString('en-us', { year: 'numeric', month: 'long'});
                            sortedLabels.push(dateString);
                        });
                    } else if(filterType === 'DAY') {
                        chartLabels.forEach(function(lable) {
                            date1 = new Date(lable);
                            dates.push(date1);
                        });
                        dates = dates.sort((a,b) => a-b);
                        dates.forEach(function(date) {
                            dateString = date.toLocaleString('en-us', { year: 'numeric', month: 'numeric' , day: 'numeric'});
                            sortedLabels.push(dateString);
                        });
                    } else if(filterType === 'QUARTER') {
                        sortedLabels = chartLabels.sort((a,b) => a[1] - b[1] &&  a.substring(a.length - 4) - b.substring(b.length - 4));
                    } else if(filterType === 'YEAR') {
                        sortedLabels = chartLabels.sort((a,b) => a.substring(a.length - 4) - b.substring(b.length - 4));
                    }
                    if(sortedLabels.length > 0) {
                        if(sortedLabels.length === 1) {
                            console.log('sortedLabels[0] ', sortedLabels[0]);
                            if(sortedLabels[0] === 'Invalid Date') {
                                sortedLabels[0] = 'No Data';
                            }
                        }
                        chartLabels = sortedLabels;
                    }
                }
                
                console.log('chartLabels ', chartLabels);
                values.forEach(function(valuesMap, key) {
                    chartData = [];
                    chartLabels.forEach(function(lable) {
                        if(!valuesMap.has(lable)) {
                            valuesMap.set(lable, "0");
                        }
                        chartData.push(valuesMap.get(lable));
                    });
                    dataSet = {
                        label: key,
                        data: chartData,
                        fill: false,
                        lineTension: 0,
                        backgroundColor: colors[0],
                    };
                    if(chartType === 'pie' || chartType === 'doughnut') {
                        dataSet.backgroundColor = colors;
                    } else {
                        dataSet.backgroundColor = colors[dataSets.length];
                    }
                    dataSets.push(dataSet);
                });
                
                if(component.get("v.isProductFilter") && (component.get("v.productFilter") !== null || component.get("v.productFilter") !== "")) {
                    if(component.get("v.productFilter") !== component.get("v.selectedProduct")) {
                        console.log('product changed');
                        filterYears = [];
                        filterYears.push('All Years');
                        component.set("v.selectedProduct", component.get("v.productFilter"));
                    }
                }
                years = years.sort();
                for(var year of years) {
                    if(year !== "" && !filterYears.includes(year)) {
                        filterYears.push(year);
                    }
                }
                component.set("v.filterYears", filterYears);
                helper.createChart(component, dataSets, chartLabels, "chart", chartType);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message on createReport: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    createChart: function (component, dataSets, chartLabels, chartName, chartType) {

        //Construct chart
        var chartCanvas = component.find(chartName).getElement();
        var ctx = chartCanvas.getContext('2d');
        var chartMap = component.get("v.chartMap");
        var maintainAspectRatio = component.get("v.maintainAspectRatio");
        var chartVar;
        if(chartMap.has(chartName)) {
            chartVar = chartMap.get(chartName);
        }
        var pieDoughnutOptions = {
            responsive: true,
            maintainAspectRatio: maintainAspectRatio,
            legend: {
                position: 'right',
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };
        var otherOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    afterFit: function(me) {
                        me.paddingLeft = 0;
                        me.paddingRight = 0;
                    }
                }]
            },
            responsive: true,
            maintainAspectRatio: maintainAspectRatio,
            legend: {
                display: true,
                position:'right',
                fullWidth:false,
                reverse:true,
                labels: {
                    fontColor: '#000',
                    fontSize:10,
                    fontFamily:"Salesforce Sans, Arial, sans-serif SANS_SERIF"
                },
                layout: {
                    padding: 70,
                }
            }
        };
        if(chartVar) {
            chartVar.destroy();
        }
        var options = otherOptions;
        if(chartType === 'pie' || chartType === 'doughnut') {
            options = pieDoughnutOptions;
        }
        //debugger;
        chartVar = new Chart(ctx,{
            
            type: chartType,
            data: {
                labels: chartLabels,
                datasets: dataSets
            },
            options: options
        });
        chartMap.set(chartName, chartVar);
        component.set("v.chartMap", chartMap);
    }
    
})